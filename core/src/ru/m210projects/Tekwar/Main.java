package ru.m210projects.Tekwar;

import org.jetbrains.annotations.NotNull;
import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Pattern.LogSender;
import ru.m210projects.Build.Types.MemLog;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.filehandle.grp.GrpFile;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.CommandResponse;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.commands.OsdCallback;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Tekwar.Factory.*;
import ru.m210projects.Tekwar.Menus.MainMenu;
import ru.m210projects.Tekwar.Menus.MenuCorruptGame;
import ru.m210projects.Tekwar.Menus.MenuHelp;
import ru.m210projects.Tekwar.Menus.MenuLastLoad;
import ru.m210projects.Tekwar.Screens.*;
import ru.m210projects.Tekwar.Types.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.NORMAL;
import static ru.m210projects.Tekwar.Animate.initanimations;
import static ru.m210projects.Tekwar.Factory.TekMenuHandler.*;
import static ru.m210projects.Tekwar.Globals.*;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekchng.changehealth;
import static ru.m210projects.Tekwar.Tekgun.guntype;
import static ru.m210projects.Tekwar.Tekldsv.FindSaves;
import static ru.m210projects.Tekwar.Tekmap.newgame;
import static ru.m210projects.Tekwar.Tekmap.symbols;
import static ru.m210projects.Tekwar.Tekprep.spriteXT;
import static ru.m210projects.Tekwar.Tekprep.tekpreinit;
import static ru.m210projects.Tekwar.Teksnd.searchCDtracks;
import static ru.m210projects.Tekwar.Teksnd.sndInit;
import static ru.m210projects.Tekwar.Tektag.*;
import static ru.m210projects.Tekwar.View.*;

public class Main extends BuildGame {

    public static final String appdef = "twgdx.def";
    public static SmkMenu gMissionScreen;
    public static LoadingScreen gLoadingScreen;
    public static GameScreen gGameScreen;
    public static Main game;
    public static TekEngine engine;

    public static BoardService boardService;
    public static Config tekcfg;
    public static short screenpeek = 0;
    public static int hours, minutes, seconds;
    public static int fortieth;
    public static UserFlag mUserFlag = UserFlag.None;
    public static boolean TEKDEMO;


    public static int lockclock;
    public static Entry boardfilename;
    public TekMenuHandler menu;

    public Main(List<String> args, GameConfig cfg, String name, String version, boolean release, boolean isDemo) throws IOException {
        super(args, cfg, name, version, release);
        game = this;
        tekcfg = (Config) cfg;
        TEKDEMO = isDemo;
    }

    @Override
    protected MessageScreen createMessage(String header, String text, MessageType type) {
        return new TekMessageScreen(this, header, text, type);
    }

    @Override
    public void onDropEntry(FileEntry entry) {
        if (!entry.isExtension("map")) {
            return;
        }

        Console.out.println("Start dropped map: " + entry.getName());
        newgame(entry, null);
    }
    @Override
    public GameProcessor createGameProcessor() {
        return new TekControl(this);
    }

    @Override
    public BuildFactory getFactory() {
        return new TekFactory(this);
    }

    @Override
    public boolean init() {
        boardService = engine.getBoardService();
        FadeInit();
        Console.out.println("tekpreinit");
        tekpreinit();
        Console.out.println("tekgamestarted");

        hcpos = (short) -engine.getTile(HCDEVICE).getWidth();
        wppos = (short) -engine.getTile(WPDEVICE).getWidth();
        rvpos = (short) -engine.getTile(RVDEVICE).getWidth();
        seconds = minutes = hours = 0;

        engine.getTile(GUN08FIRESTART).disableAnimation(); // anm &= ~0xFF0000FF; //disable animation

        if (TEKDEMO) {
            KENSPLAYERHEIGHT = 44;
            guntype[2] = new Guntype(GUNDEMREADY, GUNDEMFIRESTART,
                    GUNDEMFIREEND, 1, new byte[]{0, 1, 0, 0, 0,
                    0, 0, 0}, 2, TICSPERFRAME * 4);
        }

        ConsoleInit();

        tekmultiskyinit();

        screenpeek = myconnectindex;

        sndInit();

//		tileLoadUserRes();

        Console.out.println("Initializing def-scripts...");

        cache.loadGdxDef(baseDef, appdef, "twgdx.dat");

        Directory gameDir = cache.getGameDirectory();
        if (pCfg.isAutoloadFolder()) {
            Console.out.println("Initializing autoload folder");
            for (Entry file : gameDir.getDirectory(gameDir.getEntry("autoload"))) {
                switch (file.getExtension()) {
                    case "PK3":
                    case "ZIP": {
                        Group group = cache.newGroup(file);
                        Entry def = group.getEntry(appdef);
                        if (def.exists()) {
                            cache.addGroup(group, NORMAL); // HIGH?
                            baseDef.loadScript(file.getName(), def);
                        }
                    }
                    break;
                    case "DEF":
                        baseDef.loadScript(file);
                        break;
                }
            }
        }

        FileEntry filgdx = gameDir.getEntry(appdef);
        if (filgdx.exists()) {
            baseDef.loadScript(filgdx);
        }
        this.setDefs(baseDef);

        initstruct();

        gMissionScreen = new MissionScreen(this);
        gLoadingScreen = new LoadingScreen(this);
        gGameScreen = new GameScreen(this);

        InitCutscenes();
        FindSaves(getUserDirectory());
        searchCDtracks();

        menu.mMenus[MAIN] = new MainMenu(this);
        menu.mMenus[HELP] = new MenuHelp(this);
        menu.mMenus[LASTSAVE] = new MenuLastLoad(this);
        menu.mMenus[CORRUPTLOAD] = new MenuCorruptGame(this);

        System.gc();
        MemLog.log("create");

        return true;
    }

    private void InitCutscenes() {
        Console.out.println("Initializing cutscenes");
        GrpFile group = new GrpFile("Cutscenes");

        Set<Group> groupList = new LinkedHashSet<>();
        Directory gameDir = cache.getGameDirectory();
        Directory smkDir = gameDir.getDirectory(gameDir.getEntry("smk"));
        if (!smkDir.isEmpty()) {
            groupList.add(smkDir);
        }
        groupList.add(gameDir);

        for(Group gr : groupList) {
            gr.stream().filter(e -> e.isExtension("smk")).forEach(group::addEntry);
        }

        if (!group.isEmpty()) {
            cache.addGroup(group, NORMAL);
        } else {
            Console.out.println("Cutscenes were not found", OsdColor.YELLOW);
        }
    }

    private void tekmultiskyinit() {
        // new-style multi-psky handling

        Arrays.fill(pskyoff, (short) 0);

        int parallaxyscale = getRenderer().getParallaxScale();
        if (parallaxyscale != 65536) {
            getRenderer().setParallaxScale(32768);
        }

        pskyoff[0] = 0;
        pskyoff[1] = 1;
        pskyoff[2] = 2;
        pskyoff[3] = 3;
        pskyoff[4] = 0;
        pskyoff[5] = 1;
        pskyoff[6] = 2;
        pskyoff[7] = 3;

        Arrays.fill(zeropskyoff, (short) 0);
        System.arraycopy(pskyoff, 0, zeropskyoff, 0, MAXPSKYTILES);

        pskybits = 3;
    }

    private void ConsoleInit() {
        Console.out.println("Initializing on-screen display system");

        Console.out.getPrompt().setVersion(getTitle(), OsdColor.BLUE, 10);

        Console.out.registerCommand(new OsdCallback("god", "", argv -> {
            if (game.isCurrentScreen(gGameScreen)) {
                gPlayer[myconnectindex].godMode = !gPlayer[myconnectindex].godMode;
                if (gPlayer[myconnectindex].godMode) {
                    Console.out.println("God mode: On");
                } else {
                    Console.out.println("God mode: Off");
                }
            } else {
                Console.out.println("god: not in a single-player game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));

        Console.out.registerCommand(new OsdCallback("noclip", "", argv -> {
            if (game.isCurrentScreen(gGameScreen)) {
                gPlayer[myconnectindex].noclip = !gPlayer[myconnectindex].noclip;
                if (gPlayer[myconnectindex].noclip) {
                    Console.out.println("Noclip: On");
                } else {
                    Console.out.println("Noclip: Off");
                }
            } else {
                Console.out.println("noclip: not in a single-player game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));

        Console.out.registerCommand(new OsdCallback("give", "", argv -> {
            if (game.isCurrentScreen(gGameScreen)) {
                if (argv.length != 1) {
                    Console.out.println("give: <weapons, health, items, symbols>");
                    return CommandResponse.SILENT_RESPONSE;
                }

                switch (argv[0]) {
                    case "health":
                        changehealth(screenpeek, 200);
                        break;
                    case "items":
                        gPlayer[myconnectindex].invredcards = 1;
                        gPlayer[myconnectindex].invbluecards = 1;
                        gPlayer[myconnectindex].invaccutrak = 1;
                        break;
                    case "symbols":
                        if (TEKDEMO) {
                            return CommandResponse.SILENT_RESPONSE;
                        }

                        symbols[0] = true;
                        symbols[1] = true;
                        symbols[2] = true;
                        symbols[3] = true;
                        symbols[4] = true;
                        symbols[5] = true;
                        symbols[6] = true;
                        break;
                    case "weapons":
                        for (int i = 0; i < 8; i++) {
                            gPlayer[myconnectindex].ammo[i] = MAXAMMO;
                        }
                        gPlayer[myconnectindex].invredcards = 1;
                        gPlayer[myconnectindex].invbluecards = 1;
                        gPlayer[myconnectindex].invaccutrak = 1;
                        gPlayer[myconnectindex].weapons = (flags32[GUN1FLAG] | flags32[GUN2FLAG] | flags32[GUN3FLAG] | flags32[GUN4FLAG]);
                        gPlayer[myconnectindex].weapons |= (flags32[GUN5FLAG] | flags32[GUN6FLAG] | flags32[GUN7FLAG] | flags32[GUN8FLAG]);
                        break;
                    default:
                        Console.out.println("give: <weapons, health, items, symbols>");
                        break;
                }

            } else {
                Console.out.println("give: not in a single-player game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));
    }

    private void initstruct() {
        for (int i = 0; i < MAXDOORS; i++) {
            doortype[i] = new Doortype();
        }
        for (int i = 0; i < MAXFLOORDOORS; i++) {
            floordoor[i] = new Floordoor();
        }

        for (int j = 0; j < MAXANIMPICS; j++) {
            animpic[j] = new Animpic();
        }
        for (int j = 0; j < MAXDELAYFUNCTIONS; j++) {
            delayfunc[j] = new Delayfunc();
        }
        for (int j = 0; j < MAXSECTORS; j++) {
            sectoreffect[j] = new Sectoreffect();
            elevator[j] = new Elevatortype();
        }
        for (int j = 0; j < MAXSPRITEELEVS; j++) {
            spriteelev[j] = new Spriteelev();
        }
        for (int j = 0; j < MAXMAPSOUNDFX; j++) {
            mapsndfx[j] = new Mapsndfxtype();
        }
        for (int j = 0; j < MAXSECTORVEHICLES; j++) {
            sectorvehicle[j] = new Sectorvehicle();
        }
        for (int i = 0; i < MAXSPRITES; i++) {
            spriteXT[i] = new SpriteXT();
        }

        initanimations();
    }

    final Runnable nextLogo = () -> changeScreen(gMissionScreen);

    final Runnable movie4 = () -> {
        CutsceneScreen gCutsceneScreen = new CutsceneScreen(this);
        if (gCutsceneScreen.init("intro.smk")) {
            gCutsceneScreen.setCallback(nextLogo).escSkipping(true);
            game.changeScreen(gCutsceneScreen);
        } else {
            nextLogo.run();
        }
    };

    final Runnable movie3 = () -> {
        CutsceneScreen gCutsceneScreen = new CutsceneScreen(this);
        if (gCutsceneScreen.init("tekintro.smk")) {
            gCutsceneScreen.setCallback(movie4).setSkipping(nextLogo).escSkipping(true);
            game.changeScreen(gCutsceneScreen);
        } else {
            movie4.run();
        }
    };

    final Runnable movie2 = () -> {
        CutsceneScreen gCutsceneScreen = new CutsceneScreen(this);
        if (gCutsceneScreen.init("tekndie.smk")) {
            gCutsceneScreen.setCallback(movie3).setSkipping(nextLogo).escSkipping(true);
            game.changeScreen(gCutsceneScreen);
        } else {
            movie3.run();
        }
    };

    @Override
    public void show() {
        CutsceneScreen gCutsceneScreen = new CutsceneScreen(this);
        if (gCutsceneScreen.init("tvopen.smk")) {
            gCutsceneScreen.setCallback(movie2).setSkipping(nextLogo).escSkipping(true);
            game.changeScreen(gCutsceneScreen);
        } else {
            movie2.run();
        }
    }

    @Override
    public DefaultPrecacheScreen getPrecacheScreen(Runnable readyCallback, PrecacheListener listener) {
        return new PrecacheScreen(readyCallback, listener);
    }

    @Override
    @NotNull
    public TekRenderer getRenderer() {
        Renderer renderer = super.getRenderer();
        if (renderer instanceof TekRenderer) {
            return (TekRenderer) renderer;
        }
        return new TekDummyRenderer();
    }

    @Override
    public LogSender getLogSender() {
        return new LogSender(this) {
            @Override
            public byte[] reportData() {
                String text = "boardfilename " + boardfilename;
                text += "\r\n";
                text += "posx " + gPlayer[0].posx;
                text += "\r\n";
                text += "posy " + gPlayer[0].posy;
                text += "\r\n";
                text += "posz " + gPlayer[0].posz;
                text += "\r\n";
                text += "sectnum " + gPlayer[0].cursectnum;
                text += "\r\n";
                return text.getBytes();
            }
        };
    }

    public enum UserFlag {
        None, UserMap
    }

}
