package ru.m210projects.Tekwar;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.Console;

import static java.lang.Math.abs;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Tekwar.Globals.*;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekchng.changehealth;
import static ru.m210projects.Tekwar.Tekgun.*;
import static ru.m210projects.Tekwar.Tekmap.missionaccomplished;
import static ru.m210projects.Tekwar.Tekprep.spriteXT;
import static ru.m210projects.Tekwar.Tekprep.switchlevelsflag;
import static ru.m210projects.Tekwar.Teksnd.playsound;
import static ru.m210projects.Tekwar.Tekspr.*;
import static ru.m210projects.Tekwar.Tektag.*;
import static ru.m210projects.Tekwar.View.showmessage;

public class Tekstat {
    public static final int MAXFRAMES = 8;
    public static final boolean NOSHOWSPRITES = false;
    public static final int SPR_LOTAG_SPAWNCHASE = 2000;
    public static final int SPR_LOTAG_MORPH = 2003;
    public static final int SPR_LOTAG_PICKUP = 2004;
    public static final int SECT_LOTAG_TRIGGERSPRITE = 5020;
    public static final int SECT_LOTAG_SHOWMESSAGE = 5030;
    public static final int SECT_LOTAG_NOSTANDING = 5040;
    public static final int SECT_LOTAG_OFFLIMITS_CIVILLIAN = 5050;
    public static final int SECT_LOTAG_OFFLIMITS_ALL = 5055;
    public static final int SECT_LOTAG_CLIMB = 5060;
    public static final int DONTBOTHERDISTANCE = 20480;
    public static final int HEARGUNSHOTDIST = 10240;
    public static final short INANIMATE = 0;
    public static final short PLAYER = 8;
    public static final short INACTIVE = 100;
    public static final short STANDING = 201;
    public static final short AMBUSH = 202;
    public static final short GUARD = 203;
    public static final short STALK = 204;
    public static final short FLEE = 205;
    public static final short CHASE = 206;
    public static final short PATROL = 207;
    public static final short STROLL = 209;
    public static final short VIRUS = 250;
    public static final short PLRVIRUS = 255;
    public static final short ATTACK = 300;
    public static final short DEATH = 301;
    public static final short PAIN = 302;
    public static final short TWITCH = 303;
    public static final short MORPH = 304;
    public static final short SQUAT = 305;
    public static final short UNSQUAT = 306;
    public static final short DODGE = 309;
    public static final short UNDODGE = 310;
    public static final short HIDE = 311;
    public static final short UNHIDE = 312;
    public static final short DELAYEDATTACK = 313;
    public static final short MIRRORMAN1 = 320;
    public static final short MIRRORMAN2 = 321;
    public static final short FLOATING = 322;
    public static final short PROJHIT = 400;
    public static final short PROJECTILE = 401;
    public static final short TOSS = 402;
    public static final short PINBALL = 403;
    public static final short DROPSIES = 406;
    public static final short RUNTHRU = 407;
    public static final short BLOODFLOW = 408;
    public static final short FLY = 500;
    public static final short RODENT = 502;
    public static final short TIMEBOMB = 602;
    public static final short STACKED = 610;
    public static final short FALL = 611;
    public static final short GENEXPLODE1 = 800;
    public static final short GENEXPLODE2 = 801;
    public static final short VANISH = 999;
    public static final int BOMBPROJECTILESTAT = 714;
    public static final int BOMBPROJECTILESTAT2 = 716;
    public static final int ENEMYCRITICALCONDITION = 25;
    public static final int AI_JUSTSHOTAT = 0x0004;
    public static final int AI_CRITICAL = 0x0008;
    public static final int AI_WASDRAWN = 0x0010;
    public static final int AI_WASATTACKED = 0x0020;
    public static final int AI_GAVEWARNING = 0x0040;
    public static final int AI_ENCROACHMENT = 0x0080;
    public static final int AI_DIDFLEESCREAM = 0x0100;
    public static final int AI_DIDAMBUSHYELL = 0x0200;
    public static final int AI_DIDHIDEPLEA = 0x0400;
    public static final int AI_TIMETODODGE = 0x0800;
    public static final int FX_HASREDCARD = 0x0001;
    public static final int FX_HASBLUECARD = 0x0002;
    public static final int FX_ANDROID = 0x0004;
    public static final int FX_HOLOGRAM = 0x0008;
    public static final int FX_NXTSTTPAIN = 0x0020;
    public static final int FX_NXTSTTDEATH = 0x0040;
    public static final int NO_PIC = 0;
    public static final int NORMALCLIP = 0;
    public static final int PROJECTILECLIP = 1;
    public static final int CLIFFCLIP = 2;
    public static final int MINATTACKDIST = 8192;
    public static final int CHASEATTDIST = 8192;
    public static final int GUARDATTDIST = 6144;
    public static final int STALKATTDIST = 8192;
    public static final int MAXBOBS = 32;
    public static final int HIDEDISTANCE = 4096;
    public static sectflashtype sectflash;
    public static Sprite pickup;
    public static int vadd;
    public static char ensfirsttime = 1;
    public static int stackedcheck;
    public static int playerhit;
    static final int[] bobbing = {0, 2, 4, 6, 8, 10, 12, 14, 16, 14, 12, 10, 8, 6, 4, 2, 0, -2, -4, -6, -8, -10, -12, -14, -16, -14, -12, -10, -8, -6, -4, -2};
    // angles, 0 east start, 22.5deg(==128scaled) resolution
    static final short[] leftof = {1920, 0, 128, 256, 384, 512, 640, 768, 896, 1024, 1152, 1280, 1408, 1536, 1664, 1792, 1920};
    static final short[] rightof = {128, 256, 384, 512, 640, 768, 896, 1024, 1152, 1280, 1408, 1536, 1664, 1792, 1920, 0, 128};

    public static int jsinsertsprite(int sect, int stat) {
        int j = engine.insertsprite(sect, stat);
        Sprite spr = boardService.getSprite(j);
        if (spr != null) {
            spr.setX(0);
            spr.setY(0);
            spr.setZ(0);
            spr.setCstat(0);
            spr.setShade(0);
            spr.setPal(0);
            spr.setClipdist(32);
            spr.setXrepeat(0);
            spr.setYrepeat(0);
            spr.setXoffset(0);
            spr.setYoffset(0);
            spr.setPicnum(0);
            spr.setAng(0);
            spr.setXvel(0);
            spr.setYvel(0);
            spr.setZvel(0);
            spr.setOwner(-1);
            spr.setLotag(0);
            spr.setHitag(0);
            spr.setExtra(-1);
        }
        return j;
    }

    public static void initsprites() {
        // initialize blast sector flashes
        if (sectflash == null) {
            sectflash = new sectflashtype();
        }
        sectflash.sectnum = 0;
        sectflash.ovis = 0;
        sectflash.step = 0;
    }

    public static void jsdeletesprite(int spritenum) {
        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null) {
            return;
        }

        int ext = spr.getExtra();
        if (validext(ext) != 0) {
            spriteXT[ext].set(0);
            spriteXT[ext].lock = 0x00;
        }

        engine.deletesprite(spritenum);
        spr.setExtra(-1);

    }

    public static int validext(int ext) {
        if ((ext >= 0) && (ext < MAXSPRITES)) {
            return (1);
        }
        return (0);
    }

    public static void toss(short snum) {
        if (!validplayer(snum)) {
            throw new AssertException("toss: bad plrnum");
        }
        if (pickup.getPicnum() == 0) {
            return;
        }
        int j = jsinsertsprite(gPlayer[snum].cursectnum, TOSS);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 != null) {
            if (gPlayer[snum].drawweap == 0) {
                spr2.setX((int) (gPlayer[snum].posx + (BCosAngle(gPlayer[snum].ang + 2048 + 256)) / 64.0f));
                spr2.setY((int) (gPlayer[snum].posy + (BSinAngle(gPlayer[snum].ang + 2048 + 256)) / 64.0f));
            } else {
                spr2.setX((int) (gPlayer[snum].posx - (BCosAngle(gPlayer[snum].ang + 2048 + 256)) / 64.0f));
                spr2.setY((int) (gPlayer[snum].posy - (BSinAngle(gPlayer[snum].ang + 2048 + 256)) / 64.0f));
            }
            spr2.setZ(gPlayer[snum].posz + (4 << 8));
            spr2.setCstat(pickup.getCstat());
            spr2.setShade(pickup.getShade());
            spr2.setPal(pickup.getPal());
            spr2.setClipdist(pickup.getClipdist());
            spr2.setXrepeat(pickup.getXrepeat());
            spr2.setYrepeat(pickup.getYrepeat());
            spr2.setXoffset(pickup.getXoffset());
            spr2.setYoffset(pickup.getYoffset());
            spr2.setAng((short) gPlayer[snum].ang);
            switch (pickup.getPicnum()) {
                case RATPIC:
                    spr2.setPicnum(RATTHROWPIC);
                    break;
                case TUBEBOMB + 1:
                    spr2.setPicnum(TUBEBOMB);
                    break;
                case DARTBOMB + 1:
                    spr2.setPicnum(DARTBOMB);
                    break;
                default:
                    spr2.setPicnum(pickup.getPicnum());
                    break;
            }

            spr2.setXvel((short) ((BCosAngle(gPlayer[snum].ang + 2048 + 256)) / 64.0f));
            spr2.setYvel((short) ((BSinAngle(gPlayer[snum].ang + 2048 + 256)) / 64.0f));
            spr2.setZvel((short) ((80 - (int) (gPlayer[snum].horiz)) << 6));
            spr2.setOwner((short) (snum + 4096));
            spr2.setSectnum(gPlayer[snum].cursectnum);
            // TOSS will be usin hi/lo tag
            spr2.setLotag(0);
            spr2.setHitag(0);
            spr2.setExtra(pickup.getExtra());
        }
        pickup.reset((byte) 0);
        pickup.setExtra(-1);
    }

    public static int pickupsprite(int sn) {
        // can only carry one at a time of these items
        if ((pickup.getPicnum() != 0) || (game.nNetMode == NetMode.Multiplayer)) {
            return (0);
        }

        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return 0;
        }

        pickup.setPicnum(spr.getPicnum());
        pickup.setCstat(spr.getCstat());
        pickup.setShade(spr.getShade());
        pickup.setPal(spr.getPal());
        pickup.setClipdist(spr.getClipdist());
        pickup.setXrepeat(spr.getXrepeat());
        pickup.setYrepeat(spr.getYrepeat());
        pickup.setXoffset(spr.getXoffset());
        pickup.setYoffset(spr.getYoffset());
        // cant set lotag/hitag as TOSS needs to use them
        pickup.setExtra(spr.getExtra());

        jsdeletesprite((short) sn);

        return (1);
    }

    public static void operatesprite(int dasprite) {
        final Sprite spr = boardService.getSprite(dasprite);
        if (spr == null) {
            return;
        }

        if ((game.nNetMode != NetMode.Multiplayer) && (spr.getLotag() == SPR_LOTAG_PICKUP)) {
            int pu = pickupsprite(dasprite);
            switch (spr.getPicnum()) {
                case RATPIC:
                    if (pu != 0) {
                        showmessage("LIVE RAT");
                    }
                    break;
                case TUBEBOMB + 1:
                case DARTBOMB + 1:
                    if (pu != 0) {
                        showmessage("GRENADE");
                    }
                default:
                    break;
            }
            return;
        }

        switch (spr.getPicnum()) {
            case 1361: // the witchaven poster for demo
                playsound(S_WITCH, spr.getX(), spr.getY(), 0, ST_UNIQUE);
                break;
            case 592: // the witchaven poster for demo
                playsound(S_FLUSH, spr.getX(), spr.getY(), 0, ST_UNIQUE);
                break;
            default:
                break;
        }

        int datag = spr.getLotag();
        switch (datag) {
            case 6:
                if ((spr.getCstat() & 0x001) != 0) {
                    setanimpic(spr.getPicnum(), TICSPERFRAME * 3, 4);
                } else {
                    setanimpic(spr.getPicnum(), TICSPERFRAME * 3, -4);
                }
                spr.setCstat(spr.getCstat() ^ 0x101);
//	          teksetdelayfunc(operatesprite,CLKIPS*4,dasprite);
                break;
            case 4:
                if (switchlevelsflag == 0) {
                    break;
                }
                if (game.nNetMode == NetMode.Multiplayer && (spr.getPicnum() == 182 || spr.getPicnum() == 803)) {
                    playsound(S_WH_SWITCH, spr.getX(), spr.getY(), 0, ST_UNIQUE);
//	               nextnetlevel();
                }
                break;
        }
    }

    public static void initspriteXTs() {
        pickup = new Sprite();
        pickup.setExtra(-1);

        switch (gDifficulty) {
            case 0:
            case 1:
                vadd = -1;
                break;
            case 2:
                vadd = 0;
                break;
            case 3:
                vadd = 3;
                break;
        }

        // adjust speed for difficulty
        for (int i = 0; i < MAXSPRITES; i++) {
            Sprite spr = boardService.getSprite(i);
            if (spr == null) {
                continue;
            }

            if (spr.getExtra() != -1) {
                if (NOSHOWSPRITES) {
                    show2dsprite.clearBit(i);
                }

                switch (spriteXT[spr.getExtra()].basestat) {
                    case GUARD:
                    case CHASE:
                    case STALK:
                        spr.setXvel(spr.getXvel() + vadd);
                        if (spr.getXvel() < 0) {
                            spr.setXvel(0);
                        }
                        spr.setYvel(spr.getYvel() + vadd);
                        if (spr.getYvel() < 0) {
                            spr.setYvel(0);
                        }
                        spr.setZvel(spr.getZvel() + vadd);
                        if (spr.getZvel() < 0) {
                            spr.setZvel(0);
                        }
                        break;
                }
            }
        }

        // only CHASE in multiplayer
        if (game.nNetMode == NetMode.Multiplayer) {
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    continue;
                }

                int ext = spr.getExtra();
                if (ext != -1) {
                    if (spr.getStatnum() != CHASE) {
                        jsdeletesprite((short) i);
                    } else if (spriteXT[ext].basestat != CHASE) {
                        jsdeletesprite((short) i);
                    }
                }
            }
        }

        if (noenemiesflag != 0) {
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    continue;
                }

                if (spr.getExtra() != -1) {
                    jsdeletesprite((short) i);
                }
            }
        }

        if (noguardflag != 0) {
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    continue;
                }

                if (spr.getStatnum() == GUARD) {
                    jsdeletesprite((short) i);
                } else {
                    if (spr.getExtra() != -1) {
                        if (spriteXT[spr.getExtra()].basestat == GUARD) {
                            jsdeletesprite((short) i);
                        }
                    }
                }
            }
        }

        if (nostalkflag != 0) {
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    continue;
                }

                if (spr.getStatnum() == STALK) {
                    jsdeletesprite((short) i);
                } else {
                    if (spr.getExtra() != -1) {
                        if (spriteXT[spr.getExtra()].basestat == STALK) {
                            jsdeletesprite((short) i);
                        }
                    }
                }
            }
        }

        if (nochaseflag != 0) {
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    continue;
                }

                if (spr.getStatnum() == CHASE) {
                    jsdeletesprite((short) i);
                } else {
                    if (spr.getExtra() != -1) {
                        if (spriteXT[spr.getExtra()].basestat == CHASE) {
                            jsdeletesprite((short) i);
                        }
                    }
                }
            }
        }

        if (nostrollflag != 0) {
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    continue;
                }

                if (spr.getStatnum() == STROLL) {
                    jsdeletesprite((short) i);
                } else {
                    if (spr.getExtra() != -1) {
                        if (spriteXT[spr.getExtra()].basestat == STROLL) {
                            jsdeletesprite((short) i);
                        }
                    }
                }
            }
        }

    }

    public static void sectortriggersprites(int snum) {
        boolean triggered = false;
        ListNode<Sprite> i, nexti;

        if (game.nNetMode == NetMode.Multiplayer) {
            return;
        }

        if (!validplayer(snum)) {
            throw new AssertException("sectrtrgrsprts: bad plrnum");
        }

        Sector plrSec = boardService.getSector(gPlayer[snum].cursectnum);
        if (plrSec == null) {
            return;
        }

        if (plrSec.getLotag() == SECT_LOTAG_SHOWMESSAGE) {
            if (plrSec.getHitag() == 0) {
                showmessage("AREA IS OFF-LIMITS");
            }
            return;
        }

        if (plrSec.getLotag() != SECT_LOTAG_TRIGGERSPRITE) {
            return;
        }

        i = boardService.getStatNode(INANIMATE);
        while (i != null) {
            nexti = i.getNext();
            if (i.get().getHitag() == plrSec.getHitag()) {
                triggersprite(i.getIndex());
                triggered = true;
            }
            i = nexti;
        }

        i = boardService.getStatNode(AMBUSH);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();
            if (spr.getHitag() == plrSec.getHitag()) {
                int ext = spr.getExtra();
                if (validext(ext) == 0) {
                    noextthrowerror(index, 300);
                }
                spriteXT[index].aimask |= AI_JUSTSHOTAT;
                ambushyell(index, ext);
                triggered = true;
            }
            i = nexti;
        }

        i = boardService.getStatNode(GUARD);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();
            if (spr.getHitag() == plrSec.getHitag()) {
                int ext = spr.getExtra();
                if (ext != -1) {
                    spriteXT[ext].aimask &= ~AI_GAVEWARNING;
                    givewarning(index, ext);
                    spriteXT[ext].aimask |= AI_ENCROACHMENT;
                    triggered = true;
                }
            }
            i = nexti;
        }

        i = boardService.getStatNode(STANDING);
        // check GUARDS who are standing
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();
            int ext = spr.getExtra();
            if ((validext(ext) == 0) || (spriteXT[ext].basestat == GUARD)) {
                if (spr.getHitag() == plrSec.getHitag()) {
                    spriteXT[ext].aimask &= ~AI_GAVEWARNING;
                }
                givewarning(index, ext);
                spriteXT[ext].aimask |= AI_ENCROACHMENT;
                triggered = true;
            }
            i = nexti;
        }

        if (triggered) {
            plrSec.setHitag(0);
        }
    }

    public static void triggersprite(final int sn) {
        if (game.nNetMode == NetMode.Multiplayer) {
            return;
        }

        final Sprite spr = boardService.getSprite(sn);
        if (spr == null || spr.getExtra() == -1) {
            return;
        }

        int datag = spr.getLotag();

        switch (datag) {

            case SPR_LOTAG_MORPH:
                break;
            case SPR_LOTAG_SPAWNCHASE: {
                int j = jsinsertsprite(spr.getSectnum(), spriteXT[spr.getExtra()].basestat);
                Sprite spr2 = boardService.getSprite(j);
                if (spr2 == null) {
                    break;
                }

                int newext = mapXT(j);
                if (validext(newext) == 0) {
                    jsdeletesprite(j);
                    break;
                }

                spr2.setX(spr.getX() + (EngineUtils.sin((spr.getAng() + 512) & 2047) >> 6));
                spr2.setY(spr.getY() + (EngineUtils.sin(spr.getAng() & 2047) >> 6));
                spr2.setZ(spr.getZ());
                spr2.setCstat(0x101);
                spr2.setPicnum((short) spriteXT[spr.getExtra()].basepic);
                spr2.setShade(spr.getShade());
                spr2.setSectnum(spr.getSectnum());
                spr2.setXrepeat(spr.getXrepeat());
                spr2.setYrepeat(spr.getYrepeat());
                spr2.setAng(spr.getAng());
                spr2.setXvel(spr.getXvel());
                spr2.setYvel(spr.getYvel());
                spr2.setZvel(spr.getZvel());
                spr2.setOwner(-1);
                spr2.setLotag(0);
                spr2.setHitag(0);
                spriteXT[spr2.getExtra()].basestat = spriteXT[spr.getExtra()].basestat;
                spriteXT[spr2.getExtra()].basepic = spriteXT[spr.getExtra()].basepic;
                spriteXT[spr2.getExtra()].walkpic = spriteXT[spr.getExtra()].walkpic;
                spriteXT[spr2.getExtra()].standpic = spriteXT[spr.getExtra()].standpic;
                spriteXT[spr2.getExtra()].runpic = spriteXT[spr.getExtra()].runpic;
                spriteXT[spr2.getExtra()].attackpic = spriteXT[spr.getExtra()].attackpic;
                spriteXT[spr2.getExtra()].deathpic = spriteXT[spr.getExtra()].deathpic;
                spriteXT[spr2.getExtra()].morphpic = spriteXT[spr.getExtra()].morphpic;
                spriteXT[spr2.getExtra()].specialpic = spriteXT[spr.getExtra()].specialpic;
                // delete trigger tag from old sprites
                spr.setLotag(0);
                spr.setHitag(0);
                clearXTpics(sn);
                break;
            }
            default:
                newstatus(sn, spriteXT[spr.getExtra()].basestat);
                break;
        }
    }

    public static boolean isaplayersprite(int sprnum) {
        Sprite spr = boardService.getSprite(sprnum);
        if (spr == null) {
            return false;
        }

        for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
            if (gPlayer[j].playersprite == sprnum) {
                return (true);
            }
        }

        if (spr.getStatnum() == 8) {
            throw new AssertException("isplrspr: non plr has statnm 8");
        }
        return (false);
    }

    public static void noextthrowerror(int i, int loc) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        Console.out.println("sprite at " + spr.getX() + " ," + spr.getY() + "  no extension from  " + loc);
    }

    public static void newstatus(int sn, int seq) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }

        if (isaplayersprite(sn)) {
            return;
        }

        int ext = spr.getExtra();
        if (validext(ext) == 0) {
            noextthrowerror(sn, 0);
        }

        switch (seq) {
            case INANIMATE:
                engine.changespritestat(sn, INANIMATE);

//	               showmessage("INANIMATE \0".toCharArray());

                break;
            case INACTIVE:
                engine.changespritestat(sn, INACTIVE);

//	               showmessage("INACTIVE \0".toCharArray());

                break;
            case FLOATING:
                game.pInt.setsprinterpolate(sn, spr);
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec == null) {
                    break;
                }

                if (sec.getLotag() == 4) {
                    int zoffs = 0;
                    if ((spr.getCstat() & 128) == 0) {
                        zoffs = -((engine.getTile(spr.getPicnum()).getHeight() * spr.getYrepeat()) << 1);
                    }
                    spr.setZ((sec.getFloorz() - zoffs));
                    spr.setLotag(0);
                    spr.setHitag(0);
                    spr.setXvel(1);
                    spr.setYvel(1);
                    engine.changespritestat(sn, FLOATING);
                } else {
                    engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                    spr.setZ(zr_florz);
                    engine.changespritestat(sn, INACTIVE);
                }
                break;
            case GUARD: {
                int newpic = spriteXT[ext].walkpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, GUARD);
                    spr.setLotag(0);
                    spr.setPicnum(newpic);
                    spriteXT[ext].basestat = GUARD;

//	                    showmessage("GUARD \0".toCharArray());

                }
                break;
            }
            case PATROL: {
                int newpic = spriteXT[ext].walkpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, PATROL);
                    spr.setLotag(0);
                    spr.setPicnum(newpic);
                    spriteXT[ext].basestat = PATROL;

//	                    showmessage("PATROL \0".toCharArray());

                }
                break;
            }
            case PINBALL:{
                int newpic = spriteXT[ext].walkpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, PINBALL);
                    spr.setPicnum(newpic);
                    spriteXT[ext].basestat = PINBALL;

//	                    showmessage("PINBALL\0".toCharArray());

                }
                break;
            }
            case STROLL: {
                int newpic = spriteXT[ext].walkpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, STROLL);
                    spr.setLotag(0);
                    spr.setPicnum(newpic);
                    spriteXT[ext].basestat = STROLL;

//	                    showmessage("STROLL\0".toCharArray());

                }
                break;
            }
            case CHASE: {
                int newpic = spriteXT[ext].runpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, CHASE);
                    spr.setLotag(0);
                    spr.setPicnum(newpic);
                    spriteXT[ext].basestat = CHASE;

//	                    showmessage("CHASE\0".toCharArray());

                }
                break;
            }
            case FLEE: {
                int newpic = spriteXT[ext].runpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, FLEE);
                    spr.setLotag(0);
                    spr.setPicnum(newpic);

//	                    showmessage("FLEE\0".toCharArray());

                }
                break;
            }
            case ATTACK: {
                // standard 5 angles, 2 frames/angle
                int newpic = spriteXT[ext].attackpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, ATTACK);
                    spr.setPicnum(newpic);
                    switch (spriteXT[ext].weapon) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            spr.setLotag(32);
                            spr.setHitag(32);
                            break;
                        default:
                            spr.setLotag(64);
                            spr.setHitag(64);
                            break;
                    }

//	                    showmessage("ATTACK\0".toCharArray());

                }
                break;
            }
            case DELAYEDATTACK:
                // standard 5 angles, 2 frames/angle
                spr.setLotag(95);
                engine.changespritestat(sn, DELAYEDATTACK);

//	               showmessage("DELAYED ATTACK\0".toCharArray());

                break;
            case STALK: {
                int newpic = spriteXT[ext].runpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, STALK);
                    spr.setLotag(0);
                    spr.setPicnum(newpic);
                    spriteXT[ext].basestat = STALK;

//	                    showmessage("STALK\0".toCharArray());

                }
                break;
            }
            case SQUAT: {
                int newpic = spriteXT[ext].squatpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, SQUAT);
                    // standard 3 frames
                    // stay squat for 4 TICSPERFRAME
                    spr.setLotag(47);
                    spr.setHitag(64);

//	                    showmessage("SQUAT\0".toCharArray());

                }
                break;
            }
            case UNSQUAT: {
                int newpic = spriteXT[ext].squatpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, UNSQUAT);
                    spr.setLotag(47);
                    spr.setHitag(0);

//	                    showmessage("UNSQUAT\0".toCharArray());

                }
                break;
            }
            case DODGE: {
                int newpic = spriteXT[ext].squatpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, SQUAT);
                    // standard 2 frames
                    spr.setLotag(31);
                    spr.setHitag(0);

//	                    showmessage("DODGE\0".toCharArray());

                }
                break;
            }
            case UNDODGE: {
                int newpic = spriteXT[ext].squatpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, UNSQUAT);
                    spr.setLotag(31);
                    spr.setHitag(0);

//	                    showmessage("UNDODGE\0".toCharArray());

                }
                break;
            }
            case HIDE: {
                int newpic = spriteXT[ext].squatpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, HIDE);
                    // standard 2 frames
                    spr.setLotag(31);
                    spr.setHitag(256);

//	                    showmessage("HIDE\0".toCharArray());

                }
                break;
            }
            case UNHIDE: {
                int newpic = spriteXT[ext].squatpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, UNHIDE);
                    spr.setLotag(31);
                    spr.setHitag(0);

//	                    showmessage("UNHIDE\0".toCharArray());

                }
                break;
            }
            case PAIN: {
                int newpic = spriteXT[ext].painpic;
                if (newpic != 0) {
                    engine.changespritestat(sn, PAIN);
                    spr.setPicnum(newpic);
                    spr.setLotag(16);

//	                    showmessage("PAIN\0".toCharArray());

                }
                break;
            }
            case STANDING: {
                int newpic = spriteXT[ext].standpic;
                if (newpic != 0) {
                    spr.setPicnum(newpic);
                    if (spr.getLotag() <= 0) {
                        spr.setLotag((short) ((krand_intercept("STAT1732")) & 1024));
                    }
                    engine.changespritestat(sn, STANDING);

//	                    showmessage("STANDING\0".toCharArray());

                }
                break;
            }
            case FLY: {
                int newpic = spriteXT[ext].runpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, FLY);
                    spr.setLotag(0);
                    spr.setPicnum(newpic);
                    spriteXT[ext].basestat = FLY;

//	                    showmessage("FLY\0".toCharArray());

                }
                break;
            }
            case RODENT: {
                int newpic = spriteXT[ext].runpic;
                if (newpic != NO_PIC) {
                    engine.changespritestat(sn, RODENT);
                    spr.setLotag(0);
                    spr.setPicnum(newpic);
                    spriteXT[ext].basestat = RODENT;

//	                    showmessage("RODENT\0".toCharArray());

                }
                break;
            }
            case MORPH:
                if (spriteXT[ext].morphpic != 0) {
                    engine.changespritestat(sn, MORPH);
                    spr.setLotag(95);

//	                    showmessage("MORPH\0".toCharArray());

                }
                break;
            case DEATH: {
                int newpic = spriteXT[ext].deathpic;
                spr.setCstat(spr.getCstat() & ~257);
                if (newpic != 0) {
                    engine.changespritestat(sn, DEATH);
                    deathdropitem(sn);
                    spr.setLotag((short) (((engine.getTile(newpic).getAnimFrames()) << 4) - 1));
                    spr.setHitag(spr.getLotag());
                    deathsounds(newpic, spr.getX(), spr.getY());
                    spr.setPicnum(newpic);
                } else {
                    engine.changespritestat(sn, INACTIVE);
                }

//	               showmessage("DEATH \0".toCharArray());

                break;
            }
        }
    }

    public static void deathsounds(int pic, int x, int y) {
        switch (pic) {
            case ANTDEATHPIC: // anton boss
            case DIDEATHPIC: // dimarco boss
            case 2165: // miles boss
            case 2978: // sonny hokuri boss
            case 2850: // carlyle boss
            case 2662: // janus boss
            case 2550: // marty dollar boss
            case RUBDEATHPIC:
            case FRGDEATHPIC:
            case JAKEDEATHPIC:
            case COP1DEATHPIC:
            case ERDEATHPIC:
            case 2415: // rebreather
            case 2295: // black cop
            case 2455: // trenchcoat
            case 2792: // blacksmith
            case 2712: // orange guy
            case 3041: // swat guy
            case 2910: // saline suit
                playsound(S_MANDIE1 + RMOD4("STAT1378") + RMOD3("STAT1378") + RMOD3("STAT1378"), x, y, 0, ST_NOUPDATE);
                break;
            case SARAHDEATHPIC:
            case SAMDEATHPIC:
            case MADEATHPIC:
            case 2340: // nika
            case 2607: // halter top
            case 2227: // cowgirl
                playsound(S_GIRLDIE1 + RMOD4("STAT1389") + RMOD3("STAT1389"), x, y, 0, ST_NOUPDATE);
                break;
            case GLASSDEATHPIC:
                playsound(S_GLASSBREAK1 + RMOD2("STAT1392"), x, y, 0, ST_NOUPDATE);
                break;
            case 570: // autogun
                playsound(S_AUTOGUNEXPLODE, x, y, 0, ST_NOUPDATE);
                break;
            case 609: // bathroom glass
                playsound(S_SMALLGLASS1 + RMOD2("STAT1398"), 0, 0, 0, ST_IMMEDIATE);
                break;
            case 3060:
            case 3064:
            case 3068:
            case 3072:
            case 3076:
            case 3080:
            case 3084:
                playsound(S_HOLOGRAMDIE, x, y, 0, ST_NOUPDATE);
                break;
            case 3973: // matrix character death
                if (krand_intercept("STAT1364") < 32767) {
                    playsound(S_MATRIX_DIE1, x, y, 0, ST_NOUPDATE);
                } else {
                    playsound(S_MATRIX_DIE2, x, y, 0, ST_NOUPDATE);
                }
                break;

        }
    }

    public static void deathdropitem(int sn) {
        if (game.nNetMode == NetMode.Multiplayer) {
            return;
        }

        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }

        int ext = spr.getExtra();
        if (validext(ext) == 0) {
            return;
        }

        int pic;
        if ((spriteXT[ext].fxmask & FX_HASREDCARD) != 0) {
            pic = RED_KEYCARD;
            spriteXT[ext].fxmask &= (~FX_HASREDCARD);
        } else if ((spriteXT[ext].fxmask & FX_HASBLUECARD) != 0) {
            pic = BLUE_KEYCARD;
            spriteXT[ext].fxmask &= (~FX_HASBLUECARD);
        } else if ((spriteXT[ext].fxmask & FX_ANDROID) != 0) {
            showmessage("WAS AN ANDROID");
            return;
        } else {
            switch (spriteXT[ext].weapon) {
                case 4:
                case 5:
                    pic = KLIPPIC;
                    break;
                default:
                    return;
            }
        }

        final int j = jsinsertsprite(spr.getSectnum(), DROPSIES);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 == null) {
            return;
        }

        fillsprite(j, spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 6), 128, 0, 0, 12, 16, 16, 0, 0, pic, spr.getAng(), EngineUtils.sin((spr.getAng() + 2560) & 2047) >> 6, EngineUtils.sin((spr.getAng() + 2048) & 2047) >> 6, 30, sn + 4096, 0, 0);

        // tweak the size of the pic
        switch (spr2.getPicnum()) {
            case KLIPPIC:
                spr2.setXrepeat(spr2.getXrepeat() - 2);
                spr2.setYrepeat(spr2.getYrepeat() - 2);
                break;
            case RED_KEYCARD:
            case BLUE_KEYCARD:
                spr2.setXrepeat(spr2.getXrepeat() >> 1);
                spr2.setYrepeat(spr2.getYrepeat() >> 1);
                break;
            default:
                break;
        }
    }

    public static int mapXT(int sn) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return 0;
        }

        for (int i = 0; i < MAXSPRITES; i++) {
            if (spriteXT[i].lock == 0x00) {
                spr.setExtra(i);
                spriteXT[i].set(0);
                spriteXT[i].lock = 0xFF;
                return (i);
            }
        }

        spr.setExtra(-1);
        return (-1); // no free spot found
    }

    public static void clearXTpics(int sn) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }

        short extno = spr.getExtra();

        if (extno != -1) {
            spriteXT[extno].basepic = spr.getPicnum();
            spriteXT[extno].standpic = NO_PIC;
            spriteXT[extno].walkpic = NO_PIC;
            spriteXT[extno].runpic = NO_PIC;
            spriteXT[extno].attackpic = NO_PIC;
            spriteXT[extno].deathpic = NO_PIC;
            spriteXT[extno].painpic = NO_PIC;
            spriteXT[extno].squatpic = NO_PIC;
            spriteXT[extno].morphpic = NO_PIC;
            spriteXT[extno].specialpic = NO_PIC;
        }
    }

    public static void ambushyell(int sn, int ext) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }

        if (validext(ext) == 0) {
            return;
        }
        if ((spriteXT[ext].aimask & AI_DIDAMBUSHYELL) != 0) {
            return;
        }

        switch (spriteXT[ext].basepic) {
            case ANTWALKPIC:
            case RUBWALKPIC:
                playsound(S_MALE_COMEONYOU + (krand_intercept("STAT2603") & 6), spr.getX(), spr.getY(), 0, ST_IMMEDIATE);
                spriteXT[ext].aimask |= AI_DIDAMBUSHYELL;
                break;
            case DIWALKPIC:
                playsound(S_DIM_WANTSOMETHIS + (krand_intercept("STAT2607") & 2), spr.getX(), spr.getY(), 0, ST_IMMEDIATE);
                spriteXT[ext].aimask |= AI_DIDAMBUSHYELL;
                break;
        }
    }

    public static void attachvirus(int i, int pic) {
        if (game.nNetMode == NetMode.Multiplayer) {
            return;
        }
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        if ((spr.getStatnum() == VIRUS) || (spr.getStatnum() == PINBALL)) {
            return;
        }
        if (isanandroid(i) != 0 || isahologram(i) != 0) {
            return;
        }

        ListNode<Sprite> j = boardService.getStatNode(VIRUS), nextj;
        while (j != null) {
            nextj = j.getNext();
            // already hosting ?
            if (j.get().getOwner() == i) {
                return;
            }
            j = nextj;
        }

        int js = jsinsertsprite(spr.getSectnum(), VIRUS);
        Sprite pSprite = boardService.getSprite(js);
        if (pSprite == null) {
            return;
        }

        pSprite.setExtra(-1);
        pSprite.setX(spr.getX());
        pSprite.setY(spr.getY());
        pSprite.setZ(spr.getZ());
        pSprite.setXrepeat(20);
        pSprite.setYrepeat(20);
        pSprite.setCstat(0x0000);
        pSprite.setShade(-28);
        pSprite.setPicnum(pic);
        pSprite.setLotag((krand_intercept("STAT1216") & 512 + 128));
        pSprite.setHitag(0);
        pSprite.setOwner(i); // host

        if (pic == FIREPIC) {
            pSprite.setXrepeat(spr.getXrepeat());
            pSprite.setYrepeat((byte) (spr.getYrepeat() + 8));
            int ext = spr.getExtra();
            playsound(S_FIRELOOP, pSprite.getX(), pSprite.getY(), 0, ST_UPDATE);
            if (validext(ext) != 0) {
                newstatus(i, PINBALL);
                if (spr.getStatnum() == PINBALL) {
                    spr.setXvel(spr.getXvel() + 4);
                    spr.setYvel(spr.getYvel() + 4);
                }
            }
        } else {
            playsound(S_FORCEFIELDHUMLOOP, spr.getX(), spr.getY(), 0, ST_UNIQUE);
        }

    }

    public static int playervirus(int pnum, int pic) {
        if (!validplayer(pnum)) {
            throw new AssertException("plrvrus: bad plrnum");
        }

        ListNode<Sprite> node = boardService.getStatNode(PLRVIRUS), nextj;
        while (node != null) {
            nextj = node.getNext();
            // already hosting ?
            if (node.get().getOwner() == pnum) {
                return (0);
            }
            node = nextj;
        }

        int j = jsinsertsprite(gPlayer[pnum].cursectnum, PLRVIRUS);
        Sprite spr = boardService.getSprite(j);
        if (spr == null) {
            return (0);
        }

        spr.setExtra(-1);
        spr.setX(gPlayer[pnum].posx);
        spr.setY(gPlayer[pnum].posy);
        spr.setZ(gPlayer[pnum].posz);
        spr.setXrepeat(18);
        spr.setYrepeat(40);
        spr.setCstat(0x0000);
        spr.setShade(-28);
        spr.setPicnum((short) pic);
        spr.setLotag((short) (krand_intercept("STAT1172") & 512 + 128));
        spr.setHitag(0);
        spr.setOwner((short) pnum); // host

        playsound(S_FIRELOOP, spr.getX(), spr.getY(), 0, ST_UPDATE);

        return (1);
    }

    public static int damagesprite(int hitsprite, int points) {
        Sprite spr = boardService.getSprite(hitsprite);
        if (spr == null) {
            return 0;
        }

        if (isaplayersprite(hitsprite)) {
            return (0);
        }

        int ext = spr.getExtra();

        switch (spr.getStatnum()) {
            case INANIMATE:
                if (validext(ext) != 0) {
                    newstatus(hitsprite, DEATH);
                }
                return (0);
            case GENEXPLODE1:
                engine.changespritestat(hitsprite, INACTIVE);
                genexplosion1(hitsprite);
                jsdeletesprite(hitsprite);
                return (0);
            case GENEXPLODE2:
                engine.changespritestat(hitsprite, INACTIVE);
                genexplosion2(hitsprite);
                jsdeletesprite(hitsprite);
                return (0);
            case INACTIVE:
            case FLOATING:
            case PROJHIT:
            case PROJECTILE:
            case TOSS:
            case PAIN:
            case RUNTHRU:
            case TWITCH:
            case DEATH:
                return (0);
            default:
                break;
        }

        if (gDifficulty == 0) {
            points <<= 1;
        }

        if (validext(ext) != 0) {
            if ((abs(points)) > spriteXT[ext].hitpoints) {
                spriteXT[ext].hitpoints = 0;
            } else {
                spriteXT[ext].hitpoints -= (byte) points;
            }
            spriteXT[ext].aimask |= AI_JUSTSHOTAT;
            spriteXT[ext].aimask |= AI_TIMETODODGE;
            spriteXT[ext].aimask |= AI_WASATTACKED;
            if (spriteXT[ext].hitpoints < ENEMYCRITICALCONDITION) {
                spriteXT[ext].aimask |= AI_CRITICAL;
            }
            if (spriteXT[ext].hitpoints <= 0) {
                spriteXT[ext].hitpoints = 0;
                // newstatus(hitsprite,DEATH);
                spriteXT[ext].fxmask |= FX_NXTSTTDEATH;
                return (1);
            } else {
                // newstatus(hitsprite,PAIN);
                spriteXT[ext].fxmask |= FX_NXTSTTPAIN;
            }
        }

        return (0);
    }

    public static int playerhit(int hitsprite) {
        Sprite spr = boardService.getSprite(hitsprite);
        if (spr == null) {
            return 0;
        }

        for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
            if (gPlayer[j].playersprite == hitsprite) {
                if (spr.getStatnum() != 8) {
                    throw new AssertException("plrhit: plrsprt lost sttnm 8");
                }
                playerhit = j;
                return (1);
            }
        }

        return (0);
    }

    public static void sectorflash(short s) {
        if (sectflash.step != 0) {
            return;
        }

        Sector sec = boardService.getSector(s);
        if (sec == null) {
            return;
        }

        sectflash.sectnum = s;
        sectflash.step = 1;
        sectflash.ovis = (char) (sec.getVisibility() & 0xff);
    }

    public static void genexplosion1(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = jsinsertsprite(spr.getSectnum(), (short) 5);
        if (j != -1) {
            fillsprite(j, spr.getX(), spr.getY(), spr.getZ(), 0, -16, 0, 0, 64, 64, 0, 0, GENEXP1PIC + 1, spr.getAng(), EngineUtils.sin((spr.getAng() + 2560) & 2047) >> 6, EngineUtils.sin((spr.getAng() + 2048) & 2047) >> 6, 30, i + 4096,24, 0);
            playsound(S_SMALLGLASS1 + RMOD2("STAT4534"), spr.getX(), spr.getY(), 0, ST_NOUPDATE);
        }
    }

    public static void genexplosion2(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        sectorflash(spr.getSectnum());
        checkblastarea(i);

        int j = jsinsertsprite(spr.getSectnum(), (short) 5);
        if (j != -1) {
            fillsprite(j, spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 3), 0, -16, 0, 0, 64, 64, 0, 0, GENEXP2PIC, spr.getAng(), EngineUtils.sin((spr.getAng() + 2560) & 2047) >> 6, EngineUtils.sin((spr.getAng() + 2048) & 2047) >> 6, 30, i + 4096,32, 0);
            playsound(S_EXPLODE1 + RMOD2("STAT4559"), spr.getX(), spr.getY(), 0, ST_NOUPDATE);
        }
    }

    public static int spewblood(int sn, int hitz) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return 0;
        }

        int ext = spr.getExtra();
        if (game.nNetMode == NetMode.Multiplayer) {
            return (0);
        }
        if (validext(ext) == 0) {
            return (0);
        }
        if (isanandroid(sn) != 0 || isahologram(sn) != 0) {
            return (0);
        }

        switch (spriteXT[ext].basepic) {
            case RUBWALKPIC:
            case FRGWALKPIC:
            case COP1WALKPIC:
            case ANTWALKPIC:
            case SARAHWALKPIC:
            case MAWALKPIC:
            case DIWALKPIC:
            case ERWALKPIC:
            case SAMWALKPIC:
                int j = jsinsertsprite(spr.getSectnum(), RUNTHRU);
                Sprite spr2 = boardService.getSprite(j);
                if (spr2 != null) {
                    fillsprite(j, spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 6), 128, 0, 0, 12, 16, 16, 0, 0, BLOODSPLAT, spr.getAng(), EngineUtils.sin((spr.getAng() + 2560) & 2047) >> 6, EngineUtils.sin((spr.getAng() + 2048) & 2047) >> 6, 30, sn + 4096,0, 0);
                    spr2.setZ(hitz);
                    spr2.setLotag((short) (engine.getTile(BLOODSPLAT).getAnimFrames()));
                    if (spr2.getLotag() > MAXFRAMES) {
                        spr2.setLotag(MAXFRAMES);
                    }
                    if (spr2.getLotag() < 0) {
                        spr2.setLotag(0);
                    }
                    spr2.setHitag(0);
                    return (1);
                }
                break;
            default:
                break;
        }

        return (0);
    }

    public static void checkblastarea(int sn) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }

        final int sect = spr.getSectnum();
        for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
            int xydist = klabs(spr.getX() - gPlayer[j].posx) + klabs(spr.getY() - gPlayer[j].posy);
            int zdist = klabs(spr.getZ() - gPlayer[j].posz);
            if ((xydist < 512) && (zdist < 10240)) {
                changehealth(j, -5000);
            } else if ((xydist < 2048) && (zdist < 20480)) {
                changehealth(j, -800);
            } else if ((xydist < 4096) && (zdist < 40960)) {
                changehealth(j, -200);
            }
        }

        if (game.nNetMode != NetMode.Multiplayer) {
            ListNode<Sprite> next;
            for (ListNode<Sprite> node = boardService.getSectNode(sect); node != null; node = next) {
                next = node.getNext();
                int i = node.getIndex();
                Sprite spr2 = node.get();

                if ((i != sn) && (!isaplayersprite(i))) {
                    switch (spr2.getStatnum()) {
                        case PLAYER:
                        case BOMBPROJECTILESTAT:
                        case BOMBPROJECTILESTAT2:
                        case RUNTHRU:
                        case INACTIVE:
                        case DEATH:
                            break;
                        default:
                            int xydist = klabs(spr.getX() - spr2.getX()) + klabs(spr.getY() - spr2.getY());
                            int zdist = klabs(spr.getZ() - spr2.getZ());
                            if ((xydist < 2560) && (zdist < 16384)) {
                                damagesprite(i, -500);
                            }
                            break;
                    }
                }
            }
        }
    }

    public static int isahologram(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return 0;
        }

        int ext = spr.getExtra();
        if (validext(ext) != 0) {
            if (((spriteXT[ext].fxmask) & FX_HOLOGRAM) != 0) {
                return (1);
            }
        }

        return (0);
    }

    public static int isanandroid(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return 0;
        }

        int ext = spr.getExtra();

        if (validext(ext) != 0) {
            if (((spriteXT[ext].fxmask) & FX_ANDROID) != 0) {
                return (1);
            }
        }

        return (0);
    }

    public static void playergunshot(int snum) {
        if (!validplayer(snum)) {
            throw new AssertException("plgunsht: bad plrnum");
        }

        ListNode<Sprite> j = boardService.getStatNode(STROLL), nextj;
        while (j != null) {
            nextj = j.getNext();
            Sprite spr = j.get();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(j.getIndex(), 103);
            }
            int dist = abs(spr.getX() - gPlayer[snum].posx) + abs(spr.getY() - gPlayer[snum].posy);
            if (dist < HEARGUNSHOTDIST) {
                spriteXT[ext].aimask |= AI_JUSTSHOTAT;
            }

            j = nextj;
        }

        j = boardService.getStatNode(STANDING);
        while (j != null) {
            nextj = j.getNext();
            Sprite spr = j.get();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(j.getIndex(), 105);
            }
            int dist = abs(spr.getX() - gPlayer[snum].posx) + abs(spr.getY() - gPlayer[snum].posy);
            if (dist < HEARGUNSHOTDIST) {
                spriteXT[ext].aimask |= AI_JUSTSHOTAT;
            }

            j = nextj;
        }

        j = boardService.getStatNode(GUARD);
        while (j != null) {
            nextj = j.getNext();
            Sprite spr = j.get();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(j.getIndex(), 106);
            }
            int dist = abs(spr.getX() - gPlayer[snum].posx) + abs(spr.getY() - gPlayer[snum].posy);
            if (dist < HEARGUNSHOTDIST) {
                spriteXT[ext].aimask |= AI_JUSTSHOTAT;
            }
            j = nextj;
        }
    }

    public static void givewarning(int i, int ext) {
        if (game.nNetMode == NetMode.Multiplayer) {
            return;
        }
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        if (validext(ext) == 0) {
            return;
        }
        if ((spriteXT[ext].aimask & AI_GAVEWARNING) != 0) {
            return;
        }

        int dist = Math.abs(gPlayer[screenpeek].posx - spr.getX()) + Math.abs(gPlayer[screenpeek].posy - spr.getY());
        if (dist > 5000) {
            return;
        }

        if (spriteXT[ext].basepic == COP1WALKPIC) {
            playsound(S_GRD_WHATDOINGHERE + (krand_intercept("STAT2636") & 4), spr.getX(), spr.getY(), 0, ST_UNIQUE);
            spriteXT[ext].aimask |= AI_GAVEWARNING;
        }
    }

    public static void dosectorflash() {
        Sector sec = boardService.getSector(sectflash.sectnum);
        if (sec == null) {
            return;
        }

        switch (sectflash.step) {
            case 0:
                break;
            case 1:
                sec.setVisibility(0);
                sectflash.step = 2;
                break;
            case 2:
                sec.setVisibility((byte) 128);
                sectflash.step = 3;
                break;
            case 3:
                sec.setVisibility(0);
                sectflash.step = 4;
                break;
            case 4:
                sec.setVisibility((byte) sectflash.ovis);
                sectflash.sectnum = 0;
                sectflash.ovis = 0;
                sectflash.step = 0;
        }
    }

    public static void splash(final int i) {
        if (game.nNetMode == NetMode.Multiplayer) {
            return;
        }

        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = jsinsertsprite(spr.getSectnum(), RUNTHRU);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 != null) {
            fillsprite(j, spr.getX(), spr.getY(), spr.getZ(), 2, -13, 0, 32, 64, 64, 0, 0, BLUESPLASH, spr.getAng(), 0, 0, 0, i + 4096,0, 0);

            spr2.setLotag((short) (engine.getTile(BLUESPLASH).getAnimFrames()));
            if (spr2.getLotag() > MAXFRAMES) {
                spr2.setLotag(MAXFRAMES);
            }
            if (spr2.getLotag() < 0) {
                spr2.setLotag(0);
            }
            spr2.setHitag(0);
            playsound(S_SPLASH, spr2.getX(), spr2.getY(), 0, ST_NOUPDATE);
        }
    }

    public static void statuslistcode() {
        ListNode<Sprite> i, nexti;

        dosectorflash();

        if ((lockclock - stackedcheck) > 30) {
            stackedcheck = lockclock;
            i = boardService.getStatNode(STACKED);
            while (i != null) {
                nexti = i.getNext();
                Sprite spr = i.get();
                int index = i.getIndex();
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null) {
                    if (spr.getZ() != sec.getFloorz()) {
                        int tempshort = spr.getCstat();
                        spr.setCstat(spr.getCstat() & ~1);
                        engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, 1);

                        spr.setCstat(tempshort);
                        if (spr.getZ() != zr_florz) {
                            spr.setHitag(0);
                            engine.changespritestat(index, FALL);
                        }
                    }
                }

                i = nexti;
            }
        }

        i = boardService.getStatNode(VANISH);
        while (i != null) {
            nexti = i.getNext();
            int index = i.getIndex();

            jsdeletesprite(index);

            i = nexti;
        }

        i = boardService.getStatNode(FALL);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            game.pInt.setsprinterpolate(index, spr);
            spr.setZ(spr.getZ() + (TICSPERFRAME << 11));

            int tempshort = spr.getCstat();
            spr.setCstat(spr.getCstat() & ~1);
            engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, 1);
            spr.setCstat(tempshort);
            Sector sec = boardService.getSector(spr.getSectnum());
            if (sec != null) {
                if (spr.getZ() >= zr_florz) {
                    spr.setZ(zr_florz);
                    if (zr_florz < sec.getFloorz()) {
                        engine.changespritestat(index, STACKED);
                    } else {
                        engine.changespritestat(index, INANIMATE);
                    }
                }
            }
            i = nexti;
        }

        i = boardService.getStatNode(3);
        // Go through smoke sprites
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();
            game.pInt.setsprinterpolate(index, spr);
            spr.setZ(spr.getZ() - (TICSPERFRAME << 3));
            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(5);
        // Go through explosion sprites
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(RUNTHRU);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            spr.setHitag(spr.getHitag() + 1);

            if (spr.getHitag() >= TICSPERFRAME) {
                spr.setPicnum(spr.getPicnum() + 1);
                spr.setHitag(0);
                spr.setLotag(spr.getLotag() - 1);
            }
            if (spr.getLotag() <= 0) {
                jsdeletesprite(index);
            }
            i = nexti;
        }

        i = boardService.getStatNode(FLOATING);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int prevx = spr.getX();
            int prevy = spr.getY();
            int prevz = spr.getZ();
            int prevsect = spr.getSectnum();

            game.pInt.setsprinterpolate(index, spr);

            // bob on surface
            spr.setHitag(spr.getHitag() + 1);
            if (spr.getHitag() >= (TICSPERFRAME)) {
                spr.setLotag(spr.getLotag() + 1);
                if (spr.getLotag() >= MAXBOBS) {
                    spr.setLotag(0);
                }
                spr.setZ(spr.getZ() + (bobbing[spr.getLotag()] << 4));
                spr.setHitag(0);
            }
            int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
            int day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
            int movestat = floatmovesprite(index, dax, day, 1024, 1024, NORMALCLIP);
            if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                engine.setsprite(index, prevx, prevy, prevz);
                spr.setAng(walldeflect(movestat & HIT_INDEX_MASK, spr.getAng()));
            } else if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) {
                engine.setsprite(index, prevx, prevy, prevz);
                spr.setAng(spritedeflect(movestat & HIT_INDEX_MASK));
            } else if (prevsect != spr.getSectnum()) {
                engine.setsprite(index, prevx, prevy, prevz);
                spr.setAng(arbitraryangle());
            }

            i = nexti;
        }

        i = boardService.getStatNode(PINBALL);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 19);
            }
            spriteXT[ext].target = 0;

            int prevx = spr.getX();
            int prevy = spr.getY();
            int prevz = spr.getZ();
            int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
            int day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
            int movestat = flymovesprite(index, dax, day, 1024, 1024, NORMALCLIP);

            if ((movestat & HIT_TYPE_MASK) == HIT_WALL) {
                engine.setsprite(index, prevx, prevy, prevz);
                spr.setAng(walldeflect(movestat & HIT_INDEX_MASK, spr.getAng()));
                movestat = flymovesprite(index, dax, day, 1024, 1024, NORMALCLIP);
            } else if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) {
                int hitsprite = (movestat & HIT_INDEX_MASK);
                engine.setsprite(index, prevx, prevy, prevz);
                int hit = playerhit(hitsprite);
                int pnum = playerhit;
                if (hit != 0) {
                    spriteXT[ext].target = pnum;
                    newstatus(index, ATTACK);
                } else {
                    spr.setAng(spritedeflect(hitsprite));
                    movestat = flymovesprite(index, dax, day, 1024, 1024, NORMALCLIP);
                }
            }
            if (movestat != 0) {
                spr.setAng(arbitraryangle());
            }

            i = nexti;
        }

        i = boardService.getStatNode(TIMEBOMB);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            spr.setHitag(spr.getHitag() - 1);
            if (spr.getHitag() <= 0) {
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null && sec.getLotag() == 4) {
                    splash(index);
                }

                engine.changespritestat(index, INACTIVE);
                genexplosion2(index);
                jsdeletesprite(index);
            }
            i = nexti;
        }

        i = boardService.getStatNode(BLOODFLOW);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            spr.setHitag(spr.getHitag() + 1);

            if (spr.getHitag() >= (TICSPERFRAME * 6)) {
                spr.setPicnum(spr.getPicnum() + 1);
                spr.setHitag(0);
                spr.setLotag(spr.getLotag() + 1);
            }
            if (spr.getLotag() == 5) {
                engine.changespritestat(index, INANIMATE);
            }

            i = nexti;
        }

        i = boardService.getStatNode(DROPSIES);
        while (i != null) {
            boolean dropsiescontinue = false;
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int dax = (((spr.getXvel()) * TICSPERFRAME) << 7);
            int day = (((spr.getYvel()) * TICSPERFRAME) << 7);
            int daz = 0;

            movesprite(index, dax, day, daz, 4 << 8, 4 << 8, 1);

            spr.setZ(spr.getZ() + spr.getZvel());
            spr.setZvel(spr.getZvel() + (TICSPERFRAME << 2));
            ArtEntry pic = engine.getTile(spr.getPicnum());

            if (spr.getZ() < zr_ceilz + ((pic.getHeight() / 2) << 6)) {
                spr.setZ(zr_ceilz + ((pic.getHeight() / 2) << 6));
                spr.setZvel((short) -(spr.getZvel() >> 1));
            }
            if (spr.getZ() > zr_florz - ((pic.getHeight() / 2) << 6)) {
                spr.setZ(zr_florz - ((pic.getHeight() / 2) << 6));
                spr.setZvel((short) -(spr.getZvel() >> 1));
            }

            dax = spr.getXvel();
            day = spr.getYvel();
            int dist = dax * dax + day * day;
            if (dist < 46000) {
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null) {
                    if (sec.getLotag() == 4) {
                        int zoffs = 0;
                        if ((spr.getCstat() & 128) == 0) {
                            zoffs = -((pic.getHeight() * spr.getYrepeat()) << 1);
                        }

                        spr.setZ(sec.getFloorz() - zoffs);
                        engine.changespritestat(index, FLOATING);
                        spr.setLotag(0);
                        spr.setHitag(0);
                        spr.setXvel(1);
                        spr.setYvel(1);
                    } else {
                        engine.changespritestat(index, INANIMATE);
                    }
                    spr.setZ(sec.getFloorz());
                    spr.setZ(spr.getZ() - ((pic.getHeight() / 2) << 6));
                }
                dropsiescontinue = true;
            }
            if (!dropsiescontinue) {
                if (mulscale(krand_intercept("STAT2934"), dist, 30) == 0) {
                    spr.setXvel(spr.getXvel() - ksgn(spr.getXvel()));
                    spr.setYvel(spr.getYvel() - ksgn(spr.getYvel()));
                    spr.setZvel(spr.getZvel() - ksgn(spr.getZvel()));
                }
            }

            i = nexti;
        }

        i = boardService.getStatNode(7);
        // Go through bomb spriral-explosion sprites
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();
            game.pInt.setsprinterpolate(index, spr);
            spr.setX(spr.getX() + ((spr.getXvel() * TICSPERFRAME) >> 4));
            spr.setY(spr.getY() + ((spr.getYvel() * TICSPERFRAME) >> 4));
            spr.setZ(spr.getZ() + ((spr.getZvel() * TICSPERFRAME) >> 4));

            spr.setZvel(spr.getZvel() + (TICSPERFRAME << 7));
            Sector sec = boardService.getSector(spr.getSectnum());
            if (sec != null) {
                if (spr.getZ() < sec.getCeilingz() + (4 << 8)) {
                    spr.setZ(sec.getCeilingz() + (4 << 8));
                    spr.setZvel((short) -(spr.getZvel() >> 1));
                }
                if (spr.getZ() > sec.getFloorz() - (4 << 8)) {
                    spr.setZ(sec.getFloorz() - (4 << 8));
                    spr.setZvel((short) -(spr.getZvel() >> 1));
                }
            }

            spr.setXrepeat((byte) (spr.getLotag() >> 2));
            spr.setYrepeat((byte) (spr.getLotag() >> 2));

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(TOSS);
        while (i != null) {
            boolean tosscontinue = false;
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int dax = (((spr.getXvel()) * TICSPERFRAME) << 11);
            int day = (((spr.getYvel()) * TICSPERFRAME) << 11);
            int daz = 0;
            int movestat = kenmovesprite(index, dax, day, daz, 4 << 8, 4 << 8, 1);
            // break stuff, but dont hurt enemies unless direct hit on head
            if ((movestat & HIT_TYPE_MASK) == HIT_SPRITE) {
                int hitsprite = (movestat & HIT_INDEX_MASK);
                Sprite spr2 = boardService.getSprite(hitsprite);
                if (spr2 != null) {
                    int ext = spr2.getExtra();
                    if (validext(ext) != 0 && (spriteXT[ext].deathpic != 0)) {
                        if (spr2.getStatnum() == INANIMATE) {
                            damagesprite(hitsprite, 20);
                        }
                    }
                }
            }

            spr.setZ(spr.getZ() + spr.getZvel());
            spr.setZvel(spr.getZvel() + (TICSPERFRAME << 5));
            ArtEntry pic = engine.getTile(BOMB);

            if (spr.getZ() < zr_ceilz + (pic.getHeight() << 6)) {
                spr.setZ(zr_ceilz + (pic.getHeight() << 6));
                spr.setZvel((short) -(spr.getZvel() >> 1));
            }
            if (spr.getZ() > zr_florz - (pic.getHeight() << 4)) {
                switch (zr_florhit & HIT_TYPE_MASK) {
                    case HIT_SPRITE:
                        // direct hit on head
                        int hitsprite = (zr_florhit & HIT_INDEX_MASK);
                        Sprite spr2 = boardService.getSprite(hitsprite);
                        if (spr2 != null) {
                            int hit = playerhit(hitsprite);
                            int pnum = playerhit;
                            if (hit != 0) {
                                changehealth(pnum, -40);
                            } else if (spr2.getExtra() != -1) {
                                playsound(S_BUSHIT, spr2.getX(), spr2.getY(), 0, ST_NOUPDATE);
                                damagesprite(hitsprite, 10);
                            }
                        }
                        break;
                    case HIT_SECTOR: {
                        Sector hitSector = boardService.getSector(zr_florhit & HIT_INDEX_MASK);

                        if (hitSector != null && (hitSector.getLotag() == 4) && (spr.getPicnum() != RATTHROWPIC)) {
                            int zoffs = 0;
                            if ((spr.getCstat() & 128) == 0) {
                                zoffs = -((engine.getTile(spr.getPicnum()).getHeight() * spr.getYrepeat()) << 1);
                            }

                            game.pInt.setsprinterpolate(index, spr);
                            Sector sec = boardService.getSector(spr.getSectnum());
                            if (sec != null) {
                                spr.setZ(sec.getFloorz() - zoffs);
                            }

                            splash(index);
                            switch (spr.getPicnum()) {
                                case TUBEBOMB:
                                    spr.setPicnum(TUBEBOMB + 1);
                                    spr.setHitag((short) (krand_intercept("STAT3031") & 255));
                                    engine.changespritestat(index, TIMEBOMB);
                                    break;
                                case DARTBOMB:
                                    spr.setHitag((short) (krand_intercept("STAT3035") & 255));
                                    spr.setPicnum(DARTBOMB + 1);
                                    engine.changespritestat(index, TIMEBOMB);
                                    break;
                                default:
                                    spr.setLotag(0);
                                    spr.setHitag(0);
                                    spr.setXvel(1);
                                    spr.setYvel(1);
                                    engine.changespritestat(index, FLOATING);
                                    break;
                            }
                            tosscontinue = true;
                        }
                        break;
                    }
                }
                if (!tosscontinue) {
                    game.pInt.setsprinterpolate(index, spr);
                    spr.setZ(zr_florz - (pic.getHeight() << 4));
                    spr.setZvel((short) -(spr.getZvel() >> 1));
                    spr.setHitag(spr.getHitag() + 1);
                }
            }
            if (!tosscontinue) {
                dax = spr.getXvel();
                day = spr.getYvel();
                int dist = dax * dax + day * day;
                if (mulscale(krand_intercept("STAT3057"), dist, 30) == 0) {
                    spr.setXvel(spr.getXvel() - ksgn(spr.getXvel()));
                    spr.setYvel(spr.getYvel() - ksgn(spr.getYvel()));
                    spr.setZvel(spr.getZvel() - ksgn(spr.getZvel()));
                }
                if (spr.getHitag() >= 3) {
                    switch (spr.getPicnum()) {
                        case RATTHROWPIC: {
                            int ext = spr.getExtra();
                            if (validext(ext) == 0) {
                                jsdeletesprite(index);
                                break;
                            }
                            game.pInt.setsprinterpolate(index, spr);
                            Sector sec = boardService.getSector(spr.getSectnum());
                            if (sec != null) {
                                spr.setZ(sec.getFloorz());
                            }
                            spr.setAng(arbitraryangle());
                            spr.setPicnum(RATPIC);
                            spriteXT[ext].basestat = RODENT;
                            newstatus(index, RODENT);
                            spr.setXvel(4);
                            spr.setYvel(4);
                            spr.setZvel(0);
                            spr.setLotag(2004);
                            spr.setHitag(0);
                            break;
                        }
                        case TUBEBOMB:
                            spr.setPicnum(TUBEBOMB + 1);
                            spr.setHitag((short) (krand_intercept("STAT3083") & 255));
                            engine.changespritestat(index, TIMEBOMB);
                            break;
                        case DARTBOMB:
                            spr.setPicnum(DARTBOMB + 1);
                            spr.setHitag((short) (krand_intercept("STAT3088") & 255));
                            engine.changespritestat(index, TIMEBOMB);
                            break;
                        default: {
                            spr.setXvel(0);
                            spr.setYvel(0);
                            spr.setZvel(0);
                            spr.setLotag(2004);
                            Sector sec = boardService.getSector(spr.getSectnum());
                            if (sec != null) {
                                if (zr_florz != sec.getFloorz()) {
                                    game.pInt.setsprinterpolate(index, spr);
                                    spr.setZ(zr_florz);
                                    engine.changespritestat(index, STACKED);
                                } else {
                                    engine.changespritestat(index, INANIMATE);
                                }
                            }
                            break;
                        }
                    }
                }
            }

            i = nexti;
        }

        i = boardService.getStatNode(AMBUSH);
        while (i != null) {
            boolean ambushcontinue = false;
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 1);
            }

            int mindist = 0x7fffffff;
            int target = connecthead;
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                int dist = abs(spr.getX() - gPlayer[p].posx) + abs(spr.getY() - gPlayer[p].posy);
                if (dist < mindist) {
                    mindist = dist;
                    target = p;
                }
            }
            if (mindist > DONTBOTHERDISTANCE) {
                ambushcontinue = true;
            }

            if (!ambushcontinue) {
                spriteXT[ext].target = target;

                if ((spriteXT[index].aimask & AI_JUSTSHOTAT) != 0 || isvisible(index, target)) {
                    if (spriteXT[ext].morphpic != 0) {
                        newstatus(index, MORPH);
                    } else {
                        ambushyell(index, spr.getExtra());
                        newstatus(index, spriteXT[ext].basestat);
                    }
                }
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        i = boardService.getStatNode(STALK);
        while (i != null) {
            boolean stalkcontinue = false;
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 2);
            }

            int mindist = 0x7fffffff;
            int target = connecthead;
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                int dist = abs(spr.getX() - gPlayer[p].posx) + abs(spr.getY() - gPlayer[p].posy);
                if (dist < mindist) {
                    mindist = dist;
                    target = p;
                }
            }
            if (mindist > DONTBOTHERDISTANCE) {
                stalkcontinue = true;
            }
            if (!stalkcontinue) {
                spriteXT[ext].target = target;

                int targx = gPlayer[target].posx;
                int targy = gPlayer[target].posy;
                int prevx = spr.getX();
                int prevy = spr.getY();
                int prevz = spr.getZ();
                int prevsect = spr.getSectnum();
                int daang = (EngineUtils.getAngle(targx - spr.getX(), targy - spr.getY()) & 2047);

                // USES SPRITESORTLIST - NOT MULTIPLAYER COMPATIBLE !

                if (((spriteXT[ext].aimask) & AI_WASDRAWN) != 0) {
                    if (((spr.getAng() + 2048 - daang) & 2047) < 1024) {
                        spr.setAng((short) ((spr.getAng() + 2048 - (TICSPERFRAME << 1)) & 2047));
                    } else {
                        spr.setAng((short) ((spr.getAng() + (TICSPERFRAME << 1)) & 2047));
                    }
                    if (RMOD16("STAT3291") == 0) {
                        attackifclose(index, target, mindist);
                    }
                    if (spr.getStatnum() != ATTACK) {
                        int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
                        int day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
                        int movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                        switch (movestat & HIT_TYPE_MASK) {
                            case HIT_WALL: // blocked by a wall
                                spr.setAng(walldeflect(movestat & HIT_INDEX_MASK, spr.getAng()));
                                break;
                            case HIT_SPRITE: // blocked by a sprite
                                spr.setAng(spritedeflect(movestat & HIT_INDEX_MASK));
                                break;
                            case HIT_SECTOR:
                                spr.setAng(arbitraryangle());
                                break;
                        }
                        if (movestat != 0) {
                            dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
                            day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
                            movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                            if (movestat != 0) {
                                spr.setAng(arbitraryangle());
                            }
                        }
                    }
                } else {
                    if (((spr.getAng() + 2048 - daang) & 2047) < 1024) {
                        spr.setAng((short) ((spr.getAng() + 2048 - (TICSPERFRAME << 1)) & 2047));
                    } else {
                        spr.setAng((short) ((spr.getAng() + (TICSPERFRAME << 1)) & 2047));
                    }
                    int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 4);
                    int day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 4);
                    int movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                    switch (movestat & HIT_TYPE_MASK) {
                        case HIT_WALL: // blocked by a wall
                            newstatus(index, SQUAT);
                            spr.setAng(walldeflect(movestat & HIT_INDEX_MASK, spr.getAng()));
                            break;
                        case HIT_SPRITE: // blocked by a sprite
                            newstatus(index, SQUAT);
                            spr.setAng(spritedeflect(movestat & HIT_INDEX_MASK));
                            break;
                        case HIT_SECTOR:
                            spr.setAng(arbitraryangle());
                            break;
                    }
                    if (movestat != 0) {
                        dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
                        day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
                        movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                        if (movestat != 0) {
                            spr.setAng(arbitraryangle());
                        }
                    }
                }

                if (spr.getSectnum() != prevsect) {
                    Sector sec = boardService.getSector(spr.getSectnum());
                    if (sec != null) {
                        if (sec.getLotag() == SECT_LOTAG_OFFLIMITS_ALL) {
                            engine.setsprite(index, prevx, prevy, prevz);
                            spr.setAng(arbitraryangle());
                        } else {
                            enemynewsector(index);
                        }
                    }
                }
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(CHASE); node != null; node = nexti) {
            nexti = node.getNext();

            Sprite spr = node.get();
            int index = node.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 3);
            }

            int mindist = 0x7fffffff;
            int target = connecthead;
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                int dist = abs(spr.getX() - gPlayer[p].posx) + abs(spr.getY() - gPlayer[p].posy);
                if (dist < mindist) {
                    mindist = dist;
                    target = p;
                }
            }
            if (mindist > DONTBOTHERDISTANCE) {
                spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
                spriteXT[ext].aimask &= ~AI_WASDRAWN;
                continue;
            }

            int dist = mindist;
            spriteXT[ext].target = target;

            int targx = gPlayer[target].posx;
            int targy = gPlayer[target].posy;
            int targz = gPlayer[target].posz;
            int targsect = gPlayer[target].cursectnum;
            int prevx = spr.getX();
            int prevy = spr.getY();
            int prevz = spr.getZ();
            int prevsect = spr.getSectnum();
            int daang = (EngineUtils.getAngle(targx - spr.getX(), targy - spr.getY()) & 2047);

            if (game.nNetMode != NetMode.Multiplayer && ((spriteXT[ext].aimask) & AI_CRITICAL) != 0) {
                if (((spriteXT[ext].fxmask) & (FX_ANDROID | FX_HOLOGRAM)) == 0) {
                    spr.setAng((short) ((daang + 1024) & 2047));
                    newstatus(index, FLEE);
                    if (spr.getStatnum() == FLEE) {
                        spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
                        spriteXT[ext].aimask &= ~AI_WASDRAWN;
                        continue;
                    }
                }
            }

            // can player see target if they squat ?
            int seecan = 0;
            if (spr.getSectnum() >= 0 && targsect >= 0 && engine.cansee(targx, targy, targz, targsect, spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 6), spr.getSectnum())) {
                seecan = 1;
            }

            if (((spr.getAng() + 2048 - daang) & 2047) < 1024) {
                spr.setAng((short) ((spr.getAng() + 2048 - (TICSPERFRAME << 1)) & 2047));
            } else {
                spr.setAng((short) ((spr.getAng() + (TICSPERFRAME << 1)) & 2047));
            }

            if (seecan == 1) {
                if (RMOD4("STAT3427") == 0) {
                    attackifclose(index, target, dist);
                }

                if (spr.getStatnum() != ATTACK) {
                    int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
                    int day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
                    int movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                    switch (movestat & HIT_TYPE_MASK) {
                        case HIT_WALL: // blocked by a wall
                            spr.setAng(walldeflect(movestat & HIT_INDEX_MASK, spr.getAng()));
                            break;
                        case HIT_SPRITE: // blocked by a sprite
                            spr.setAng(spritedeflect(movestat & HIT_INDEX_MASK));
                            break;
                        case HIT_SECTOR:
                            spr.setAng(arbitraryangle());
                            break;
                    }
                    if (movestat != 0) {
                        dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
                        day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
                        movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                        if (movestat != 0) {
                            spr.setAng(arbitraryangle());
                        }
                    }
                }
            } else {
                int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
                int day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
                int movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                switch (movestat & HIT_TYPE_MASK) {
                    case HIT_WALL: // blocked by a wall
                        newstatus(index, SQUAT);
                        spr.setAng(walldeflect(movestat & HIT_INDEX_MASK, spr.getAng()));
                        break;
                    case HIT_SPRITE: // blocked by a sprite
                        newstatus(index, SQUAT);
                        spr.setAng(spritedeflect(movestat & HIT_INDEX_MASK));
                        break;
                    case HIT_SECTOR:
                        spr.setAng(arbitraryangle());
                        break;
                }
                if (movestat != 0) {
                    dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
                    day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
                    movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                    if (movestat != 0) {
                        spr.setAng(arbitraryangle());
                    }
                }
            }

            if (spr.getSectnum() != prevsect) {
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null) {
                    if (sec.getLotag() == SECT_LOTAG_OFFLIMITS_ALL) {
                        engine.setsprite(index, prevx, prevy, prevz);
                        spr.setAng(arbitraryangle());
                    } else {
                        enemynewsector(index);
                    }
                }
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(GUARD); node != null; node = nexti) {
            nexti = node.getNext();

            Sprite spr = node.get();
            int index = node.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 4);
            }

            int mindist = 0x7fffffff;
            int target = connecthead;
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                int dist = abs(spr.getX() - gPlayer[p].posx) + abs(spr.getY() - gPlayer[p].posy);
                if (dist < mindist) {
                    mindist = dist;
                    target = p;
                }
            }
            if (mindist > DONTBOTHERDISTANCE) {
                spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
                spriteXT[ext].aimask &= ~AI_WASDRAWN;
                continue;
            }

            int dist = mindist;
            spriteXT[ext].target = target;

            int targx = gPlayer[target].posx;
            int targy = gPlayer[target].posy;
            int targz = gPlayer[target].posz;
            int targsect = gPlayer[target].cursectnum;
            int prevx = spr.getX();
            int prevy = spr.getY();
            int prevz = spr.getZ();
            int prevsect = spr.getSectnum();

            if ((((spriteXT[ext].aimask) & AI_JUSTSHOTAT) != 0)) {
                if (engine.cansee(targx, targy, targz, targsect, spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                    spr.setAng(EngineUtils.getAngle(targx - spr.getX(), targy - spr.getY()));
                    spriteXT[ext].aimask |= AI_WASATTACKED; // guard needs to take action
                }
                spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
                spriteXT[ext].aimask &= ~AI_WASDRAWN;
                continue;
            }

            if ((gPlayer[target].drawweap) != 0 && isvisible(index, target) && ((spriteXT[ext].aimask) & (AI_WASATTACKED | AI_ENCROACHMENT)) == 0) {
                givewarning(index, ext);
                spr.setAng(EngineUtils.getAngle(targx - spr.getX(), targy - spr.getY()));
                spr.setPicnum((short) (spriteXT[ext].attackpic + 1));
                if (dist < 1024) {
                    newstatus(index, ATTACK);
                }
                spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
                spriteXT[ext].aimask &= ~AI_WASDRAWN;
                continue;
            } else {
                spr.setPicnum((short) spriteXT[ext].basepic);
            }

            if (((spriteXT[ext].aimask) & (AI_WASATTACKED | AI_ENCROACHMENT)) == 0) {
                spriteXT[ext].aimask &= ~AI_GAVEWARNING;
                if (RMOD16("STAT3561") == 0) {
                    spr.setAng(EngineUtils.getAngle(targx - spr.getX(), targy - spr.getY()));
                    newstatus(index, STANDING);
                } else {
                    int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
                    int day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
                    int movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                    if (movestat != 0) {
                        spr.setAng(arbitraryangle());
                    }
                }
            } else {
                if ((RMOD4("STAT3575") == 0) && (engine.cansee(targx, targy, targz, targsect, spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum()))) {
                    spr.setAng(EngineUtils.getAngle(targx - spr.getX(), targy - spr.getY()));
                    newstatus(index, ATTACK);
                } else {
                    int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * (spr.getXvel() + 2)) << 3);
                    int day = (((EngineUtils.sin(spr.getAng() & 2047)) * (spr.getYvel() + 2)) << 3);
                    int movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                    if (movestat != 0) {
                        spr.setAng(arbitraryangle());
                    }
                }
            }

            if ((prevsect != spr.getSectnum())) {
                engine.setsprite(index, prevx, prevy, prevz);
                spr.setAng(arbitraryangle());
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;
        }

        i = boardService.getStatNode(FLEE);
        while (i != null) {
            boolean fleecontinue = false;
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 5);
            }

            int mindist = 0x7fffffff;
            int target = connecthead;
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                int dist = abs(spr.getX() - gPlayer[p].posx) + abs(spr.getY() - gPlayer[p].posy);
                if (dist < mindist) {
                    mindist = dist;
                    target = p;
                }
            }
            if ((gPlayer[target].drawweap == 0) || (mindist > DONTBOTHERDISTANCE)) {
                newstatus(index, spriteXT[ext].basestat);
                fleecontinue = true;
            }
            if (!fleecontinue) {
                spriteXT[ext].target = target;

                int targx = gPlayer[target].posx;
                int targy = gPlayer[target].posy;
                int targz = gPlayer[target].posz;
                int targsect = gPlayer[target].cursectnum;
                int prevx = spr.getX();
                int prevy = spr.getY();
                int prevz = spr.getZ();
                int prevsect = spr.getSectnum();

                int FLEESPEED = 5;
                int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * FLEESPEED) << 3);
                int day = (((EngineUtils.sin(spr.getAng() & 2047)) * FLEESPEED) << 3);
                int movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);

                if (movestat != 0) {
                    switch (movestat & HIT_TYPE_MASK) {
                        case HIT_WALL: // blocked by a wall
                            spr.setAng(walldeflect(movestat & HIT_INDEX_MASK, spr.getAng()));
                            break;
                        case HIT_SPRITE: // blocked by a sprite
                            spr.setAng(spritedeflect(movestat & HIT_INDEX_MASK));
                            break;
                        case HIT_SECTOR:
                            spr.setAng(arbitraryangle());
                            break;
                    }
                    if (engine.cansee(targx, targy, targz, targsect, spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                        attackifclose(index, target, mindist);
                        if (spr.getStatnum() != ATTACK) {
                            int daang = EngineUtils.getAngle(targx - spr.getX(), targy - spr.getY());
                            spr.setAng((short) ((daang + 1024) & 2047));
                            movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);
                            if ((movestat != 0) && RMOD2("STAT3663") != 0) {
                                newstatus(index, HIDE);
                            }
                        }
                    } else {
                        if (RMOD3("STAT3668") == 0) {
                            newstatus(index, HIDE);
                        }
                    }
                }

                if (spr.getSectnum() != prevsect) {
                    Sector sec = boardService.getSector(spr.getSectnum());
                    if (sec != null) {
                        if (sec.getLotag() == SECT_LOTAG_OFFLIMITS_ALL) {
                            engine.setsprite(index, prevx, prevy, prevz);
                            spr.setAng(arbitraryangle());
                        } else {
                            enemynewsector(index);
                        }
                    }
                }
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(STROLL); node != null; node = nexti) {
            nexti = node.getNext();
            Sprite spr = node.get();
            final int index = node.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 6);
            }

            int mindist = 0x7fffffff;
            int target = connecthead;
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                int dist = abs(spr.getX() - gPlayer[p].posx) + abs(spr.getY() - gPlayer[p].posy);
                if (dist < mindist) {
                    mindist = dist;
                    target = p;
                }
            }
            if (mindist > DONTBOTHERDISTANCE) {
                spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
                spriteXT[ext].aimask &= ~AI_WASDRAWN;
                continue;
            }

            int dist = mindist;
            spriteXT[ext].target = target;

            int prevx = spr.getX();
            int prevy = spr.getY();
            int prevz = spr.getZ();
            int prevsect = spr.getSectnum();

            if (((spriteXT[ext].aimask) & AI_JUSTSHOTAT) != 0) {
                attackifclose(index, target, dist);
                if (spr.getStatnum() != ATTACK) {
                    int daang = (EngineUtils.getAngle(gPlayer[target].posx - spr.getX(), gPlayer[target].posy - spr.getY()) & 2047);
                    spr.setAng(((daang + 1024) & 2047));
                    fleescream(index, ext);
                    newstatus(index, FLEE);
                }
                spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
                spriteXT[ext].aimask &= ~AI_WASDRAWN;
                continue; // goto strollcontinue;
            }

            if ((gPlayer[target].drawweap != 0) && isvisible(index, target)) {
                int daang = (EngineUtils.getAngle(gPlayer[target].posx - spr.getX(), gPlayer[target].posy - spr.getY()) & 2047);
                spr.setAng(((daang + 1024) & 2047));

                if (dist < HIDEDISTANCE) {
                    newstatus(index, HIDE);
                }
                if (spr.getStatnum() == HIDE) {
                    hideplea(index, ext);
                } else {
                    fleescream(index, ext);
                    newstatus(index, FLEE);
                }
                spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
                spriteXT[ext].aimask &= ~AI_WASDRAWN;
                continue; // goto strollcontinue;
            }

            int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
            int day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
            int movestat = movesprite(index, dax, day, 0, 1024, 1024, CLIFFCLIP);

            if (movestat != 0) {
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null) {
                    if ((RMOD10("STAT3757") == 0) && (sec.getLotag() != SECT_LOTAG_NOSTANDING)) {
                        spr.setAng((short) ((spr.getAng() + 1024) & 2047));
                        newstatus(index, STANDING);
                    } else {
                        switch (movestat & HIT_TYPE_MASK) {
                            case HIT_WALL: // blocked by a wall
                                spr.setAng(walldeflect(movestat & HIT_INDEX_MASK, spr.getAng()));
                                break;
                            case HIT_SPRITE: // blocked by a sprite
                                spr.setAng(spritedeflect(movestat & HIT_INDEX_MASK));
                                break;
                            case HIT_SECTOR:
                                spr.setAng(arbitraryangle());
                                break;
                        }
                    }
                }
            }

            if (spr.getSectnum() != prevsect) {
                Sector sec = boardService.getSector(spr.getSectnum());
                if (sec != null) {
                    if ((sec.getLotag() == SECT_LOTAG_OFFLIMITS_CIVILLIAN) || (sec.getLotag() == SECT_LOTAG_OFFLIMITS_ALL)) {
                        engine.setsprite(index, prevx, prevy, prevz);
                        spr.setAng(arbitraryangle());
                    } else {
                        enemynewsector(index);
                    }
                }
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;
        }

        i = boardService.getStatNode(FLY);
        while (i != null) {
            boolean flycontinue = false;
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 7);
            }

            int mindist = 0x7fffffff;
            int target = connecthead;
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                int dist = abs(spr.getX() - gPlayer[p].posx) + abs(spr.getY() - gPlayer[p].posy);
                if (dist < mindist) {
                    mindist = dist;
                    target = p;
                }
            }
            if (mindist > DONTBOTHERDISTANCE) {
                flycontinue = true;
            }
            if (!flycontinue) {
                spriteXT[ext].target = target;

                if (spriteXT[ext].basepic != AUTOGUN) {
                    int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * ((spr.getXvel()) << 1)) << 3);
                    int day = (((EngineUtils.sin(spr.getAng() & 2047)) * ((spr.getYvel()) << 1)) << 3);
                    int movestat = flymovesprite(index, dax, day, 1024, 1024, NORMALCLIP);
                    if (movestat != 0) {
                        spr.setAng(arbitraryangle());
                    }
                }
                if (gPlayer[target].cursectnum >= 0 && engine.cansee(gPlayer[target].posx, gPlayer[target].posy, gPlayer[target].posz, gPlayer[target].cursectnum, spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum())) {
                    if (spriteXT[ext].weapon == 0) {
                        if (mindist < 5120) {
                            spr.setAng(EngineUtils.getAngle(gPlayer[target].posx - spr.getX(), gPlayer[target].posy - spr.getY()));
                        }
                        if (mindist < 1024) {
                            spr.setAng(EngineUtils.getAngle(gPlayer[target].posx - spr.getX(), gPlayer[target].posy - spr.getY()));
                            if (RMOD4("STAT3835") == 0) {
                                newstatus(index, ATTACK);
                            }
                        }
                    } else {
                        attackifclose(index, target, mindist);
                    }
                }
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        i = boardService.getStatNode(RODENT);
        while (i != null) {
            boolean rodentcontinue = false;
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 3);
            }

            int mindist = 0x7fffffff;
            int target = connecthead;
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                int dist = abs(spr.getX() - gPlayer[p].posx) + abs(spr.getY() - gPlayer[p].posy);
                if (dist < mindist) {
                    mindist = dist;
                    target = p;
                }
            }
            if (mindist > DONTBOTHERDISTANCE) {
                rodentcontinue = true;
            }
            if (!rodentcontinue) {
                int daang = (EngineUtils.getAngle(gPlayer[target].posx - spr.getX(), gPlayer[target].posy - spr.getY()) & 2047);
                spriteXT[ext].target = target;

                if (((spr.getAng() + 2048 - daang) & 2047) < 1024) {
                    spr.setAng((short) ((spr.getAng() + 2048 - (TICSPERFRAME << 1)) & 2047));
                } else {
                    spr.setAng((short) ((spr.getAng() + (TICSPERFRAME << 1)) & 2047));
                }
                int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
                int day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
                int movestat = movesprite(index, dax, day, 0, 1024, 1024, 2);

                switch (movestat & HIT_TYPE_MASK) {
                    case HIT_WALL: // blocked by a wall
                        spr.setAng(walldeflect(movestat & HIT_INDEX_MASK, spr.getAng()));
                        break;
                    case HIT_SPRITE: // blocked by a sprite
                        spr.setAng(spritedeflect(movestat & HIT_INDEX_MASK));
                        break;
                    case HIT_SECTOR:
                        spr.setAng(arbitraryangle());
                        break;
                }
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        i = boardService.getStatNode(STANDING);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 8);
            }

            int mindist = 0x7fffffff;
            int target = connecthead;
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                int dist = abs(spr.getX() - gPlayer[p].posx) + abs(spr.getY() - gPlayer[p].posy);
                if (dist < mindist) {
                    mindist = dist;
                    target = p;
                }
            }
            Sector sec = boardService.getSector(spr.getSectnum());
            if (sec != null) {
                if (sec.getLotag() == SECT_LOTAG_NOSTANDING) {
                    spr.setLotag(0);
                    spr.setHitag(0);
                    spr.setPicnum((short) spriteXT[ext].basepic);
                    newstatus(index, spriteXT[ext].basestat);
                }
            }

            if (((spriteXT[ext].aimask) & AI_JUSTSHOTAT) != 0) {
                spr.setLotag(0);
            }
            if (((spriteXT[ext].aimask) & AI_ENCROACHMENT) != 0) {
                spr.setLotag(0);
            }
            if ((gPlayer[target].drawweap != 0)) {
                if (isvisible(index, target)) {
                    spr.setLotag(0);
                }
            }

            spr.setLotag(spr.getLotag() - (TICSPERFRAME));
            if (spr.getLotag() < 0) {
                spr.setLotag(0);
                newstatus(index, spriteXT[ext].basestat);
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        i = boardService.getStatNode(ATTACK);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 9);
            }

            if (spr.getLotag() == spr.getHitag()) { // fire instance
                enemyshootgun(index, spr.getX(), spr.getY(), spr.getZ(), spr.getAng(), 100, spr.getSectnum(), spriteXT[ext].weapon);
            }

            spr.setLotag(spr.getLotag() - (TICSPERFRAME));

            if (spr.getLotag() < 0) {
                spr.setLotag(0);
                if (((spriteXT[ext].aimask) & AI_TIMETODODGE) != 0) {
                    spriteXT[ext].aimask &= ~AI_TIMETODODGE;
                    newstatus(index, DODGE);
                } else {
                    newstatus(index, spriteXT[ext].basestat);
                }
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        i = boardService.getStatNode(DELAYEDATTACK);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 10);
            }

            spr.setLotag(spr.getLotag() - (TICSPERFRAME));

            if (spr.getLotag() < 0) {
                spr.setLotag(0);
                newstatus(index, ATTACK);
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        i = boardService.getStatNode(SQUAT);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 11);
            }

            Sector sec = boardService.getSector(spr.getSectnum());
            if (sec != null) {
                if (sec.getLotag() == SECT_LOTAG_NOSTANDING) {
                    spr.setLotag(0);
                    spr.setHitag(0);
                    spr.setPicnum((short) spriteXT[ext].basepic);
                    newstatus(index, spriteXT[ext].basestat);
                }
            }

            spr.setLotag(spr.getLotag() - (TICSPERFRAME));
            if (spr.getLotag() < 0) {
                spr.setHitag(spr.getHitag() - (TICSPERFRAME));
                if (spr.getHitag() < 0) {
                    spr.setLotag(0);
                    spr.setHitag(0);
                    newstatus(index, UNSQUAT);
                }
            } else {
                spr.setPicnum((short) (spriteXT[ext].squatpic + ((47 - spr.getLotag()) >> 4)));
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        i = boardService.getStatNode(UNSQUAT);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 12);
            }

            spr.setLotag(spr.getLotag() - (TICSPERFRAME));
            if (spr.getLotag() < 0) {
                spr.setLotag(0);
                spr.setHitag(0);
                spr.setPicnum((short) spriteXT[ext].basepic);
                newstatus(index, spriteXT[ext].basestat);
            } else {
                spr.setPicnum((short) (spriteXT[ext].squatpic + (((47) >> 4)) - ((47 - spr.getLotag()) >> 4)));
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        i = boardService.getStatNode(HIDE);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 13);
            }

            Sector sec = boardService.getSector(spr.getSectnum());
            if (sec != null) {
                if (sec.getLotag() == SECT_LOTAG_NOSTANDING) {
                    spr.setLotag(0);
                    spr.setHitag(0);
                    spr.setPicnum((short) spriteXT[ext].basepic);
                    newstatus(index, spriteXT[ext].basestat);
                }
            }

            if (((spriteXT[ext].aimask) & AI_JUSTSHOTAT) != 0) {
                spr.setLotag(0);
                spr.setHitag(0);
            }

            spr.setLotag(spr.getLotag() - (TICSPERFRAME));
            if (spr.getLotag() < 0) {
                spr.setHitag(spr.getHitag() - (TICSPERFRAME));
                if (spr.getHitag() < 0) {
                    spr.setLotag(0);
                    spr.setHitag(0);
                    newstatus(index, UNHIDE);
                }
            } else {
                spr.setPicnum((short) (spriteXT[ext].squatpic + ((47 - spr.getLotag()) >> 4)));
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        i = boardService.getStatNode(UNHIDE);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 14);
            }

            spr.setLotag(spr.getLotag() - (TICSPERFRAME));
            if (spr.getLotag() < 0) {
                spr.setLotag(0);
                spr.setHitag(0);
                spr.setPicnum((short) spriteXT[ext].basepic);
                newstatus(index, FLEE);
            } else {
                spr.setPicnum((short) (spriteXT[ext].squatpic + (((47) >> 4)) - ((47 - spr.getLotag()) >> 4)));
            }

            spriteXT[ext].aimask &= ~AI_JUSTSHOTAT;
            spriteXT[ext].aimask &= ~AI_WASDRAWN;

            i = nexti;
        }

        i = boardService.getStatNode(PAIN);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 15);
            }

            spr.setLotag(spr.getLotag() - (TICSPERFRAME));
            if (spr.getLotag() < 0) {
                spr.setLotag(0);
                newstatus(index, spriteXT[ext].basestat);
            }

            i = nexti;
        }

        i = boardService.getStatNode(PLRVIRUS);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int host = spr.getOwner();
            if (!validplayer(host)) {
                throw new AssertException("plrvirus lost host");
            }

            game.pInt.setsprinterpolate(index, spr);
            spr.setX(gPlayer[host].posx);
            spr.setY(gPlayer[host].posy);
            spr.setZ(gPlayer[host].posz + (8 << 8));

            spr.setLotag(spr.getLotag() - (TICSPERFRAME));
            if ((spr.getLotag() > 0) && ((spr.getLotag() & 3) == 0)) {
                if (changehealth(host, -64)) {
                    spr.setLotag(0);
                }
            }
            if (spr.getLotag() <= 0) {
                changehealth(host, -8192);
                if (host == screenpeek) {
                    showmessage("BURNED");
                }
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(VIRUS);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            final int host = spr.getOwner();
            Sprite hostSprite = boardService.getSprite(host);
            if (hostSprite == null || (hostSprite.getStatnum() >= MAXSTATUS)) {
                jsdeletesprite(index);
                i = nexti;
                continue;
            }

            game.pInt.setsprinterpolate(index, spr);
            spr.setX(hostSprite.getX());
            spr.setY(hostSprite.getY());
            if (spr.getPicnum() == FIREPIC) {
                spr.setZ(hostSprite.getZ() - (engine.getTile(hostSprite.getPicnum()).getHeight() << 4));
            } else {
                spr.setZ(hostSprite.getZ());
            }

            if (spr.getPicnum() == FIREPIC) {
                spr.setHitag(spr.getHitag() + 4);
                if (spr.getHitag() >= (TICSPERFRAME << 3)) {
                    hostSprite.setShade(hostSprite.getShade() + 1);
                    spr.setHitag(0);
                }
                if (hostSprite.getShade() > 12) {
                    damagesprite(host, 1024);
                    spr.setLotag(0);
                }
            } else {
                spr.setLotag(spr.getLotag() - (TICSPERFRAME));
                if (damagesprite(host, 4) == 1) { // killed 'em
                    // NOT NETWORK COMPATIBLE
                    killscore(host, screenpeek, 0);
                    spr.setLotag(0);
                }
            }

            if (spr.getLotag() <= 0) {
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(DEATH);
        while (i != null) {
            boolean deathcontinue = false;
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();
            int ext = spr.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(index, 16);
            }

            if (isanandroid(index) != 0) {
                androidexplosion(index);
                jsdeletesprite(index);
                showmessage("WAS AN ANDROID");
                deathcontinue = true;
            }
            if (!deathcontinue) {
                int targsect = spr.getSectnum();
                Sector targSec = boardService.getSector(targsect);
                if (targSec != null && targSec.getLotag() != 4) {
                    engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, CLIPMASK0);
                    game.pInt.setsprinterpolate(index, spr);
                    if (spr.getZ() < zr_florz) {
                        spr.setZ(spr.getZ() + 1024);
                    } else {
                        spr.setZ(zr_florz);
                    }
                }

                spr.setLotag(spr.getLotag() - (TICSPERFRAME << 1));
                if (spr.getLotag() < 0) {
                    spr.setLotag(0);
                    tweakdeathdist(index);
                    spr.setCstat(spr.getCstat() & 0xFFFE);
                    if (isahologram(index) != 0) {
                        showmessage("WAS A HOLOGRAM");
                        jsdeletesprite(index);
                    } else {
                        missionaccomplished(index);
                        newstatus(index, FLOATING);
                    }
                } else {
                    spr.setPicnum((short) (spriteXT[ext].deathpic + ((spr.getHitag() - spr.getLotag()) >> 4)));
                }
            }

            i = nexti;
        }

        i = boardService.getStatNode(MIRRORMAN1);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int px = gPlayer[screenpeek].posx;
            int py = gPlayer[screenpeek].posy;
            int pz = gPlayer[screenpeek].posz;

            if ((px < -55326) || (px > -52873) || (py > 40521) || (py < 36596)) {
                i = nexti;
                continue;
            }
            int deltapy = py - 36596;

            if (gPlayer[screenpeek].drawweap != 0) {
                spr.setPicnum(1079);
            } else {
                spr.setPicnum(1074);
            }

            game.pInt.setsprinterpolate(index, spr);
            spr.setX(px);
            spr.setY(36596 - deltapy);
            spr.setZ(pz + (42 << 8));

            spr.setAng((short) BClampAngle(gPlayer[screenpeek].ang + 1024));

            i = nexti;
        }

        i = boardService.getStatNode(MIRRORMAN2);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int px = gPlayer[screenpeek].posx;
            int py = gPlayer[screenpeek].posy;
            int pz = gPlayer[screenpeek].posz;

            if ((px < -34792) || (px > -32404) || (py > 38980) || (py < 35074)) {
                i = nexti;
                continue;
            }
            int deltapy = 38980 - py;

            if (gPlayer[screenpeek].drawweap != 0) {
                spr.setPicnum(1079);
            } else {
                spr.setPicnum(1074);
            }

            game.pInt.setsprinterpolate(index, spr);
            spr.setX(px);
            spr.setY(38980 + deltapy);
            spr.setZ(pz + (42 << 8));
            spr.setAng((short) BClampAngle(gPlayer[screenpeek].ang + 1024));
            i = nexti;
        }

        i = boardService.getStatNode(PROJECTILE);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int dax = (((spr.getXvel()) * TICSPERFRAME) << 11);
            int day = (((spr.getYvel()) * TICSPERFRAME) << 11);
            int daz = (((spr.getZvel()) * TICSPERFRAME) >> 2); // was 3
            int hitobject = movesprite(index, dax, day, daz, 4 << 8, 4 << 8, 1);
            if (hitobject != 0) {
                if ((hitobject & HIT_TYPE_MASK) == HIT_SPRITE) { // hit a sprite
                    int hitsprite = hitobject & HIT_INDEX_MASK;
                    int hit = playerhit(hitsprite);
                    int pnum = playerhit;
                    if (hit != 0) {
                        playerpainsound(pnum);
                        enemywoundplayer(pnum, 6);
                    } else {
                        damagesprite(hitsprite, tekgundamage(6));
                    }
                }
                jsdeletesprite(index);
            }

            i = nexti;
        }

        for (int i1 = 0; i1 < MAXSPRITES; i1++) {
            Sprite spr2 = boardService.getSprite(i1);
            if (spr2 == null) {
                continue;
            }

            int ext = spr2.getExtra();
            if (validext(ext) != 0) {
                if (((spriteXT[ext].fxmask) & FX_NXTSTTDEATH) != 0) {
                    spriteXT[ext].fxmask &= (~FX_NXTSTTDEATH);
                    newstatus(i1, DEATH);
                } else if (((spriteXT[ext].fxmask) & FX_NXTSTTPAIN) != 0) {
                    spriteXT[ext].fxmask &= (~FX_NXTSTTPAIN);
                    newstatus(i1, PAIN);
                }
            }
        }

        gunstatuslistcode();
    }

    public static void blastmark(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null || spr.getStatnum() != GENEXPLODE2) {
            return;
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null) {
            return;
        }

        switch (spr.getPicnum()) {
            case BARRELL:
            case 175:
                break;
            default:
                return;
        }

        int j = jsinsertsprite(spr.getSectnum(), 100);
        Sprite spr2 = boardService.getSprite(j);
        if (spr2 != null) {
            fillsprite(j, spr.getX(), spr.getY(), sec.getFloorz(), 0x00A2, 4, 0, 0, 34, 34, 0, 0, 465, spr.getAng(), 0, 0, 30, i + 4096,0, 0);
        }
    }

    public static void bombexplosion(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = jsinsertsprite(spr.getSectnum(), (short) 5);
        if (j != -1) {
            fillsprite(j, spr.getX(), spr.getY(), spr.getZ(), 0, -16, 0, 0, 34, 34, 0, 0, BOMBEXP1PIC, spr.getAng(), EngineUtils.sin((spr.getAng() + 2560) & 2047) >> 6, EngineUtils.sin((spr.getAng() + 2048) & 2047) >> 6, 30, i + 4096,32, 0);
            playsound(S_RIC2, spr.getX(), spr.getY(), 0, ST_NOUPDATE);
        }
    }

    public static void forceexplosion(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = jsinsertsprite(spr.getSectnum(), (short) 5);
        if (j != -1) {
            fillsprite(j, spr.getX(), spr.getY(), spr.getZ(), 0, -4, 0, 32, 34, 34, 0, 0, FORCEBALLPIC, spr.getAng(), 0, 0, 0, spr.getOwner(), 31, 0);
        }

        for (int k = 0; k < 6; k++) {
            j = jsinsertsprite(spr.getSectnum(), (short) 7);
            if (j != -1) {
                fillsprite(j, spr.getX(), spr.getY(), spr.getZ() + (8 << 10), 2, -4, 0, 32, 24, 24, 0, 0, FORCEBALLPIC, spr.getAng(), (krand_intercept("STAT4497") & 511) - 256, (krand_intercept("STAT4497") & 511) - 256, (krand_intercept("STAT4497") & 16384) - 8192, spr.getOwner(), 96, 0);
            }
        }

        playsound(S_FORCEFIELD2, spr.getX(), spr.getY(), 0, ST_UPDATE);
    }

    public static void enemywoundplayer(int plrhit, int guntype) {
        if (!validplayer(plrhit)) {
            return;
        }

        int damage;
        switch (guntype) {
            case 0:
                damage = 64;
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                damage = 128;
                break;
            case 6:
            default:
                damage = 256;
                break;
        }
        switch (gDifficulty) {
            case 0:
            case 1:
                damage >>= 4;
                break;
            case 2:
                damage >>= 3;
                break;
            case 3:
                damage >>= 2;
                break;
        }

        changehealth(plrhit, -damage);
    }

    public static void androidexplosion(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        int j = jsinsertsprite(spr.getSectnum(), (short) 5);
        if (j != -1) {
            fillsprite(j, spr.getX(), spr.getY(), spr.getZ(), 0, -16, 0, 0, 34, 34, 0, 0, 456, spr.getAng(), EngineUtils.sin((spr.getAng() + 2560) & 2047) >> 6, EngineUtils.sin((spr.getAng() + 2048) & 2047) >> 6, 30, i + 4096,32, 0);
        }
        playsound(S_ANDROID_DIE, spr.getX(), spr.getY(), 0, ST_NOUPDATE);
    }

    public static void tweakdeathdist(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        spr.setClipdist(spr.getClipdist() << 1);
        int dax = (((EngineUtils.sin((spr.getAng() + 512) & 2047)) * spr.getXvel()) << 3);
        int day = (((EngineUtils.sin(spr.getAng() & 2047)) * spr.getYvel()) << 3);
        movesprite((short) i, dax, day, 0, 1024, 1024, CLIFFCLIP);

        if (spr.getExtra() == -1) {
            spr.setLotag(SPR_LOTAG_PICKUP);
        }
    }

    public static int RMOD16(String s) {
        return krand_intercept(s) & 15;
    }

    public static int RMOD10(String s) {
        return krand_intercept(s) & 9;
    }

    public static int RMOD2(String s) {
        return ((krand_intercept(s)) > 0x00008000) ? 1 : 0;
    }

    public static int RMOD3(String s) {
        return ((krand_intercept(s)) > 0x00008000) ? 1 : (((krand_intercept(s)) >> 1) & 1);
    }

    public static int RMOD4(String s) {
        return (((krand_intercept(s)) >> 3) & 0x0000000F) & 3;
    }

    public static void enemynewsector(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        Sector sec = boardService.getSector(spr.getSectnum());
        if (sec == null) {
            return;
        }

        int ext = spr.getExtra();

        if (game.nNetMode == NetMode.Multiplayer) {
            return;
        }

        if (ensfirsttime == 1) {
            ensfirsttime = 0;
            return;
        }

        if (sec.getLotag() == 4) {
            playsound(S_SPLASH, spr.getX(), spr.getY(), 0, ST_UPDATE);
        }

        switch (sec.getHitag()) {
            case 1010:
            case 1011:
            case 1012:
            case 1013:
            case 1014:
            case 1015:
            case 1016:
            case 1017:
            case 1018:
            case 1019:
                if (isahologram(i) != 0) {
                    jsdeletesprite(i);
                    return;
                }
                if (tekexplodebody(i) != 0) {
                    missionaccomplished(i);
                }
                playsound(S_BUSHIT, spr.getX(), spr.getY(), 0, ST_UPDATE);
                if (validext(ext) != 0) {
                    switch (spriteXT[ext].basepic) {
                        case ERWALKPIC:
                            playsound(S_MANDIE1 + RMOD4("STAT2615") + RMOD3("STAT2615"), spr.getX(), spr.getY(), 0, ST_UPDATE);
                            break;
                        case SARAHWALKPIC:
                            playsound(S_GIRLDIE1 + RMOD4("STAT2618") + RMOD3("STAT2618"), spr.getX(), spr.getY(), 0, ST_UPDATE);
                            break;
                        case SAMWALKPIC:
                            playsound(S_MANDIE1 + RMOD4("STAT2621") + RMOD3("STAT2621"), spr.getX(), spr.getY(), 0, ST_UPDATE);
                            break;
                    }
                }
                jsdeletesprite(i);
                break;
            default:
                operatesector(spr.getSectnum()); // JEFF TEST
                break;
        }
    }

    public static void attackifclose(int i, int snum, int dist) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return;
        }

        if (!validplayer(snum)) {
            throw new AssertException("atakifclse: bad plrnum");
        }

        if (gPlayer[snum].health <= 0) {
            return;
        }

        int ext = spr.getExtra();
        if (validext(ext) == 0) {
            return;
        }

        int mindist;
        switch (spr.getStatnum()) {
            case CHASE:
                mindist = CHASEATTDIST;
                break;
            case PATROL:
            case GUARD:
                mindist = GUARDATTDIST;
                break;
            case STALK:
                mindist = STALKATTDIST;
                break;
            default:
                mindist = MINATTACKDIST;
                break;
        }

        switch (spr.getPicnum()) {
            case RATPIC:
                mindist = 1024;
                break;
            case BATPIC:
                if (RMOD4("STAT1968") != 0) {
                    return;
                }
                mindist = 1024;
                if (dist <= 1024) {
                    game.pInt.setsprinterpolate(i, spr);
                    spr.setZ(gPlayer[snum].posz - 4096);
                }

                break;
            default:
                break;
        }

        if ((dist < mindist)) {
            spr.setAng(EngineUtils.getAngle(gPlayer[snum].posx - spr.getX(), gPlayer[snum].posy - spr.getY()));
            newstatus(i, ATTACK);
        }

    }

    public static void hideplea(int sn, int ext) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }
        if ((spriteXT[ext].aimask & AI_DIDHIDEPLEA) != 0 && (krand_intercept("STAT1383") & 1) != 0) {
            return;
        }

        switch (spriteXT[ext].basepic) {
            case ERWALKPIC:
                playsound(S_MALE_DONTHURT + (krand_intercept("STAT1390") & 5), spr.getX(), spr.getY(), 0, ST_UNIQUE);
                if ((krand_intercept("STAT1391") & 1) != 0) {
                    spriteXT[ext].aimask |= AI_DIDHIDEPLEA;
                }
                break;
            case SARAHWALKPIC:
            case SAMWALKPIC:
                playsound(S_DIANE_DONTSHOOTP + (krand_intercept("STAT1397") & 5), spr.getX(), spr.getY(), 0, ST_UNIQUE);
                if ((krand_intercept("STAT1398") & 1) != 0) {
                    spriteXT[ext].aimask |= AI_DIDHIDEPLEA;
                }
                break;
        }
    }

    public static void fleescream(int sn, int ext) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }

        if (validext(ext) == 0) {
            return;
        }

        if ((spriteXT[ext].aimask & AI_DIDFLEESCREAM) != 0 && (krand_intercept("STAT1417") & 1) != 0) {
            return;
        }

        switch (spriteXT[ext].basepic) {
            case ERWALKPIC:
                playsound(S_MALE_OHMYGOD + (krand_intercept("STAT1424") & 5), spr.getX(), spr.getY(), 0, ST_UNIQUE);
                if ((krand_intercept("STAT1425") & 1) != 0) {
                    spriteXT[ext].aimask |= AI_DIDFLEESCREAM;
                }
                break;
            case SARAHWALKPIC:
            case SAMWALKPIC:
                switch (RMOD4("STAT1478")) {
                    case 0:
                    case 2:
                        playsound(S_FEM_RUNHEGOTGUN + (krand_intercept("STAT1434") & 11), spr.getX(), spr.getY(), 0, ST_UNIQUE);
                        break;
                    default:
                        playsound(S_SCREAM1 + RMOD3("STAT1484"), spr.getX(), spr.getY(), 0, ST_UNIQUE);
                        break;
                }
                if ((krand_intercept("STAT1440") & 1) != 0) {
                    spriteXT[ext].aimask |= AI_DIDFLEESCREAM;
                }
                break;
        }
    }

    public static int spritedeflect(int sn) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return 0;
        }

        int angout;
        switch (spr.getStatnum()) {
            case INANIMATE:
            case INACTIVE:
            case AMBUSH:
                switch (RMOD2("STAT697 ")) {
                    case 0:
                        angout = leftof[RMOD16("STAT")];
                        break;
                    case 1:
                    default:
                        angout = rightof[RMOD16("STAT")];
                        break;
                }
                break;
            default:
                angout = spr.getAng();
                break;
        }

        return (angout);
    }

    public static short arbitraryangle() {
        switch (RMOD4("STAT642 ")) {
            case 0:
            case 1:
                return (leftof[RMOD16("STAT645 ")]);
            default:
                return (rightof[RMOD16("STAT648 ")]);
        }
    }

    public static int walldeflect(int wn, int angin) {
        Wall wal = boardService.getWall(wn);
        if (wal == null) {
            return 0;
        }

        int wnorm = wallnormal(wal);
        int refract = ((angin + 1024) & 2047);
        int delta = (wnorm - refract);
        int angout = (wnorm + delta);

        if (angout < 0) {
            angout += 2048;
        }

        return (angout);
    }

    public static boolean isvisible(int i, int target) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return false;
        }

        if (!validplayer(target)) {
            throw new AssertException("isvisible: bad targetnum");
        }

        if (gPlayer[target].cursectnum >= 0 && EngineUtils.sin((spr.getAng() + 2560) & 2047) * (gPlayer[target].posx - spr.getX()) + EngineUtils.sin((spr.getAng() + 2048) & 2047) * (gPlayer[target].posy - spr.getY()) >= 0) {
            return engine.cansee(gPlayer[target].posx, gPlayer[target].posy, (gPlayer[target].posz) >> 1, gPlayer[target].cursectnum, spr.getX(), spr.getY(), spr.getZ() - (engine.getTile(spr.getPicnum()).getHeight() << 7), spr.getSectnum());
        }
        return false;
    }

    public static void enemygunshot(int sn) {
        final Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }

        ListNode<Sprite> j = boardService.getStatNode(STROLL), nextj;
        while (j != null) {
            nextj = j.getNext();
            Sprite spr2 = j.get();

            int ext = spr2.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(sn, 100);
            }

            int dist = abs(spr.getX() - spr2.getX()) + abs(spr.getY() - spr2.getY());
            if (dist < HEARGUNSHOTDIST) {
                spriteXT[ext].aimask |= AI_JUSTSHOTAT;
            }

            j = nextj;
        }

        j = boardService.getStatNode(STANDING);
        while (j != null) {
            nextj = j.getNext();
            Sprite spr2 = j.get();

            int ext = spr2.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(sn, 101);
            }

            // guards not woken by enemies gun shot
            if (spriteXT[ext].basestat != GUARD) {
                int dist = abs(spr.getX() - spr2.getX()) + abs(spr.getY() - spr2.getY());
                if (dist < HEARGUNSHOTDIST) {
                    spriteXT[ext].aimask |= AI_JUSTSHOTAT;
                }
            }

            j = nextj;
        }

        j = boardService.getStatNode(GUARD);
        while (j != null) {
            nextj = j.getNext();

            if (j.getIndex() == sn) {
                j = nextj;
                continue;
            }
            Sprite spr2 = j.get();

            int ext = spr2.getExtra();
            if (validext(ext) == 0) {
                noextthrowerror(sn, 102);
            }

            int dist = abs(spr.getX() - spr2.getX()) + abs(spr.getY() - spr2.getY());
            if (dist < HEARGUNSHOTDIST) {
                spriteXT[ext].aimask |= AI_JUSTSHOTAT;
            }

            j = nextj;
        }
    }

    public static void enemyshootgun(int sprnum, int x, int y, int z, short daang, int dahoriz, short dasectnum, char guntype) {
        Sprite spr = boardService.getSprite(sprnum);
        if (spr == null) {
            return;
        }

        int ext = spr.getExtra();
        if (validext(ext) == 0) {
            throw new AssertException("enemyshootgun: bad ext");
        }

        int target = spriteXT[sprnum].target;
        if (!validplayer(target)) {
            return;
        }

        // enemy gun fire sounds
        switch (guntype) {
            case 0:
                switch (spriteXT[ext].attackpic) {
                    case RATATTACKPIC:
                        break;
                    case 3910:
                        playsound(S_WH_8, x, y, 0, ST_NOUPDATE);
                        break;
                    default:
                        playsound(S_MATRIX_ATTACK2, x, y, 0, ST_NOUPDATE);
                        break;
                }
                break;
            case 1:
            case 2:
            case 3:
            case 4:
                switch (spriteXT[ext].attackpic) {
                    case 3773:
                    case 3800:
                    case 3805:
                    case 3810:
                    case 3818:
                        playsound(S_MATRIX_ATTACK, x, y, 0, ST_NOUPDATE);
                        break;
                    case 3850:
                    case 3860:
                    case 3890:
                    case 3909:
                    case 3952:
                    case 4001:
                        playsound(S_MATRIX_ATTACK2, x, y, 0, ST_NOUPDATE);
                        break;
                    case ANTATTACKPIC:
                        playsound(S_ENEMYGUN2, x, y, 0, ST_NOUPDATE);
                        break;
                    case MAATTACKPIC:
                        playsound(S_ENEMYGUN3, x, y, 0, ST_NOUPDATE);
                        break;
                    case SAMATTACKPIC:
                        playsound(S_ENEMYGUN4, x, y, 0, ST_NOUPDATE);
                        break;
                    default:
                        playsound(S_ENEMYGUN1, x, y, 0, ST_NOUPDATE);
                        break;
                }
                break;
            case 5:
            case 6:
            case 7:
            case 8:
                switch (spriteXT[ext].attackpic) {
                    case 3773:
                    case 3800:
                    case 3805:
                    case 3810:
                    case 3818:
                        playsound(S_MATRIX_ATTACK, x, y, 0, ST_NOUPDATE);
                        break;
                    case 3850:
                    case 3860:
                    case 3890:
                    case 3909:
                    case 3952:
                    case 4001:
                        playsound(S_MATRIX_ATTACK2, x, y, 0, ST_NOUPDATE);
                        break;
                    default:
                        playsound(S_AUTOGUN, x, y, 0, ST_NOUPDATE);
                }
                break;
            default:
                break;
        }

        // gun shot code
        switch (guntype) {
            case 0:
                if ((abs(x - gPlayer[target].posx) + abs(y - gPlayer[target].posy)) < 1024) {
                    if (abs(z - gPlayer[target].posz) < 12288) {
                        playerpainsound(target);
                        if (((spriteXT[ext].fxmask) & FX_HOLOGRAM) != 0) {
                            playsound(S_MATRIX_ATTACK, x, y, 0, ST_NOUPDATE);
                            enemywoundplayer(target, 0);
                        }
                        if (((spriteXT[ext].fxmask) & FX_ANDROID) != 0) {
                            androidexplosion(sprnum);
                            enemywoundplayer(target, 6);
                            // engine.changespritestat(sprnum, VANISH);
                            jsdeletesprite(sprnum);
                        } else {
                            enemywoundplayer(target, 0);
                        }
                    }
                }
                return;
            case GUN3FLAG:
            case GUN4FLAG:
            case GUN5FLAG:
                int daz2 = ((100 - dahoriz) << 11);
                z = gPlayer[target].posz; // instead of calculating a dot product angle
                engine.hitscan(x, y, z, dasectnum, EngineUtils.sin(((int) daang + 2560) & 2047), EngineUtils.sin(((int) daang + 2048) & 2047), daz2, pHitInfo, CLIPMASK1);
                int hitsect = pHitInfo.hitsect;
                int hitsprite = pHitInfo.hitsprite;
                int hitx = pHitInfo.hitx;
                int hity = pHitInfo.hity;
                int hitz = pHitInfo.hitz;

                if (hitsprite != -1) {
                    int hit = playerhit(hitsprite);
                    int pnum = playerhit;
                    if (hit != 0) {
                        playerpainsound(pnum);
                        enemywoundplayer(pnum, 3);
                    } else {
                        damagesprite(hitsprite, tekgundamage(guntype));
                    }
                } else {
                    int j = jsinsertsprite(hitsect, (short) 3);
                    if (j != -1) {
                        fillsprite(j, hitx, hity, hitz + (8 << 8), 2, -4, 0, 32, 16, 16, 0, 0, EXPLOSION, daang, 0, 0, 0, sprnum + MAXSPRITES, 63, 0);
                    }
                }
                break;
            case GUN6FLAG: {
                int j = jsinsertsprite(spr.getSectnum(), PROJECTILE);
                Sprite spr2 = boardService.getSprite(j);
                if (spr2 == null) {
                    break;
                }
                spr2.setX(spr.getX());
                spr2.setY(spr.getY());
                if (spriteXT[ext].basestat == FLY) {
                    spr2.setZ(spr.getZ());
                } else {
                    Sector sec = boardService.getSector(spr.getSectnum());
                    if (sec != null) {
                        spr2.setZ(sec.getFloorz() - 8192);
                    }
                }

                spr2.setCstat(0x01);
                spr2.setPicnum(PULSARPIC);
                spr2.setShade(-32);
                spr2.setXrepeat(32);
                spr2.setYrepeat(32);
                spr2.setAng(spr.getAng());
                spr2.setXvel((short) (EngineUtils.sin((spr2.getAng() + 2560) & 2047) >> 4));
                spr2.setYvel((short) (EngineUtils.sin((spr2.getAng() + 2048) & 2047) >> 4));
                int discrim = EngineUtils.sqrt((gPlayer[target].posx - spr2.getX()) * (gPlayer[target].posx - spr2.getX()) + (gPlayer[target].posy - spr2.getY()) * (gPlayer[target].posy - spr2.getY()));
                if (discrim == 0) {
                    discrim = 1;
                }
                spr2.setZvel((short) (((gPlayer[target].posz + (8 << 8) - spr2.getZ()) << 9) / discrim));
                spr2.setOwner(sprnum);
                spr2.setClipdist(16);
                spr2.setLotag(0);
                spr2.setHitag(0);
                break;
            }
            default:
                break;
        }

        enemygunshot(sprnum);
    }

    public static class sectflashtype {
        short sectnum;
        int step;
        char ovis;
    }
}


