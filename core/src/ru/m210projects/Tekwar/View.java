package ru.m210projects.Tekwar;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Types.DefaultScreenFade;
import ru.m210projects.Build.Render.Types.ScreenFade;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Tekwar.Menus.MenuInterfaceSet;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Math.abs;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Strhandler.Bitoa;
import static ru.m210projects.Build.Strhandler.buildString;
import static ru.m210projects.Tekwar.Config.*;
import static ru.m210projects.Tekwar.Globals.*;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekchng.stun;
import static ru.m210projects.Tekwar.Tekgun.tekdrawgun;
import static ru.m210projects.Tekwar.Tekmap.symbols;
import static ru.m210projects.Tekwar.Tekmap.symbolsdeposited;
import static ru.m210projects.Tekwar.Teksnd.updatesounds;
import static ru.m210projects.Tekwar.Tekspr.analyzesprites;
import static ru.m210projects.Tekwar.Tektag.headbob;

public class View {

    public static final String PICKUP_SCREEN_NAME = "PICKUP";
    public static final String DAMAGE_SCREEN_NAME = "DAMAGE";

    public final static ScreenFade PICKUP_DAC = new DefaultScreenFade(PICKUP_SCREEN_NAME);
    public final static ScreenFade DAMAGE_DAC = new DefaultScreenFade(DAMAGE_SCREEN_NAME);

    public final static ScreenFade[] SCREEN_DAC_ARRAY = {
            PICKUP_DAC, DAMAGE_DAC
    };

    public static final int kView2D = 2;
    public static final int kView3D = 3;
    public static final int kView2DIcon = 4;
    public static final int HCSCALE = 100;
    public static final int AMMOSCALE = 10;
    public static final int NUMWHITESHIFTS = 3;
    public static final int WHITESTEPS = 20;
    public static final int WHITETICS = 6;
    public static final int NUMREDSHIFTS = 4;
    public static final int REDSTEPS = 8;
    public static final int TEKTEMPBUFSIZE = 256;
    public static final int MSGBUFSIZE = 40;
    public static final AtomicInteger fz = new AtomicInteger();
    public static final AtomicInteger cz = new AtomicInteger();
    public static byte gViewMode;
    public static int zoom, ozoom;
    public static char rearviewdraw;
    public static int hcmoving, rvmoving, wpmoving;
    public static short hcpos, wppos, rvpos;
    public static final boolean[] otoggles = new boolean[MAXTOGGLES];
    public static int timedinv;
    public static final char demowon = 0;
    public static int messagex;
    public static int messageon = 0;
    public static int redcount, whitecount;
    public static char rvonemotime;
    public static char wponemotime;
    public static char hconemotime;
    public static boolean palshifted;
    public static final byte[][] whiteshifts = new byte[NUMREDSHIFTS][768];
    public static final byte[][] redshifts = new byte[NUMREDSHIFTS][768];
    public static final char[] messagebuf = new char[MSGBUFSIZE];
    public static short hcpic, rvpic, wppic;
    private static final char[] tektempbuf = new char[TEKTEMPBUFSIZE];

    public static void overwritesprite(int thex, int they, int scale, int tilenum, int shade, int stat, int dapalnum) {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(thex << 16, they << 16, scale, (stat & 8) << 7, tilenum, shade, dapalnum,
                (((stat & 1) ^ 1) << 4) + (stat & 2) // 16 + 2
                        + ((stat & 4) >> 2) // 1
                        + (((stat & 16) >> 2) ^ ((stat & 8) >> 1)) + ((stat & 32) >> 2) + (stat & 512) + (stat & 256));
    }

    public static void printext(int x, int y, char[] buffer) {
        game.getFont(2).drawText(game.getRenderer(), x, y, buffer, (float)tekcfg.gHUDSize / 32768.0f, 0, 0, TextAlign.Left, Transparent.None, false);
    }

    public static void FadeInit() {
        Console.out.println("Initializing fade effects");
        initpaletteshifts();
    }

    public static void resetEffects() {
        redcount = 0;
        whitecount = 0;
        PICKUP_DAC.setIntensive(whitecount | 128);
        DAMAGE_DAC.setIntensive(redcount | 128);
    }

    public static void showmessage(String fmt) {
        if (!tekcfg.showMessages) {
            return;
        }

        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        Arrays.fill(messagebuf, '\0');

        System.arraycopy(fmt.toCharArray(), 0, messagebuf, 0, Math.min(fmt.length(), MSGBUFSIZE));
        messagex = (xdim >> 1) - (mulscale((fmt.length() >> 1) * 8L, tekcfg.gHUDSize, 16));
        if (messagex < 0) {
            messagex = 0;
        }

        messageon = 1;
    }

    public static void showtime() {
        if (tekcfg.toggles[TOGGLE_TIME]) {
            Renderer renderer = game.getRenderer();
            int xdim = renderer.getWidth();
            int ydim = renderer.getHeight();

            Bitoa(hours, tektempbuf, 2);
            int offs = buildString(tektempbuf, 2, ":", minutes, 2);
            buildString(tektempbuf, offs, ":", seconds, 2);

            printext(xdim - mulscale(72, tekcfg.gHUDSize, 16), ydim - mulscale(12, tekcfg.gHUDSize, 16), tektempbuf
            );
        }
    }

    public static void showscore() {

        if (tekcfg.toggles[TOGGLE_SCORE]) {
            if (gPlayer[screenpeek].score == 1) {
                buildString(tektempbuf, 0, "0");
            } else {
                Bitoa(gPlayer[screenpeek].score, tektempbuf);
            }
            Renderer renderer = game.getRenderer();
            int xdim = renderer.getWidth();
            int ydim = renderer.getHeight();

            printext(xdim - mulscale(160, tekcfg.gHUDSize, 16), ydim - mulscale(12, tekcfg.gHUDSize, 16), tektempbuf
            );
        }
    }

    public static void rearview(int snum, int smooth) {
        if (!tekcfg.toggles[TOGGLE_REARVIEW]) {
            return;
        }
        Renderer renderer = game.getRenderer();
        int windowx1 = 0;
        int windowy1 = 0;
        int windowx2 = renderer.getWidth();
        int windowy2 = renderer.getHeight();

        float plrang = gPlayer[snum].ang;

        float plrhoriz = gPlayer[snum].horiz;
        renderer.setview(mulscale(66, tekcfg.gHUDSize, 16), mulscale(7, tekcfg.gHUDSize, 16),
                mulscale(131, tekcfg.gHUDSize, 16), mulscale(42, tekcfg.gHUDSize, 16));

        float cang = BClampAngle(plrang + 1024);

        int cposx = gPlayer[snum].posx, cposy = gPlayer[snum].posy, cposz = gPlayer[snum].posz;
        float choriz = 200 - plrhoriz;

        renderer.drawrooms(cposx, cposy, cposz, cang, choriz, gPlayer[snum].cursectnum);
        rearviewdraw = 1;
        analyzesprites(gPlayer[snum].posx, gPlayer[snum].posy, smooth);
        rearviewdraw = 0;
        renderer.drawmasks();

        renderer.setview(windowx1, windowy1, windowx2, windowy2);
    }

    public static void dorearviewscreen() {
        if (otoggles[TOGGLE_REARVIEW]) {
            if (rvpos < 0) {
                rvpos += (TICSPERFRAME << 2);
                if (rvpos > 0) {
                    rvpos = 0;
                }
            }
        } else if (abs(rvpos) < engine.getTile(RVDEVICE).getWidth()) {
            rvpos -= (TICSPERFRAME << 2);
            if (abs(rvpos) > engine.getTile(RVDEVICE).getWidth()) {
                rvpos = (short) -engine.getTile(RVDEVICE).getWidth();
            }
        }
    }

    public static void douprtscreen() {
        if (otoggles[TOGGLE_UPRT]) {
            if (wppos < 0) {
                wppos += (TICSPERFRAME << 2);
                if (wppos > 0) {
                    wppos = 0;
                }
            }
        } else if (abs(wppos) < engine.getTile(WPDEVICE).getWidth()) {
            wppos -= (TICSPERFRAME << 2);
            if (abs(wppos) > engine.getTile(WPDEVICE).getWidth()) {
                wppos = (short) -engine.getTile(WPDEVICE).getWidth();
            }
        }
    }

    public static void dohealthscreen() {
        if (otoggles[TOGGLE_HEALTH]) {
            if (hcpos < 0) {
                hcpos += (TICSPERFRAME << 2);
                if (hcpos > 0) {
                    hcpos = 0;
                }
            }
        } else if (abs(hcpos) < engine.getTile(HCDEVICE).getWidth()) {
            hcpos -= (TICSPERFRAME << 2);
            if (abs(hcpos) > engine.getTile(HCDEVICE).getWidth()) {
                hcpos = (short) -engine.getTile(HCDEVICE).getWidth();
            }
        }
    }

    public static void tekmapmode(int mode) {
        switch (mode) {
            case kView2D:
                if (tekcfg.gOverlayMap == 1) {
                    gViewMode = kView3D;
                } else {
                    gViewMode = kView2DIcon;
                }
                break;
            case kView3D:
                if (tekcfg.gOverlayMap != 0) {
                    gViewMode = kView2D;
                } else {
                    gViewMode = kView2DIcon;
                }
                break;
            default:
                gViewMode = kView3D;
                break;
        }
    }

    public static void tekscreenfx(int smooth) {
        Renderer renderer = game.getRenderer();
        int ammo, n;
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        updatesounds(screenpeek);

        // #define COMMITTEE
        // printext((xdim>>1)-25,windowy1+24,"THURS 6PM \0".toCharArray(),ALPHABET,255);

        if (TEKDEMO) {
            printext((xdim >> 1) - mulscale(16, tekcfg.gHUDSize, 16),  mulscale(24, tekcfg.gHUDSize, 16),
                    "DEMO".toCharArray());
        }

        if (mUserFlag != UserFlag.None) {
            printext((xdim >> 1) - mulscale(16, tekcfg.gHUDSize, 16), (ydim - mulscale(24, tekcfg.gHUDSize, 16)),
                    "USERMAP".toCharArray());
        }

        if ((messageon == 0) && notininventory != 0) {
            showmessage("NOT IN INVENTORY");
            notininventory = 0;
        }

        ArtEntry pic = engine.getTile(RVDEVICE);
        if (otoggles[TOGGLE_REARVIEW]) {
            if (rvpos == 0) {
                rvpic = RVDEVICEON;
            }
            if (rvpos < 0) {
                n = pic.getWidth() / (RVDEVICEON - RVDEVICE);
                n = (pic.getWidth() - abs(rvpos)) / n;
                rvpic = (short) (RVDEVICE + n);
            }
            renderer.rotatesprite(mulscale(rvpos, tekcfg.gHUDSize, 16) << 16, 0, tekcfg.gHUDSize, 0, rvpic, 0, 0,
                    8 + 16);
            if (rvpos == 0) {
                rearview(screenpeek, smooth);
            }
        } else if (abs(rvpos) < pic.getWidth()) {
            if (abs(rvpos) > pic.getWidth()) {
                rvpic = RVDEVICE;
            } else {
                n = pic.getWidth() / (RVDEVICEON - RVDEVICE);
                n = (pic.getWidth() - abs(rvpos)) / n;
                rvpic = (short) (RVDEVICE + n);
            }
            renderer.rotatesprite(mulscale(rvpos, tekcfg.gHUDSize, 16) << 16, 0, tekcfg.gHUDSize, 0, rvpic, 0, 0,
                    8 + 16);
        }

        pic = engine.getTile(WPDEVICE);
        if (otoggles[TOGGLE_UPRT]) {
            if (wppos == 0) {
                wppic = WPDEVICEON;
            }

            if (wppos < 0) {
                n = pic.getWidth() / (WPDEVICEON - WPDEVICE);
                n = (pic.getWidth() - abs(wppos)) / n;
                wppic = (short) (WPDEVICE + n);
            }

            renderer.rotatesprite((xdim - 1 - mulscale((pic.getWidth() + wppos), tekcfg.gHUDSize, 16)) << 16, 0,
                    tekcfg.gHUDSize, 0, WPDEVICE, 0, 0, 8 + 16);
        } else if (abs(wppos) < pic.getWidth()) {
            if (abs(wppos) > pic.getWidth()) {
                wppic = WPDEVICE;
            } else {
                n = pic.getWidth() / (WPDEVICEON - WPDEVICE);
                n = (pic.getWidth() - abs(wppos)) / n;
                wppic = (short) (WPDEVICE + n);
            }

            renderer.rotatesprite((xdim - 1 - mulscale((pic.getWidth() + wppos), tekcfg.gHUDSize, 16)) << 16, 0,
                    tekcfg.gHUDSize, 0, WPDEVICE, 0, 0, 8 + 16);
        }

        if ((wppic == WPDEVICEON) && (!game.menu.gShowMenu || game.menu.getCurrentMenu() instanceof MenuInterfaceSet)) {
            Bitoa(hours, tektempbuf, 2);
            int offs = buildString(tektempbuf, 2, ":", minutes, 2);
            buildString(tektempbuf, offs, ":", seconds, 2);

            int x = xdim - mulscale(74, tekcfg.gHUDSize, 16);
            int y = mulscale(8, tekcfg.gHUDSize, 16);
            game.getFont(1).drawText(game.getRenderer(), x, y, tektempbuf, (float)tekcfg.gHUDSize / 32768.0f, 0, 0, TextAlign.Left, Transparent.None, false);

            Bitoa(gPlayer[screenpeek].score, tektempbuf, 8);

            x = xdim - mulscale(74, tekcfg.gHUDSize, 16);
            y = mulscale(18, tekcfg.gHUDSize, 16);
            game.getFont(1).drawText(game.getRenderer(), x, y, tektempbuf, (float)tekcfg.gHUDSize / 32768.0f, 0, 0, TextAlign.Left, Transparent.None, false);
        }

        if ((!game.menu.gShowMenu || game.menu.getCurrentMenu() instanceof MenuInterfaceSet)) {
            showtime();
            showscore();
            showinv(screenpeek);
        }

        if (gPlayer[screenpeek].godMode) {
            printext((xdim >> 1) - mulscale(16, tekcfg.gHUDSize, 16), 4, "HOLY \0".toCharArray());
        }

        if (gPlayer[myconnectindex].health <= -160) {
            game.getFont(0).drawTextScaled(renderer, 160, 80, "Press \"USE\" to restart", 1.0f, 0, 4, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
        }

        pic = engine.getTile(HCDEVICE);
        if (otoggles[TOGGLE_HEALTH]) {
            if (hcpos == 0) {
                hcpic = HCDEVICEON;
            }

            if (hcpos < 0) {
                n = pic.getWidth() / (HCDEVICEON - HCDEVICE);
                n = (pic.getWidth() - abs(hcpos)) / n;
                hcpic = (short) (HCDEVICE + n);
            }

            renderer.rotatesprite(mulscale(hcpos, tekcfg.gHUDSize, 16) << 16,
                    (ydim - mulscale(engine.getTile(hcpic).getHeight(), tekcfg.gHUDSize, 16)) << 16, tekcfg.gHUDSize, 0,
                    hcpic, 0, 0, 8 + 16);

            if (hcpic == HCDEVICEON) {
                for (n = 0; n < gPlayer[screenpeek].health / HCSCALE; n++) {
                    renderer.rotatesprite(mulscale(hcpos + 34 + (n * 5L), tekcfg.gHUDSize, 16) << 16,
                            (ydim - mulscale(engine.getTile(hcpic).getHeight() - 7, tekcfg.gHUDSize, 16)) << 16,
                            tekcfg.gHUDSize, 0, GREENLIGHTPIC, (gPlayer[screenpeek].health / HCSCALE) - n, 0, 8 + 16, 0,
                            0, xdim - 1, ydim - 1);
                }
                for (n = 0; n < stun[screenpeek] / HCSCALE; n++) {
                    renderer.rotatesprite(mulscale(hcpos + 34 + (n * 5L), tekcfg.gHUDSize, 16) << 16,
                            (ydim - mulscale(engine.getTile(hcpic).getHeight() - 13, tekcfg.gHUDSize, 16)) << 16,
                            tekcfg.gHUDSize, 0, YELLOWLIGHTPIC, (stun[screenpeek] / HCSCALE) - n, 0, 8 + 16, 0, 0,
                            xdim - 1, ydim - 1);
                }

                if (gPlayer[screenpeek].lastgun < 8) {
                    ammo = gPlayer[screenpeek].ammo[gPlayer[screenpeek].lastgun];
                } else {
                    ammo = MAXAMMO;
                }

                for (n = 0; n < ammo / AMMOSCALE; n++) {
                    renderer.rotatesprite(mulscale(hcpos + 34 + (n * 5L), tekcfg.gHUDSize, 16) << 16,
                            (ydim - mulscale(engine.getTile(hcpic).getHeight() - 19, tekcfg.gHUDSize, 16)) << 16,
                            tekcfg.gHUDSize, 0, BLUELIGHTPIC, (ammo / AMMOSCALE) - n, 0, 8 + 16, 0, 0, xdim - 1,
                            ydim - 1);
                }
            }
        } else if (abs(hcpos) < pic.getWidth()) {
            if (abs(hcpos) > pic.getWidth()) {
                hcpic = HCDEVICE;
            } else {
                n = pic.getWidth() / (HCDEVICEON - HCDEVICE);
                n = (pic.getWidth() - abs(hcpos)) / n;
                hcpic = (short) (HCDEVICE + n);
            }
            renderer.rotatesprite(mulscale(hcpos, tekcfg.gHUDSize, 16) << 16,
                    (ydim - mulscale(engine.getTile(hcpic).getHeight(), tekcfg.gHUDSize, 16)) << 16, tekcfg.gHUDSize, 0,
                    hcpic, 0, 0, 8 + 16);
        }

        if ((!game.menu.gShowMenu) && messageon != 0) {
            printext(messagex, ydim - mulscale(32, tekcfg.gHUDSize, 16), messagebuf);
        }

        if ((!game.menu.gShowMenu || game.menu.getCurrentMenu() instanceof MenuInterfaceSet)
                && !tekcfg.toggles[TOGGLE_HEALTH] && (hcpos == -pic.getWidth())) {
            if (gPlayer[screenpeek].health < 0) {
                buildString(tektempbuf, 0, "0");
            } else if (gPlayer[screenpeek].health > MAXHEALTH) {
                buildString(tektempbuf, 0, "1000");
            } else {
                Bitoa(gPlayer[screenpeek].health, tektempbuf);
            }
            printext(mulscale(6, tekcfg.gHUDSize, 16), ydim - mulscale(10, tekcfg.gHUDSize, 16),
                    tektempbuf);
        }

        if (game.gPaused) {
            game.getFont(0).drawTextScaled(renderer, 160, 5, "Pause", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
        }

    }

    public static void drawscreen(short snum, int dasmoothratio) {
        int cposx, cposy, cposz, czoom;
        int csect;
        Renderer renderer = game.getRenderer();

        cposx = gPlayer[snum].posx;
        cposy = gPlayer[snum].posy;
        cposz = gPlayer[snum].posz;
        float choriz = gPlayer[snum].horiz;
        czoom = zoom;
        float cang = gPlayer[snum].ang;
        csect = gPlayer[snum].cursectnum;
        if (!game.menu.gShowMenu && !Console.out.isShowing()) {

            int ix = gPlayer[snum].oposx;
            int iy = gPlayer[snum].oposy;
            int iz = gPlayer[snum].oposz;
            float iHoriz = gPlayer[snum].ohoriz;
            float inAngle = gPlayer[snum].oang;
            int izoom = ozoom;

            ix += mulscale(cposx - gPlayer[snum].oposx, dasmoothratio, 16);
            iy += mulscale(cposy - gPlayer[snum].oposy, dasmoothratio, 16);
            iz += mulscale(cposz - gPlayer[snum].oposz, dasmoothratio, 16);
            iHoriz += ((choriz - gPlayer[snum].ohoriz) * dasmoothratio) / 65536.0f;
            inAngle += ((BClampAngle(cang - gPlayer[snum].oang + 1024) - 1024) * dasmoothratio) / 65536.0f;
            izoom += mulscale(czoom - ozoom, dasmoothratio, 16);

            cposx = ix;
            cposy = iy;
            cposz = iz;
            czoom = izoom;

            choriz = iHoriz;
            cang = inAngle;
        }

        cposz += headbob;

        engine.getzsofslope(csect, cposx, cposy, fz, cz);
        int lz = 4 << 8;
        if (cposz < cz.get() + lz) {
            cposz = cz.get() + lz;
        }
        if (cposz > fz.get() - lz) {
            cposz = fz.get() - lz;
        }

        if (gViewMode != kView2DIcon) {
            redrawbackfx();
            renderer.drawrooms(cposx, cposy, cposz, cang, choriz, csect);
            analyzesprites(gPlayer[snum].posx, gPlayer[snum].posy, dasmoothratio);
            renderer.drawmasks();
            tekdrawgun(snum);
        }

        if (gViewMode != kView3D) {
            if (gViewMode == kView2DIcon) {
                renderer.clearview(0); // Clear screen to specified color
                renderer.drawmapview(cposx, cposy, czoom, (int) cang);
            }
            renderer.drawoverheadmap(cposx, cposy, czoom, (short) cang);
        }
    }

    public static void redrawbackfx() {
        System.arraycopy(tekcfg.toggles, 0, otoggles, 0, MAXTOGGLES);

        if ((rvmoving != 0 || rvonemotime != 0)) {
            // overwritesprite(0,0,tekcfg.gHUDSize, RVDEVRES,0,0,0);
            if ((tekcfg.toggles[TOGGLE_REARVIEW] && rvpos == 0)
                    || (!tekcfg.toggles[TOGGLE_REARVIEW] && rvpos == -engine.getTile(RVDEVICE).getWidth())) {
                rvmoving = 0;
                rvonemotime++;
                if (rvonemotime == 4) {
                    rvonemotime = 0;
                }

            }
        }
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();

        if ((wpmoving != 0 || wponemotime != 0)) {
            // overwritesprite(xdim-tilesizx[WPDEVRES],0,tekcfg.gHUDSize,WPDEVRES,0,0,0);
            if ((tekcfg.toggles[TOGGLE_UPRT] && wppos == xdim - engine.getTile(WPDEVICE).getWidth())
                    || (!tekcfg.toggles[TOGGLE_UPRT] && wppos == xdim)) {
                wpmoving = 0;
                wponemotime++;
                if (wponemotime == 4) {
                    wponemotime = 0;
                }
            }
        }
        if ((hcmoving != 0 || hconemotime != 0)) {
            // overwritesprite(0,ydim-tilesizy[HCDEVRES],tekcfg.gHUDSize,HCDEVRES,0,0,0);
            if ((tekcfg.toggles[TOGGLE_HEALTH] && hcpos == 0)
                    || (!tekcfg.toggles[TOGGLE_HEALTH] && hcpos == xdim - engine.getTile(HCDEVICE).getWidth())) {
                hcmoving = 0;
                hconemotime++;
                if (hconemotime == 4) {
                    hconemotime = 0;
                }
            }
        }
    }

    public static void startwhiteflash(int bonus) {
        whitecount = BClipHigh(whitecount + (bonus << 3), 100);

    }

    public static void startredflash(int damage) {
        redcount = BClipHigh(redcount + (damage << 4), 100);

    }

    public static void initpaletteshifts() {
        int delta, workptr, baseptr;

        byte[] palette = engine.getPaletteManager().getBasePalette();
        for (int i = 1; i <= NUMREDSHIFTS; i++) {
            workptr = 0;
            baseptr = 0;
            for (int j = 0; j <= 255; j++) {
                delta = 64 - (palette[baseptr] & 0xFF);
                redshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / REDSTEPS) << 2);
                delta = -(palette[baseptr] & 0xFF);
                redshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / REDSTEPS) << 2);
                delta = -(palette[baseptr] & 0xFF);
                redshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / REDSTEPS) << 2);
            }
        }

        for (int i = 1; i <= NUMWHITESHIFTS; i++) {
            workptr = 0;
            baseptr = 0;
            for (int j = 0; j <= 255; j++) {
                delta = 64 - (palette[baseptr] & 0xFF);
                whiteshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / WHITESTEPS) << 2);
                delta = 62 - (palette[baseptr] & 0xFF);
                whiteshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / WHITESTEPS) << 2);
                delta = -(palette[baseptr] & 0xFF);
                whiteshifts[i - 1][workptr++] = (byte) (((palette[baseptr++] & 0xFF) + delta * i / WHITESTEPS) << 2);
            }
        }
    }

    public static void updatepaletteshifts() {
        if (whitecount != 0) {
            whitecount = BClipLow(whitecount - TICSPERFRAME, 0);
        }

        if (redcount != 0) {
            redcount = BClipLow(redcount - TICSPERFRAME, 0);
        }
    }

    public static void showinv(int snum) {
        char shade;
        boolean skipsyms = !tekcfg.toggles[TOGGLE_INVENTORY];
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        if (!skipsyms) {
            for (int i = 0; i < 7; i++) {
                shade = 0;
                if (symbols[i]) {
                    if (symbolsdeposited[i]) {
                        shade = 32;
                    }
                    overwritesprite(xdim - mulscale(30 * (8 - i), tekcfg.gHUDSize, 16),
                            ydim - mulscale(32, tekcfg.gHUDSize, 16), tekcfg.gHUDSize, SYMBOL1PIC + i, shade, 0, 0);
                }
            }
        }

        if (tekcfg.toggles[TOGGLE_INVENTORY]) {
            timedinv--;
            if (timedinv == 0) {
                tekcfg.toggles[TOGGLE_INVENTORY] = false;
            }
        }

        if ((xdim >= (xdim - 24)) && tekcfg.toggles[TOGGLE_INVENTORY]) {
            if (gPlayer[snum].invbluecards != 0) {
                overwritesprite(xdim - mulscale(24, tekcfg.gHUDSize, 16),
                        (ydim >> 1) - mulscale(14, tekcfg.gHUDSize, 16), tekcfg.gHUDSize, 483, 0, 0, 0);
            }
            if (gPlayer[snum].invredcards != 0) {
                overwritesprite(xdim - mulscale(24, tekcfg.gHUDSize, 16),
                        (ydim >> 1) - mulscale(2, tekcfg.gHUDSize, 16), tekcfg.gHUDSize, 484, 0, 0, 0);
            }
            if (gPlayer[snum].invaccutrak != 0) {
                overwritesprite(xdim - mulscale(24, tekcfg.gHUDSize, 16),
                        (ydim >> 1) + mulscale(10, tekcfg.gHUDSize, 16), tekcfg.gHUDSize, 485, 0, 0, 0);
            }
        }
    }

}
