package ru.m210projects.Tekwar.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.DefaultPaletteManager;
import ru.m210projects.Build.filehandle.Entry;

import java.io.IOException;

public class TekPaletteManager extends DefaultPaletteManager {

    public TekPaletteManager(Engine engine, Entry entry) throws IOException {
        super(engine, new TekPaletteLoader(entry));
    }

}
