package ru.m210projects.Tekwar.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.FontHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.SliderDrawable;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Renderer.RenderType;
import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.OsdFunc;
import ru.m210projects.Tekwar.Fonts.MenuFont;
import ru.m210projects.Tekwar.Fonts.TekFontA;
import ru.m210projects.Tekwar.Fonts.TekFontB;
import ru.m210projects.Tekwar.Main;
import ru.m210projects.Tekwar.Menus.TekSliderDrawable;

public class TekFactory extends BuildFactory {

    private final Main app;

    public TekFactory(Main app) {
        super("tekwar.dat");
        this.app = app;

        OsdColor.DEFAULT.setPal(4);
    }

    @Override
    public void drawInitScreen() {
        Renderer renderer = app.getRenderer();
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, 321, 0, 0, 2 | 8);
    }

    @Override
    public Engine engine() throws Exception {
        return Main.engine = new TekEngine(app);
    }

    @Override
    public Renderer renderer(RenderType type) {
        if (type == RenderType.Software) {
            return new TekSoftware(app.pCfg);
        } else if (type == RenderType.PolyGDX) {
            return new TekPolygdx(app.pCfg);
        } else {
            return new TekPolymost(app.pCfg);
        }
    }

    @Override
    public DefScript getBaseDef(Engine engine) {
        return new DefScript(engine);
    }

    @Override
    public OsdFunc getOsdFunc() {
        return new TekwarOSDFunc();
    }

    @Override
    public MenuHandler menus() {
        return app.menu = new TekMenuHandler(app, 32);
    }

    @Override
    public FontHandler fonts() {
        return new FontHandler(4) {
            @Override
            protected Font init(int i) {
                if (i == 0) {
                    return new MenuFont(app.pEngine);
                }
                if (i == 1) {
                    return new TekFontA(app.pEngine);
                }
                if (i == 2) {
                    return new TekFontB(app.pEngine);
                }
                return EngineUtils.getLargeFont();
            }
        };
    }

    @Override
    public BuildNet net() {
        return new TekNetwork(app);
    }

    @Override
    public SliderDrawable slider() {
        return new TekSliderDrawable(app);
    }

}
