package ru.m210projects.Tekwar.Factory;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.SpriteMap;
import ru.m210projects.Build.Types.collections.ValueSetter;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Tekwar.Types.XTtrailertype;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Tekwar.Tekprep.spriteXT;

public class TekBoardService extends BoardService {

    public static final String TRAILERID = "**MAP_EXTS**";

    public TekBoardService() {
        registerBoard(6, SectorVer6.class, WallVer6.class, SpriteVer6.class);
    }

    @Override
    protected TekBoard loadBoard(Entry entry) throws IOException {
        Board b = super.loadBoard(entry);
        TekBoard tekBoard = new TekBoard(b.getPos(), b.getSectors(), b.getWalls(), b.getSprites());

        // it will load in XTsave
        for (int i = 0; i < MAXSPRITES; i++) {
            spriteXT[i].set(0);
        }

        XTtrailertype XTtrailer = tekBoard.getXTtrailer();
        // read in XTtrailer
        try (InputStream is = entry.getInputStream()) {
            StreamUtils.skip(is, (int) (entry.getSize() - XTtrailertype.sizeof));
            XTtrailer.load(is);
        }

        // if no previous extension info then continue
        boolean noext = XTtrailer.ID == null || XTtrailer.ID.compareToIgnoreCase(TRAILERID) != 0;
        if (!noext) {
            // load and intialize spriteXT array members
            try (InputStream is = entry.getInputStream()) {
                StreamUtils.skip(is, XTtrailer.start);
                for (int i = 0; i < XTtrailer.numXTs; i++) {
                    tekBoard.getXTsave().load(is);
                }
            }
        }

        return tekBoard;
    }

    @Override
    protected SpriteMap createSpriteMap(int listCount, List<Sprite> spriteList, int spriteCount, ValueSetter<Sprite> valueSetter) {
        return new SpriteMap(listCount, spriteList, spriteCount, valueSetter) {
            @Override
            protected Sprite getInstance() {
                Sprite spr = new SpriteVer6();

                spr.setSectnum((short) MAXSECTORS);
                spr.setStatnum(MAXSTATUS);
                spr.setExtra(-1);

                return spr;
            }
        };
    }
}
