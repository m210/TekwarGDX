package ru.m210projects.Tekwar.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.GdxRender.GDXOrtho;
import ru.m210projects.Build.Render.GdxRender.GDXRenderer;
import ru.m210projects.Build.settings.GameConfig;

public class TekPolygdx extends GDXRenderer implements TekRenderer {

    public TekPolygdx(GameConfig config) {
        super(config);
    }

    @Override
    protected GDXOrtho allocOrphoRenderer(Engine engine) {
        return new GDXOrtho(this, new TekMapSettings(engine.getBoardService()));
    }
}
