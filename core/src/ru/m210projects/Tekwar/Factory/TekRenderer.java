package ru.m210projects.Tekwar.Factory;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.PaletteManager;

import static ru.m210projects.Build.Gameutils.BClipHigh;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.View.*;

public interface TekRenderer extends Renderer {

    default void scrSetDac() {
        int red = 0, white = 0;

        if (PICKUP_DAC.getIntensive() != 0) {
            white = BClipHigh(PICKUP_DAC.getIntensive() / WHITETICS + 1, NUMWHITESHIFTS);
        }

        if (DAMAGE_DAC.getIntensive() != 0) {
            red = BClipHigh(DAMAGE_DAC.getIntensive() / 10 + 1, NUMREDSHIFTS);
        }

        PaletteManager manager = engine.getPaletteManager();
        if (red != 0) {
            manager.changePalette(redshifts[red - 1]);
            palshifted = true;
        } else if (white != 0) {
            manager.changePalette(whiteshifts[white - 1]);
            palshifted = true;
        } else if (palshifted) {
            engine.setbrightness(manager.getPaletteGamma(), manager.getBasePalette());
            palshifted = false;
        }
    }
}
