package ru.m210projects.Tekwar.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Tables;
import ru.m210projects.Build.Types.PaletteManager;

import java.io.IOException;

import static ru.m210projects.Tekwar.Globals.CLKIPS;
import static ru.m210projects.Tekwar.Globals.TICSPERFRAME;

public class TekEngine extends Engine {

    public TekEngine(BuildGame game) throws Exception {
        super(game);
        inittimer(game.pCfg.isLegacyTimer(), CLKIPS, TICSPERFRAME);
    }

    @Override
    protected BoardService createBoardService() {
        return new TekBoardService();
    }

    @Override
    protected Tables loadtables() throws Exception {
        return new TekTables(game.getCache().getEntry("tables.dat", true));
    }

    @Override
    public PaletteManager loadpalette() throws IOException {
        return new TekPaletteManager(this, game.getCache().getEntry("palette.dat", true));
    }
}
