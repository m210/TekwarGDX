package ru.m210projects.Tekwar.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Render.DefaultMapSettings;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;

import static ru.m210projects.Build.Engine.show2dwall;
import static ru.m210projects.Tekwar.Main.screenpeek;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.View.*;

public class TekMapSettings extends DefaultMapSettings {

    public TekMapSettings(BoardService boardService) {
        super(boardService);
    }

    @Override
    public int getWallColor(int w, int s) {
        Wall wal = boardService.getWall(w);
        if (wal == null) {
            return 0;
        }

        Sector nextsec = boardService.getSector(wal.getNextsector());

        if (nextsec != null) {// red wall
            return 232;
        }
        return 239; // white wall
    }

    @Override
    public boolean isWallVisible(int w, int s) {
        Wall wal = boardService.getWall(w);
        if (gViewMode == kView2DIcon) {
            if (wal != null && wal.getNextsector() != 0) {
                int k = wal.getNextwall();
                if (k < 0) {
                    return false;
                }
                if (!show2dwall.getBit(w)) {
                    return false;
                }
                if ((k > w) && show2dwall.getBit(k)) {
                    return false;
                }

                Sector nextsec = boardService.getSector(wal.getNextsector());
                Sector sec = boardService.getSector(s);
                if (nextsec == null || sec == null) {
                    return false;
                }

                if (nextsec.getCeilingz() == sec.getCeilingz()) {
                    if (nextsec.getFloorz() == sec.getFloorz()) {
                        Wall nextwal = boardService.getWall(wal.getNextwall());
                        if (nextwal != null && ((wal.getCstat() | nextwal.getCstat()) & (16 + 32)) == 0) {
                            return false;
                        }
                    }
                }


                if (sec.getFloorz() != sec.getCeilingz()) {
                    if (nextsec.getFloorz() != nextsec.getCeilingz()) {
                        Wall nextwal = boardService.getWall(wal.getNextwall());
                        if (nextwal != null && ((wal.getCstat() | nextwal.getCstat()) & (16 + 32)) == 0) {
                            if (sec.getFloorz() == nextsec.getFloorz()) {
                                return false;
                            }
                        }
                    }
                }
                if (sec.getFloorpicnum() != nextsec.getFloorpicnum()) {
                    return false;
                }
                return sec.getFloorshade() == nextsec.getFloorshade();
            }
        }
        return true;
    }

    @Override
    public boolean isSpriteVisible(MapView view, int index) {
        if (view == MapView.Polygons) {
            return false;
        }

        Sprite sp = boardService.getSprite(index);
        if (sp == null) {
            return false;
        }

        switch (sp.getCstat() & 48) {
            case 0:
                return true;
            case 16: // wall sprites
                return true;
            case 32: // floor sprites
                return gViewMode == kView2D;
        }
        return true;
    }

    @Override
    public int getPlayerPicnum(int player) {
        if (gViewMode == kView2D) {
            return -1;
        }

        int spr = getPlayerSprite(player);
        Sprite psp = boardService.getSprite(spr);
        return psp != null ? psp.getPicnum() : -1;
    }

    @Override
    public int getSpriteColor(int s) {
        Sprite spr = boardService.getSprite(s);
        if (s == gPlayer[screenpeek].playersprite) {
            return 31;
        }

        if (spr != null && (spr.getCstat() & 1) != 0) {
            return 248;
        }
        return 56;
    }

    @Override
    public int getPlayerSprite(int player) {
        return gPlayer[player].playersprite;
    }

    @Override
    public int getViewPlayer() {
        return screenpeek;
    }
}
