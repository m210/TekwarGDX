package ru.m210projects.Tekwar.Factory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Build.Pattern.BuildNet.NetInput;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Tekwar.Main;
import ru.m210projects.Tekwar.Types.Input;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Tekwar.Config.*;
import static ru.m210projects.Tekwar.Globals.*;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekchng.changehealth;
import static ru.m210projects.Tekwar.Tekgun.changegun;
import static ru.m210projects.Tekwar.Tekgun.tekhasweapon;
import static ru.m210projects.Tekwar.Tekmap.mission;
import static ru.m210projects.Tekwar.Tekmap.symbols;
import static ru.m210projects.Tekwar.Teksnd.playsound;
import static ru.m210projects.Tekwar.Tektag.flags32;
import static ru.m210projects.Tekwar.View.*;

public class TekControl extends GameProcessor {

    private int vel, svel;

    public TekControl(Main app) {
        super(app);
    }

    @Override
    public void fillInput(NetInput input) {
        mouseDelta.y /= 16.0f;

        Input gInput = (Input) input;

        if (Console.out.isShowing()) {
            return;
        }

        gInput.reset();
//		vel = svel = 0;
        float angvel;
        float horiz = angvel = 0;

        if (game.pMenu.gShowMenu) {
            return;
        }

        tekcheatkeys();

        // normal game keys active
        for (int i = 0; i < 8; i++) {
            if (isGameKeyJustPressed(tekcfg.getKeymap()[i + tekcfg.weaponIndex])
                    && tekhasweapon(i, myconnectindex) != 0) {
                gInput.selectedgun = i;
            }
        }

        if (isGameKeyJustPressed(GameKeys.Next_Weapon) && mission != 7) {
            int gun = changegun(myconnectindex, 1);
            if (gun != -1) {
                gInput.selectedgun = gun;
            }
        }

        if (isGameKeyJustPressed(GameKeys.Previous_Weapon) && mission != 7) {
            int gun = changegun(myconnectindex, -1);
            if (gun != -1) {
                gInput.selectedgun = gun;
            }
        }

        if (isGameKeyJustPressed(GameKeys.Map_Toggle)) {
            tekmapmode(gViewMode);
        }

        boolean strafe = isGameKeyPressed(GameKeys.Strafe);

        // keyboard survey - use to be keytimerstuff() called from keyhandler
        if (!strafe) { // Les 09/28/95
            if (isGameKeyPressed(GameKeys.Turn_Left)) {
                angvel = max(angvel - 100 * TICSPERFRAME, -128); // Les 09/28/95
            }
            if (isGameKeyPressed(GameKeys.Turn_Right)) {
                angvel = min(angvel + 100 * TICSPERFRAME, 127); // Les 09/28/95
            }

        } else { // Les 09/28/95
            if (isGameKeyPressed(GameKeys.Turn_Left)) {
                svel = min(svel + 8 * TICSPERFRAME, 127); // Les 09/28/95
            }
            if (isGameKeyPressed(GameKeys.Turn_Right)) {
                svel = max(svel - 8 * TICSPERFRAME, -128); // Les 09/28/95
            }
        }

        if (isGameKeyPressed(GameKeys.Move_Forward)) {
            vel = min(vel + 8 * TICSPERFRAME, 127); // Les 09/28/95
        }
        if (isGameKeyPressed(GameKeys.Move_Backward)) {
            vel = max(vel - 8 * TICSPERFRAME, -128); // Les 09/28/95
        }
        if (!tekcfg.isgMouseAim()) {
            float move = ctrlGetMouseMove();
            if (move != 0) {
                vel = (byte) BClipRange(vel - move, -128, 127);
            }
        }

        if (isGameKeyPressed(GameKeys.Strafe_Left)) {
            svel = min(svel + 8 * TICSPERFRAME, 127); // Les 09/28/95
        }
        if (isGameKeyPressed(GameKeys.Strafe_Right)) {
            svel = max(svel - 8 * TICSPERFRAME, -128); // Les 09/28/95
        }

        Vector2 stick1 = ctrlGetStick(JoyStick.LOOKING);
        Vector2 stick2 = ctrlGetStick(JoyStick.MOVING);

        float lookx = stick1.x;
        float looky = stick1.y;

        if (looky != 0) {
            float k = 4.0f;
            Renderer renderer = game.getRenderer();
            int ydim = renderer.getHeight();

            horiz = BClipRange(horiz + k * looky * tekcfg.getJoyLookSpeed(), -(ydim >> 1), 100 + (ydim >> 1));
        }

        if (lookx != 0) {
            float k = 80.0f;
            angvel = (short) BClipRange(gInput.angvel + k * lookx * tekcfg.getJoyTurnSpeed(), -1024, 1024);
        }

        if (stick2.y != 0) {
            float k = 8 * TICSPERFRAME;
            vel = (short) BClipRange(vel - (k * stick2.y), -128, 127);
        }
        if (stick2.x != 0) {
            float k = 8 * TICSPERFRAME;
            svel = (short) BClipRange(svel - (k * stick2.x), -128, 127);
        }

        if (angvel < 0) {
            angvel = min(angvel + 12 * TICSPERFRAME, 0);
        }
        if (angvel > 0) {
            angvel = max(angvel - 12 * TICSPERFRAME, 0);
        }
        if (svel < 0) {
            svel = min(svel + 2 * TICSPERFRAME, 0);
        }
        if (svel > 0) {
            svel = max(svel - 2 * TICSPERFRAME, 0);
        }
        if (vel < 0) {
            vel = min(vel + 2 * TICSPERFRAME, 0);
        }
        if (vel > 0) {
            vel = max(vel - 2 * TICSPERFRAME, 0);
        }

        gInput.vel = (byte) BClipRange(vel, -128 + 8, 127 - 8);
        gInput.svel = (short) BClipRange(svel, -128 + 8, 127 - 8);
        gInput.angvel = BClipRange(angvel, -512 + 16, 511 - 16);

        if (tekcfg.isgMouseAim()) {
            gInput.mlook = BClipRange(horiz + ctrlGetMouseLook(tekcfg.isgInvertmouse()), -512, 511);
        }

        if (!strafe) {
            gInput.angvel = (short) BClipRange(gInput.angvel + ctrlGetMouseTurn(), -1024, 1024);
        } else {
            svel = BClipRange(svel - (int) ctrlGetMouseStrafe(), -128, 127);
        }

        gInput.Jump = isGameKeyPressed(GameKeys.Jump);
        gInput.Crouch = isGameKeyPressed(GameKeys.Crouch);
        gInput.Look_Up = isGameKeyPressed(GameKeys.Look_Up);
        gInput.Look_Down = isGameKeyPressed(GameKeys.Look_Down);
        gInput.Center = isGameKeyJustPressed(TekKeys.Aim_Center);
        gInput.Holster_Weapon = isGameKeyJustPressed(TekKeys.Holster_Weapon);
        gInput.Run = isGameKeyPressed(GameKeys.Run);
        gInput.Use = isGameKeyJustPressed(GameKeys.Open);
        gInput.Fire = isGameKeyPressed(GameKeys.Weapon_Fire);

        // overhead maps zoom in/out
        if (gViewMode != 3) {
            if (isGameKeyPressed(GameKeys.Shrink_Screen) && (zoom > 48)) {
                zoom -= (zoom >> 4);
            }
            if (isGameKeyPressed(GameKeys.Enlarge_Screen) && (zoom < 4096)) {
                zoom += (zoom >> 4);
            }
        }

        if (isGameKeyJustPressed(TekKeys.Make_Screenshot)) {
            String name;
            if (mUserFlag == UserFlag.UserMap) {
                name = "scr-" + FileUtils.getFullName(boardfilename.getName()) + "-xxxx.png";
            } else {
                name = "scr-mission" + mission + "-xxxx.png";
            }

            Renderer renderer = game.getRenderer();
            String filename = renderer.screencapture(game.getUserDirectory(), name);
            if (filename != null) {
                showmessage(filename + " saved");
            } else {
                showmessage("Screenshot not saved. Access denied!");
            }
        }

        // if typing mode reset kbrd fifo
//        if (isGameKeyPressed(GameKeys.Send_Message, false)) {
////			typemode = 1;
//fixme            getInputController().initMessageInput(null);
//        }

        tekprivatekeys(gInput);
    }



    public void tekcheatkeys() {
        if (keyStatus(Keys.SHIFT_LEFT) && keyStatus(Keys.ALT_LEFT)) {
            if (keyStatusOnce(Keys.G)) {
                gPlayer[myconnectindex].godMode = !gPlayer[myconnectindex].godMode;
            }

            if (keyStatusOnce(Keys.W)) {
                for (int i = 0; i < 8; i++) {
                    gPlayer[myconnectindex].ammo[i] = MAXAMMO;
                }
                gPlayer[myconnectindex].invredcards = 1;
                gPlayer[myconnectindex].invbluecards = 1;
                gPlayer[myconnectindex].invaccutrak = 1;
                gPlayer[myconnectindex].weapons = (flags32[GUN1FLAG] | flags32[GUN2FLAG] | flags32[GUN3FLAG]
                        | flags32[GUN4FLAG]);
                gPlayer[myconnectindex].weapons |= (flags32[GUN5FLAG] | flags32[GUN6FLAG] | flags32[GUN7FLAG]
                        | flags32[GUN8FLAG]);
            }
            if (keyStatusOnce(Keys.H)) {
                changehealth(myconnectindex, 200);
            }
            if (keyStatusOnce(Keys.J)) {
                symbols[0] = true;
                symbols[1] = true;
                symbols[2] = true;
                symbols[3] = true;
                symbols[4] = true;
                symbols[5] = true;
                symbols[6] = true;
            }
        }
    }

    private boolean keyStatus(int i) {
        return keyStatus[i];
    }

    private boolean keyStatusOnce(int i) {
        if (keyStatus[i]) {
            keyStatus[i] = false;
            return true;
        }
        return false;
    }

    public void tekprivatekeys(Input gInput) {
        if (isGameKeyJustPressed(TekKeys.Toggle_Crosshair)) {
            tekcfg.gCrosshair = !tekcfg.gCrosshair;
        }
        if (isGameKeyJustPressed(TekKeys.Elapsed_Time)) {
            tekcfg.toggles[TOGGLE_TIME] = !tekcfg.toggles[TOGGLE_TIME];
        }
        if (isGameKeyJustPressed(TekKeys.Score)) {
            tekcfg.toggles[TOGGLE_SCORE] = !tekcfg.toggles[TOGGLE_SCORE];
        }
        if (isGameKeyJustPressed(TekKeys.Rearview)) {
            tekcfg.toggles[TOGGLE_REARVIEW] = !tekcfg.toggles[TOGGLE_REARVIEW];
            playsound(S_REARMONITOR, 0, 0, 0, ST_IMMEDIATE);
            rvmoving = 1;
        }
        if (isGameKeyJustPressed(TekKeys.Prepared_Item)) {
            tekcfg.toggles[TOGGLE_UPRT] = !tekcfg.toggles[TOGGLE_UPRT];
            playsound(S_STATUS1 + (tekcfg.toggles[TOGGLE_UPRT] ? 1 : 0), 0, 0, 0, ST_IMMEDIATE);
            wpmoving = 1;
        }
        if (isGameKeyJustPressed(TekKeys.Health_Meter)) {
            tekcfg.toggles[TOGGLE_HEALTH] = !tekcfg.toggles[TOGGLE_HEALTH];
            playsound(S_HEALTHMONITOR, 0, 0, 0, ST_IMMEDIATE);
            hcmoving = 1;
        }
        if (isGameKeyJustPressed(TekKeys.Inventory)) {
            tekcfg.toggles[TOGGLE_INVENTORY] = !tekcfg.toggles[TOGGLE_INVENTORY];
        }

        if (isGameKeyJustPressed(GameKeys.Mouse_Aiming)) {
            if (!tekcfg.isgMouseAim()) {
                tekcfg.setgMouseAim(true);
                showmessage("Mouse aiming on");
            } else {
                tekcfg.setgMouseAim(false);
                gInput.Center = true;
                showmessage("Mouse aiming off");
            }
        }

    }

    private final boolean[] keyStatus = new boolean[257];

    @Override
    public boolean keyDown(int keycode) {
        boolean result = super.keyDown(keycode);
        keyStatus[keycode] = true;
        return result;
    }

    @Override
    public boolean keyUp(int i) {
        boolean result = super.keyUp(i);
        keyStatus[i] = false;
        return result;
    }
}
