package ru.m210projects.Tekwar.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Polymost.Polymost;
import ru.m210projects.Build.Render.Polymost.Polymost2D;
import ru.m210projects.Build.Render.Types.ScreenFade;
import ru.m210projects.Build.settings.GameConfig;

import static com.badlogic.gdx.graphics.GL20.*;
import static ru.m210projects.Tekwar.Main.game;
import static ru.m210projects.Tekwar.View.*;

public class TekPolymost extends Polymost implements TekRenderer {

    public TekPolymost(GameConfig config) {
        super(config);
    }

    @Override
    protected Polymost2D allocOrphoRenderer(Engine engine) {
        return new Polymost2D(this, new TekMapSettings(engine.getBoardService()));
    }

    private void renderScreenFade() {
        gl.glBegin(GL_TRIANGLES);
        for (int i = 0; i < 6; i += 2) {
            gl.glVertex2f(vertices[i], vertices[i + 1]);
        }
        gl.glEnd();
    }

    @Override
    public void scrSetDac() {
        if (config.isPaletteEmulation()) {
            TekRenderer.super.scrSetDac();
            return;
        }

        if (game.menu.gShowMenu) {
            return;
        }

        boolean hasShader = beginShowFade();
        for (ScreenFade fade : SCREEN_DAC_ARRAY) {
            showScreenFade(fade);
        }
        endShowFade(hasShader);
    }

    @Override
    public void showScreenFade(ScreenFade screenFade) {
        int intensive = screenFade.getIntensive();
        if (intensive == 0) {
            return;
        }

        if (intensive > 32) {
            intensive = 32;
        }

        int r;
        int g = 0;
        int b = 0;
        int a;
        switch (screenFade.getName()) {
            case PICKUP_SCREEN_NAME:
                r = g = (int) Math.min(1.5f * (intensive + (intensive < 10 ? ((float) intensive / 2) : 0)), 255);
                a = Math.min(intensive + 32, 255);

                gl.glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, b, a);
                renderScreenFade();

                gl.glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, 0, 0);
                renderScreenFade();
                break;
            case DAMAGE_SCREEN_NAME:
                r = a = (int) Math.min(3.5f * (intensive + (intensive < 10 ? ((float) intensive / 2) : 0)), 255);
                gl.glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, b, a);
                renderScreenFade();

                gl.glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, 0, 0, 0);
                renderScreenFade();
                break;
        }
    }
}
