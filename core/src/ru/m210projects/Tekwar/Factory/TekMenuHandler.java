package ru.m210projects.Tekwar.Factory;


import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuRendererSettings;
import ru.m210projects.Build.Pattern.CommonMenus.MenuVideoMode;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Tekwar.Menus.MenuInterfaceSet;

import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Build.Pragmas.scale;
import static ru.m210projects.Tekwar.Globals.*;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Teksnd.playsound;

public class TekMenuHandler extends MenuHandler {

    public static final int MAIN = 0;
    public static final int GAME = 1;
    public static final int LOADGAME = 3;
    public static final int SAVEGAME = 4;
    public static final int QUIT = 6;
    public static final int HELP = 7;
    public static final int AUDIOSET = 8;
    public static final int OPTIONS = 10;
    public static final int COLORCORR = 11;
    public static final int LASTSAVE = 12;
    public static final int CORRUPTLOAD = 13;
    private final Engine engine;
    private final BuildGame app;

    public TekMenuHandler(BuildGame app, int nMaxMenus) {
        super(app);
        mMenus = new BuildMenu[nMaxMenus];
        this.engine = app.pEngine;
        this.app = app;
    }

    @Override
    public void mDrawMenu() {
        Renderer renderer = game.getRenderer();
        if (!(app.pMenu.getCurrentMenu() instanceof BuildMenuList) && !(app.pMenu.getCurrentMenu() instanceof MenuInterfaceSet)) {
            int tile = 321;
            ArtEntry pic = engine.getTile(tile);
            int xdim = renderer.getWidth();
            int ydim = renderer.getHeight();

            float kt = xdim / (float) ydim;
            float kv = pic.getWidth() / (float) pic.getHeight();
            float scale;
            if (kv >= kt) {
                scale = (ydim + 1) / (float) pic.getHeight();
            } else {
                scale = (xdim + 1) / (float) pic.getWidth();
            }

            renderer.rotatesprite(0, 0, (int) (scale * 65536), 0, tile, 127, 0, 8 | 16 | 1);
        }

        if (!(app.pMenu.getCurrentMenu() instanceof MenuInterfaceSet)) {
            renderer.rotatesprite(160 << 16, 60 << 16, 32768, 0, MENUPANEL4801, 0, 0, 2 | 8 | 1024);
            renderer.rotatesprite(160 << 16, 180 << 16, 32768, 0, MENUPANEL4802, 0, 0, 2 | 8 | 1024);
        }

        super.mDrawMenu();
    }

    @Override
    public int getShade(MenuItem item) {
        if (item != null && item.isFocused()) {
            return 16 - (engine.getTotalClock() & 0x3F);
        }
        return 0;
    }

    @Override
    public int getPal(Font font, MenuItem item) {
        if (item != null) {
            if (!item.isEnabled()) {
                return 2;
            }
            if (item.isFocused()) {
                return 4;
            }
            return item.pal;
        }

        return 0;
    }

    @Override
    public void mDrawMouse(int x, int y) {
        if (!app.pCfg.isMenuMouse()) {
            return;
        }
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        int zoom = scale(0x10000, ydim, 200);
        int czoom = mulscale(0x8000, mulscale(zoom, app.pCfg.getgMouseCursorSize(), 16), 16);
        int xoffset = 0;
        int yoffset = 0;
        int ang = 0;

        renderer.rotatesprite((x + xoffset) << 16, (y + yoffset) << 16, czoom, ang, MOUSECURSOR, 0, 0, 8, 0, 0, xdim - 1, ydim - 1);
    }

    @Override
    public void mPostDraw(MenuItem item) {
    }

    @Override
    public void mSound(MenuItem item, MenuOpt opt) {
        switch (opt) {
            case Open:
                if (mCount == 1) {
                    playsound(S_MENUSOUND1, 0, 0, 0, ST_IMMEDIATE);
                } else {
                    playsound(S_MENUSOUND2, 0, 0, 0, ST_IMMEDIATE);
                }
                break;
            case Close:
                if (mCount == 1) {
                    playsound(S_MENUSOUND2, 0, 0, 0, ST_IMMEDIATE);
                } else {
                    playsound(S_BEEP, 0, 0, 0, ST_IMMEDIATE);
                }
                break;
            case UP:
            case DW:
                playsound(S_BOOP, 0, 0, 0, ST_IMMEDIATE);
                break;
            case LEFT:
            case RIGHT:
            case ENTER:
                if (opt == MenuOpt.ENTER && item instanceof MenuConteiner && item.getClass().getEnclosingClass() == MenuVideoMode.class) {
                    break;
                }

                if (item instanceof MenuSlider || item instanceof MenuSwitch || item instanceof MenuConteiner) {
                    playsound(S_MENUSOUND1, 0, 0, 0, ST_IMMEDIATE);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void mDrawBackButton() {
        if (!app.pCfg.isMenuMouse()) {
            return;
        }

        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();

        int zoom = scale(BACKSCALE, ydim, 200);
        if (mCount > 1) {
            //Back button
            ArtEntry pic = engine.getTile(BACKBUTTON);

            int shade = 4 + mulscale(16, EngineUtils.sin((20 * engine.getTotalClock()) & 2047), 16);
            renderer.rotatesprite(0, (ydim - mulscale(pic.getHeight(), zoom, 16)) << 16, zoom, 0, BACKBUTTON, shade, 0, 8 | 16, 0, 0, mulscale(zoom, pic.getWidth() - 1, 16), ydim - 1);
        }
    }

    @Override
    public boolean mCheckBackButton(int mx, int my) {
        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();

        int zoom = scale(BACKSCALE, ydim, 200);
        ArtEntry pic = engine.getTile(BACKBUTTON);

        int size = mulscale(pic.getWidth(), zoom, 16);
        int bx = 0;
        int by = ydim - mulscale(pic.getHeight(), zoom, 16);
        if (mx >= bx && mx < bx + size) {
            return my >= by && my < by + size;
        }
        return false;
    }
}
