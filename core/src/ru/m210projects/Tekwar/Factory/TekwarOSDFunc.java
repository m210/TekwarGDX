package ru.m210projects.Tekwar.Factory;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Pattern.ScreenAdapters.InitScreen;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.DefaultOsdFunc;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Tekwar.Fonts.ConsoleFont;
import ru.m210projects.Tekwar.Main;

import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Tekwar.Main.gMissionScreen;
import static ru.m210projects.Tekwar.Main.game;
import static ru.m210projects.Tekwar.Names.*;

public class TekwarOSDFunc extends DefaultOsdFunc {

    public TekwarOSDFunc() {
        super(game.getRenderer());
        BGTILE = BACKGROUND;
        BGCTILE = DEMOSIGN;
        BORDTILE = BOUNCYMAT;

        OsdColor.RED.setPal(1);
        OsdColor.BLUE.setPal(0);
        OsdColor.YELLOW.setPal(5);
        //OsdColor.BROWN.setPal(231);
        OsdColor.GREEN.setPal(3);
        OsdColor.WHITE.setPal(4);
    }

    @Override
    protected Font getFont() {
        return new ConsoleFont(Main.engine);
    }

    @Override
    public void showOsd(boolean cursorCatched) {
        super.showOsd(cursorCatched);

        if (!(game.getScreen() instanceof InitScreen) && !game.pMenu.gShowMenu && !game.getScreen().equals(gMissionScreen)) {
            Gdx.input.setCursorCatched(!cursorCatched);
        }
    }

    @Override
    public int getcolumnwidth(int osdtextscale) {
        return (int) (divscale(renderer.getWidth(), osdtextscale, 16) / 8.3f);
    }
}
