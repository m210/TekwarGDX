package ru.m210projects.Tekwar.Factory;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Tekwar.Types.XTsavetype;
import ru.m210projects.Tekwar.Types.XTtrailertype;

import java.util.List;

public class TekBoard extends Board {

    private final XTtrailertype XTtrailer;
    private final XTsavetype XTsave;

    public TekBoard(BuildPos pos, Sector[] sectors, Wall[] walls, List<Sprite> sprites) {
        super(pos, sectors, walls, sprites);
        XTtrailer = new XTtrailertype();
        XTsave = new XTsavetype();
    }

    public XTtrailertype getXTtrailer() {
        return XTtrailer;
    }

    public XTsavetype getXTsave() {
        return XTsave;
    }
}
