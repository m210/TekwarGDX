package ru.m210projects.Tekwar.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Software.Software;
import ru.m210projects.Build.Render.Software.SoftwareOrpho;
import ru.m210projects.Build.settings.GameConfig;

public class TekSoftware extends Software implements TekRenderer {

    public TekSoftware(GameConfig config) {
        super(config);
    }

    @Override
    protected SoftwareOrpho allocOrphoRenderer(Engine engine) {
        return new SoftwareOrpho(this, new TekMapSettings(engine.getBoardService()));
    }
}
