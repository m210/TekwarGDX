package ru.m210projects.Tekwar.Menus;

import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuFileBrowser;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Tekwar.Main;

import static ru.m210projects.Tekwar.Factory.TekMenuHandler.GAME;
import static ru.m210projects.Tekwar.Main.*;

public class MenuUserContent extends BuildMenu {

    public MenuUserContent(final Main app) {
        super(app.pMenu);
        MenuTitle title = new MenuTitle(app.pEngine, "User content", app.getFont(0), 160, 15, -1);
        title.pal = 3;

        Font pathFont = app.getFont(2);

        int width = 240;
        MenuFileBrowser list = new MenuFileBrowser(app, pathFont, app.getFont(0), pathFont, 40, 30, width, 1, 15, 321) {
            @Override
            public void init() {
                registerExtension("map", 0, 0);
            }

            @Override
            public void handleDirectory(Directory dir) {
                /* nothing */
            }

            @Override
            public void handleFile(FileEntry file) {
                if (file.isExtension("map")) {
                    addFile(file);
                }
            }

            @Override
            public void invoke(FileEntry fil) {
                if (fil.getExtension().equals("MAP")) {
                    boardfilename = fil;
                    mUserFlag = UserFlag.UserMap;
                    app.pMenu.mOpen(game.menu.mMenus[GAME], -1);
                }
            }
        };

        list.topPal = list.pathPal = 5;
        list.back = "back";
        addItem(title, false);
        addItem(list, true);
    }
}
