package ru.m210projects.Tekwar.Menus;

import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler.MenuOpt;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Tekwar.Main;

import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Tekwar.Names.*;

public class MenuHelp extends BuildMenu {

    public MenuHelp(Main app) {
        super(app.pMenu);
        MenuTitle mTitle = new MenuTitle(app.pEngine, null, null, 0, 0, -1) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = app.getRenderer();
                ArtEntry pic = renderer.getTile(BACKGROUND);
                int xdim = renderer.getWidth();

                int frames = xdim / pic.getWidth();
                int x = 0;
                for (int i = 0; i <= frames; i++) {
                    renderer.rotatesprite(x << 16, 0, 0x10000, 0, BACKGROUND, 0, 0, 2 + 8 + 16 + 256);
                    x += pic.getWidth();
                }
                renderer.rotatesprite(160 << 16, 50 << 16, divscale(200, draw.getTile(HELPSCREEN4801).getHeight(), 15), 0, HELPSCREEN4801, 0, 0, 2 | 8);
                renderer.rotatesprite(160 << 16, 150 << 16, divscale(200, draw.getTile(HELPSCREEN4802).getHeight(), 15), 0, HELPSCREEN4802, 0, 0, 2 | 8);
            }

            @Override
            public boolean callback(MenuHandler handler, MenuOpt opt) {
                return opt == MenuOpt.ESC;
            }

        };
        mTitle.flags = 7;

        addItem(mTitle, true);
    }
}
