package ru.m210projects.Tekwar.Menus;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuAudio;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;

import static ru.m210projects.Tekwar.Main.gGameScreen;
import static ru.m210projects.Tekwar.Tekprep.subwaysound;
import static ru.m210projects.Tekwar.Teksnd.*;
import static ru.m210projects.Tekwar.Tektag.*;

public class TekMenuAudio extends MenuAudio {

    public TekMenuAudio(BuildGame app) {
        super(app, 40, 15, 240, app.getFont(0).getSize() + 4, app.getFont(0).getSize(), app.getFont(0));
        AudioListener listener = new AudioListener() {

            @Override
            public void PreDrvChange() {
                SoundOff();
                MusicOff();
            }


            @Override
            public void PostDrvChange() {
                sndInit();
                MusicOn();
            }

            @Override
            public void SoundOff() {
                ambsubloop = -1;
                for (int i = 0; i < MAXSECTORVEHICLES; i++) {
                    sectorvehicle[i].soundindex = -1;
                }
                for (int i = 0; i < subwaytrackcnt; i++) {
                    subwaysound[i] = -1;
                }
                for (int i = 0; i < totalmapsndfx; i++) {
                    mapsndfx[i].id = -1;
                }
                stopallsounds();
            }

            @Override
            public void MusicOn() {
                if (app.isCurrentScreen(gGameScreen)) {
                    playsong();
                }
            }

            @Override
            public void MusicOff() {
                sndStopMusic();
            }
        };
        this.setListener(listener);
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        MenuTitle title = new MenuTitle(app.pEngine, text, app.getFont(0), 160, 15, -1);
        title.pal = 3;

        return title;
    }

    @Override
    protected char[][] getMusicTypeList() {
        char[][] list = new char[2][];
        list[0] = "midi".toCharArray();
        list[1] = "cd audio".toCharArray();
        return list;
    }
}
