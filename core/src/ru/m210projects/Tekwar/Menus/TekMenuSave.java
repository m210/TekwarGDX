package ru.m210projects.Tekwar.Menus;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuLoadSave;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;

import static ru.m210projects.Tekwar.Main.gGameScreen;
import static ru.m210projects.Tekwar.Main.game;
import static ru.m210projects.Tekwar.Tekldsv.*;

public class TekMenuSave extends MenuLoadSave {

    public TekMenuSave(final BuildGame app) {
        super(app, app.getFont(0), 40, 40, 120, 240, 9, 0, 5, 321, (handler, pItem) -> {
            MenuSlotList item = (MenuSlotList) pItem;
            String filename;
            Directory userDir = app.getUserDirectory();
            if (item.l_nFocus == 0) {
                int num = 0;
                do {
                    if (num > 9999) {
                        return;
                    }
                    filename = "game" + makeNum(num) + ".sav";
                    if (!userDir.getEntry(filename).exists()) {
                        break;
                    }

                    num++;
                } while (true);
            } else {
                filename = item.getFileEntry().getName();
            }
            if (item.typed == null || item.typed.isEmpty()) {
                item.typed = filename;
            }

            savegame(userDir, item.typed, filename);
            handler.mClose();
        }, true);
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        MenuTitle title = new MenuTitle(app.pEngine, text, app.getFont(0), 160, 15, -1);
        title.pal = 3;

        return title;
    }

    @Override
    public MenuPicnum getPicnum(Engine draw, int x, int y) {
        return new MenuPicnum(draw, x + 5, y - 3, 321, 321, 0x5800) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = game.getRenderer();
                if (nTile != defTile) {
                    renderer.rotatesprite(x << 16, y << 16, 2 * nScale, 0, nTile, 0, 0, 10 | 16);
                } else {
                    renderer.rotatesprite(x << 16, y << 16, nScale, 0, nTile, 0, 0, 10 | 16);
                }
            }

            @Override
            public void close() {
                gGameScreen.captBuffer = null;
            }
        };
    }

    @Override
    public boolean loadData(FileEntry filename) {
        return lsReadLoadData(filename) != -1;
    }

    @Override
    public MenuText getInfo(BuildGame app, int x, int y) {
        return new MenuText(lsInf.info, app.getFont(1), x + 10, y + 60, 0) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = handler.getRenderer();
                int ty = y;
                if (lsInf.date != null && !lsInf.date.isEmpty()) {
                    font.drawTextScaled(renderer, x, ty, lsInf.date, 1.0f, -128, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                    ty -= 5;
                }
                if (lsInf.info != null) {
                    font.drawTextScaled(renderer, x, ty, lsInf.info, 1.0f, -128, 4, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                }
            }
        };
    }

}
