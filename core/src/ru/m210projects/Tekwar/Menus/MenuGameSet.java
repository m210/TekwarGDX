package ru.m210projects.Tekwar.Menus;

import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Tekwar.Config;
import ru.m210projects.Tekwar.Main;

import static ru.m210projects.Tekwar.Globals.CLKIPS;
import static ru.m210projects.Tekwar.Globals.TICSPERFRAME;
import static ru.m210projects.Tekwar.Main.engine;

public class MenuGameSet extends BuildMenu {

    public MenuGameSet(Main app) {
        super(app.pMenu);
        MenuTitle Title = new MenuTitle(app.pEngine, "Game setup", app.getFont(0), 160, 15, -1);
        int pos = 35;
        final Config cfg = Main.tekcfg;

        int x = 45;
        int width = 240;

        MenuSwitch sAutoload = new MenuSwitch("Autoload folder", app.getFont(0), x, pos += 10, width, cfg.isAutoloadFolder(), (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.setAutoloadFolder(sw.value);
        }, "Enabled", "Disabled");
        sAutoload.pal = 2;

        MenuSwitch sShowCutscenes = new MenuSwitch("Show cutscenes:", app.getFont(0), x,
                pos += 10, width, cfg.showCutscenes, (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.showCutscenes = sw.value;
                }, null, null);
        sShowCutscenes.pal = 2;

        MenuSwitch mHead = new MenuSwitch("Head bob:", app.getFont(0), x, pos += 10, width, cfg.gHeadBob, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.gHeadBob = sw.value;
        }, null, null);
        mHead.pal = 2;

        MenuConteiner sOverlay = new MenuConteiner("Overlay map:", app.getFont(0), x, pos += 10, width, null, 0, (handler, pItem) -> {
            MenuConteiner item = (MenuConteiner) pItem;
            cfg.gOverlayMap = item.num;
        }) {

            @Override
            public void open() {
                if (this.list == null) {
                    this.list = new char[3][];
                    this.list[0] = "Full only".toCharArray();
                    this.list[1] = "Overlay only".toCharArray();
                    this.list[2] = "Full and overlay".toCharArray();
                }
                num = cfg.gOverlayMap;
            }
        };
        sOverlay.pal = 2;

        MenuSwitch saveguns = new MenuSwitch("Save weapons in next map:", app.getFont(0), x,
                pos += 10, width, cfg.gSaveWeapons, (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.gSaveWeapons = sw.value;
                }, "Yes", "No");
        saveguns.pal = 2;


        MenuSwitch mTimer = new MenuSwitch("Game loop timer:", app.getFont(0), x, pos + 10, width, cfg.isLegacyTimer(),
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.setLegacyTimer(sw.value);
                    engine.inittimer(cfg.isLegacyTimer(), CLKIPS, TICSPERFRAME);
                }, "Legacy", "Gdx") {
        };
        mTimer.pal = 2;

        addItem(Title, false);
        addItem(sAutoload, true);
        addItem(sShowCutscenes, false);
        addItem(mHead, false);
        addItem(sOverlay, false);
        addItem(saveguns, false);
        addItem(mTimer, false);
    }
}
