package ru.m210projects.Tekwar.Menus;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Tekwar.Screens.CreditsScreen;

public class MenuQuit extends BuildMenu {

    public MenuQuit(final BuildGame game) {
        super(game.pMenu);
        MenuTitle QuitTitle = new MenuTitle(game.pEngine, "Quit game", game.getFont(0), 160, 15, -1);
        QuitTitle.pal = 3;
        MenuText QuitQuestion = new MenuText("Do you really want to quit?", game.getFont(0), 160, 50, 1);
        QuitQuestion.pal = 3;
        MenuVariants QuitVariants = new MenuVariants(game.pEngine, "[Y/N]", game.getFont(0), 160, 60) {
            @Override
            public void positive(MenuHandler menu) {
                game.changeScreen(new CreditsScreen());
                menu.mClose();
            }
        };
        QuitVariants.pal = 3;
        addItem(QuitTitle, false);
        addItem(QuitQuestion, false);
        addItem(QuitVariants, true);
    }
}
