package ru.m210projects.Tekwar.Menus;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuRendererSettings;
import ru.m210projects.Build.Pattern.CommonMenus.MenuVideoMode;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.Tekwar.Factory.TekMenuHandler.COLORCORR;

public class TekMenuVideoMode extends MenuVideoMode {

    public TekMenuVideoMode(BuildGame app) {
        super(app, 46, 20, 240, app.getFont(0).getSize() + 4, app.getFont(0), 15, 240, 321);
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        MenuTitle title = new MenuTitle(app.pEngine, text, app.getFont(0), 160, 15, -1);
        title.pal = 3;

        return title;
    }

    @Override
    public MenuRendererSettings getRenSettingsMenu(BuildGame app, int posx, int posy, int width,
                                                   int nHeight, Font style) {
        MenuRendererSettings menu = new MenuRendererSettings(app, posx, posy, width, nHeight, style) {
            @Override
            public MenuTitle getTitle(BuildGame app, String text) {
                MenuTitle title = new MenuTitle(app.pEngine, text, app.getFont(0), 160, 15, -1);
                title.pal = 3;

                return title;
            }
        };
        app.pMenu.mMenus[COLORCORR] = menu;
        return menu;
    }
}
