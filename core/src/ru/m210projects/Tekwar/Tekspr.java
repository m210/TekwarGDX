package ru.m210projects.Tekwar;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;
import ru.m210projects.Build.Render.RenderedSpriteList;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.art.ArtEntry;

import static java.lang.Math.abs;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClipHigh;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Tekwar.Globals.*;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekchng.changehealth;
import static ru.m210projects.Tekwar.Tekgun.validplayer;
import static ru.m210projects.Tekwar.Tekprep.spriteXT;
import static ru.m210projects.Tekwar.Teksnd.playsound;
import static ru.m210projects.Tekwar.Tekstat.*;
import static ru.m210projects.Tekwar.Tektag.flags32;
import static ru.m210projects.Tekwar.View.*;

public class Tekspr {
    public static final short DROPSIES = 406;
    public static final int MAXDROPANGLES = 6;
    public static char dropanglecnt;
    public static final short[] dropangles = {0, 1792, 512, 768, 1536, 1024};

    public static void fillsprite(int newspriteindex2, int x2, int y2, int z2, int cstat2, int shade2, int pal2,
                                  int clipdist2, int xrepeat2, int yrepeat2, int xoffset2, int yoffset2, int picnum2, int ang2, int xvel2,
                                  int yvel2, int zvel2, int owner2, int lotag2, int hitag2) {
        Sprite spr2 = boardService.getSprite(newspriteindex2);
        if (spr2 == null) {
            return;
        }

        spr2.setX(x2);
        spr2.setY(y2);
        spr2.setZ(z2);
        spr2.setCstat(cstat2);
        spr2.setShade(shade2);
        spr2.setPal(pal2);
        spr2.setClipdist(clipdist2);
        spr2.setXrepeat(xrepeat2);
        spr2.setYrepeat(yrepeat2);
        spr2.setXoffset(xoffset2);
        spr2.setYoffset(yoffset2);
        spr2.setPicnum(picnum2);
        spr2.setAng(ang2);
        spr2.setXvel(xvel2);
        spr2.setYvel(yvel2);
        spr2.setZvel(zvel2);
        spr2.setOwner(owner2);
        spr2.setLotag(lotag2);
        spr2.setHitag(hitag2);
        spr2.setExtra(-1);

        game.pInt.setsprinterpolate(newspriteindex2, spr2);
    }

    public static int movesprite(int spritenum, int dx, int dy, int dz, int ceildist, int flordist, int cliptype) {
        int dcliptype = 0;
        switch (cliptype) {
            case NORMALCLIP:
            case CLIFFCLIP:
                dcliptype = CLIPMASK0;
                break;
            case PROJECTILECLIP:
                dcliptype = CLIPMASK1;
                break;
        }

        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null) {
            return 0;
        }

        game.pInt.setsprinterpolate(spritenum, spr);

        int zoffs = 0;
        ArtEntry pic = engine.getTile(spr.getPicnum());
        if ((spr.getCstat() & 128) == 0) {
            zoffs = -((pic.getHeight() * spr.getYrepeat()) << 1);
        }

        int dasectnum = spr.getSectnum();
        int px = spr.getX();
        int py = spr.getY();
        int pz = spr.getZ();
        int daz = spr.getZ() + zoffs;
        int retval = engine.clipmove(spr.getX(), spr.getY(), daz, dasectnum, dx, dy, (spr.getClipdist()) << 2, ceildist, flordist,
                dcliptype);

        if (clipmove_sectnum != -1) {
            spr.setX(clipmove_x);
            spr.setY(clipmove_y);
            dasectnum = clipmove_sectnum;
        }

        if ((dasectnum != spr.getSectnum()) && (dasectnum >= 0)) {
            engine.changespritesect(spritenum, dasectnum);
        }

        Sector sec = boardService.getSector(dasectnum);
        if (sec != null && (sec.getLotag() == 4) && (spr.getExtra() != -1)) {
            if (spr.getStatnum() != FLOATING) {
                spr.setZ(sec.getFloorz() - zoffs);
            }
            return (retval);
        }

        int tempshort = spr.getCstat();
        spr.setCstat(spr.getCstat() & ~1);
        engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, dcliptype);

        spr.setCstat(tempshort);
        daz = spr.getZ() + zoffs + dz;

        if ((daz <= zr_ceilz) || (daz > zr_florz)) {
            if (retval != 0) {
                return (retval);
            } else {
                for (ListNode<Sprite> node = boardService.getStatNode(dasectnum); node != null; node = node.getNext()) {
                    int j = node.getIndex();
                    Sprite spr2 = node.get();
                    int dist = EngineUtils.qdist(spr.getX() - spr2.getX(), spr.getY() - spr2.getY()) >> 2;
                    if (dist > 0 && dist <= (spr.getClipdist()) << 2) {
                        return (j | HIT_SPRITE);
                    }
                }
                return (HIT_SECTOR | dasectnum);
            }
        }
        if ((zr_florz != pz) && (spr.getExtra() >= 0) && (spr.getExtra() < MAXSPRITES)) {
            spr.setZ(zr_florz);
            int deltaz = abs(pz - zr_florz);
            int jumpz = pic.getHeight() + (spr.getYrepeat() - 64);
             jumpz <<= 8;
            if (deltaz > jumpz) {
                int failedsectnum = spr.getSectnum();
                engine.setsprite(spritenum, px, py, pz);
                retval = (failedsectnum | HIT_SECTOR);
            }
        } else {
            spr.setZ(daz - zoffs);
        }

        return (retval);
    }

    public static void dropit(int x, int y, int z, int sect, int pic) {
        int j = jsinsertsprite(sect, DROPSIES);
        if (j != -1) {
            int ang = dropangles[dropanglecnt];
            fillsprite(j, x, y, z, 0, 0, 0, 12, 16, 16, 0, 0, pic, ang, EngineUtils.sin((ang + 2560) & 2047) >> 6,
                    EngineUtils.sin((ang + 2048) & 2047) >> 6, 30, 0, 0, 0);
        }
        dropanglecnt++;
        if (dropanglecnt >= MAXDROPANGLES) {
            dropanglecnt = 0;
        }
    }

    public static void playerdropitems(int snum) {
        if (!validplayer(snum)) {
            throw new AssertException("dropitems on bad plrnum");
        }

        if ((gPlayer[snum].weapons & flags32[GUN2FLAG]) != 0) {
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    924);
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    3102);
        }
        if ((gPlayer[snum].weapons & flags32[GUN3FLAG]) != 0) {
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    3104);
        }
        if ((gPlayer[snum].weapons & flags32[GUN4FLAG]) != 0) {
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    3103);
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    3093);
        }
        if ((gPlayer[snum].weapons & flags32[GUN5FLAG]) != 0) {
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    3095);
        }
        if ((gPlayer[snum].weapons & flags32[GUN6FLAG]) != 0) {
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    925);
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    3101);
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    3091);
        }
        if ((gPlayer[snum].weapons & flags32[GUN7FLAG]) != 0) {
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    3100);
            dropit(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz - (32 << 8), gPlayer[snum].cursectnum,
                    3090);
        }
    }

    public static void checktouchsprite(int snum, int sectnum) {
        if (!boardService.isValidSector(sectnum)) {
            return;
        }

        String message = null;
        int dosound = 0;
        ListNode<Sprite> node = boardService.getSectNode(sectnum), nexti;
        while (node != null) {
            nexti = node.getNext();
            int i = node.getIndex();
            Sprite spr = node.get();
            if ((abs(gPlayer[snum].posx - spr.getX()) + abs(gPlayer[snum].posy - spr.getY()) < 512) && (abs(
                    (gPlayer[snum].posz >> 8) - ((spr.getZ() >> 8) - (engine.getTile(spr.getPicnum()).getHeight() >> 1))) <= 40)) {
                // must jive with tekinitplayer settings

                int healthmax = MAXHEALTH;
                if (gDifficulty == 3) {
                    healthmax = (MAXHEALTH >> 1);
                }

                switch (spr.getPicnum()) {
                    case MEDICKIT2PIC:
                        if (gPlayer[snum].health < healthmax) {
                            changehealth(snum, 100);
                            jsdeletesprite(i);
                            message = "MEDIC KIT";
                            dosound++;
                        }
                        break;
                    case MEDICKITPIC:
                        if (gPlayer[snum].health < healthmax) {
                            message = "MEDIC KIT";
                            changehealth(snum, 250);
                            jsdeletesprite(i);
                            dosound++;
                        }
                        break;
                    case 3637:
                        if (gPlayer[snum].health < healthmax) {
                            message = "ENERGY PELLET";
                            changehealth(snum, 50);
                            jsdeletesprite(i);
                            dosound++;
                        }
                        break;
                    case DONUTSPIC:
                        if (gPlayer[snum].health < healthmax) {
                            message = "MMMMMMM DONUTS";
                            changehealth(snum, 50);
                            jsdeletesprite(i);
                            dosound++;
                        }
                        break;
                    case ACCUTRAKPIC:
                        gPlayer[snum].invaccutrak = 1;
                        message = "ACCU TRAK";
                        startwhiteflash(3);
                        jsdeletesprite(i);
                        dosound++;
                        break;
                    // gun pick ups
                    case 3092:
                        if (((gPlayer[snum].weapons) & flags32[GUN2FLAG]) == 0) {
                            gPlayer[snum].weapons |= flags32[GUN2FLAG];

                            gPlayer[snum].ammo[1] = BClipHigh((short) (gPlayer[snum].ammo[1] + 25), MAXAMMO);
                            message = "PISTOL";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case 3094:
                        if (((gPlayer[snum].weapons) & flags32[GUN3FLAG]) == 0) {
                            gPlayer[snum].weapons |= flags32[GUN3FLAG];
                            gPlayer[snum].ammo[2] = BClipHigh((short) (gPlayer[snum].ammo[2] + 15), MAXAMMO);
                            message = "SHRIKE DBK";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case 3093:
                        if (((gPlayer[snum].weapons) & flags32[GUN4FLAG]) == 0) {
                            gPlayer[snum].weapons |= flags32[GUN4FLAG];
                            gPlayer[snum].ammo[3] = BClipHigh((short) (gPlayer[snum].ammo[3] + 15), MAXAMMO);
                            message = "ORLOW";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case GUN3DEMO:
                        if (TEKDEMO && ((gPlayer[snum].weapons) & flags32[GUN3FLAG]) == 0) {
                            gPlayer[snum].weapons |= flags32[GUN3FLAG];
                            gPlayer[snum].ammo[2] = BClipHigh((short) (gPlayer[snum].ammo[2] + MAXAMMO / 2), MAXAMMO);
                            message = "RAW DALOW 34 - FIND ACCUTRAK";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case 3095:
                        if (((gPlayer[snum].weapons) & flags32[GUN5FLAG]) == 0) {
                            gPlayer[snum].weapons |= flags32[GUN5FLAG];
                            gPlayer[snum].ammo[4] = BClipHigh((short) (gPlayer[snum].ammo[4] + 15), MAXAMMO);
                            message = "EMP GUN";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case 3091:
                        if (((gPlayer[snum].weapons) & flags32[GUN6FLAG]) == 0) {
                            gPlayer[snum].weapons |= flags32[GUN6FLAG];
                            gPlayer[snum].ammo[5] = BClipHigh((short) (gPlayer[snum].ammo[5] + 15), MAXAMMO);
                            message = "FLAMER";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case 3090:
                        if (((gPlayer[snum].weapons) & flags32[GUN7FLAG]) == 0) {
                            gPlayer[snum].weapons |= flags32[GUN7FLAG];
                            gPlayer[snum].ammo[6] = BClipHigh((short) (gPlayer[snum].ammo[6] + 10), MAXAMMO);
                            message = "BAZOOKA";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    // ammo pick ups
                    case 924:
                    case 3102:
                        if ((gPlayer[snum].ammo[1] < MAXAMMO)) { // && ((gPlayer[snum].weapons&flags32[GUN2FLAG]) != 0) ) {
                            gPlayer[snum].ammo[1] = BClipHigh((short) (gPlayer[snum].ammo[1] + 20), MAXAMMO);
                            message = "PISTOL KLIP";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case 3104:
                        if ((gPlayer[snum].ammo[2] < MAXAMMO)) { // && ((gPlayer[snum].weapons&flags32[GUN3FLAG]) != 0) ) {
                            gPlayer[snum].ammo[2] = BClipHigh((short) (gPlayer[snum].ammo[2] + 10), MAXAMMO);
                            message = "SHRIKE CHARGES";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case 3103:
                        if ((gPlayer[snum].ammo[3] < MAXAMMO)) { // && ((gPlayer[snum].weapons&flags32[GUN4FLAG]) != 0) ) {
                            gPlayer[snum].ammo[3] = BClipHigh((short) (gPlayer[snum].ammo[3] + 10), MAXAMMO);
                            message = "ORLOW CHARGES";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case 925:
                    case 3101:
                        if ((gPlayer[snum].ammo[5] < MAXAMMO)) { // && ((gPlayer[snum].weapons&flags32[GUN6FLAG]) != 0) ) {
                            gPlayer[snum].ammo[5] = BClipHigh((short) (gPlayer[snum].ammo[5] + 10), MAXAMMO);
                            message = "FUEL BOTTLE";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case CHARGEPAKPIC:
                    case 3100:
                        if ((gPlayer[snum].ammo[6] < MAXAMMO)) { // && ((gPlayer[snum].weapons&flags32[GUN7FLAG]) != 0) ) {
                            gPlayer[snum].ammo[6] = BClipHigh((short) (gPlayer[snum].ammo[6] + 5), MAXAMMO);
                            message = "BAZOOKA FUEL";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case 3836:
                        if ((gPlayer[snum].ammo[7] < MAXAMMO)) {
                            gPlayer[snum].ammo[7] = BClipHigh((short) (gPlayer[snum].ammo[7] + 25), MAXAMMO);
                            message = "GLOVE CHARGE";
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case RED_KEYCARD:
                        if (gPlayer[snum].invredcards == 0) {
                            message = "RED KEY CARD";
                            gPlayer[snum].invredcards = 1;
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    case BLUE_KEYCARD:
                        if (gPlayer[snum].invbluecards == 0) {
                            message = "BLUE KEY CARD";
                            gPlayer[snum].invbluecards = 1;
                            jsdeletesprite(i);
                            startwhiteflash(3);
                            dosound++;
                        }
                        break;
                    default:
                        break;
                }
            }
            if (dosound != 0 && (screenpeek == snum)) {
                playsound(S_PICKUP_BONUS, 0, 0, 0, ST_UNIQUE);
                showmessage(message);
            }
            dosound = 0;

            node = nexti;
        }
    }

    public static int floatmovesprite(int spritenum, int dx, int dy, int ceildist, int flordist,
                                      int cliptype) {
        int dcliptype = 0;
        switch (cliptype) {
            case NORMALCLIP:
            case CLIFFCLIP:
                dcliptype = CLIPMASK0;
                break;
            case PROJECTILECLIP:
                dcliptype = CLIPMASK1;
                break;
        }

        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null) {
            return 0;
        }

        int zoffs = 0;
        game.pInt.setsprinterpolate(spritenum, spr);
        if ((spr.getCstat() & 128) == 0) {
            zoffs = -((engine.getTile(spr.getPicnum()).getHeight() * spr.getYrepeat()) << 1);
        }

        int dasectnum = spr.getSectnum(); // Can't modify sprite sectors directly becuase of linked lists
        int daz = spr.getZ() + zoffs; // Must do this if not using the new centered centering (of course)
        int retval = engine.clipmove(spr.getX(), spr.getY(), daz, dasectnum, dx, dy, (spr.getClipdist()) << 2, ceildist, flordist,
                dcliptype);
        spr.setX(clipmove_x);
        spr.setY(clipmove_y);
        dasectnum = clipmove_sectnum;
        if ((dasectnum != spr.getSectnum()) && (dasectnum >= 0)) {
            engine.changespritesect(spritenum, dasectnum);
        }

        return (retval);
    }

    public static int flymovesprite(int spritenum, int dx, int dy, int ceildist, int flordist,
                                    int cliptype) {
        int dcliptype = 0;
        switch (cliptype) {
            case NORMALCLIP:
            case CLIFFCLIP:
                dcliptype = CLIPMASK0;
                break;
            case PROJECTILECLIP:
                dcliptype = CLIPMASK1;
                break;
        }

        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null) {
            return 0;
        }

        game.pInt.setsprinterpolate(spritenum, spr);
        int dasectnum = spr.getSectnum();
        int retval = engine.clipmove(spr.getX(), spr.getY(), spr.getZ(), dasectnum, dx, dy, (spr.getClipdist()) << 2, ceildist,
                flordist, dcliptype);
        spr.setX(clipmove_x);
        spr.setY(clipmove_y);
        spr.setZ(clipmove_z);
        dasectnum = clipmove_sectnum;

        if ((dasectnum != spr.getSectnum()) && (dasectnum >= 0)) {
            engine.changespritesect(spritenum, dasectnum);
        }

        if (spr.getStatnum() != PINBALL) {
            int tempshort = spr.getCstat();
            spr.setCstat(spr.getCstat() & ~1);
            engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, dcliptype);

            spr.setCstat(tempshort);
            int daz = (zr_florz + zr_ceilz);
            spr.setZ((daz >> 1));
        }

        return (retval);
    }

    public static int kenmovesprite(int spritenum, int dx, int dy, int dz, int ceildist, int flordist,
                                      int cliptype) {
        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null) {
            return 0;
        }

        int zoffs = 0;
        game.pInt.setsprinterpolate(spritenum, spr);
        if ((spr.getCstat() & 128) == 0) {
            zoffs = -((engine.getTile(spr.getPicnum()).getHeight() * spr.getYrepeat()) << 1);
        }

        int dcliptype = 0;
        switch (cliptype) {
            case NORMALCLIP:
            case CLIFFCLIP:
                dcliptype = CLIPMASK0;
                break;
            case PROJECTILECLIP:
                dcliptype = CLIPMASK1;
                break;
        }

        int dasectnum = spr.getSectnum(); // Can't modify sprite sectors directly becuase of linked lists
        int daz = spr.getZ() + zoffs; // Must do this if not using the new centered centering (of course)
        int retval = engine.clipmove(spr.getX(), spr.getY(), daz, dasectnum, dx, dy, (spr.getClipdist()) << 2, ceildist,
                flordist, dcliptype);
        spr.setX(clipmove_x);
        spr.setY(clipmove_y);
        dasectnum = clipmove_sectnum;
        if ((dasectnum != spr.getSectnum()) && (dasectnum >= 0)) {
            engine.changespritesect(spritenum, dasectnum);
        }

        // Set the blocking bit to 0 temporarly so engine.getzrange doesn't pick up
        // its own sprite
        int tempshort = spr.getCstat();
        spr.setCstat(spr.getCstat() & ~1);
        engine.getzrange(spr.getX(), spr.getY(), spr.getZ() - 1, spr.getSectnum(), (spr.getClipdist()) << 2, dcliptype);
        spr.setCstat(tempshort);

        daz = spr.getZ() + zoffs + dz;
        if ((daz <= zr_ceilz) || (daz > zr_florz)) {
            if (retval != 0) {
                return (retval);
            }
            return (HIT_SECTOR | dasectnum);
        }
        spr.setZ((daz - zoffs));
        return (retval);
    }

    public static void analyzesprites(int dax, int day, int smoothratio) {
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();
        for (int i = 0; i < renderedSpriteList.getSize(); i++) {
            Sprite tspr = renderedSpriteList.get(i);
            Sector sec = boardService.getSector(tspr.getSectnum());
            if (sec == null) {
                tspr.setOwner(-1);
                continue;
            }

            int ext = tspr.getExtra();
            if (validext(ext) != 0) {
                if (rearviewdraw == 0) {
                    spriteXT[ext].aimask |= AI_WASDRAWN;
                }
            }

            ILoc oldLoc = game.pInt.getsprinterpolate(tspr.getOwner());
            // only interpolate certain moving things
            if (oldLoc != null && (tspr.getHitag() & 0x0200) == 0) {
                int x = oldLoc.x;
                int y = oldLoc.y;
                int z = oldLoc.z;
                int nAngle = oldLoc.ang;

                // interpolate sprite position
                x += mulscale(tspr.getX() - oldLoc.x, smoothratio, 16);
                y += mulscale(tspr.getY() - oldLoc.y, smoothratio, 16);
                z += mulscale(tspr.getZ() - oldLoc.z, smoothratio, 16);
                nAngle += mulscale(((tspr.getAng() - oldLoc.ang + 1024) & 2047) - 1024, smoothratio, 16);

                tspr.setX(x);
                tspr.setY(y);
                tspr.setZ(z);
                tspr.setAng(nAngle);
            }

            int k = EngineUtils.getAngle(tspr.getX() - dax, tspr.getY() - day);
            k = (((tspr.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7;
            tspr.setShade(sec.getFloorshade());

            switch (tspr.getPicnum()) {
                case DOOMGUY:
                case RUBWALKPIC:
                case FRGWALKPIC:
                case SAMWALKPIC:
                case COP1WALKPIC:
                case ANTWALKPIC:
                case SARAHWALKPIC:
                case MAWALKPIC:
                case ERWALKPIC:
                case DIWALKPIC:
                case RATPIC:
                case SUNGWALKPIC:
                case COWWALKPIC:
                case COPBWALKPIC:
                case NIKAWALKPIC:
                case REBRWALKPIC:
                case TRENWALKPIC:
                case WINGWALKPIC:
                case HALTWALKPIC:
                case REDHWALKPIC:
                case ORANWALKPIC:
                case BLKSWALKPIC:
                case SFROWALKPIC:
                case SSALWALKPIC:
                case SGOLWALKPIC:
                case SWATWALKPIC:
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k << 2));
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) << 2));
                        tspr.setCstat(tspr.getCstat() | 4); // set x-flipping bit
                    }
                    break;
                case JAKESTANDPIC:
                case RUBSTANDPIC:
                case FRGSTANDPIC:
                case SAMSTANDPIC:
                case COP1STNDPIC:
                case ANTSTANDPIC:
                case MASTANDPIC:
                case DISTANDPIC:
                case ERSTANDPIC:
                case SUNGSTANDPIC:
                case COWSTANDPIC:
                case COPBSTANDPIC:
                case NIKASTANDPIC:
                case REBRSTANDPIC:
                case TRENSTANDPIC:
                case WINGSTANDPIC:
                case HALTSTANDPIC:
                case REDHSTANDPIC:
                case ORANSTANDPIC:
                case BLKSSTANDPIC:
                case SFROSTANDPIC:
                case SSALSTANDPIC:
                case SGOLSTANDPIC:
                case SWATSTANDPIC:
                case PROBE1:
                case RS232:

                case JAKEPAINPIC:
                case RUBPAINPIC:
                case FRGPAINPIC:
                case SAMPAINPIC:
                case COP1PAINPIC:
                case ANTPAINPIC:
                case MAPAINPIC:
                case ERPAINPIC:
                case SUNGPAINPIC:
                case COWPAINPIC:
                case NIKAPAINPIC:
                case REBRPAINPIC:
                case TRENPAINPIC:
                case HALTPAINPIC:
                case ORANPAINPIC:
                case BLKSPAINPIC:
                case SFROPAINPIC:
                case SGOLPAINPIC:
                case SWATPAINPIC:
                case JAKEDEATHPIC:
                case JAKEDEATHPIC + 1:
                case JAKEDEATHPIC + 2:
                case JAKEDEATHPIC + 3:
                case JAKEDEATHPIC + 4:
                case JAKEDEATHPIC + 5:
                case JAKEDEATHPIC + 6:
                case JAKEDEATHPIC + 7:
                case JAKEDEATHPIC + 8:
                case AUTOGUN:
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + k);
                        tspr.setCstat(tspr.getCstat() & ~4);
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + (8 - k));
                        tspr.setCstat(tspr.getCstat() | 4);
                    }
                    break;
                case RUBATAKPIC:
                case FRGATTACKPIC:
                case SAMATTACKPIC:
                case COPATTACKPIC:
                case ANTATTACKPIC:
                case MAATTACKPIC:
                case DIATTACKPIC:
                case SUNGATTACKPIC:
                case COWATTACKPIC:
                case COPBATTACKPIC:
                case NIKAATTACKPIC:
                case REBRATTACKPIC:
                case WINGATTACKPIC:
                case HALTATTACKPIC:
                case REDHATTACKPIC:
                case ORANATTACKPIC:
                case BLKSATTACKPIC:
                case SFROATTACKPIC:
                case SSALATTACKPIC:
                case SGOLATTACKPIC:
                case SWATATTACKPIC:
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k << 1));
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) << 1));
                        tspr.setCstat(tspr.getCstat() | 4);
                    }
                    break;
                case 3708:
                case 3709:
                    tspr.setShade(-128);
                    break;
                // mirrorman
                case 1079:
                case 1074:
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k));
                        tspr.setCstat(tspr.getCstat() | 4);
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k)));
                        tspr.setCstat(tspr.getCstat() & ~4); // clear x-flipping bit
                    }
                    break;
                case JAKEATTACKPIC:
                case JAKEATTACKPIC + 1:
                    if (k <= 4) {
                        tspr.setPicnum(tspr.getPicnum() + (k * 3));
                        tspr.setCstat(tspr.getCstat() & ~4);
                    } else {
                        tspr.setPicnum(tspr.getPicnum() + ((8 - k) * 3));
                        tspr.setCstat(tspr.getCstat() | 4);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
