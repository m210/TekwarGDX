package ru.m210projects.Tekwar;

import com.badlogic.gdx.Input.Keys;
import ru.m210projects.Build.settings.*;
import ru.m210projects.Build.input.GameKey;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.stream.IntStream;

import static ru.m210projects.Build.input.keymap.Keymap.*;
import static ru.m210projects.Tekwar.View.*;

public class Config extends GameConfig {

    public static final int[] defclassickeys = {
            Keys.UP,    //Move_Forward
            Keys.DOWN,    //Move_Backward
            Keys.LEFT,    //Turn_Left
            Keys.RIGHT,    //Turn_Right
            0,            //Turn_Around
            Keys.ALT_LEFT,  //Strafe
            Keys.COMMA,     //Strafe_Left
            Keys.SEMICOLON, //Strafe_Right
            Keys.X,        //
            Keys.C,        //Crouch
            Keys.SHIFT_LEFT, //Run
            Keys.SPACE, //Open
            Keys.CONTROL_LEFT,    //Weapon_Fire
            Keys.APOSTROPHE,//Next_Weapon
            Keys.SEMICOLON,    //Previous_Weapon
            Keys.PAGE_UP,    //Look_Up
            Keys.PAGE_DOWN, //Look_Down
            Keys.TAB,        //Map_Toggle
            Keys.EQUALS,    //Enlarge_Screen
            Keys.MINUS,        //Shrink_Screen
            Keys.M,        //Send_Message
            Keys.U,        //Mouse_Aiming
            Keys.ESCAPE,//Menu_open
            Keys.GRAVE,    //Console

            Keys.ENTER,    //Throw_Item
            Keys.HOME,        //Aim_Center
            Keys.R,        //Rearview
            Keys.E,    //Prepared_Item
            Keys.H,    //Health_Meter
            Keys.G,        //Toggle_Crosshair
            Keys.T,        //Elapsed_Time
            Keys.S,        //Score
            Keys.I,        //Inventory
            Keys.SLASH,    //Holster_Weapon
            Keys.F1, //Show_HelpScreen
            Keys.F2, //Show_SaveMenu
            Keys.F3, //Show_LoadMenu
            Keys.F4, //Show_SoundSetup
            Keys.F5, //Show_Options
            Keys.F6, //Quicksave
            Keys.F9, //Quickload
            Keys.F10,//Quit
            Keys.F11,//Gamma
            Keys.F12,//Make_Screenshot
            Keys.NUM_1, Keys.NUM_2, Keys.NUM_3, Keys.NUM_4, Keys.NUM_5, Keys.NUM_6, Keys.NUM_7, Keys.NUM_8
    };
    public static final int TOGGLE_TIME = 0;
    public static final int TOGGLE_SCORE = 1;
    public static final int TOGGLE_REARVIEW = 2;
    public static final int TOGGLE_UPRT = 3;
    public static final int TOGGLE_HEALTH = 4;
    public static final int TOGGLE_INVENTORY = 5;
    public int weaponIndex = -1;

    public static final int[] defkeys = {
            Keys.W,    //Move_Forward
            Keys.S,    //Move_Backward
            Keys.LEFT,    //Turn_Left
            Keys.RIGHT,    //Turn_Right
            Keys.BACKSPACE,            //Turn_Around
            Keys.ALT_LEFT,    //Strafe
            Keys.A,        //Strafe_Left
            Keys.D, //Strafe_Right
            Keys.SPACE,        //Jump
            Keys.CONTROL_LEFT,        //Crouch
            Keys.SHIFT_LEFT, //Run
            Keys.E, //Open
            0,    //Weapon_Fire
            Keys.APOSTROPHE,//Next_Weapon
            Keys.SEMICOLON,    //Previous_Weapon
            Keys.PAGE_UP,    //Look_Up
            Keys.PAGE_DOWN, //Look_Down
            Keys.TAB,        //Map_Toggle
            Keys.EQUALS,    //Enlarge_Screen
            Keys.MINUS,        //Shrink_Screen
            Keys.M,        //Send_Message
            Keys.U,        //Mouse_Aiming
            Keys.ESCAPE,//Menu_open
            Keys.GRAVE,    //Console


            Keys.ENTER,    //Throw_Item
            Keys.HOME,        //Aim_Center
            Keys.R,        //Rearview
            0,            //Prepared_Item
            Keys.H,    //Health_Meter
            Keys.G,        //Toggle_Crosshair
            Keys.T,        //Elapsed_Time
            0,            //Score
            Keys.I,        //Inventory
            Keys.SLASH,    //Holster_Weapon
            Keys.F1, //Show_HelpScreen
            Keys.F2, //Show_SaveMenu
            Keys.F3, //Show_LoadMenu
            Keys.F4, //Show_SoundSetup
            Keys.F5, //Show_Options
            Keys.F6, //Quicksave
            Keys.F9, //Quickload
            Keys.F10,//Quit
            Keys.F11,//Gamma
            Keys.F12,//Make_Screenshot
            Keys.NUM_1, Keys.NUM_2, Keys.NUM_3, Keys.NUM_4, Keys.NUM_5, Keys.NUM_6, Keys.NUM_7, Keys.NUM_8
    };

    public boolean gCrosshair = true;
    public boolean gHeadBob = true;
    public boolean gAutoRun = true;
    public boolean showMessages = false;
    public int gHUDSize = 65536;
    public int gStatSize = 65536;
    public int gCrossSize = 65536;
    public int gShowStat = 1;
    public boolean showCutscenes = true;
    public int showMapInfo = 1;
    public boolean gSaveWeapons = false;
    public final boolean[] toggles = new boolean[] {false, false, false, true, true, true};
    public int gOverlayMap = 2;

    public Config(Path path) {
        super(path);
    }

    public GameKey[] getKeyMap() {
        return new GameKey[]{GameKeys.Move_Forward, GameKeys.Move_Backward, GameKeys.Turn_Left, GameKeys.Turn_Right, GameKeys.Turn_Around, GameKeys.Open, GameKeys.Run, GameKeys.Strafe, GameKeys.Weapon_Fire, GameKeys.Jump, GameKeys.Crouch, GameKeys.Look_Up, GameKeys.Look_Down, GameKeys.Strafe_Left, GameKeys.Strafe_Right, GameKeys.Map_Toggle, TekKeys.Throw_Item, GameKeys.Enlarge_Screen, GameKeys.Shrink_Screen, GameKeys.Send_Message, TekKeys.Aim_Center, TekKeys.Rearview, TekKeys.Prepared_Item, TekKeys.Health_Meter, TekKeys.Toggle_Crosshair, TekKeys.Elapsed_Time, TekKeys.Score, TekKeys.Inventory, TekKeys.Weapon_1, TekKeys.Weapon_2, TekKeys.Weapon_3, TekKeys.Weapon_4, TekKeys.Weapon_5, TekKeys.Weapon_6, TekKeys.Weapon_7, TekKeys.Weapon_8, GameKeys.Next_Weapon, GameKeys.Previous_Weapon, TekKeys.Holster_Weapon, GameKeys.Mouse_Aiming, GameKeys.Show_Console, TekKeys.Show_HelpScreen, TekKeys.Show_SaveMenu, TekKeys.Show_LoadMenu, TekKeys.Show_SoundSetup, TekKeys.Show_Options, TekKeys.Quicksave, TekKeys.Quickload, TekKeys.Quit, TekKeys.Gamma, TekKeys.Make_Screenshot, GameKeys.Menu_Toggle};
    }
    
    @Override
    protected InputContext createDefaultInputContext() {
        return new InputContext(getKeyMap(), defkeys, defclassickeys) {

            @Override
            protected void clearInput() {
                super.clearInput();
                weaponIndex = IntStream.range(0, keymap.length).filter(i -> keymap[i].equals(TekKeys.Weapon_1)).findFirst().orElse(-1);
            }

            @Override
            public void resetInput(boolean classicKeys) {
                super.resetInput(classicKeys);

                primarykeys[MOUSE_KEYS_INDEX][TekKeys.Holster_Weapon.getNum()] = MOUSE_RBUTTON;
                primarykeys[MOUSE_KEYS_INDEX][GameKeys.Jump.getNum()] = MOUSE_MBUTTON;
            }
        };
    }

    @Override
    protected ConfigContext createDefaultGameContext() {
        return new ConfigContext() {
            @Override
            public void load(Properties prop) {
                if (prop.setContext("Options")) {
                    gHUDSize = prop.getIntValue("HUDSize", gHUDSize);
                    gCrosshair = prop.getBooleanValue("Crosshair", gCrosshair);
                    showMessages = prop.getBooleanValue("Show_Messages", showMessages);
                    gAutoRun = prop.getBooleanValue("AutoRun", gAutoRun);
                    gHeadBob = prop.getBooleanValue("HeadBobbing", gHeadBob);
                    gStatSize = Math.max(16384, prop.getIntValue("StatSize", gStatSize));
                    gCrossSize = Math.max(16384, prop.getIntValue("CrossSize", gCrossSize));
                    gShowStat = prop.getIntValue("ShowStat", gShowStat);
                    showCutscenes = prop.getBooleanValue("showCutscenes", showCutscenes);
                    showMapInfo = prop.getIntValue("showMapInfo", showMapInfo);
                    gOverlayMap = prop.getIntValue("OverlayMap", gOverlayMap);
                    gSaveWeapons = prop.getBooleanValue("Save_weapons", gSaveWeapons);

                    toggles[TOGGLE_REARVIEW]  = prop.getBooleanValue("Rearview", toggles[TOGGLE_REARVIEW]);
                    toggles[TOGGLE_UPRT] = prop.getBooleanValue("Prepared_Item", toggles[TOGGLE_UPRT]);
                    toggles[TOGGLE_HEALTH]  = prop.getBooleanValue("Health_Meter",  toggles[TOGGLE_HEALTH]);
                    toggles[TOGGLE_TIME] = prop.getBooleanValue("Elapsed_Time", toggles[TOGGLE_TIME]);
                    toggles[TOGGLE_SCORE] = prop.getBooleanValue("Score",  toggles[TOGGLE_SCORE]);
                    toggles[TOGGLE_INVENTORY] = prop.getBooleanValue("Inventory",   toggles[TOGGLE_INVENTORY]);

                    rvmoving = toggles[TOGGLE_REARVIEW] ? 1 : 0;
                    wpmoving = toggles[TOGGLE_UPRT] ? 1 : 0;
                    hcmoving = toggles[TOGGLE_HEALTH] ? 1 : 0;
                }
            }

            @Override
            public void save(OutputStream os) throws IOException {
                putString(os, "[Options]\r\n");
                //Options

                putInteger(os, "HUDSize", gHUDSize);
                putBoolean(os, "Crosshair", gCrosshair);
                putBoolean(os, "Show_Messages", showMessages);
                putBoolean(os, "AutoRun", gAutoRun);
                putBoolean(os, "HeadBobbing", gHeadBob);
                putString(os, "AdultPassword = " + "\"\"" + "\r\n");
                putInteger(os, "StatSize", gStatSize);
                putInteger(os, "CrossSize", gCrossSize);
                putInteger(os, "ShowStat", gShowStat);
                putBoolean(os, "showCutscenes", showCutscenes);
                putInteger(os, "showMapInfo", showMapInfo);
                putInteger(os, "OverlayMap", gOverlayMap);
                putBoolean(os, "Save_weapons", gSaveWeapons);
                putBoolean(os, "Rearview", toggles[TOGGLE_REARVIEW]);
                putBoolean(os, "Prepared_Item", toggles[TOGGLE_UPRT]);
                putBoolean(os, "Health_Meter", toggles[TOGGLE_HEALTH]);
                putBoolean(os, "Elapsed_Time", toggles[TOGGLE_TIME]);
                putBoolean(os, "Score", toggles[TOGGLE_SCORE]);
                putBoolean(os, "Inventory", toggles[TOGGLE_INVENTORY]);
            }
        };
    }
    public enum TekKeys implements GameKey {

        Throw_Item,
        Aim_Center,
        Rearview,
        Prepared_Item,
        Health_Meter,
        Toggle_Crosshair,
        Elapsed_Time,
        Score,
        Inventory,
        Holster_Weapon,
        Show_HelpScreen,
        Show_SaveMenu,
        Show_LoadMenu,
        Show_SoundSetup,
        Show_Options,
        Quicksave,
        Quickload,
        Quit,
        Gamma,
        Make_Screenshot,
        Weapon_1,
        Weapon_2,
        Weapon_3,
        Weapon_4,
        Weapon_5,
        Weapon_6,
        Weapon_7,
        Weapon_8
        ;

        public int getNum() {
            return GameKeys.values().length + ordinal();
        }

        public String getName() {
            return name();
        }

    }

}
