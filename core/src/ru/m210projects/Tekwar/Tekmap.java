package ru.m210projects.Tekwar;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.Entry;

import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Tekwar.Globals.gDifficulty;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekchng.stun;
import static ru.m210projects.Tekwar.Tekgun.*;
import static ru.m210projects.Tekwar.Tekprep.spriteXT;
import static ru.m210projects.Tekwar.Teksnd.*;
import static ru.m210projects.Tekwar.Tekstat.validext;
import static ru.m210projects.Tekwar.View.*;

public class Tekmap {

    public static final boolean MUSICINMATRIX = true;

    public static final int MAXSYMBOLS = 7;
//    public static final int TOTALMISSIONS = 7;
    public static final int MAXMAPSPERMISSION = 6;
    public static final int TOTALMAPS = 32;
    public static final String[] mapnames = {"subway0.map", // 0
            "subway1.map", // 1
            "subway2.map", // 2
            "subway3.map", // 3
            "level1.map", // 4
            "level2.map", // 5
            "", // 6
            "", // 7
            "city1.map", // 8
            "", // 9
            "city,map", // 10
            "beach1.map", // 11
            "park1.map", // 12
            "", // 13
            "", // 14
            "mid1.map", // 15
            "mid2.map", // 16
            "mid3.map", // 17
            "sewer1.map", // 18
            "sewer2.map", // 19
            "inds1.map", // 20
            "", // 21
            "free1.map", // 22
            "free2.map", // 23
            "", // 24
            "ware1.map", // 25
            "ware2.map", // 26
            "ware3.map", // 27
            "", // 28
            "", // 29
            "final1.map", // 30
            "" // 31
    };
    public static final boolean[] symbols = new boolean[MAXSYMBOLS];
    public static final boolean[] symbolsdeposited = new boolean[MAXSYMBOLS];
    public static int allsymsdeposited = 0;
    public static char generalplay = 0;
    public static char singlemapmode;
    public static int killedsonny = 0;
    public static int civillianskilled = 0;
    public static char mission_accomplished = 0;
    public static char gameover = 0;
    public static char numlives = 0;
    public static int mission = 0;
    public static int warpretx, warprety;
    public static short warpretang, warpretsect;
    public static int currentmapno = 0;
    static final int[][] missionset = {
            // SUB M1 M2 M3 M4 MTRX
            {0, 4, 5, -1, 10, -1}, // mission 0
            {0, 8, -1, -1, -1, -1}, // mission 1
            {1, 11, 12, -1, -1, -1}, // mission 2
            {1, 15, 16, 17, -1, -1}, // mission 3
            {2, 18, 19, 20, -1, -1}, // mission 4
            {2, 22, 23, -1, -1, -1}, // mission 5
            {3, 25, 26, 27, -1, -1}, // mission 6
    };

    public static int mapcoords(int cmap) {
        /*
         * "level1.map", // 4 "level2.map", // 5 "city1.map", // 8
         *
         * "beach1.map", // 11 "park1.map", // 12 "mid1.map", // 15 "mid2.map", // 16
         * "mid3.map", // 17
         *
         * "sewer1.map", // 18 "sewer2.map", // 19 "inds1.map", // 20 "free1.map", // 22
         * "free2.map", // 23
         *
         * "ware1.map", // 25 "ware2.map", // 26 "ware3.map", // 27
         */
        int rv;

        switch (cmap) {
            case 4:
                rv = 0;
                warpretx = -27200;
                warprety = 21500;
                warpretang = 512;
                warpretsect = 351;
                break;
            case 5:
                rv = 0;
                warpretx = 7295;
                warprety = 21500;
                warpretang = 512;
                warpretsect = 349;
                break;
            case 8:
                rv = 0;
                warpretx = -11902;
                warprety = 39300;
                warpretang = 1536;
                warpretsect = 353;
                break;
            case 11:
                rv = 1;
                warpretx = -27200;
                warprety = 21500;
                warpretang = 512;
                warpretsect = 489;
                break;
            case 12:
                rv = 1;
                warpretx = 7295;
                warprety = 21500;
                warpretang = 512;
                warpretsect = 492;
                break;
            case 15:
                rv = 1;
                warpretx = -11904;
                warprety = 39300;
                warpretang = 1536;
                warpretsect = 488;
                break;
            case 16:
                rv = 1;
                warpretx = 24322;
                warprety = 39300;
                warpretang = 1536;
                warpretsect = 486;
                break;
            case 17:
                rv = 1;
                warpretx = 57346;
                warprety = 39300;
                warpretang = 1536;
                warpretsect = 483;
                break;
            case 18:
                rv = 2;
                warpretx = -27200;
                warprety = 21500;
                warpretang = 512;
                warpretsect = 486;
                break;
            case 19:
                rv = 2;
                warpretx = 7295;
                warprety = 21500;
                warpretang = 512;
                warpretsect = 488;
                break;
            case 20:
                rv = 2;
                warpretx = 41600;
                warprety = 21500;
                warpretang = 512;
                warpretsect = 490;
                break;
            case 22:
                rv = 2;
                warpretx = -11904;
                warprety = 39300;
                warpretang = 1536;
                warpretsect = 483;
                break;
            case 23:
                rv = 2;
                warpretx = 24380;
                warprety = 39300;
                warpretang = 1536;
                warpretsect = 482;
                break;
            case 25:
                rv = 3;
                warpretx = -27200;
                warprety = 21500;
                warpretang = 512;
                warpretsect = 477;
                break;
            case 26:
                rv = 3;
                warpretx = 7295;
                warprety = 21500;
                warpretang = 512;
                warpretsect = 472;
                break;
            case 27:
                rv = 3;
                warpretx = 41600;
                warprety = 21500;
                warpretang = 512;
                warpretsect = 494;
                break;
            default:
                rv = -1;
                break;
        }

        return (rv);
    }

    public static void changemap(final int mapno) {
        final int p = myconnectindex;

        if (game.nNetMode == NetMode.Multiplayer) {
            return;
        }

        if (mUserFlag == UserFlag.UserMap) {
            gameover = 1;
//			fadein();
            return;
        }

        if (mapno >= TOTALMAPS) {
            return;
        }

        final int newmap;
        switch (mapno) {
            case 0:
            case 1:
            case 2:
            case 3:
                newmap = mapcoords(currentmapno);
                if (newmap != mapno) {
                    throw new AssertException("bad return map");
                }
                break;
        }

        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();
        printext((xdim >> 1) - 44, (ydim >> 1) - 32, "LOADING MAP \0".toCharArray());

        resetEffects();

        int savhealth = gPlayer[p].health;
        int savestun = stun[p];
        int savereds = gPlayer[p].invredcards;
        int saveblues = gPlayer[p].invbluecards;
        int saveaccutrk = gPlayer[p].invaccutrak;

        final short[] saveammo = new short[8];
        if (tekcfg.gSaveWeapons) {
            System.arraycopy(gPlayer[p].ammo, 0, saveammo, 0, 8);
        }
        final int saveweap = gPlayer[p].weapons;
        final int savefireseq = gPlayer[p].fireseq;

        newgame(game.cache.getEntry(mapnames[mapno], true), () -> {
            gPlayer[p].health = savhealth;
            stun[p] = savestun;
            gPlayer[p].invredcards = savereds;
            gPlayer[p].invbluecards = saveblues;
            gPlayer[p].invaccutrak = saveaccutrk;

            if (tekcfg.gSaveWeapons) {
                System.arraycopy(saveammo, 0, gPlayer[p].ammo, 0, 8);
                gPlayer[p].weapons = saveweap;
                gPlayer[p].fireseq = savefireseq;
            }

            switch (mapno) {
                case 0:
                case 1:
                case 2:
                case 3: {
                    int sn = gPlayer[p].playersprite;
                    Sprite spr = boardService.getSprite(sn);
                    if (spr == null) {
                        break;
                    }

                    spr.setX(warpretx);
                    spr.setY(warprety);
                    spr.setAng(warpretang);
                    game.pInt.setsprinterpolate(sn, spr);
                    engine.changespritesect(sn, warpretsect);

                    gPlayer[p].posx = spr.getX();
                    gPlayer[p].posy = spr.getY();
                    gPlayer[p].ang = spr.getAng();
                    gPlayer[p].cursectnum = spr.getSectnum();
                    break;
                }
                default:
                    break;
            }

            game.pNet.gInput.reset();

            currentmapno = mapno;

            showmessage(mapnames[mapno]);

            if (mapno <= 3) {
                menusong(1);
            } else {
                startmusic(mission);
            }
        });
    }

    public static int missionfailed() {
        if (game.nNetMode == NetMode.Multiplayer) {
            return (0);
        }

        numlives++;

        switch (gDifficulty) {
            case 0:
            case 1:
                if (numlives < 6) {
                    return (0);
                }
                break;
            case 2:
                if (numlives < 4) {
                    return (0);
                }
                break;
            default:
                if (numlives < 2) {
                    return (0);
                }
                break;
        }

        mission_accomplished = 0;
        gameover = 1;
//		fadein();
        return (1);
    }

    public static void missionaccomplished(final int sn) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null || game.nNetMode == NetMode.Multiplayer) {
            return;
        }

        int ext = spr.getExtra();
        if (validext(ext) == 0) {
            return;
        }

        if (spriteXT[ext].sclass == CLASS_CIVILLIAN) {
            civillianskilled++;
        }

        if (TEKDEMO && spriteXT[ext].sclass == CLASS_TEKBOSS) {
            if (spriteXT[ext].deathpic == DIDEATHPIC) {
                mission_accomplished = 1;
                gameover = 1;
//				fadein();
            }
        }

        if (spriteXT[ext].sclass != CLASS_TEKLORD) {
            return;
        }

        if (mUserFlag == UserFlag.UserMap) {
            gameover = 1;
        }

//		fadein();
        if (mUserFlag == UserFlag.None) {
            switch (spriteXT[ext].deathpic) {
                case WINGDEATHPIC:
                    symbols[0] = true;
                    mission_accomplished = 1;
                    gameover = 1;
                    break;
                case DIDEATHPIC:
                    symbols[1] = true;
                    mission_accomplished = 1;
                    gameover = 1;
                    break;
                case SFRODEATHPIC:
                    symbols[2] = true;
                    mission_accomplished = 1;
                    gameover = 1;
                    break;
                case ANTDEATHPIC:
                    symbols[3] = true;
                    mission_accomplished = 1;
                    gameover = 1;
                    break;
                case SGOLDEATHPIC:
                    symbols[4] = true;
                    mission_accomplished = 1;
                    gameover = 1;
                    break;
                case SUNGDEATHPIC:
                    symbols[5] = true;
                    mission_accomplished = 1;
                    gameover = 1;
                    break;
                case REDHDEATHPIC:
                    symbols[6] = true;
                    mission_accomplished = 1;
                    gameover = 1;
                    break;
                case SSALDEATHPIC:
                    killedsonny = 1;
                    mission_accomplished = 1;
                    gameover = 1;
                    break;
            }
        }
    }

    public static void newgame(Entry map, Runnable callback) {
        if (game.nNetMode == NetMode.Multiplayer) {
            return;
        }

        stopallsounds();
        resetEffects();
        boardfilename = map;
        gGameScreen.loadboard(boardfilename, callback);
    }

    public static void donewgame() {
        gameover = 0;
        numlives = 0;
        civillianskilled = 0;
        mission_accomplished = 0;
        currentmapno = 0;

        Entry entry = DUMMY_ENTRY;
        switch (mission) {
            case 0:
            case 1:
                entry = game.getCache().getEntry("subway0.map", true);
                break;
            case 2:
            case 3:
                entry = game.getCache().getEntry("subway1.map", true);
                break;
            case 4:
            case 5:
                entry = game.getCache().getEntry("subway2.map", true);
                break;
            case 6:
                entry = game.getCache().getEntry("subway3.map", true);
                break;
            case 7:
                entry = game.getCache().getEntry("matrix.map", true);
                break;
            case 8:
                entry = game.getCache().getEntry("load.map", true);
                break;
            case 9:
                entry = game.getCache().getEntry("final1.map", true);
                break;
            case 10: // DEMO VERSION
                entry = game.getCache().getEntry("city.map", true);
                break;
        }

        newgame(entry, null);
        game.getRenderer().clearview(0);
        resetEffects();

        if (mission == 7) {
            if (MUSICINMATRIX) {
                startmusic((int) (7 * Math.random()));
            }
        } else {
            menusong(1);
        }

        // if matrix, reset time
        if (mission == 7) {
            seconds = minutes = hours = 0;
        }
    }

    public static int accessiblemap(int mn) {
        if ((mn < 0) || (mn >= TOTALMAPS)) {
            return (0);
        }
        if (mapnames[mn].length() < 2) {
            return (0);
        }
        for (int i = 0; i < MAXMAPSPERMISSION; i++) {
            if (mission < 7 && missionset[mission][i] == mn) {
                return (1);
            }
        }
        return (0);
    }

    public static String debriefing() {
        String name = null;

        if (mUserFlag == UserFlag.UserMap || gameover == 2) {
            return null;
        }

        if (mission_accomplished != 0) {
            if (civillianskilled == 0) {
                switch (mission) {
                    case 2:
                        name = "ROSSI2.SMK";
                        break;
                    case 1:
                        name = "DIMARCO2.SMK";
                        break;
                    case 5:
                        name = "CONNOR2.SMK";
                        break;
                    case 4:
                        name = "SONNY2.SMK";
                        break;
                    case 6:
                        name = "JANUS2.SMK";
                        break;
                    case 3:
                        name = "LOWELL2.SMK";
                        break;
                    case 0:
                        name = "DOLLAR2.SMK";
                        break;
                }
            } else {
                switch (mission) {
                    case 2:
                        name = "ROSSI3.SMK";
                        break;
                    case 1:
                        name = "DIMARCO3.SMK";
                        break;
                    case 5:
                        name = "CONNOR3.SMK";
                        break;
                    case 4:
                        name = "SONNY3.SMK";
                        break;
                    case 6:
                        name = "JANUS3.SMK";
                        break;
                    case 3:
                        name = "LOWELL3.SMK";
                        break;
                    case 0:
                        name = "DOLLAR3.SMK";
                        break;
                }
            }
        } else {
            if (civillianskilled == 0) {
                switch (mission) {
                    case 2:
                        name = "ROSSI4.SMK";
                        break;
                    case 1:
                        name = "DIMARCO4.SMK";
                        break;
                    case 5:
                        name = "CONNOR4.SMK";
                        break;
                    case 4:
                        name = "SONNY4.SMK";
                        break;
                    case 6:
                        name = "JANUS4.SMK";
                        break;
                    case 3:
                        name = "LOWELL4.SMK";
                        break;
                    case 0:
                        name = "DOLLAR4.SMK";
                        break;
                }
            } else {
                switch (mission) {
                    case 2:
                        name = "ROSSI5.SMK";
                        break;
                    case 1:
                        name = "DIMARCO5.SMK";
                        break;
                    case 5:
                        name = "CONNOR5.SMK";
                        break;
                    case 4:
                        name = "SONNY5.SMK";
                        break;
                    case 6:
                        name = "JANUS5.SMK";
                        break;
                    case 3:
                        name = "LOWELL5.SMK";
                        break;
                    case 0:
                        name = "DOLLAR5.SMK";
                        break;
                }
            }
        }
        return name;
    }
}
