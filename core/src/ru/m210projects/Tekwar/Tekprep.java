package ru.m210projects.Tekwar;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Tekwar.Types.SpriteXT;
import ru.m210projects.Tekwar.Types.Startspottype;

import java.util.Arrays;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Tekwar.Animate.gAnimationCount;
import static ru.m210projects.Tekwar.Globals.*;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Names.STARTPOS;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekchng.fallz;
import static ru.m210projects.Tekwar.Tekchng.stun;
import static ru.m210projects.Tekwar.Tekgun.restockammo;
import static ru.m210projects.Tekwar.Tekmap.*;
import static ru.m210projects.Tekwar.Tekstat.*;
import static ru.m210projects.Tekwar.Tektag.*;
import static ru.m210projects.Tekwar.View.*;

public class Tekprep {
    public static final int MAXSTARTSPOTS = 16;

    public static final SpriteXT[] spriteXT = new SpriteXT[MAXSPRITES];
    public static int startx, starty, startz, starta, starts;
    public static final int[] subwaysound = new int[4];
    static int firsttimethru = 1;
    static int switchlevelsflag;
    static int startspotcnt;
    static final Startspottype[] startspot = new Startspottype[MAXSTARTSPOTS];
    static final short[] dieframe = new short[MAXPLAYERS];
    static final boolean RESETSCORE = false;

    public static void tekpreinit() {
        game.getCache().getEntry("lookup.dat", true).load(is -> {
            int num_tables = StreamUtils.readUnsignedByte(is);
            for (int j = 0; j < num_tables; j++) {
                int lookup_num = StreamUtils.readUnsignedByte(is);
                engine.getPaletteManager().makePalookup(lookup_num, StreamUtils.readBytes(is, 256), 0, 0, 0, 1);
            }
        });

        if ((game.nNetMode == NetMode.Multiplayer)) {
            game.getCache().getEntry("nlookup.dat", true).load(is -> {
                int num_tables = StreamUtils.readUnsignedByte(is);
                for (int j = 0; j < num_tables; j++) {
                    int lookup_num = StreamUtils.readUnsignedByte(is);
                    engine.getPaletteManager().makePalookup(lookup_num, StreamUtils.readBytes(is, 256), 0, 0, 0, 1);
                }
            });
        }

//        engine.getPaletteManager().makePalookup(255, tempbuf, 60, 60, 60, 1);

        pskyoff[0] = 0; // 2 tiles
        pskyoff[1] = 1;
        pskyoff[2] = 2;
        pskyoff[3] = 3;
        pskybits = 2; // tile 4 times, every 90 deg.
        parallaxtype = 1;
        Renderer renderer = game.getRenderer();
        renderer.setParallaxOffset(112);
        if (game.nNetMode == NetMode.Multiplayer) {
            gDifficulty = 2;
        }

    }

    public static void initplayersprite(short snum) {
        if (gPlayer[snum].playersprite >= 0) {
            return;
        }

        int i = jsinsertsprite(gPlayer[snum].cursectnum, 8);
        Sprite spr2 = boardService.getSprite(i);
        if (spr2 == null) {
            Console.out.println("initplayersprite: jsinsertsprite on player " + snum + " failed", OsdColor.RED);
            return;
        }

        gPlayer[snum].playersprite = (short) i;
        spr2.setX(gPlayer[snum].posx);
        spr2.setY(gPlayer[snum].posy);
        spr2.setZ(gPlayer[snum].posz + (KENSPLAYERHEIGHT << 8));
        spr2.setCstat(0x101);
        spr2.setShade(0);
        if (game.nNetMode != NetMode.Multiplayer) {
            spr2.setPal(0);
        } else {
            spr2.setPal((byte) (snum + 16));
        }
        spr2.setClipdist(32);
        spr2.setXrepeat(24);
        spr2.setYrepeat(24);
        spr2.setXoffset(0);
        spr2.setYoffset(0);
        spr2.setPicnum(DOOMGUY);
        spr2.setAng((short) gPlayer[snum].ang);
        spr2.setXvel(0);
        spr2.setYvel(0);
        spr2.setZvel(0);
        spr2.setOwner((short) (snum + 4096));
        spr2.setSectnum(gPlayer[snum].cursectnum);
        spr2.setStatnum(8);
        spr2.setLotag(0);
        spr2.setHitag(0);
        // important to set extra = -1
        spr2.setExtra(-1);

        gPlayer[snum].pInput.reset();

        tekrestoreplayer(snum);
    }

    public static void tekrestoreplayer(short snum) {
        resetEffects();
        engine.setsprite(gPlayer[snum].playersprite, gPlayer[snum].posx, gPlayer[snum].posy,
                gPlayer[snum].posz + (KENSPLAYERHEIGHT << 8));

        Sprite spr = boardService.getSprite(gPlayer[snum].playersprite);
        if (spr == null) {
            return;
        }

        spr.setAng((int) gPlayer[snum].ang);
        spr.setXrepeat(24);
        spr.setYrepeat(24);
        gPlayer[snum].horiz = 100;
        gPlayer[snum].health = MAXHEALTH;
        gPlayer[snum].fireseq = 0;
        restockammo(snum);
        stun[snum] = MAXSTUN;
        fallz[snum] = 0;
        gPlayer[snum].drawweap = 0;
        gPlayer[snum].invredcards = 0;
        gPlayer[snum].invbluecards = 0;
        gPlayer[snum].invaccutrak = 0;
        dieframe[snum] = 0;
        if (game.nNetMode == NetMode.Multiplayer) {
            gPlayer[snum].weapons = 0;
            gPlayer[snum].weapons = flags32[GUN2FLAG] | flags32[GUN3FLAG];
        } else {
            gPlayer[snum].weapons = 0;
            if (TEKDEMO) {
                gPlayer[snum].weapons = flags32[GUN1FLAG] | flags32[GUN2FLAG];
            } else {
                gPlayer[snum].weapons = flags32[GUN1FLAG] | flags32[GUN2FLAG] | flags32[GUN3FLAG];
            }
        }
    }

    public static boolean prepareboard(Entry daboardfilename) {
        try {
            Board board = engine.loadboard(daboardfilename);

            BuildPos out = board.getPos();

            startx = out.x;
            starty = out.y;
            startz = out.z;
            starta = out.ang;
            starts = out.sectnum;

            boardfilename = daboardfilename;

            initsprites();
            if (firsttimethru != 0) {
                engine.srand(17);
            }

            gAnimationCount = 0;

            gViewMode = kView3D;
            zoom = 768;
            for (int i = 0; i < MAXPLAYERS; i++) {
                gPlayer[i].posx = startx;
                gPlayer[i].posy = starty;
                gPlayer[i].posz = startz;
                gPlayer[i].ang = starta;
                gPlayer[i].cursectnum = (short) starts;
                gPlayer[i].ocursectnum = (short) starts;
                gPlayer[i].horiz = 100;
                gPlayer[i].lastchaingun = 0;
                gPlayer[i].health = 100;
                if (RESETSCORE) {
                    gPlayer[i].score = 0;
                }

                gPlayer[i].numbombs = -1;
                gPlayer[i].deaths = 0;
                gPlayer[i].playersprite = -1;
                gPlayer[i].saywatchit = -1;
            }

            if (firsttimethru != 0) {
                for (int i = 0; i < MAXPLAYERS; i++) {
                    gPlayer[i].pInput.reset();
                }
            }

            for (int i = 0; i < MAXPLAYERS; i++) {
                waterfountainwall[i] = -1;
                waterfountaincnt[i] = 0;
                slimesoundcnt[i] = 0;
            }

            for (int i = 0; i < MAXPLAYERS; i++) {
                gPlayer[i].updategun = 1;
            }

            warpsectorcnt = 0; // Make a list of warping sectors
            xpanningsectorcnt = 0; // Make a list of wall x-panning sectors
            floorpanningcnt = 0; // Make a list of slime sectors
            dragsectorcnt = 0; // Make a list of moving platforms
            swingcnt = 0; // Make a list of swinging doors
            revolvecnt = 0; // Make a list of revolving doors
            subwaytrackcnt = 0; // Make a list of subways

            // intitialize subwaysound[]s
            for (int i = 0; i < 4; i++) {
                subwaysound[i] = -1;
            }

            // scan sector tags
            for (int i = 0; i < boardService.getSectorCount(); i++) {
                Sector sec = boardService.getSector(i);
                if (sec == null || sec.getWallNode() == null) {
                    continue;
                }

                Wall firstwall = sec.getWallNode().get();
                switch (sec.getLotag()) {
                    case 4:
                        if (floorpanningcnt < 64) {
                            floorpanninglist[floorpanningcnt++] = (short) i;
                        }
                        break;
                    case 5060:
                        if (game.nNetMode == NetMode.Multiplayer) {
                            sec.setLotag(0);
                        }
                        break;
                    case 25:
                        if ((singlemapmode != 0) || (generalplay != 0) || (game.nNetMode == NetMode.Multiplayer)) {
                            sec.setLotag(0);
                        }
                        break;
                    case 10:
                        if ((generalplay == 0) && (game.nNetMode != NetMode.Multiplayer) && (warpsectorcnt < 64)) {
                            warpsectorlist[warpsectorcnt++] = (short) i;
                        }
                        break;
                    case 11:
                        xpanningsectorlist[xpanningsectorcnt++] = (short) i;
                        break;
                    case 12: {
                        int dax = 0x7fffffff;
                        int day = 0x7fffffff;
                        int dax2 = 0x80000000;
                        int day2 = 0x80000000;
                        Wall kwall = null;
                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wall = wn.get();
                            if (wall.getX() < dax) {
                                dax = wall.getX();
                            }
                            if (wall.getY() < day) {
                                day = wall.getY();
                            }
                            if (wall.getX() > dax2) {
                                dax2 = wall.getX();
                            }
                            if (wall.getY() > day2) {
                                day2 = wall.getY();
                            }
                            if (wall.getLotag() == 3) {
                                kwall = wall;
                            }
                        }

                        if (kwall != null) {
                            if (kwall.getX() == dax) {
                                dragxdir[dragsectorcnt] = -16;
                            }
                            if (kwall.getY() == day) {
                                dragydir[dragsectorcnt] = -16;
                            }
                            if (kwall.getX() == dax2) {
                                dragxdir[dragsectorcnt] = 16;
                            }
                            if (kwall.getY() == day2) {
                                dragydir[dragsectorcnt] = 16;
                            }
                        }

                        Sector nextSec = boardService.getSector(firstwall.getNextsector());
                        if (nextSec != null) {
                            dragx1[dragsectorcnt] = 0x7fffffff;
                            dragy1[dragsectorcnt] = 0x7fffffff;
                            dragx2[dragsectorcnt] = 0x80000000;
                            dragy2[dragsectorcnt] = 0x80000000;

                            for (ListNode<Wall> wn = nextSec.getWallNode(); wn != null; wn = wn.getNext()) {
                                Wall wall = wn.get();
                                if (wall.getX() < dragx1[dragsectorcnt]) {
                                    dragx1[dragsectorcnt] = wall.getX();
                                }
                                if (wall.getY() < dragy1[dragsectorcnt]) {
                                    dragy1[dragsectorcnt] = wall.getY();
                                }
                                if (wall.getX() > dragx2[dragsectorcnt]) {
                                    dragx2[dragsectorcnt] = wall.getX();
                                }
                                if (wall.getY() > dragy2[dragsectorcnt]) {
                                    dragy2[dragsectorcnt] = wall.getY();
                                }
                            }

                            dragx1[dragsectorcnt] += firstwall.getX() - dax;
                            dragy1[dragsectorcnt] += (firstwall.getY() - day);
                            dragx2[dragsectorcnt] -= (dax2 - firstwall.getX());
                            dragy2[dragsectorcnt] -= (day2 - firstwall.getY());

                            dragfloorz[dragsectorcnt] = sec.getFloorz();
                            dragsectorlist[dragsectorcnt++] = (short) i;
                        }
                        break;
                    }
                    case 13:
                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wall = wn.get();
                            if (wall.getLotag() == 4) {
                                Wall wal4 = wall.getWall2().getWall2().getWall2().getWall2();
                                if ((wall.getX() == wal4.getX()) && (wall.getY() == wal4.getY())) { // Door opens counterclockwise
                                    swingwall[swingcnt][0] = (short) wn.getIndex();
                                    swingwall[swingcnt][1] = wall.getPoint2();
                                    swingwall[swingcnt][2] = wall.getWall2().getPoint2();
                                    swingwall[swingcnt][3] = wall.getWall2().getWall2().getPoint2();
                                    swingangopen[swingcnt] = 1536;
                                    swingangclosed[swingcnt] = 0;
                                    swingangopendir[swingcnt] = -1;
                                } else { // Door opens clockwise
                                    swingwall[swingcnt][0] = wall.getPoint2();
                                    swingwall[swingcnt][1] = (short) wn.getIndex();
                                    swingwall[swingcnt][2] = (short) engine.lastwall(wn.getIndex());
                                    swingwall[swingcnt][3] = (short) engine.lastwall(swingwall[swingcnt][2]);
                                    swingwall[swingcnt][4] = (short) engine.lastwall(swingwall[swingcnt][3]);
                                    swingangopen[swingcnt] = 512;
                                    swingangclosed[swingcnt] = 0;
                                    swingangopendir[swingcnt] = 1;
                                }

                                for (int k = 0; k < 4; k++) {
                                    Wall swingWall = boardService.getWall(swingwall[swingcnt][k]);
                                    if (swingWall != null) {
                                        swingx[swingcnt][k] = swingWall.getX();
                                        swingy[swingcnt][k] = swingWall.getY();
                                    }
                                }

                                swingsector[swingcnt] = (short) i;
                                swingang[swingcnt] = swingangclosed[swingcnt];
                                swinganginc[swingcnt] = 0;
                                swingcnt++;
                            }
                        }
                        break;
                    case 14: {
                        int dax = 0;
                        int day = 0;

                        int startwall = sec.getWallptr();
                        int endwall = (short) (startwall + sec.getWallnum() - 1);

                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wall = wn.get();
                            dax += wall.getX();
                            day += wall.getY();
                        }

                        revolvepivotx[revolvecnt] = dax / (endwall - startwall + 1);
                        revolvepivoty[revolvecnt] = day / (endwall - startwall + 1);

                        int k = 0;
                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wall = wn.get();
                            revolvex[revolvecnt][k] = wall.getX();
                            revolvey[revolvecnt][k] = wall.getY();
                            k++;
                        }
                        revolvesector[revolvecnt] = (short) i;
                        revolveang[revolvecnt] = 0;

                        revolvecnt++;
                        break;
                    }
                    case 15:
                        subwaytracksector[subwaytrackcnt][0] = (short) i;
                        subwaystopcnt[subwaytrackcnt] = 0;
                        int dax = 0x7fffffff;
                        int day = 0x7fffffff;
                        int dax2 = 0x80000000;
                        int day2 = 0x80000000;
                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wall = wn.get();
                            if (wall.getX() < dax) {
                                dax = wall.getX();
                            }
                            if (wall.getY() < day) {
                                day = wall.getY();
                            }
                            if (wall.getX() > dax2) {
                                dax2 = wall.getX();
                            }
                            if (wall.getY() > day2) {
                                day2 = wall.getY();
                            }
                        }
                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wall = wn.get();
                            if (wall.getLotag() == 5) {
                                if ((wall.getX() > dax) && (wall.getY() > day) && (wall.getX() < dax2) && (wall.getY() < day2)) {
                                    subwayx[subwaytrackcnt] = wall.getX();
                                } else {
                                    subwaystop[subwaytrackcnt][subwaystopcnt[subwaytrackcnt]] = wall.getX();
                                    if (accessiblemap(wall.getHitag()) == 0) {
                                        subwaystop[subwaytrackcnt][subwaystopcnt[subwaytrackcnt]] = 0;
                                    }
                                    subwaystopcnt[subwaytrackcnt]++;
                                }
                            }
                        }
                        // de-sparse stoplist but keep increasing x order
                        for (int j = 0; j < subwaystopcnt[subwaytrackcnt]; j++) {
                            if (subwaystop[subwaytrackcnt][j] == 0) {
                                for (int l = j + 1; l < subwaystopcnt[subwaytrackcnt]; l++) {
                                    if (subwaystop[subwaytrackcnt][l] != 0) {
                                        subwaystop[subwaytrackcnt][j] = subwaystop[subwaytrackcnt][l];
                                        subwaystop[subwaytrackcnt][l] = 0;
                                        l = subwaystopcnt[subwaytrackcnt];
                                    }
                                }
                            }
                        }
                        // recount stopcnt
                        subwaystopcnt[subwaytrackcnt] = 0;
                        while (subwaystop[subwaytrackcnt][subwaystopcnt[subwaytrackcnt]] != 0) {
                            subwaystopcnt[subwaytrackcnt]++;
                        }

                        for (int j = 1; j < subwaystopcnt[subwaytrackcnt]; j++) {
                            for (int k = 0; k < j; k++) {
                                if (subwaystop[subwaytrackcnt][j] < subwaystop[subwaytrackcnt][k]) {
                                    int s = subwaystop[subwaytrackcnt][j];
                                    subwaystop[subwaytrackcnt][j] = subwaystop[subwaytrackcnt][k];
                                    subwaystop[subwaytrackcnt][k] = s;
                                }
                            }
                        }

                        subwaygoalstop[subwaytrackcnt] = 0;
                        for (int j = 0; j < subwaystopcnt[subwaytrackcnt]; j++) {
                            if (Math.abs(subwaystop[subwaytrackcnt][j] - subwayx[subwaytrackcnt]) < Math
                                    .abs(subwaystop[subwaytrackcnt][subwaygoalstop[subwaytrackcnt]] - subwayx[subwaytrackcnt])) {
                                subwaygoalstop[subwaytrackcnt] = j;
                            }
                        }

                        subwaytrackx1[subwaytrackcnt] = dax;
                        subwaytracky1[subwaytrackcnt] = day;
                        subwaytrackx2[subwaytrackcnt] = dax2;
                        subwaytracky2[subwaytrackcnt] = day2;

                        subwaynumsectors[subwaytrackcnt] = 1;
                        for (int j = 0; j < boardService.getSectorCount(); j++) {
                            if (j != i) {
                                Sector sec2 = boardService.getSector(j);
                                if (sec2 == null || sec2.getWallNode() == null) {
                                    continue;
                                }

                                Wall firstwall2 = sec2.getWallNode().get();
                                if (firstwall2.getX() > subwaytrackx1[subwaytrackcnt]) {
                                    if (firstwall2.getY() > subwaytracky1[subwaytrackcnt]) {
                                        if (firstwall2.getX() < subwaytrackx2[subwaytrackcnt]) {
                                            if (firstwall2.getY() < subwaytracky2[subwaytrackcnt]) {
                                                if (sec2.getLotag() == 16) {
                                                    sec2.setLotag(17); // Make special subway door
                                                }

                                                if (sec2.getFloorz() != sec.getFloorz()) {
                                                    sec2.setCeilingstat(sec2.getCeilingstat() | 64);
                                                    sec2.setFloorstat(sec2.getFloorstat() | 64);
                                                }
                                                subwaytracksector[subwaytrackcnt][subwaynumsectors[subwaytrackcnt]] = (short) j;
                                                subwaynumsectors[subwaytrackcnt]++;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        subwayvel[subwaytrackcnt] = 32; // orig 64
                        subwaypausetime[subwaytrackcnt] = 720;
                        subwaytrackcnt++;
                        break;
                }
            }

            // scan wall tags
            ypanningwallcnt = 0;
            for (int i = 0; i < boardService.getWallCount(); i++) {
                Wall wal = boardService.getWall(i);
                if (wal != null && wal.getLotag() == 1) {
                    ypanningwalllist[ypanningwallcnt++] = (short) i;
                }
            }

            // scan sprite tags&picnum's
            rotatespritecnt = 0;
            startspotcnt = 0;
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite spr = boardService.getSprite(i);
                if (spr == null) {
                    continue;
                }

                if (spr.getPicnum() == STARTPOS) {
                    if (startspotcnt < MAXSTARTSPOTS) {
                        if (startspot[startspotcnt] == null) {
                            startspot[startspotcnt] = new Startspottype();
                        }
                        startspot[startspotcnt].x = spr.getX();
                        startspot[startspotcnt].y = spr.getY();
                        startspot[startspotcnt].z = spr.getZ();
                        startspot[startspotcnt].sectnum = spr.getSectnum();
                        startspotcnt++;
                    }
                    jsdeletesprite((short) i);
                } else if (spr.getLotag() == 3) {
                    rotatespritelist[rotatespritecnt++] = (short) i;
                } else if (game.nNetMode == NetMode.Multiplayer) {
                    if (spr.getLotag() == 1009) {
                        jsdeletesprite((short) i);
                    }
                }
            }
            if ((startspotcnt == 0) && (game.nNetMode == NetMode.Multiplayer)) {
                System.err.println("no net startspots");
            }

            Arrays.fill(show2dsector.getArray(), (byte) 255);
            Arrays.fill(show2dwall.getArray(), (byte) 255);

            automapping = 0;
            // tags that make wall/sector not show up on 2D map
            for (int i = 0; i < MAXSECTORS; i++) {
                Sector sec2 = boardService.getSector(i);
                if (sec2 != null && sec2.getLotag() == 9901) {
                    show2dsector.clearBit(i);
                    for (ListNode<Wall> wn = sec2.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wall = wn.get();
                        show2dwall.clearBit(wn.getIndex());
                        if (wall.getNextwall() != -1) {
                            show2dwall.clearBit(wall.getNextwall());
                        }
                    }
                }
            }
            for (int i = 0; i < MAXWALLS; i++) {
                Wall wal = boardService.getWall(i);
                if (wal != null && wal.getLotag() == 9900) {
                    show2dwall.clearBit(i);
                }
            }

            if (firsttimethru != 0) {
                lockclock = 0;
            }

            if (game.nNetMode == NetMode.Multiplayer) {
                firsttimethru = 0;
            }

            tekpreptags();
            initspriteXTs();

            if (currentmapno == 0) {
                Sector sec = boardService.getSector(333);
                if (sec != null && sec.getLotag() == 5020 && sec.getHitag() == 901) {
                    sec.setLotag(0);
                    sec.setHitag(0);
                }
            }

            return true;
        } catch (Exception e) {
            Console.out.println("Load map exception: " + e);
            if (e.getMessage() != null) {
                game.GameMessage("Load map exception:\n" + e.getMessage());
            } else {
                game.GameMessage("Can't load the map " + daboardfilename);
            }
        }
        return false;
    }

}
