package ru.m210projects.Tekwar.Screens;

import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.MenuItems.MenuItem;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.Renderer;

public class TekMessageScreen extends MessageScreen {

    public TekMessageScreen(BuildGame game, String header, String message, MessageType type) {
        super(game, header, message, game.getFont(0), game.getFont(1), type);

        for (MenuItem item : messageItems) {
            item.pal = 4;
        }

        for (MenuItem item : variantItems) {
            item.pal = 4;
        }
    }

    @Override
    public void drawBackground(Renderer renderer) {
//        ArtEntry pic = renderer.getTile(BACKGROUND);
//        int xdim = renderer.getWidth();
//        int ydim = renderer.getHeight();
//
//        int frames = xdim / pic.getWidth();
//        int x = 160;
//        for (int i = 0; i <= frames; i++) {
//            renderer.rotatesprite(x << 16, 100 << 16, 0x10000, 0, BACKGROUND, 0, 5, 2 + 8 + 256, 0, 0, xdim - 1,
//                    ydim - 1);
//            x += pic.getWidth();
//        }

        renderer.rotatesprite(160 << 16, 100 << 16, 44000, 0, 319, 24, 5, 2 + 8);

    }
}
