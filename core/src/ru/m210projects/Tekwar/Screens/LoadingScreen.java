package ru.m210projects.Tekwar.Screens;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.LoadingAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.art.ArtEntry;

import static ru.m210projects.Tekwar.Names.BACKGROUND;

public class LoadingScreen extends LoadingAdapter {

    public LoadingScreen(BuildGame game) {
        super(game);
    }

    @Override
    public void draw(String title, float delta) {
        Renderer renderer = game.getRenderer();
        ArtEntry pic = renderer.getTile(BACKGROUND);
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        int frames = xdim / pic.getWidth();
        int x = 160;
        for (int i = 0; i <= frames; i++) {
            renderer.rotatesprite(x << 16, 100 << 16, 0x10000, 0, BACKGROUND, 0, 0, 2 + 8 + 256, 0, 0, xdim - 1,
                    ydim - 1);
            x += pic.getWidth();
        }

        game.getFont(0).drawTextScaled(renderer, 160, 100, "Loading...", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
    }

}
