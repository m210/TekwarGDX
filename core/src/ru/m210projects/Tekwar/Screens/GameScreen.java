package ru.m210projects.Tekwar.Screens;

import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.ScreenAdapters.GameAdapter;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Tekwar.Config.TekKeys;
import ru.m210projects.Tekwar.Factory.TekMenuHandler;
import ru.m210projects.Tekwar.Main;
import ru.m210projects.Tekwar.Player;
import ru.m210projects.Tekwar.Tekprep;
import ru.m210projects.Tekwar.Teksnd;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Tekwar.Animate.doanimations;
import static ru.m210projects.Tekwar.Factory.TekMenuHandler.*;
import static ru.m210projects.Tekwar.Globals.KENSPLAYERHEIGHT;
import static ru.m210projects.Tekwar.Globals.TICSPERFRAME;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekchng.*;
import static ru.m210projects.Tekwar.Tekgun.doweapanim;
import static ru.m210projects.Tekwar.Tekgun.tekfiregun;
import static ru.m210projects.Tekwar.Tekldsv.*;
import static ru.m210projects.Tekwar.Tekmap.*;
import static ru.m210projects.Tekwar.Tekprep.*;
import static ru.m210projects.Tekwar.Tekspr.checktouchsprite;
import static ru.m210projects.Tekwar.Tekstat.*;
import static ru.m210projects.Tekwar.Tektag.*;
import static ru.m210projects.Tekwar.View.*;

public class GameScreen extends GameAdapter {

    private final Main game;

    public GameScreen(Main game) {
        super(game, Main.gLoadingScreen);
        this.game = game;
        for (int i = 0; i < MAXPLAYERS; i++) {
            gPlayer[i] = new Player();
        }
        show2dsprite.setBit(gPlayer[myconnectindex].playersprite);
    }

    @Override
    public void show() {
        super.show();
        if (mission != 8) {
            pMenu.mClose();
        }
    }

    @Override
    public void PreFrame(BuildNet net) {
        if (gameover != 0) {
            net.ready2send = false;

            if (allsymsdeposited == 3) {
                if (killedsonny != 0) {
                    CompleteGame();
                } else {
                    game.gExit = true;
                }
                return;
            }

            String debriefing = debriefing();
            if (allsymsdeposited == 1) {
                debriefing = "FINALB.SMK";
                allsymsdeposited = 2;
            }

            CutsceneScreen gCutsceneScreen = new CutsceneScreen(game);
            if (debriefing != null && gCutsceneScreen.init(debriefing)) {
                gCutsceneScreen.setCallback(() -> game.changeScreen(gMissionScreen)).escSkipping(true);
                game.changeScreen(gCutsceneScreen);
            } else {
                game.changeScreen(gMissionScreen);
            }
        }

        if (gQuickSaving) {
            if (captBuffer != null) {
                savegame(game.getUserDirectory(), "[quicksave_" + quickslot + "]", "quicksav" + quickslot + ".sav");
                quickslot ^= 1;
                gQuickSaving = false;
            } else {
                gGameScreen.capture(160, 100);
            }
        }
    }

    @Override
    public void ProcessFrame(BuildNet net) {
        for (short i = connecthead; i >= 0; i = connectpoint2[i]) {
            gPlayer[i].pInput.Copy(net.gFifoInput[net.gNetFifoTail & 0xFF][i]);
        }
        net.gNetFifoTail++;

        if (game.gPaused || game.menu.gShowMenu || Console.out.isShowing()) {
            if (!game.menu.isOpened(game.menu.mMenus[LASTSAVE])) {
                return;
            }
        }

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            gPlayer[i].ocursectnum = gPlayer[i].cursectnum;
        }

//		if (recstat) {
//			for (int i = connecthead; i >= 0; i = connectpoint2[i])
//				pDemoInput[reccnt][i].copy(Player.input[i]);
//			reccnt++;
//			if (reccnt > 16383)
//				reccnt = 16383;
//		}

        ozoom = zoom;
        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            gPlayer[i].oposx = gPlayer[i].posx;
            gPlayer[i].oposy = gPlayer[i].posy;
            gPlayer[i].oposz = gPlayer[i].posz;
            gPlayer[i].ohoriz = gPlayer[i].horiz;
            gPlayer[i].oang = gPlayer[i].ang;
        }

        for (int i = 1; i <= 8; i++) {
            for (ListNode<Sprite> node = boardService.getStatNode(i); node != null; node = node.getNext()) {
                game.pInt.setsprinterpolate(node.getIndex(), node.get());
            }
        }

        for (short i = connecthead; i >= 0; i = connectpoint2[i]) {
            processinput(i);
            if (!gPlayer[i].noclip && gPlayer[i].cursectnum != -1) {
                checktouchsprite(i, gPlayer[i].cursectnum);
                Sector sec = boardService.getSector(gPlayer[i].cursectnum);
                if (sec != null) {
                    short startwall = sec.getWallptr();
                    int endwall = startwall + sec.getWallnum();
                    for (int j = startwall; j < endwall; j++) {
                        Wall wal = boardService.getWall(j);
                        if (wal != null && wal.getNextsector() >= 0) {
                            checktouchsprite(i, wal.getNextsector());
                        }
                    }
                }
            }
        }

        doanimations();
        tagcode();
        statuslistcode();
        dorearviewscreen();
        douprtscreen();
        dohealthscreen();
        doweapanim(screenpeek);
        tektime();
        updatepaletteshifts();

        int TICSPERFRAME = engine.getTimer().getFrameTicks(); // TODO: Temporaly code to reset interpolation in LegacyTimer
        lockclock += TICSPERFRAME;
    }

    @Override
    public void DrawHud(float smooth) {
        tekscreenfx((int) smooth);

        PICKUP_DAC.setIntensive(whitecount);
        DAMAGE_DAC.setIntensive(redcount);

        game.getRenderer().scrSetDac();
    }

    @Override
    public void DrawWorld(float smooth) {
        drawscreen(screenpeek, (int) smooth);
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        if (super.gameKeyDown(gameKey)) {
            return true;
        }

        TekMenuHandler menu = game.menu;
        if (GameKeys.Menu_Toggle.equals(gameKey)) {
            menu.mOpen(menu.mMenus[MAIN], -1);
        }

        // non mappable function keys
        if (TekKeys.Show_HelpScreen.equals(gameKey)) {
            menu.mOpen(menu.mMenus[HELP], -1);
        } else if (TekKeys.Show_SaveMenu.equals(gameKey)) {
            if (game.nNetMode == NetMode.Single) {
                gGameScreen.capture(160, 100);
                menu.mOpen(menu.mMenus[SAVEGAME], -1);
            }
        } else if (TekKeys.Show_LoadMenu.equals(gameKey)) {
            if (game.nNetMode == NetMode.Single) {
                menu.mOpen(menu.mMenus[LOADGAME], -1);
            }
        } else if (TekKeys.Quit.equals(gameKey)) {
            menu.mOpen(menu.mMenus[QUIT], -1);
        } else if (TekKeys.Show_Options.equals(gameKey)) {
            menu.mOpen(menu.mMenus[OPTIONS], -1);
        } else if (TekKeys.Quicksave.equals(gameKey)) {
            quicksave();
        } else if (TekKeys.Quickload.equals(gameKey)) {
            quickload();
        } else if (TekKeys.Show_SoundSetup.equals(gameKey)) {
            menu.mOpen(menu.mMenus[AUDIOSET], -1);
        } else if (TekKeys.Gamma.equals(gameKey)) {
            menu.mOpen(menu.mMenus[COLORCORR], -1);
        }

        return false;
    }

    @Override
    protected boolean prepareboard(final Entry map) {
        boolean out = Tekprep.prepareboard(map);
        if (out) {
            for (short i = connecthead; i >= 0; i = connectpoint2[i]) {
                initplayersprite(i);
            }
            checkmapsndfx(screenpeek);
            if (mission == 7) {
                for (int i = 0; i < MAXPLAYERS; i++) {
                    gPlayer[i].updategun = 7;
                }
            }
            game.nNetMode = NetMode.Single;
        } else {
            game.show();
        }

        return out;
    }

    public void processinput(short snum) {
        int i, j, doubvel, xvect, yvect, goalz;

        // move player snum
        if (snum < 0 || snum >= MAXPLAYERS) {
            throw new AssertException("game712: Invalid player number " + snum);
        }

        if ((gPlayer[snum].pInput.vel | gPlayer[snum].pInput.svel) != 0 && gPlayer[snum].health > 0) {
            // no run while crouching
            if (gPlayer[snum].pInput.Crouch && (mission != 7)) {
                doubvel = 1 + (gPlayer[snum].pInput.Run ? 1 : 0);
            } else {
                doubvel = (TICSPERFRAME << (gPlayer[snum].pInput.Run ? 1 : 0));
                doubvel <<= 1;
            }
            xvect = 0;
            yvect = 0;

            if (gPlayer[snum].pInput.vel != 0) {
                xvect += (int) ((int) (gPlayer[snum].pInput.vel * doubvel * BCosAngle(gPlayer[snum].ang)) / 8.0f);
                yvect += (int) ((int) (gPlayer[snum].pInput.vel * doubvel * BSinAngle(gPlayer[snum].ang)) / 8.0f);
            }
            if (gPlayer[snum].pInput.svel != 0) {
                xvect += (int) ((int) (gPlayer[snum].pInput.svel * doubvel * BCosAngle(gPlayer[snum].ang + 1536)) / 8.0f);
                yvect += (int) ((int) (gPlayer[snum].pInput.svel * doubvel * BSinAngle(gPlayer[snum].ang + 1536)) / 8.0f);
            }
            if (gPlayer[snum].noclip) {
                gPlayer[snum].posx += xvect >> 14;
                gPlayer[snum].posy += yvect >> 14;
                int sect = pEngine.updatesector(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].cursectnum);
                if (sect != -1) {
                    gPlayer[snum].cursectnum = sect;
                    pEngine.changespritesect(snum, gPlayer[snum].cursectnum);
                }
            } else {
                pEngine.clipmove(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz, gPlayer[snum].cursectnum,
                        xvect, yvect, 128, 4 << 8, 4 << 8, CLIPMASK0);
                gPlayer[snum].posx = clipmove_x;
                gPlayer[snum].posy = clipmove_y;
                gPlayer[snum].posz = clipmove_z;
                gPlayer[snum].cursectnum = clipmove_sectnum;
            }
            if (game.nNetMode != NetMode.Multiplayer) {
                tekheadbob();
            }

        } else {
            headbob = 0;
        }

        int sec = pEngine.updatesector(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].cursectnum);
        if (!gPlayer[snum].noclip) {
            if (sec >= 0) {
                // push player away from walls if clipmove doesn't work

                int push = pEngine.pushmove(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz,
                        gPlayer[snum].cursectnum, 128, 4 << 8, 4 << 8, CLIPMASK0);
                gPlayer[snum].posx = pushmove_x;
                gPlayer[snum].posy = pushmove_y;
                gPlayer[snum].posz = pushmove_z;
                gPlayer[snum].cursectnum = pushmove_sectnum;

                if (push < 0) {
                    changehealth(snum, -1000); // if this fails then instant
                    // death
                    changescore(snum, -5);
                }
            } else {
                changehealth(snum, -1000);
                changescore(snum, -5);
            }
        }

        if (!gPlayer[snum].noclip && !boardService.isValidSector(gPlayer[snum].cursectnum)) {
            throw new AssertException("game718: Invalid sector for player " + snum + " @ " + gPlayer[snum].posx + " "
                    + gPlayer[snum].posy + " " + (gPlayer[snum].cursectnum));
        }
        if (gPlayer[snum].playersprite < 0 || gPlayer[snum].playersprite >= MAXSPRITES) {
            throw new AssertException("game751: Invalid sprite for player " + snum + " " + (gPlayer[snum].playersprite));
        }

        // getzrange returns the highest and lowest z's for an entire box,
        // NOT just a point. This prevents you from falling off cliffs
        // when you step only slightly over the cliff.

        Sprite sprite = boardService.getSprite(gPlayer[snum].playersprite);
        if (sprite == null) {
            return;
        }

        sprite.setCstat(sprite.getCstat() ^ 1);
        pEngine.getzrange(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz, gPlayer[snum].cursectnum, 128,
                CLIPMASK0);
        int globhiz = zr_ceilz;
        int globloz = zr_florz;
        int globlohit = zr_florhit;
        sprite.setCstat(sprite.getCstat() ^ 1);

        if (!gPlayer[snum].noclip && gPlayer[snum].cursectnum != gPlayer[snum].ocursectnum) {
            teknewsector(snum);
        }

        if (gPlayer[snum].pInput.angvel != 0) {
            doubvel = TICSPERFRAME;
            // if run key then turn faster
            if (gPlayer[snum].pInput.Run) {
                doubvel += (TICSPERFRAME >> 1);
            }

            gPlayer[snum].ang += gPlayer[snum].pInput.angvel * doubvel / 16.0f;
            gPlayer[snum].ang = BClampAngle(gPlayer[snum].ang);
        }

        if (gPlayer[snum].health < 0) {
            gPlayer[snum].health -= (TICSPERFRAME << 1);
            if (gPlayer[snum].health <= -160) {
                gPlayer[snum].hvel = 0;

                if (gPlayer[snum].pInput.Use) {
                    gPlayer[snum].deaths++;

                    gPlayer[snum].posx = startx;
                    gPlayer[snum].posy = starty;
                    gPlayer[snum].posz = startz;
                    gPlayer[snum].ang = (short) starta;
                    gPlayer[snum].cursectnum = (short) starts;

                    tekrestoreplayer(snum);

                    if (game.nNetMode != NetMode.Multiplayer && (missionfailed() == 0)) {
                        newgame(boardfilename, null);
                    }
                    return;
                }

            } // Les 10/01/95
            else { // if 0
                sprite.setXrepeat((byte) Math.max(((128 + gPlayer[snum].health) >> 1), 0));
                sprite.setYrepeat((byte) Math.max(((128 + gPlayer[snum].health) >> 1), 0));

                gPlayer[snum].hvel += (TICSPERFRAME << 2);
                gPlayer[snum].horiz = Math.max(gPlayer[snum].horiz - 4, 0);
                gPlayer[snum].posz += gPlayer[snum].hvel;
                if (gPlayer[snum].posz > globloz - (4 << 8)) {
                    gPlayer[snum].posz = globloz - (4 << 8);
                    gPlayer[snum].horiz = Math.min(gPlayer[snum].horiz + 5, 200);
                    gPlayer[snum].hvel = 0;
                }
            } // Les 10/01/95

            return;
        }
        if (gPlayer[snum].pInput.Center) {
            gPlayer[snum].autocenter = true;
        }
        if (gPlayer[snum].autocenter) {
            if (gPlayer[snum].horiz > 100) {
                gPlayer[snum].horiz -= 4;
                if (gPlayer[snum].horiz < 100) {
                    gPlayer[snum].horiz = 100;
                }
            } else if (gPlayer[snum].horiz < 100) {
                gPlayer[snum].horiz += 4;
                if (gPlayer[snum].horiz > 100) {
                    gPlayer[snum].horiz = 100;
                }
            }
            if (gPlayer[snum].horiz == 100) {
                gPlayer[snum].autocenter = false;
            }
        }

        if (gPlayer[snum].pInput.mlook > 0 && gPlayer[snum].horiz > 0) {
            gPlayer[snum].horiz -= gPlayer[snum].pInput.mlook;
        }
        if (gPlayer[snum].pInput.mlook < 0 && gPlayer[snum].horiz < 200) {
            gPlayer[snum].horiz -= gPlayer[snum].pInput.mlook;
        }

        if ((gPlayer[snum].pInput.Look_Down) && (gPlayer[snum].horiz > 0)) {
            gPlayer[snum].horiz -= 4; // -
        }
        if ((gPlayer[snum].pInput.Look_Up) && (gPlayer[snum].horiz < 200)) {
            gPlayer[snum].horiz += 4; // +
        }

        // 32 pixels above floor is where player should be
        goalz = globloz - (KENSPLAYERHEIGHT << 8);

        Sector psec = boardService.getSector(gPlayer[snum].cursectnum);
        // kens slime sector
        if (psec != null && psec.getLotag() == 4) {
            // if not on a sprite
            if ((globlohit & HIT_TYPE_MASK) != HIT_SPRITE) {
                goalz = globloz - (8 << 8);
                if (gPlayer[snum].posz >= goalz - (2 << 8)) {
                    pEngine.clipmove(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz,
                            gPlayer[snum].cursectnum, -TICSPERFRAME << 14, -TICSPERFRAME << 14, 128, 4 << 8, 4 << 8,
                            CLIPMASK0);
                    gPlayer[snum].posx = clipmove_x;
                    gPlayer[snum].posy = clipmove_y;
                    gPlayer[snum].posz = clipmove_z;
                    gPlayer[snum].cursectnum = clipmove_sectnum;
                    if (slimesoundcnt[snum] >= 0) {
                        slimesoundcnt[snum] -= TICSPERFRAME;
                        while (slimesoundcnt[snum] < 0) {
                            slimesoundcnt[snum] += 120;
                        }
                    }
                }
            }
        }

        // case where ceiling & floor are too close
        if (goalz < globhiz + 4096) {
            goalz = (globloz + globhiz) >> 1;
        }

        psec = boardService.getSector(gPlayer[snum].cursectnum);
        if (psec != null) {
            // climb ladder or regular z movement
            if ((mission == 7) || (psec.getLotag() == SECT_LOTAG_CLIMB)) {
                if (gPlayer[snum].pInput.Jump) {
                    if (gPlayer[snum].posz > (psec.getCeilingz() + 2048)) {
                        gPlayer[snum].posz -= 64 << 2;
                        if (gPlayer[snum].pInput.Run) {
                            gPlayer[snum].posz -= 128 << 2;
                        }
                        if (mission == 7) {
                            gPlayer[snum].posz -= 256 << 2;
                        }
                    }
                } else if (gPlayer[snum].pInput.Crouch) {
                    if (gPlayer[snum].posz < (psec.getFloorz() - 2048)) {
                        gPlayer[snum].posz += 64 << 2;
                        if (gPlayer[snum].pInput.Run) {
                            gPlayer[snum].posz += 128 << 2;
                        }
                        if (mission == 7) {
                            gPlayer[snum].posz += 256 << 2;
                        }
                    }
                }
            } else {
                if (gPlayer[snum].health >= 0) {
                    if (gPlayer[snum].pInput.Jump) {
                        if (gPlayer[snum].posz >= globloz - (KENSPLAYERHEIGHT << 8)) {
                            goalz -= (16 << 8);
                            goalz -= (24 << 8);
                        }
                    }
                    if (gPlayer[snum].pInput.Crouch) {
                        goalz += (12 << 8);
                        goalz += (12 << 8);
                    }
                }
                // player is on a groudraw area
                if ((psec.getFloorstat() & 2) > 0) {
                    ArtEntry pic = engine.getTile(psec.getFloorpicnum());
                    if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
                        pic = engine.allocatepermanenttile(pic);
                        if (pic.hasSize()) {
                            goalz -= ((pic.getBytes()[0] + (((gPlayer[snum].posx >> 4) & 63) << 6) + ((gPlayer[snum].posy >> 4) & 63)) << 8);
                        }
                    }
                }
                // gravity, plus check for if on an elevator
                if (gPlayer[snum].posz < goalz) {
                    gPlayer[snum].hvel += (TICSPERFRAME << 5) + 1;
                } else {
                    if ((globlohit & HIT_TYPE_MASK) == HIT_SPRITE) { // on a sprite
                        int sprIndex = globlohit & HIT_INDEX_MASK;
                        Sprite spr = boardService.getSprite(sprIndex);
                        if (spr == null) {
                            throw new AssertException("game961: Invalid sprite index " + sprIndex);
                        } else if (spr.getLotag() >= 1500) {
                            onelev[snum] = true;
                        }
                    } else onelev[snum] = psec.getLotag() == 1004
                            || psec.getLotag() == 1005;
                    if (onelev[snum] && !gPlayer[snum].pInput.Crouch) {
                        gPlayer[snum].hvel = 0;
                        gPlayer[snum].posz = globloz - (KENSPLAYERHEIGHT << 8);
                    } else {
                        gPlayer[snum].hvel = (((goalz - gPlayer[snum].posz) * TICSPERFRAME) >> 5);
                    }
                }
                tekchangefallz(snum, globloz, globhiz);
            }
        }

        // update sprite representation of player
        // should be after movement, but before shooting code
        pEngine.setsprite(gPlayer[snum].playersprite, gPlayer[snum].posx, gPlayer[snum].posy,
                gPlayer[snum].posz + (KENSPLAYERHEIGHT << 8));
        sprite.setAng((short) gPlayer[snum].ang);

        // in wrong sector or is ceiling/floor smooshing player
        if (!gPlayer[snum].noclip) {
            if (!boardService.isValidSector(gPlayer[snum].cursectnum)) {
                changehealth(snum, -200);
                changescore(snum, -5);
            } else if (globhiz + (8 << 8) > globloz) {
                changehealth(snum, -200);
                changescore(snum, -5);
            }
        }

        // kens waterfountain
        if ((waterfountainwall[snum] >= 0) && (gPlayer[snum].health >= 0)) {
            Wall nwall = boardService.getWall(neartag.tagwall);
            if (nwall == null) {
                throw new AssertException("game1009: Invalid wall " + neartag.tagwall);
            }

            if ((nwall.getLotag() != 7) || (!gPlayer[snum].pInput.Use)) {
                i = waterfountainwall[snum];
                Wall wal = boardService.getWall(i);
                if (wal == null) {
                    throw new AssertException("game1014: Invalid wall index " + i);
                }

                if (wal.getOverpicnum() == USEWATERFOUNTAIN) {
                    wal.setOverpicnum(WATERFOUNTAIN);
                } else if (wal.getPicnum() == USEWATERFOUNTAIN) {
                    wal.setPicnum(WATERFOUNTAIN);
                }
                waterfountainwall[snum] = -1;
            }
        }

        // enter throw
        if ((game.nNetMode != NetMode.Multiplayer) && (pickup.getPicnum() != 0)
                && (game.getProcessor().isGameKeyJustPressed(TekKeys.Throw_Item))) {
            toss(snum);
        }

        psec = boardService.getSector(gPlayer[snum].cursectnum);
        if (psec != null) {
            // space bar (use) code
            if (gPlayer[snum].pInput.Use && (psec.getLotag() == 4444)) {
                depositsymbol(snum);
            } else if (gPlayer[snum].pInput.Use) {
                // continuous triggers

                pEngine.neartag(gPlayer[snum].posx, gPlayer[snum].posy, (gPlayer[snum].posz + (8 << 8)),
                        gPlayer[snum].cursectnum, (short) gPlayer[snum].ang, neartag, 1024, 3);
                pEngine.hitscan(gPlayer[snum].posx, gPlayer[snum].posy, gPlayer[snum].posz, gPlayer[snum].cursectnum,
                        EngineUtils.cos(((short) gPlayer[snum].ang + 2048) & 2047),
                        EngineUtils.sin(((short) gPlayer[snum].ang + 2048) & 2047),
                        (int) (100 - gPlayer[snum].horiz) * 2000,
                        pHitInfo, 0xFFFF0000);

                int hitDist = EngineUtils.qdist(gPlayer[snum].posx - pHitInfo.hitx, gPlayer[snum].posy - pHitInfo.hity);

                if (neartag.tagsector == -1) {
                    i = gPlayer[snum].cursectnum;
                    Sector nsec = boardService.getSector(i);
                    if (nsec != null && ((nsec.getLotag() | nsec.getHitag()) != 0)) {
                        neartag.tagsector = (short) i;
                    }
                }
                // kens water fountain
                if (neartag.tagwall >= 0) {
                    Wall nwall = boardService.getWall(neartag.tagwall);
                    if (nwall == null) {
                        throw new AssertException("game1053: Invalid wall index " + neartag.tagwall);
                    }

                    if (nwall.getLotag() == 7) {
                        if (nwall.getOverpicnum() == WATERFOUNTAIN) {
                            nwall.setOverpicnum(USEWATERFOUNTAIN);
                            waterfountainwall[snum] = neartag.tagwall;
                        } else if (nwall.getPicnum() == WATERFOUNTAIN) {
                            nwall.setPicnum(USEWATERFOUNTAIN);
                            waterfountainwall[snum] = neartag.tagwall;
                        }
                        if (waterfountainwall[snum] >= 0) {
                            waterfountaincnt[snum] -= TICSPERFRAME;
                            while (waterfountaincnt[snum] < 0) {
                                waterfountaincnt[snum] += 120;
                                changehealth(snum, 2);
                            }
                        }
                    }
                }
                // 1-time triggers
                if (!gPlayer[snum].ouse) {
                    if (neartag.tagsector >= 0) {
                        Sector nsec = boardService.getSector(neartag.tagsector);
                        if (nsec == null) {
                            throw new AssertException("game1070: Invalid sector index " + neartag.tagsector);
                        }

                        if (nsec.getHitag() == 0) {
                            operatesector(neartag.tagsector);
                        }
                    }
                    if (neartag.tagwall >= 0) {
                        Wall nwall = boardService.getWall(neartag.tagwall);
                        if (nwall == null) {
                            throw new AssertException("game1078: Invalid wall index " + neartag.tagwall);
                        }

                        if (nwall.getLotag() == 2) {
                            for (i = 0; i < boardService.getSectorCount(); i++) {
                                Sector s = boardService.getSector(i);
                                if (s != null && s.getHitag() == nwall.getHitag()) {
                                    if (s.getLotag() != 1) {
                                        operatesector(i);
                                    }
                                }
                            }
                            ListNode<Sprite> node = boardService.getStatNode(0), nexti;
                            while (node != null) {
                                nexti = node.getNext();
                                if (node.get().getHitag() == nwall.getHitag()) {
                                    operatesprite(node.getIndex());
                                }
                                node = nexti;
                            }
                            j = nwall.getOverpicnum();
                            if (j == SWITCH1ON) {
                                nwall.setOverpicnum(GIFTBOX);
                                nwall.setLotag(0);
                                nwall.setHitag(0);
                            }
                            if (j == GIFTBOX) {
                                nwall.setOverpicnum(SWITCH1ON);
                                nwall.setLotag(0);
                                nwall.setHitag(0);
                            }
                            if (j == SWITCH2ON) {
                                nwall.setOverpicnum(SWITCH2OFF);
                            }
                            if (j == SWITCH2OFF) {
                                nwall.setOverpicnum(SWITCH2ON);
                            }
                            if (j == SWITCH3ON) {
                                nwall.setOverpicnum(SWITCH3OFF);
                            }
                            if (j == SWITCH3OFF) {
                                nwall.setOverpicnum(SWITCH3ON);
                            }
                        }
                    }

                    if (hitDist < 512 && (neartag.tagsprite != -1 || pHitInfo.hitsprite != -1)) {
                        int spr = -1;

                        Sprite hitspr = boardService.getSprite(pHitInfo.hitsprite);
                        if (hitspr != null) {
                            switch (hitspr.getPicnum()) {
                                case 3708:
                                case 3709:
                                case SWITCH2ON:
                                case SWITCH2OFF:
                                case SWITCH3ON:
                                case SWITCH3OFF:
                                case SWITCH4ON:
                                case SWITCH4OFF:
                                    spr = pHitInfo.hitsprite;
                                    break;
                            }
                        }

                        if (spr == -1) {
                            if (boardService.isValidSprite(neartag.tagsprite)) {
                                spr = neartag.tagsprite;
                            } else if (boardService.isValidSprite(pHitInfo.hitsprite)) {
                                spr = pHitInfo.hitsprite;
                            }
                        }

                        if (spr != -1) {
                            Sprite s = boardService.getSprite(spr);
                            if (s != null) {
                                if (s.getLotag() == 4) {
                                    tekswitchtrigger(snum, spr);
                                } else {
                                    operatesprite(spr);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (gPlayer[snum].pInput.Fire) {
            tekfiregun(gPlayer[snum].lastgun, snum);
        }

        gPlayer[snum].ouse = gPlayer[snum].pInput.Use;
        gPlayer[snum].ofire = gPlayer[snum].pInput.Fire;
    }

    public void depositsymbol(int snum) {
        int i, findpic = 0;
        Sector psec = boardService.getSector(gPlayer[snum].cursectnum);
        if (psec == null) {
            return;
        }

        int sym = psec.getHitag();
        switch (sym) {
            case 0:
                findpic = 3600;
                break;
            case 1:
                findpic = 3604;
                break;
            case 2:
                findpic = 3608;
                break;
            case 3:
                findpic = 3612;
                break;
            case 4:
                findpic = 3592;
                break;
            case 5:
                findpic = 3596;
                break;
            case 6:
                findpic = 3616;
                break;
        }

        if (symbols[sym]) {
            for (i = 0; i < MAXSPRITES; i++) {
                Sprite s = boardService.getSprite(i);
                if (s != null && s.getPicnum() == findpic) {
                    s.setPicnum(findpic + 1);
                    symbolsdeposited[sym] = true;
                    break;
                }
            }
        }

        if (symbolsdeposited[0] && symbolsdeposited[1] && symbolsdeposited[2] && symbolsdeposited[3]
                && symbolsdeposited[4] && symbolsdeposited[5] && symbolsdeposited[6]) {
            allsymsdeposited = 1;
            gameover = 1;
//	         fadein();
        }
    }

    public void tektime() {
        fortieth++;
        if (fortieth == 40) {
            fortieth = 0;
            seconds++;
        }
        if (seconds == 60) {
            minutes++;
            seconds = 0;
        }
        if (minutes == 60) {
            hours++;
            minutes = 0;
        }
        if (hours > 99) {
            hours = 0;
        }
        if (messageon != 0) {
            messageon++;
            if (messageon == 160) {
                messageon = 0;
            }
        }
    }

    public void CompleteGame() {
        CutsceneScreen gCutsceneScreen = new CutsceneScreen(game);
        if (gCutsceneScreen.init("FINALDB.SMK")) {
            gCutsceneScreen.setCallback(() -> {
                if (gCutsceneScreen.init("CREDITS.SMK")) {
                    gCutsceneScreen.setCallback(this::ExitWithComplete).escSkipping(true);
                    game.changeScreen(gCutsceneScreen);
                } else {
                    ExitWithComplete();
                }
            }).escSkipping(true);
            game.changeScreen(gCutsceneScreen);
        } else {
            ExitWithComplete();
        }
    }

    public void ExitWithComplete() {
        Console.out.println("Game completed!");
        game.showGameMessage("Congratulation", "Game completed!", MessageType.Info).setMessageListener(r -> game.gExit = true);
    }

    @Override
    public void sndHandlePause(boolean pause) {
        Teksnd.sndHandlePause(pause);
    }
}
