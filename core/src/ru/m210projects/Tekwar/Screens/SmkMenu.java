package ru.m210projects.Tekwar.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.input.InputListener;
import ru.m210projects.Build.input.keymap.Keymap;
import ru.m210projects.BuildSmacker.SMKFile;
import ru.m210projects.BuildSmacker.SMKFile.Track;
import ru.m210projects.Tekwar.Main;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static ru.m210projects.Build.Engine.MAXTILES;
import static ru.m210projects.Build.Gameutils.BClipLow;
import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Tekwar.Globals.ST_IMMEDIATE;
import static ru.m210projects.Tekwar.Main.engine;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Teksnd.playsound;

public abstract class SmkMenu extends ScreenAdapter implements InputListener {

    private final int kSMKTile = MAXTILES - 3;
    protected int currmission;
    protected MessageType message = MessageType.NONE;
    protected final Main game;
    private SMKFile smkfil;
    private Runnable chooseCallback;
    private float delayclock, helpclock;
    private boolean rebuild;

    public SmkMenu(Main game) {
        this.game = game;
    }

    public abstract int skip();

    public abstract void keyUp();

    public abstract void keyDown();

    public abstract void keyLeft();

    public abstract void keyRight();

    public abstract void loadGame();

    public abstract String init();

    public abstract void rebuildFrame();

    public abstract boolean mouseHandler(int x, int y);

    public void setCallback(Runnable chooseCallback) {
        this.chooseCallback = chooseCallback;
    }

    @Override
    public synchronized void show() {
        rebuild = true;
        delayclock = 0;
        helpclock = 0;
        message = MessageType.NONE;

        Gdx.input.setCursorCatched(false);
        init(init());
    }

    @Override
    public void hide() {
        if (smkfil == null) {
            return;
        }

        Gdx.input.setCursorCatched(true);
        smkfil = null;
    }

    @Override
    public synchronized void render(float delta) {
        if (smkfil == null) {
            return;
        }

        if (rebuild || mouseHandler(Gdx.input.getX(), Gdx.input.getY())) {
            rebuildFrame();

            DynamicArtEntry pic = (DynamicArtEntry) game.pEngine.getTile(kSMKTile);
            pic.copyData(smkfil.getVideoBuffer().array());
            rebuild = false;
        }

        DrawMenu();
        game.pEngine.nextpage(delta);
    }

    private void keyhandler(int keyCode) {
        int mission = skip();
        if (mission != -1) {
            return;
        }

        if (message == MessageType.EXIT) {
            if (keyCode == (Keys.Y) || keyCode == Keys.BUTTON_A) {
                game.changeScreen(new CreditsScreen());
            }

            if (keyCode == (Keys.N) || keyCode == (Keys.ESCAPE) || keyCode == Keys.BUTTON_START) {
                resetStatus();
            }

            return;
        }

        if (delayclock > 0) { // can't access to matrix
            return;
        }

        helpclock += Gdx.graphics.getDeltaTime();
        if (helpclock > 8 && message != MessageType.HELP) // Help state
        {
            HelpMessage();
        }


        if (keyCode == (Keys.H)) {
            HelpMessage();
        } else if (keyCode == (Keys.ESCAPE) || keyCode == Keys.BUTTON_START) {
            if (message == MessageType.HELP) {
                resetStatus();
            } else {
                ExitMessage();
            }
        } else if (keyCode == (Keys.LEFT) || keyCode == Keymap.BUTTON_LEFT) {
            message = MessageType.NONE;
            keyLeft();
            helpclock = 0;
            rebuild = true;
        } else if (keyCode == (Keys.RIGHT) || keyCode == Keymap.BUTTON_RIGHT) {
            message = MessageType.NONE;
            keyRight();
            helpclock = 0;
            rebuild = true;
        } else if (keyCode == (Keys.UP) || keyCode == Keymap.BUTTON_UP) {
            message = MessageType.NONE;
            keyUp();
            helpclock = 0;
            rebuild = true;
        } else if (keyCode == (Keys.DOWN) || keyCode == Keymap.BUTTON_DOWN) {
            message = MessageType.NONE;
            keyDown();
            helpclock = 0;
            rebuild = true;
        } else if (keyCode == (Keys.L)) {
            loadGame();
            if (chooseCallback != null) {
                chooseCallback.run();
            }

        } else if (keyCode == (Keys.ENTER) || keyCode == (Keys.SPACE) || keyCode == Keys.BUTTON_A) {
            helpclock = 0;
            rebuild = true;

            if (chooseCallback != null) {
                chooseCallback.run();
            }

        }

    }

    @Override
    public void processInput(GameProcessor processor) {
        if (delayclock > 0) { // can't access to matrix
            delayclock = BClipLow(delayclock - Gdx.graphics.getDeltaTime(), 0);
            if (delayclock == 0) {
                resetStatus();
            }
        }
    }

    protected void DrawFrame(int fn) {
        if (smkfil != null) {
            if (fn < smkfil.getFrames()) {
                smkfil.setFrame(fn - 1);
            }
        }
    }

    protected void resetStatus() {
        helpclock = 0;
        message = MessageType.NONE;
        rebuild = true;
    }

    protected void AccessWarning() {
        playsound(S_BOOP, 0, 0, 0, ST_IMMEDIATE);
        message = MessageType.ACCESS;
        delayclock = 1;
        rebuild = true;
    }

    protected void HelpMessage() {
        message = MessageType.HELP;
        playsound(S_PICKUP_BONUS, 0, 0, 0, ST_IMMEDIATE);
        rebuild = true;
    }

    protected void ExitMessage() {
        message = MessageType.EXIT;
        playsound(S_PICKUP_BONUS, 0, 0, 0, ST_IMMEDIATE);
        rebuild = true;
    }

    private void init(String name) {
        Entry entry = game.cache.getEntry(name, true);
        if (!entry.exists()) {
            return;
        }

        ByteBuffer bb = ByteBuffer.wrap(entry.getBytes());
        bb.order(ByteOrder.LITTLE_ENDIAN);
        try {
            smkfil = new SMKFile(bb);
        } catch (Exception e) {
            return;
        }

        smkfil.setEnable(Track.All, Track.Video.mask());

        ArtEntry dst = engine.getTile(kSMKTile);
        if (!(dst instanceof DynamicArtEntry) || !dst.exists() || dst.getWidth() != smkfil.getHeight() || dst.getHeight() != smkfil.getWidth()) {
            dst = engine.allocatepermanenttile(kSMKTile, smkfil.getHeight(), smkfil.getWidth());
            if (!dst.exists()) {
                skip();
                return;
            }
        }

        // we should call invalidate in all cases
        ((DynamicArtEntry) dst).clearData();
    }

    private void DrawMenu() {
        Renderer renderer = game.getRenderer();
        renderer.clearview(0);
        if (smkfil == null) {
            return;
        }

        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        renderer.setview(0, 0, xdim - 1, ydim - 1);
        ArtEntry pic = game.pEngine.getTile(BACKGROUND);

        if (pic.exists() && pic.getWidth() != 0) { // #GDX 31.12.2024
            int frames = xdim / pic.getWidth();
            int x = 160;
            for (int i = 0; i <= frames; i++) {
                renderer.rotatesprite(x << 16, 100 << 16, 0x10000, 0, BACKGROUND, 0, 0, 2 + 8 + 256, 0, 0, xdim - 1,
                        ydim - 1);
                x += pic.getWidth();
            }
        }

        pic = game.pEngine.getTile(kSMKTile);

        renderer.rotatesprite(xdim << 15, ydim << 15, divscale(ydim, pic.getWidth(), 16),
                512, kSMKTile, 0, 0, 4 | 8);

        game.menu.mDrawMouse(Gdx.input.getX(), Gdx.input.getY());
    }

    @Override
    public boolean keyDown(int keycode) {
        keyhandler(keycode);
        return false;
    }

    protected enum MessageType {
        NONE(-1), EXIT(1), ACCESS(2), HELP(3);

        private int num;

        MessageType(int num) {
            this.num = num;
        }

        public int get() {
            return num;
        }

        public void set(int num) {
            this.num = num;
        }
    }
}
