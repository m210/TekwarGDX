package ru.m210projects.Tekwar.Screens;

import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Tekwar.Main;

import static ru.m210projects.Build.Engine.MAXSPRITES;
import static ru.m210projects.Build.Engine.MAXSTATUS;
import static ru.m210projects.Tekwar.Main.boardService;
import static ru.m210projects.Tekwar.Names.BACKGROUND;

public class PrecacheScreen extends DefaultPrecacheScreen {

    public PrecacheScreen(Runnable toLoad, PrecacheListener listener) {
        super(Main.game, toLoad, listener);

        addQueue("Preload floor and ceiling tiles...", () -> {
            Sector[] sectors = boardService.getBoard().getSectors();
            for (Sector sec : sectors) {
                addTile(sec.getFloorpicnum());
                addTile(sec.getCeilingpicnum());
            }
            doprecache(0);
        });

        addQueue("Preload wall tiles...", () -> {
            for (Wall wall : boardService.getBoard().getWalls()) {
                addTile(wall.getPicnum());
                if (wall.getOverpicnum() >= 0) {
                    addTile(wall.getOverpicnum());
                }
            }
            doprecache(0);
        });

        addQueue("Preload sprite tiles...", () -> {
            for (int i = 0; i < MAXSPRITES; i++) {
                Sprite sp = boardService.getSprite(i);
                if (sp != null && sp.getStatnum() < MAXSTATUS) {
                    addTile(sp.getPicnum());
                }
            }

            doprecache(1);
        });
    }

    @Override
    protected void draw(String title, int index) {
        Renderer renderer = game.getRenderer();
        ArtEntry pic = renderer.getTile(BACKGROUND);
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        int frames = xdim / pic.getWidth();
        int x = 160;
        for (int i = 0; i <= frames; i++) {
            renderer.rotatesprite(x << 16, 100 << 16, 0x10000, 0, BACKGROUND, 0, 0, 2 + 8 + 256, 0, 0, xdim - 1,
                    ydim - 1);
            x += pic.getWidth();
        }

        game.getFont(0).drawTextScaled(renderer, 160, 100, "Loading...", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        game.getFont(0).drawTextScaled(renderer, 160, 120, title, 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
    }

}
