package ru.m210projects.Tekwar.Screens;

import ru.m210projects.Build.Architecture.common.audio.Source;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.MovieScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.BuildSmacker.SMKAudio;
import ru.m210projects.BuildSmacker.SMKFile;
import ru.m210projects.BuildSmacker.SMKFile.Track;
import ru.m210projects.Tekwar.Main;
import ru.m210projects.Tekwar.Teksnd;

import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Tekwar.Main.tekcfg;
import static ru.m210projects.Tekwar.Teksnd.sndStopMusic;
import static ru.m210projects.Tekwar.Teksnd.stopallsounds;

public class CutsceneScreen extends MovieScreen {

    public CutsceneScreen(BuildGame game) {
        super(game, MAXTILES - 3);

        this.nFlags |= 4;
    }

    @Override
    public void hide() {
        // #GDX 19.08.2024 revive old palette and close movie file if it opened
        close();
    }

    @Override
    public boolean init(String path) {
        if (!tekcfg.showCutscenes || isInited()) {
            return false;
        }

        return open(path);
    }

    @Override
    protected MovieFile GetFile(String file) {
        try {
            return new SMKMovieFile(file);
        } catch (Exception e) {
            Console.out.println(e + ": " + file, OsdColor.RED);
            return null;
        }
    }

    @Override
    protected void StopAllSounds() {
        stopallsounds();
        sndStopMusic();
    }

    @Override
    protected byte[] DoDrawFrame(int num) {
        byte[] pic = mvfil.getFrame(num);
        if (((SMKMovieFile) mvfil).paletteChanged) {
            changepalette(mvfil.getPalette());
        }
        return pic;
    }

    @Override
    protected Font GetFont() {
        return game.getFont(0);
    }

    @Override
    protected void DrawEscText(Font font, int pal) {
        Renderer renderer = game.getRenderer();
        int shade = 16 + mulscale(32, EngineUtils.sin((20 * engine.getTotalClock()) & 2047), 16);
        game.getFont(0).drawTextScaled(renderer, 160, 5, "Press ESC to skip", 1.0f, shade, MAXPALOOKUPS - RESERVEDPALS - 1, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
    }

    protected static class SMKMovieFile implements MovieFile {
        protected SMKFile smkfil;
        protected boolean paletteChanged;
        protected Source audio;

        protected int sourceRate;
        protected int sourceBits;
        protected int sourceChannels;
        protected ByteBuffer sourceBuffer;

        public SMKMovieFile(String file) throws Exception {
            Entry entry = Main.game.getCache().getEntry(file, true);
            if (!entry.exists()) {
                throw new FileNotFoundException();
            }

            ByteBuffer bb = ByteBuffer.wrap(entry.getBytes());
            bb.order(ByteOrder.LITTLE_ENDIAN);

            smkfil = new SMKFile(bb);
            smkfil.setEnable(Track.All, Track.Video.mask() | Track.Audio.mask());

            if (!tekcfg.isNoSound()) {
                SMKAudio aud = smkfil.getAudio(0);
                if (aud != null) {
                    sourceRate = aud.getRate();
                    sourceBits = aud.getBits().get();
                    sourceChannels = aud.getChannels().get();

                    if (sourceRate != 0 && sourceBits != 0) {
                        sourceBuffer = smkfil.getAudioBuffer(0);
                        bb.order(ByteOrder.LITTLE_ENDIAN);
                        bb.rewind();
                    }
                }
            }
        }

        @Override
        public void playAudio() {
            if (tekcfg.isNoSound()) {
                return;
            }

            if (sourceRate != 0 && sourceBits != 0 && sourceBuffer != null) {
                audio = Teksnd.newSound(sourceBuffer, sourceRate, sourceBits, sourceChannels, 255);
                if (audio != null) {
                    audio.play(1.0f);
                }
            }
        }

        @Override
        public int getFrames() {
            return smkfil.getFrames();
        }

        @Override
        public float getRate() {
            return smkfil.getRate() / 1000f;
        }

        @Override
        public byte[] getFrame(int num) {
            paletteChanged = smkfil.setFrame(num) != 0;
            return smkfil.getVideoBuffer().array();
        }

        @Override
        public byte[] getPalette() {
            return smkfil.getPalette();
        }

        @Override
        public int getWidth() {
            return (short) smkfil.getWidth();
        }

        @Override
        public int getHeight() {
            return (short) smkfil.getHeight();
        }

        @Override
        public void close() {
            if (audio != null) {
                audio.stop();
            }
        }
    }
}
