package ru.m210projects.Tekwar.Screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.input.InputListener;

import static ru.m210projects.Tekwar.Main.engine;
import static ru.m210projects.Tekwar.Main.game;

public class CreditsScreen extends ScreenAdapter implements InputListener {

    private long showTime;

    @Override
    public void show() {
        game.getProcessor().resetPollingStates();
        showTime = System.currentTimeMillis();
    }

    @Override
    public void render(float delta) {
        if (game.getProcessor().isKeyJustPressed(Input.Keys.ANY_KEY)) {
            anyKeyPressed();
        }

        Renderer renderer = game.getRenderer();
        renderer.clearview(0);

        renderer.rotatesprite(160 << 16, 100 << 16, 0x10000, 0, 321, 0, 0, 2 + 8);

        engine.nextpage(delta);
    }

    public void anyKeyPressed() {
        game.getProcessor().prepareNext();
        if (System.currentTimeMillis() - showTime >= 100) {
            game.gExit = true;
        }
    }
}
