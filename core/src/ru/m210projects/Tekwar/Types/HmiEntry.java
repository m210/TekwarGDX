package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Architecture.common.audio.MidiDevice;
import ru.m210projects.Build.filehandle.InputStreamProvider;
import ru.m210projects.Build.filehandle.grp.GrpEntry;

import static ru.m210projects.Tekwar.Types.HMIMIDIP.hmpinit;

public class HmiEntry extends GrpEntry {

    public HmiEntry(InputStreamProvider provider, String name, int offset, int size) {
        super(provider, String.format("%s.%s", name, MidiDevice.MIDI_EXTENSION), offset, size);
    }

    @Override
    public byte[] getBytes() {
        return hmpinit(super.getBytes());
    }
}
