package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import static ru.m210projects.Tekwar.Tektag.*;

public class Sectorvehicle implements Serializable<Sectorvehicle> {
    
    public short acceleration, accelto;
    public short speed, speedto, movespeed;
    public short angle, angleto;
    public int pivotx, pivoty;
    public short numpoints;
    public final short[] point = new short[MAXVEHICLEPOINTS];
    public final int[] pointx = new int[MAXVEHICLEPOINTS];
    public final int[] pointy = new int[MAXVEHICLEPOINTS];
    public short track;
    public short tracknum;
    public final int[] trackx = new int[MAXVEHICLETRACKS];
    public final int[] tracky = new int[MAXVEHICLETRACKS];
    public final char[] stop = new char[MAXVEHICLETRACKS];
    public int distx, disty;
    public final short[] sector = new short[MAXVEHICLESECTORS];
    public short numsectors;
    public int waittics, waitdelay;
    public short stoptrack;
    public final short[] killw = new short[4];
    public int soundindex;

    public void reset() {
        acceleration = 0;
        accelto = 0;
        speed = 0;
        speedto = 0;
        movespeed = 0;
        angle = 0;
        angleto = 0;
        pivotx = 0;
        pivoty = 0;
        numpoints = 0;

        Arrays.fill(point, (short) 0);
        Arrays.fill(pointx, 0);
        Arrays.fill(pointy, 0);
        track = 0;
        tracknum = 0;
        Arrays.fill(trackx, 0);
        Arrays.fill(tracky, 0);
        Arrays.fill(stop, (char) 0);

        distx = 0;
        disty = 0;
        for (int i = 0; i < MAXVEHICLESECTORS; i++) {
            sector[i] = 0;
        }
        numsectors = 0;
        waittics = 0;
        waitdelay = 0;
        stoptrack = 0;
        for (int i = 0; i < 4; i++) {
            killw[i] = 0;
        }
        soundindex = -1;
    }

    @Override
    public Sectorvehicle readObject(InputStream is) throws IOException {
        acceleration = StreamUtils.readShort(is);
        accelto = StreamUtils.readShort(is);
        speed = StreamUtils.readShort(is);
        speedto = StreamUtils.readShort(is);
        movespeed = StreamUtils.readShort(is);
        angle = StreamUtils.readShort(is);
        angleto = StreamUtils.readShort(is);
        pivotx = StreamUtils.readInt(is);
        pivoty = StreamUtils.readInt(is);
        numpoints = StreamUtils.readShort(is);
        for (int i = 0; i < MAXVEHICLEPOINTS; i++) {
            point[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < MAXVEHICLEPOINTS; i++) {
            pointx[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < MAXVEHICLEPOINTS; i++) {
            pointy[i] = StreamUtils.readInt(is);
        }
        track = StreamUtils.readShort(is);
        tracknum = StreamUtils.readShort(is);
        for (int i = 0; i < MAXVEHICLETRACKS; i++) {
            trackx[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < MAXVEHICLETRACKS; i++) {
            tracky[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < MAXVEHICLETRACKS; i++) {
            stop[i] = (char) (StreamUtils.readUnsignedByte(is));
        }
        distx = StreamUtils.readInt(is);
        disty = StreamUtils.readInt(is);
        for (int i = 0; i < MAXVEHICLESECTORS; i++) {
            sector[i] = StreamUtils.readShort(is);
        }
        numsectors = StreamUtils.readShort(is);
        waittics = StreamUtils.readInt(is);
        waitdelay = StreamUtils.readInt(is);
        stoptrack = StreamUtils.readShort(is);
        for (int i = 0; i < 4; i++) {
            killw[i] = StreamUtils.readShort(is);
        }
        soundindex = StreamUtils.readInt(is);
        
        return this;
    }

    @Override
    public Sectorvehicle writeObject(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, acceleration);
        StreamUtils.writeShort(os, accelto);
        StreamUtils.writeShort(os, speed);
        StreamUtils.writeShort(os, speedto);
        StreamUtils.writeShort(os, movespeed);
        StreamUtils.writeShort(os, angle);
        StreamUtils.writeShort(os, angleto);
        StreamUtils.writeInt(os,pivotx);
        StreamUtils.writeInt(os,pivoty);
        StreamUtils.writeShort(os, numpoints);
        for (int j = 0; j < MAXVEHICLEPOINTS; j++) {
            StreamUtils.writeShort(os, point[j]);
        }
        for (int j = 0; j < MAXVEHICLEPOINTS; j++) {
            StreamUtils.writeInt(os,pointx[j]);
        }
        for (int j = 0; j < MAXVEHICLEPOINTS; j++) {
            StreamUtils.writeInt(os,pointy[j]);
        }
        StreamUtils.writeShort(os, track);
        StreamUtils.writeShort(os, tracknum);
        for (int j = 0; j < MAXVEHICLETRACKS; j++) {
            StreamUtils.writeInt(os,trackx[j]);
        }
        for (int j = 0; j < MAXVEHICLETRACKS; j++) {
            StreamUtils.writeInt(os,tracky[j]);
        }
        for (int j = 0; j < MAXVEHICLETRACKS; j++) {
            StreamUtils.writeByte(os, (byte) stop[j]);
        }
        StreamUtils.writeInt(os,distx);
        StreamUtils.writeInt(os,disty);
        for (int j = 0; j < MAXVEHICLESECTORS; j++) {
            StreamUtils.writeShort(os, sector[j]);
        }
        StreamUtils.writeShort(os, numsectors);
        StreamUtils.writeInt(os,waittics);
        StreamUtils.writeInt(os,waitdelay);
        StreamUtils.writeShort(os, stoptrack);
        for (int j = 0; j < 4; j++) {
            StreamUtils.writeShort(os, killw[j]);
        }
        StreamUtils.writeInt(os,soundindex);
        
        return this;
    }
}
