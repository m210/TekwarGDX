package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

public class Mapsndfxtype implements Serializable<Mapsndfxtype> {
    
    public int x, y;
    public short sector;
    public int snum;
    public int loops;
    public int type;
    public int id;

    public void reset() {
        x = 0;
        y = 0;
        sector = 0;
        snum = 0;
        loops = 0;
        type = 0;
        id = 0;
    }

    @Override
    public Mapsndfxtype readObject(InputStream is) throws IOException {
        x = StreamUtils.readInt(is);
        y = StreamUtils.readInt(is);
        sector = StreamUtils.readShort(is);
        snum = StreamUtils.readInt(is);
        loops = StreamUtils.readInt(is);
        type = StreamUtils.readInt(is);
        id = StreamUtils.readInt(is);
        
        return this;
    }

    @Override
    public Mapsndfxtype writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os,x);
        StreamUtils.writeInt(os,y);
        StreamUtils.writeShort(os, sector);
        StreamUtils.writeInt(os,snum);
        StreamUtils.writeInt(os,loops);
        StreamUtils.writeInt(os,type);
        StreamUtils.writeInt(os,id);
        
        return this;
    }
}
