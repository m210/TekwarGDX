package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import static ru.m210projects.Tekwar.Tekprep.spriteXT;

public class XTsavetype {
    public short XTnum;

    public void load(InputStream is) throws IOException {
        XTnum = StreamUtils.readShort(is);
        spriteXT[XTnum].readObject(is);
    }
}
