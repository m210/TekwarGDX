package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Animpic implements Serializable<Animpic> {
    
    public short frames;
    public short pic;
    public short tics;
    public int nextclock;

    public void set(Animpic src) {
        frames = src.frames;
        pic = src.pic;
        tics = src.tics;
        nextclock = src.nextclock;
    }

    public void set(int var) {
        frames = (short) var;
        pic = (short) var;
        tics = (short) var;
        nextclock = var;
    }

    @Override
    public Animpic readObject(InputStream is) throws IOException {
        frames = StreamUtils.readShort(is);
        pic = StreamUtils.readShort(is);
        tics = StreamUtils.readShort(is);
        nextclock = StreamUtils.readInt(is);
        
        return this;
    }

    @Override
    public Animpic writeObject(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, frames);
        StreamUtils.writeShort(os, pic);
        StreamUtils.writeShort(os, tics);
        StreamUtils.writeInt(os, nextclock);
        
        return this;
    }
}
