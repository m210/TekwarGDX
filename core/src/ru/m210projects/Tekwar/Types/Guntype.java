package ru.m210projects.Tekwar.Types;

public class Guntype {
    public final short pic;               // gun frame when carrying weapon
    public final short firepic;           // 1st gun frame when firing weapon
    public final short endfirepic;        // last gun frame when firing weapon
    public final byte rep;                // 1=automatic weapon, 0=semi-automatic
    public final byte[] action; // 8 frame action bytes - 1=shootgun()
    public final short tics;

    public Guntype(int pic, int firepic, int endfirepic, int rep, byte[] action, int pos, int tics) {
        this.pic = (short) pic;
        this.firepic = (short) firepic;
        this.endfirepic = (short) endfirepic;
        this.rep = (byte) rep;
        this.action = action;
        this.tics = (short) tics;
    }
}
