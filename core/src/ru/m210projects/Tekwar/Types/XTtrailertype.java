package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

public class XTtrailertype {
    public static final int sizeof = 34;
    public int numXTs;     //4
    public int start;        //8
    public String mapname;      //[13] 21
    public String ID;           //[13] 34

    public void load(InputStream is) throws IOException {
        this.numXTs = StreamUtils.readInt(is);
        this.start = StreamUtils.readInt(is);
        this.mapname = StreamUtils.readString(is, 13); // new String(data, 8, 13);
        this.ID = StreamUtils.readString(is, 13); // new String(data, 21, 13);
    }
}
