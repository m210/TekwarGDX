package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.BitMap;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Tekwar.Player;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Tekwar.Animate.MAXANIMATES;
import static ru.m210projects.Tekwar.Tekldsv.*;
import static ru.m210projects.Tekwar.Tekmap.MAXSYMBOLS;
import static ru.m210projects.Tekwar.Tektag.*;

public class SafeLoader {

    public String boardfilename;

    public int gAnimationCount = 0;
    public final ANIMATION[] gAnimationData = new ANIMATION[MAXANIMATES];
    public final Player[] gPlayer = new Player[MAXPLAYERS];
    public short numplayers;

    public final short[] pskyoff = new short[MAXPSKYTILES];
    public short pskybits;
    public int parallaxyoffs;
    public byte parallaxtype;
    public int visibility, parallaxvisibility;

    public final short[] connectpoint2 = new short[MAXPLAYERS];
    public int randomseed;

    public boolean gUserMap;
    public int mission, difficulty;
    public int hours, minutes, seconds;
    public int lockclock;

    // MapInfo

    public Sector[] sector;
    public Wall[] wall;
    public List<Sprite> sprite;

    public byte automapping;
    public final BitMap show2dsector = new BitMap(MAXSECTORSV7);
    public final BitMap show2dwall = new BitMap(MAXWALLSV7);
    public final BitMap show2dsprite = new BitMap(MAXSPRITESV7);

    public final SpriteXT[] spriteXT = new SpriteXT[MAXSPRITES];

    public final boolean[] symbols = new boolean[MAXSYMBOLS];
    public final boolean[] symbolsdeposited = new boolean[MAXSYMBOLS];

    public int currentmapno;
    public int allsymsdeposited;
    public char generalplay;
    public char singlemapmode;
    public int killedsonny;
    public int civillianskilled;
    public char mission_accomplished;
    public char numlives;

    public boolean goreflag;

    public final Delayfunc[] delayfunc = new Delayfunc[MAXDELAYFUNCTIONS];
    public final Animpic[] animpic = new Animpic[MAXANIMPICS];
    public final Spriteelev[] spriteelev = new Spriteelev[MAXSPRITEELEVS];
    public final Doortype[] doortype = new Doortype[MAXDOORS];
    public final Floordoor[] floordoor = new Floordoor[MAXFLOORDOORS];
    public final Sectoreffect[] sectoreffect = new Sectoreffect[MAXSECTORS];
    public final Elevatortype[] elevator = new Elevatortype[MAXSECTORS];
    public final Sectorvehicle[] sectorvehicle = new Sectorvehicle[MAXSECTORVEHICLES];
    public final Mapsndfxtype[] mapsndfx = new Mapsndfxtype[MAXMAPSOUNDFX];

    public final int[] doorxref = new int[MAXSECTORS];
    public final int[] fdxref = new int[MAXSECTORS];
    public final int[] sexref = new int[MAXSECTORS];
    public final boolean[] onelev = new boolean[MAXPLAYERS];

    public int numanimates;
    public short numdelayfuncs;
    public int secnt;
    public int numdoors, numfloordoors, numvehicles;
    public int sprelevcnt;
    public int totalmapsndfx;

    // Board animation variables
    public final short[] rotatespritelist = new short[16];
    public short rotatespritecnt;
    public final short[] warpsectorlist = new short[64];
    public short warpsectorcnt;
    public final short[] xpanningsectorlist = new short[16];
    public short xpanningsectorcnt;
    public final short[] ypanningwalllist = new short[64];
    public short ypanningwallcnt;
    public final short[] floorpanninglist = new short[64];
    public short floorpanningcnt;
    public final short[] dragsectorlist = new short[16];
    public final short[] dragxdir = new short[16];
    public final short[] dragydir = new short[16];
    public short dragsectorcnt;
    public final int[] dragx1 = new int[16];
    public final int[] dragy1 = new int[16];
    public final int[] dragx2 = new int[16];
    public final int[] dragy2 = new int[16];
    public final int[] dragfloorz = new int[16];
    public short swingcnt;
    public final short[][] swingwall = new short[32][5];
    public final short[] swingsector = new short[32];
    public final short[] swingangopen = new short[32];
    public final short[] swingangclosed = new short[32];
    public final short[] swingangopendir = new short[32];
    public final short[] swingang = new short[32];
    public final short[] swinganginc = new short[32];
    public final int[][] swingx = new int[32][8];
    public final int[][] swingy = new int[32][8];
    public final short[] revolvesector = new short[4];
    public final short[] revolveang = new short[4];
    public short revolvecnt;
    public final int[][] revolvex = new int[4][32];
    public final int[][] revolvey = new int[4][32];
    public final int[] revolvepivotx = new int[4];
    public final int[] revolvepivoty = new int[4];
    public final short[][] subwaytracksector = new short[4][128];
    public final short[] subwaynumsectors = new short[4];
    public short subwaytrackcnt;
    public final int[][] subwaystop = new int[4][8];
    public final int[] subwaystopcnt = new int[4];
    public final int[] subwaytrackx1 = new int[4];
    public final int[] subwaytracky1 = new int[4];
    public final int[] subwaytrackx2 = new int[4];
    public final int[] subwaytracky2 = new int[4];
    public final int[] subwayx = new int[4];
    public final int[] subwaygoalstop = new int[4];
    public final int[] subwayvel = new int[4];
    public final int[] subwaypausetime = new int[4];
    public final short[] waterfountainwall = new short[MAXPLAYERS];
    public final short[] waterfountaincnt = new short[MAXPLAYERS];
    public final short[] slimesoundcnt = new short[MAXPLAYERS];

    public SafeLoader() {
        for (int i = 0; i < MAXPLAYERS; i++) {
            gPlayer[i] = new Player();
        }

        for (int i = 0; i < MAXANIMATES; i++) {
            gAnimationData[i] = new ANIMATION();
        }

        for (int i = 0; i < MAXSPRITES; i++) {
            spriteXT[i] = new SpriteXT();
        }
        for (int i = 0; i < MAXANIMPICS; i++) {
            animpic[i] = new Animpic();
        }
        for (int i = 0; i < MAXDELAYFUNCTIONS; i++) {
            delayfunc[i] = new Delayfunc();
        }
        for (int i = 0; i < MAXSPRITEELEVS; i++) {
            spriteelev[i] = new Spriteelev();
        }
        for (int i = 0; i < MAXDOORS; i++) {
            doortype[i] = new Doortype();
        }
        for (int i = 0; i < MAXFLOORDOORS; i++) {
            floordoor[i] = new Floordoor();
        }
        for (int i = 0; i < MAXSECTORS; i++) {
            sectoreffect[i] = new Sectoreffect();
        }
        for (int i = 0; i < MAXSECTORS; i++) {
            elevator[i] = new Elevatortype();
        }
        for (int i = 0; i < MAXSECTORVEHICLES; i++) {
            sectorvehicle[i] = new Sectorvehicle();
        }
        for (int i = 0; i < MAXMAPSOUNDFX; i++) {
            mapsndfx[i] = new Mapsndfxtype();
        }
    }

    public boolean load(InputStream is) {
        try {
            LoadGDXHeader(is);
            LoadGDXBlock(is);
            PlayerLoad(is);
            MapLoad(is);
            SectorLoad(is);
            AnimationLoad(is);

            randomseed = StreamUtils.readInt(is);

            hours = StreamUtils.readInt(is);
            minutes = StreamUtils.readInt(is);
            seconds = StreamUtils.readInt(is);
            lockclock = StreamUtils.readInt(is);

            parallaxtype = StreamUtils.readByte(is);
            parallaxyoffs = StreamUtils.readInt(is);
            for (int i = 0; i < MAXPSKYTILES; i++) {
                pskyoff[i] = StreamUtils.readShort(is);
            }
            pskybits = StreamUtils.readShort(is);
            visibility = StreamUtils.readInt(is);
            parallaxvisibility = StreamUtils.readInt(is);

            show2dsector.readObject(is);
            show2dwall.readObject(is);
            show2dsprite.readObject(is);

            automapping = StreamUtils.readByte(is);

            goreflag = StreamUtils.readBoolean(is);

            TagLoad(is);
            for (int i = 0; i < MAXSPRITES; i++) {
                spriteXT[i].readObject(is);
            }
            MissionInfoLoad(is);

            if (is.available() == 0) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void TagLoad(InputStream is) throws IOException {
        numanimates = StreamUtils.readInt(is);
        for (int i = 0; i < numanimates; i++) {
            animpic[i].readObject(is);
        }

        numdelayfuncs = StreamUtils.readShort(is);
        for (int i = 0; i < numdelayfuncs; i++) {
            delayfunc[i].readObject(is);
        }

        for (int i = 0; i < MAXPLAYERS; i++) {
            onelev[i] = StreamUtils.readBoolean(is);
        }

        secnt = StreamUtils.readInt(is);
        for (int i = 0; i < MAXSECTORS; i++) {
            sectoreffect[i].readObject(is);
        }

        for (int i = 0; i < MAXSECTORS; i++) {
            sexref[i] = StreamUtils.readInt(is);
        }

        numdoors = StreamUtils.readInt(is);
        for (int i = 0; i < numdoors; i++) {
            doortype[i].readObject(is);
        }

        for (int i = 0; i < MAXSECTORS; i++) {
            doorxref[i] = StreamUtils.readInt(is);
        }

        numfloordoors = StreamUtils.readInt(is);
        for (int i = 0; i < numfloordoors; i++) {
            floordoor[i].readObject(is);
        }

        for (int i = 0; i < MAXSECTORS; i++) {
            fdxref[i] = StreamUtils.readInt(is);
        }

        numvehicles = StreamUtils.readInt(is);
        for (int i = 0; i < numvehicles; i++) {
            sectorvehicle[i].readObject(is);
        }

        for (int i = 0; i < MAXSECTORS; i++) {
            elevator[i].readObject(is);
        }

        sprelevcnt = StreamUtils.readInt(is);
        for (int i = 0; i < sprelevcnt; i++) {
            spriteelev[i].readObject(is);
        }

        totalmapsndfx = StreamUtils.readInt(is);
        for (int i = 0; i < totalmapsndfx; i++) {
            mapsndfx[i].readObject(is);
        }
    }

    public void MissionInfoLoad(InputStream is) throws IOException {
        for (int i = 0; i < MAXSYMBOLS; i++) {
            symbols[i] = StreamUtils.readBoolean(is);
        }
        for (int i = 0; i < MAXSYMBOLS; i++) {
            symbolsdeposited[i] = StreamUtils.readBoolean(is);
        }

        currentmapno = StreamUtils.readInt(is);
        numlives = (char) (StreamUtils.readUnsignedByte(is));
        mission_accomplished = (char) (StreamUtils.readUnsignedByte(is));
        civillianskilled = StreamUtils.readInt(is);
        generalplay = (char) (StreamUtils.readUnsignedByte(is));
        singlemapmode = (char) (StreamUtils.readUnsignedByte(is));
        allsymsdeposited = StreamUtils.readInt(is);
        killedsonny = StreamUtils.readInt(is);
    }

    public void PlayerLoad(InputStream is) throws IOException {
        numplayers = StreamUtils.readShort(is);
        for (int i = 0; i < numplayers - 1; i++) {
            connectpoint2[i] = (short) (i + 1);
        }
        connectpoint2[numplayers - 1] = -1;

        for (int i = 0; i < MAXPLAYERS; i++) {
            gPlayer[i].readObject(is);
            gPlayer[i].pInput.readObject(is);
        }
    }

    public void AnimationLoad(InputStream is) throws IOException {
        for (int i = 0; i < MAXANIMATES; i++) {
            short index = StreamUtils.readShort(is);
            byte type = StreamUtils.readByte(is);
            gAnimationData[i].id = index;
            gAnimationData[i].type = type;
            gAnimationData[i].ptr = null;
            gAnimationData[i].goal = StreamUtils.readInt(is);
            gAnimationData[i].vel = StreamUtils.readInt(is);
            gAnimationData[i].acc = StreamUtils.readInt(is);
        }
        gAnimationCount = StreamUtils.readInt(is);
    }

    public void SectorLoad(InputStream is) throws IOException {
        for (int i = 0; i < 16; i++) {
            rotatespritelist[i] = StreamUtils.readShort(is);
        }
        rotatespritecnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            warpsectorlist[i] = StreamUtils.readShort(is);
        }
        warpsectorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            xpanningsectorlist[i] = StreamUtils.readShort(is);
        }
        xpanningsectorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 64; i++) {
            ypanningwalllist[i] = StreamUtils.readShort(is);
        }
        ypanningwallcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 64; i++) {
            floorpanninglist[i] = StreamUtils.readShort(is);
        }
        floorpanningcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            dragsectorlist[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 16; i++) {
            dragxdir[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 16; i++) {
            dragydir[i] = StreamUtils.readShort(is);
        }
        dragsectorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 16; i++) {
            dragx1[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 16; i++) {
            dragy1[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 16; i++) {
            dragx2[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 16; i++) {
            dragy2[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 16; i++) {
            dragfloorz[i] = StreamUtils.readInt(is);
        }
        swingcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 5; j++) {
                swingwall[i][j] = StreamUtils.readShort(is);
            }
        }
        for (int i = 0; i < 32; i++) {
            swingsector[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 32; i++) {
            swingangopen[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 32; i++) {
            swingangclosed[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 32; i++) {
            swingangopendir[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 32; i++) {
            swingang[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 32; i++) {
            swinganginc[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 8; j++) {
                swingx[i][j] = StreamUtils.readInt(is);
            }
        }
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 8; j++) {
                swingy[i][j] = StreamUtils.readInt(is);
            }
        }
        for (int i = 0; i < 4; i++) {
            revolvesector[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 4; i++) {
            revolveang[i] = StreamUtils.readShort(is);
        }
        revolvecnt = StreamUtils.readShort(is);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 16; j++) {
                revolvex[i][j] = StreamUtils.readInt(is);
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 16; j++) {
                revolvey[i][j] = StreamUtils.readInt(is);
            }
        }
        for (int i = 0; i < 4; i++) {
            revolvepivotx[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            revolvepivoty[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 128; j++) {
                subwaytracksector[i][j] = StreamUtils.readShort(is);
            }
        }
        for (int i = 0; i < 4; i++) {
            subwaynumsectors[i] = StreamUtils.readShort(is);
        }
        subwaytrackcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 8; j++) {
                subwaystop[i][j] = StreamUtils.readInt(is);
            }
        }
        for (int i = 0; i < 4; i++) {
            subwaystopcnt[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            subwaytrackx1[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            subwaytracky1[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            subwaytrackx2[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            subwaytracky2[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            subwayx[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            subwaygoalstop[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            subwayvel[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            subwaypausetime[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < MAXPLAYERS; i++) {
            waterfountainwall[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < MAXPLAYERS; i++) {
            waterfountaincnt[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < MAXPLAYERS; i++) {
            slimesoundcnt[i] = StreamUtils.readShort(is);
        }
    }

    public void LoadGDXHeader(InputStream is) {
        mission = -1;
        difficulty = -1;
        gUserMap = false;
        boardfilename = null;

        try {
            StreamUtils.skip(is, SAVETIME + SAVENAME);

            mission = StreamUtils.readInt(is);
            difficulty = StreamUtils.readInt(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LoadGDXBlock(InputStream is) throws IOException {
        StreamUtils.skip(is, SAVESCREENSHOTSIZE);

        gUserMap = StreamUtils.readBoolean(is);
        boardfilename = null;
        String name = StreamUtils.readString(is, 144).trim();
        if (!name.isEmpty()) {
            boardfilename = name;
        }
    }

    public void MapLoad(InputStream is) throws IOException {
        sector = new Sector[StreamUtils.readInt(is)];
        for (int i = 0; i < sector.length; i++) {
            sector[i] = new Sector().readObject(is);
        }

        wall = new Wall[StreamUtils.readInt(is)];
        for (int i = 0; i < wall.length; i++) {
            wall[i] = new Wall().readObject(is);
        }

        int numSprites = StreamUtils.readInt(is);
        sprite = new ArrayList<>(numSprites * 2);

        for (int i = 0; i < numSprites; i++) {
            sprite.add(new Sprite().readObject(is));
        }
    }

}
