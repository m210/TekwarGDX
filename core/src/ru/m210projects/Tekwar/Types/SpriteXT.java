package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class SpriteXT implements Serializable<SpriteXT> {

    public int sclass;
    public byte hitpoints;
    public int target;
    public int fxmask;
    public int aimask;
    public short basestat;
    public int basepic;
    public int standpic;
    public int walkpic;
    public int runpic;
    public int attackpic;
    public int deathpic;
    public int painpic;
    public int squatpic;
    public int morphpic;
    public int specialpic;
    public char lock;
    public char weapon;
    public short ext2;

    public void set(int var) {
        hitpoints = (byte) var;
        target = var;
        fxmask = var;
        aimask = var;
        basestat = (short) var;
        basepic = var;
        standpic = var;
        walkpic = var;
        runpic = var;
        attackpic = var;
        deathpic = var;
        painpic = var;
        squatpic = var;
        morphpic = var;
        specialpic = var;
        lock = (char) var;
        weapon = (char) var;
        ext2 = (short) var;
    }

    @Override
    public SpriteXT readObject(InputStream is) throws IOException {
        sclass = StreamUtils.readByte(is);
        hitpoints = StreamUtils.readByte(is);
        target = StreamUtils.readShort(is);
        fxmask = StreamUtils.readShort(is);
        aimask = StreamUtils.readShort(is);
        basestat = StreamUtils.readShort(is);
        basepic = StreamUtils.readShort(is);
        standpic = StreamUtils.readShort(is);
        walkpic = StreamUtils.readShort(is);
        runpic = StreamUtils.readShort(is);
        attackpic = StreamUtils.readShort(is);
        deathpic = StreamUtils.readShort(is);
        painpic = StreamUtils.readShort(is);
        squatpic = StreamUtils.readShort(is);
        morphpic = StreamUtils.readShort(is);
        specialpic = StreamUtils.readShort(is);
        lock = (char) (StreamUtils.readUnsignedByte(is));
        weapon = (char) (StreamUtils.readUnsignedByte(is));
        ext2 = StreamUtils.readShort(is);
        
        return this;
    }

    @Override
    public SpriteXT writeObject(OutputStream os) throws IOException {
        StreamUtils.writeByte(os, (byte) sclass);
        StreamUtils.writeByte(os, hitpoints);
        StreamUtils.writeShort(os, (short) target);
        StreamUtils.writeShort(os, (short) fxmask);
        StreamUtils.writeShort(os, (short) aimask);
        StreamUtils.writeShort(os, basestat);
        StreamUtils.writeShort(os, (short) basepic);
        StreamUtils.writeShort(os, (short) standpic);
        StreamUtils.writeShort(os, (short) walkpic);
        StreamUtils.writeShort(os, (short) runpic);
        StreamUtils.writeShort(os, (short) attackpic);
        StreamUtils.writeShort(os, (short) deathpic);
        StreamUtils.writeShort(os, (short) painpic);
        StreamUtils.writeShort(os, (short) squatpic);
        StreamUtils.writeShort(os, (short) morphpic);
        StreamUtils.writeShort(os, (short) specialpic);
        StreamUtils.writeByte(os, (byte) lock);
        StreamUtils.writeByte(os, (byte) weapon);
        StreamUtils.writeShort(os, ext2);
        
        return this;
    }
}
