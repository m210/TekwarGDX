package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Floordoor implements Serializable<Floordoor> {

    public int state;
    public int sectnum;
    public short wall1, wall2;
    public int dist1, dist2;
    public int dir;

    public void reset() {
        state = 0;
        sectnum = 0;
        wall1 = 0;
        wall2 = 0;
        dist1 = 0;
        dist2 = 0;
        dir = 0;
    }

    @Override
    public Floordoor readObject(InputStream is) throws IOException {
        state = StreamUtils.readInt(is);
        sectnum = StreamUtils.readInt(is);
        wall1 = StreamUtils.readShort(is);
        wall2 = StreamUtils.readShort(is);
        dist1 = StreamUtils.readInt(is);
        dist2 = StreamUtils.readInt(is);
        dir = StreamUtils.readInt(is);
        
        return this;
    }

    @Override
    public Floordoor writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os,state);
        StreamUtils.writeInt(os,sectnum);
        StreamUtils.writeShort(os, wall1);
        StreamUtils.writeShort(os, wall2);
        StreamUtils.writeInt(os,dist1);
        StreamUtils.writeInt(os,dist2);
        StreamUtils.writeInt(os,dir);
        
        return this;
    }
}
