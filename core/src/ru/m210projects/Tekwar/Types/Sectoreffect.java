package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

public class Sectoreffect implements Serializable<Sectoreffect> {
    
    public int sectorflags;
    public int animate;
    public int hi, lo;
    public int delay, delayreset;
    public int ang;
    public int triggerable;
    public short warpto = -1;
    public int warpx, warpy, warpz;
    public int sin, cos;
    public short damage;

    public void reset() {
        sectorflags = 0;
        animate = 0;
        hi = 0;
        lo = 0;

        delay = 0;
        delayreset = 0;
        ang = 0;
        triggerable = 0;
        warpto = -1;
        warpx = 0;
        warpy = 0;
        warpz = 0;
        sin = 0;
        cos = 0;
        damage = 0;
    }

    @Override
    public Sectoreffect readObject(InputStream is) throws IOException {
        sectorflags = StreamUtils.readInt(is);
        animate = StreamUtils.readInt(is);
        hi = StreamUtils.readInt(is);
        lo = StreamUtils.readInt(is);

        delay = StreamUtils.readInt(is);
        delayreset = StreamUtils.readInt(is);
        ang = StreamUtils.readInt(is);
        triggerable = StreamUtils.readInt(is);
        warpto = StreamUtils.readShort(is);
        warpx = StreamUtils.readInt(is);
        warpy = StreamUtils.readInt(is);
        warpz = StreamUtils.readInt(is);
        sin = StreamUtils.readInt(is);
        cos = StreamUtils.readInt(is);
        damage = StreamUtils.readShort(is);
        
        return this;
    }

    @Override
    public Sectoreffect writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os,sectorflags);
        StreamUtils.writeInt(os,animate);
        StreamUtils.writeInt(os,hi);
        StreamUtils.writeInt(os,lo);

        StreamUtils.writeInt(os,delay);
        StreamUtils.writeInt(os,delayreset);
        StreamUtils.writeInt(os,ang);
        StreamUtils.writeInt(os,triggerable);
        StreamUtils.writeShort(os, warpto);
        StreamUtils.writeInt(os,warpx);
        StreamUtils.writeInt(os,warpy);
        StreamUtils.writeInt(os,warpz);
        StreamUtils.writeInt(os,sin);
        StreamUtils.writeInt(os,cos);
        StreamUtils.writeShort(os, damage);

        return this;
    }
}
