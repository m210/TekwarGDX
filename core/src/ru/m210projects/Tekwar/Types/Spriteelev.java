package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static ru.m210projects.Tekwar.Tektag.*;

public class Spriteelev implements Serializable<Spriteelev> {

    public final int[] sprnum = new int[MAXPARTS];
    public final int[] door = new int[MAXELEVDOORS];
    public final int[] startz = new int[MAXPARTS];
    public final int[] floorz = new int[MAXELEVFLOORS];
    public int state;
    public int parts;
    public int doorpos;
    public int curfloor;
    public int curdir;
    public int delay;
    public int floors;
    public int floorpos;
    public int doors;

    public void reset() {
        state = 0;
        parts = 0;
        Arrays.fill(sprnum, 0);
        Arrays.fill(door, 0);
        Arrays.fill(startz, 0);
        Arrays.fill(floorz, 0);

        doorpos = 0;
        curfloor = 0;
        curdir = 0;
        delay = 0;
        floors = 0;
        floorpos = 0;
        doors = 0;
    }

    @Override
    public Spriteelev readObject(InputStream is) throws IOException {
        state = StreamUtils.readInt(is);
        parts = StreamUtils.readInt(is);
        for (int i = 0; i < MAXPARTS; i++) {
            sprnum[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < MAXELEVDOORS; i++) {
            door[i] = StreamUtils.readInt(is);
        }
        doorpos = StreamUtils.readInt(is);
        for (int i = 0; i < MAXPARTS; i++) {
            startz[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < MAXELEVFLOORS; i++) {
            floorz[i] = StreamUtils.readInt(is);
        }
        curfloor = StreamUtils.readInt(is);
        curdir = StreamUtils.readInt(is);
        delay = StreamUtils.readInt(is);
        floors = StreamUtils.readInt(is);
        floorpos = StreamUtils.readInt(is);
        doors = StreamUtils.readInt(is);

        return this;
    }

    @Override
    public Spriteelev writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, state);
        StreamUtils.writeInt(os, parts);
        for (int j = 0; j < MAXPARTS; j++) {
            StreamUtils.writeInt(os, sprnum[j]);
        }
        for (int j = 0; j < MAXELEVDOORS; j++) {
            StreamUtils.writeInt(os, door[j]);
        }
        StreamUtils.writeInt(os, doorpos);
        for (int j = 0; j < MAXPARTS; j++) {
            StreamUtils.writeInt(os, startz[j]);
        }
        for (int j = 0; j < MAXELEVFLOORS; j++) {
            StreamUtils.writeInt(os, floorz[j]);
        }
        StreamUtils.writeInt(os, curfloor);
        StreamUtils.writeInt(os, curdir);
        StreamUtils.writeInt(os, delay);
        StreamUtils.writeInt(os, floors);
        StreamUtils.writeInt(os, floorpos);
        StreamUtils.writeInt(os, doors);

        return this;
    }
}
