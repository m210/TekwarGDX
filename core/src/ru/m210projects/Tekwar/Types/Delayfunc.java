package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

public class Delayfunc implements Serializable<Delayfunc> {
    
    public short func;
    public int tics;
    public short parm;

    public void set(Delayfunc src) {
        func = src.func;
        tics = src.tics;
        parm = src.parm;
    }

    public Delayfunc readObject(InputStream is) throws IOException {
        func = StreamUtils.readShort(is);
        tics = StreamUtils.readInt(is);
        parm = StreamUtils.readShort(is);
        return this;
    }

    @Override
    public Delayfunc writeObject(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, func);
        StreamUtils.writeInt(os,tics);
        StreamUtils.writeShort(os, parm);
        
        return this;
    }

    public void set(int var) {
        func = (short) var;
        tics = var;
        parm = (short) var;
    }
}
