package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

public class Elevatortype implements Serializable<Elevatortype> {
    
    public int hilevel;
    public int lolevel;

    public void reset() {
        hilevel = 0;
        lolevel = 0;
    }

    @Override
    public Elevatortype readObject(InputStream is) throws IOException {
        hilevel = StreamUtils.readInt(is);
        lolevel = StreamUtils.readInt(is);
        
        return this;
    }

    @Override
    public Elevatortype writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os,hilevel);
        StreamUtils.writeInt(os,lolevel);
        
        return this;
    }
}
