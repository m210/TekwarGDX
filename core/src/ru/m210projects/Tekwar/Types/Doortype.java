package ru.m210projects.Tekwar.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import java.io.OutputStream;

public class Doortype implements Serializable<Doortype> {
    
    public int type;
    public int state;
    public int sector;
    public int step;
    public int delay;
    public final int[] goalz = new int[4];
    public final int[] points = new int[4];
    public final short[] walls = new short[8];
    public int centx;
    public int centy;
    public int centz;
    public int subtype; // jeff added 9-20

    public void reset() {
        type = 0;
        state = 0;
        sector = 0;
        step = 0;
        delay = 0;
        for (int i = 0; i < 4; i++) {
            goalz[i] = 0;
        }
        for (int i = 0; i < 4; i++) {
            points[i] = 0;
        }
        for (int i = 0; i < 8; i++) {
            walls[i] = 0;
        }
        centx = 0;
        centy = 0;
        centz = 0;
        subtype = 0;
    }

    @Override
    public Doortype readObject(InputStream is) throws IOException {
        type = StreamUtils.readInt(is);
        state = StreamUtils.readInt(is);
        sector = StreamUtils.readInt(is);
        step = StreamUtils.readInt(is);
        delay = StreamUtils.readInt(is);
        for (int i = 0; i < 4; i++) {
            goalz[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++) {
            points[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 8; i++) {
            walls[i] = StreamUtils.readShort(is);
        }
        centx = StreamUtils.readInt(is);
        centy = StreamUtils.readInt(is);
        centz = StreamUtils.readInt(is);
        subtype = StreamUtils.readInt(is);
        
        return this;
    }

    @Override
    public Doortype writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os,type);
        StreamUtils.writeInt(os,state);
        StreamUtils.writeInt(os,sector);
        StreamUtils.writeInt(os,step);
        StreamUtils.writeInt(os,delay);
        for (int j = 0; j < 4; j++) {
            StreamUtils.writeInt(os,goalz[j]);
        }
        for (int j = 0; j < 4; j++) {
            StreamUtils.writeInt(os,points[j]);
        }
        for (int j = 0; j < 8; j++) {
            StreamUtils.writeShort(os, walls[j]);
        }
        StreamUtils.writeInt(os,centx);
        StreamUtils.writeInt(os,centy);
        StreamUtils.writeInt(os,centz);
        StreamUtils.writeInt(os,subtype);
        
        return this;
    }
}
