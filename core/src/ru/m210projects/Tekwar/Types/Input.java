package ru.m210projects.Tekwar.Types;

import java.io.IOException;
import java.io.InputStream;
import ru.m210projects.Build.Pattern.BuildNet.NetInput;
import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class Input implements NetInput, Serializable<Input> {

    public byte vel;
    public short svel;
    public float angvel;
    public int selectedgun;
    public float mlook;
    public boolean Jump;
    public boolean Crouch;
    public boolean Look_Up;
    public boolean Look_Down;
    public boolean Center;
    public boolean Holster_Weapon;
    public boolean Master;
    public boolean Run;
    public boolean Use;
    public boolean Fire;

    @Override
    public void reset() {
        vel = 0;
        svel = 0;
        angvel = 0;
        selectedgun = -1;
        mlook = 0;

        Jump = false;
        Crouch = false;
        Look_Up = false;
        Look_Down = false;
        Center = false;
        Holster_Weapon = false;
        Master = false;
        Run = false;
        Use = false;
        Fire = false;
    }

    @Override
    public NetInput Copy(NetInput src) {
        Input tekinput = (Input) src;

        this.vel = tekinput.vel;
        this.svel = tekinput.svel;
        this.angvel = tekinput.angvel;
        this.selectedgun = tekinput.selectedgun;
        this.mlook = tekinput.mlook;

        this.Jump = tekinput.Jump;
        this.Crouch = tekinput.Crouch;
        this.Look_Up = tekinput.Look_Up;
        this.Look_Down = tekinput.Look_Down;
        this.Center = tekinput.Center;
        this.Holster_Weapon = tekinput.Holster_Weapon;
        this.Master = tekinput.Master;
        this.Run = tekinput.Run;
        this.Use = tekinput.Use;
        this.Fire = tekinput.Fire;

        return this;
    }

    @Override
    public int GetInput(byte[] p, int offset, NetInput oldInput) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int PutInput(byte[] p, int offset, NetInput oldInput) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Input readObject(InputStream is) throws IOException {
        this.selectedgun = StreamUtils.readInt(is);
        this.vel = StreamUtils.readByte(is);
        this.svel = StreamUtils.readShort(is);
        this.angvel = StreamUtils.readFloat(is);
        short bits = StreamUtils.readShort(is);
        Jump = (bits & 1) == 1;
        Crouch = ((bits & 2) >> 1) == 1;
        Look_Up = ((bits & 4) >> 2) == 1;
        Look_Down = ((bits & 8) >> 3) == 1;
        Center = ((bits & 64) >> 6) == 1;
        Holster_Weapon = ((bits & 128) >> 7) == 1;
        Run = ((bits & 256) >> 8) == 1;
        Master = ((bits & 512) >> 9) == 1;
        Use = ((bits & 1024) >> 10) == 1;
        Fire = ((bits & 2048) >> 11) == 1;

        this.mlook = StreamUtils.readFloat(is);

        return this;
    }

    @Override
    public Input writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, selectedgun);
        StreamUtils.writeByte(os, vel);
        StreamUtils.writeShort(os, svel);
        StreamUtils.writeFloat(os, angvel);

        short bits = 0;
        bits |= (short) (Jump ? 1 : 0);
        bits |= (short) (Crouch ? 2 : 0);
        bits |= (short) (Look_Up ? 4 : 0);
        bits |= (short) (Look_Down ? 8 : 0);
        bits |= (short) (Center ? 64 : 0);
        bits |= (short) (Holster_Weapon ? 128 : 0);
        bits |= (short) (Run ? 256 : 0);
        bits |= (short) (Master ? 512 : 0);
        bits |= (short) (Use ? 1024 : 0);
        bits |= (short) (Fire ? 2048 : 0);
        StreamUtils.writeShort(os, bits);
        StreamUtils.writeFloat(os, mlook);

        return this;
    }
}
