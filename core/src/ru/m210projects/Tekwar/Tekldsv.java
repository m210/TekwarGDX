package ru.m210projects.Tekwar;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.Pattern.Tools.SaveManager;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Tekwar.Types.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Tekwar.Animate.*;
import static ru.m210projects.Tekwar.Globals.ST_UNIQUE;
import static ru.m210projects.Tekwar.Globals.gDifficulty;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekgun.goreflag;
import static ru.m210projects.Tekwar.Tekmap.*;
import static ru.m210projects.Tekwar.Tekprep.*;
import static ru.m210projects.Tekwar.Teksnd.*;
import static ru.m210projects.Tekwar.Tekstat.sectflash;
import static ru.m210projects.Tekwar.Tektag.*;
import static ru.m210projects.Tekwar.Types.ANIMATION.*;
import static ru.m210projects.Tekwar.View.*;

public class Tekldsv {

    public static final String savsign = "TKWR";
    public static final int gdxSave = 100;
    public static final int currentGdxSave = 103; // v1.16 == 102
    public static final int SAVETIME = 8;
    public static final int SAVENAME = 32;
    public static final int SAVESCREENSHOTSIZE = 160 * 100;
    public static final char[] filenum = new char[4];
    public static boolean gQuickSaving;
//    public static boolean gAutosaveRequest;
    public static final LSInfo lsInf = new LSInfo();
    public static FileEntry lastload;
    public static int quickslot = 0;
    public static final SafeLoader loader = new SafeLoader();

    public static void FindSaves(Directory dir) {
        for (Entry file : dir) {
            if (file.isExtension("sav") && file instanceof FileEntry) {
                try (InputStream is = file.getInputStream()) {
                    String signature = StreamUtils.readString(is, 4);
                    if (signature.isEmpty()) {
                        continue;
                    }

                    if (signature.equals(savsign)) {
                        int nVersion = StreamUtils.readShort(is);
                        if (nVersion >= gdxSave) {
                            long time = StreamUtils.readLong(is);
                            String savname = StreamUtils.readString(is, SAVENAME);
                            game.pSavemgr.add(savname, time, (FileEntry) file);
                        }
                    }
                } catch (Exception ignored) {
                }
            }
        }
        game.pSavemgr.sort();
    }

    public static int lsReadLoadData(FileEntry file) {
        if (file.exists()) {
            ArtEntry pic = engine.getTile(SaveManager.Screenshot);
            if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
                pic = engine.allocatepermanenttile(SaveManager.Screenshot, 160, 100);
            }

            try (InputStream is = file.getInputStream()) {
                int nVersion = checkSave(is) & 0xFFFF;
                lsInf.clear();

                if (nVersion == currentGdxSave) {
                    lsInf.date = game.date.getDate(StreamUtils.readLong(is));
                    StreamUtils.skip(is, SAVENAME);

                    lsInf.read(is);
                    if (is.available() <= SAVESCREENSHOTSIZE) {
                        return -1;
                    }

                    ((DynamicArtEntry) pic).copyData(StreamUtils.readBytes(is, SAVESCREENSHOTSIZE));

                    return 1;
                } else {
                    lsInf.info = "Incompatible ver. " + nVersion + " != " + currentGdxSave;
                    return -1;
                }
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }

        lsInf.clear();
        return -1;
    }

    public static String makeNum(int num) {
        filenum[3] = (char) ((num % 10) + 48);
        filenum[2] = (char) (((num / 10) % 10) + 48);
        filenum[1] = (char) (((num / 100) % 10) + 48);
        filenum[0] = (char) (((num / 1000) % 10) + 48);

        return new String(filenum);
    }

    public static int checkSave(InputStream is) throws IOException {
        String signature = StreamUtils.readString(is, 4);
        if (!signature.equals(savsign)) {
            return 0;
        }

        return StreamUtils.readShort(is);
    }

    public static boolean checkfile(InputStream is) throws IOException {
        int nVersion = checkSave(is);
        if (nVersion != currentGdxSave) {
            return false;
        }
        return loader.load(is);
    }

    public static void LoadGDXBlock() {
        mUserFlag = loader.gUserMap ? UserFlag.UserMap : UserFlag.None;
        boardfilename = game.getCache().getEntry(loader.boardfilename, true);
    }

    public static void load() {
        mission = loader.mission;
        gDifficulty = loader.difficulty;

        LoadGDXBlock();
        PlayerLoad();
        MapLoad();
        SectorLoad();
        AnimationLoad();

        screenpeek = myconnectindex;
        lockclock = loader.lockclock;
        engine.srand(loader.randomseed);

        hours = loader.hours;
        minutes = loader.minutes;
        seconds = loader.seconds;

        visibility = loader.visibility;
        parallaxtype = loader.parallaxtype;
        Renderer renderer = game.getRenderer();
        renderer.setParallaxOffset(loader.parallaxyoffs);
        System.arraycopy(loader.pskyoff, 0, pskyoff, 0, MAXPSKYTILES);
        pskybits = loader.pskybits;
        parallaxvisibility = loader.parallaxvisibility;

        show2dsector.copy(loader.show2dsector);
        show2dwall.copy(loader.show2dwall);
        show2dsprite.copy(loader.show2dsprite);
        automapping = loader.automapping;

        goreflag = loader.goreflag;
        gViewMode = kView3D;

        tektagload();

        System.arraycopy(loader.spriteXT, 0, spriteXT, 0, MAXSPRITES);

        tekloadmissioninfo();

        // initialize blast sector flashes
        if (sectflash == null) {
            sectflash = new Tekstat.sectflashtype();
        }
        sectflash.sectnum = 0;
        sectflash.ovis = 0;
        sectflash.step = 0;

        // intitialize subwaysound[]s
        for (int i = 0; i < 4; i++) {
            subwaysound[i] = -1;
        }

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            initplayersprite((short) i);
        }

        game.doPrecache(() -> {
            engine.getTimer().setTotalClock(lockclock);
            game.pNet.ototalclock = lockclock;

            game.gPaused = false;
            game.changeScreen(gGameScreen);
            game.pNet.ready2send = true;

            game.nNetMode = NetMode.Single;

            showmessage("GAME LOADED");

            if (generalplay == 1) {
                startmusic((int) (7 * Math.random()));
            } else {
//						    	if(oldmapno != currentmapno) {
                if (currentmapno <= 3) {
                    menusong(1);
                } else {
                    startmusic(mission);
                }
//						    	}
            }

            System.gc();
            Console.out.println("debug: end loadgame()", OsdColor.BLUE);
        });
    }

    public static void tekloadmissioninfo() {

        System.arraycopy(loader.symbols, 0, symbols, 0, MAXSYMBOLS);
        System.arraycopy(loader.symbolsdeposited, 0, symbolsdeposited, 0, MAXSYMBOLS);

        currentmapno = loader.currentmapno;
        numlives = loader.numlives;
        mission_accomplished = loader.mission_accomplished;
        civillianskilled = loader.civillianskilled;
        generalplay = loader.generalplay;
        singlemapmode = loader.singlemapmode;
        allsymsdeposited = loader.allsymsdeposited;
        killedsonny = loader.killedsonny;
    }

    public static void tektagload() {

        numanimates = loader.numanimates;
        System.arraycopy(loader.animpic, 0, animpic, 0, numanimates);
        numdelayfuncs = loader.numdelayfuncs;
        System.arraycopy(loader.delayfunc, 0, delayfunc, 0, numdelayfuncs);
        System.arraycopy(loader.onelev, 0, onelev, 0, MAXPLAYERS);
        secnt = loader.secnt;
        System.arraycopy(loader.sectoreffect, 0, sectoreffect, 0, MAXSECTORS);
        System.arraycopy(loader.sexref, 0, sexref, 0, MAXSECTORS);
        numdoors = loader.numdoors;
        System.arraycopy(loader.doortype, 0, doortype, 0, numdoors);
        System.arraycopy(loader.doorxref, 0, doorxref, 0, MAXSECTORS);
        numfloordoors = loader.numfloordoors;
        System.arraycopy(loader.floordoor, 0, floordoor, 0, numfloordoors);
        System.arraycopy(loader.fdxref, 0, fdxref, 0, MAXSECTORS);
        numvehicles = loader.numvehicles;
        System.arraycopy(loader.sectorvehicle, 0, sectorvehicle, 0, numvehicles);

        // must reinvoke vehicle sounds since all sounds were stopped
        // else updatevehiclesounds will update whatever is using
        // dsoundptr[vptr.soundindex]
        clearvehiclesoundindexes();

        System.arraycopy(loader.elevator, 0, elevator, 0, MAXSECTORS);
        sprelevcnt = loader.sprelevcnt;
        System.arraycopy(loader.spriteelev, 0, spriteelev, 0, sprelevcnt);
        totalmapsndfx = loader.totalmapsndfx;
        System.arraycopy(loader.mapsndfx, 0, mapsndfx, 0, totalmapsndfx);

        for (int i = 0; i < totalmapsndfx; i++) {
            // did we leave with a TOGGLED sound playong ?
            if ((mapsndfx[i].type == MAP_SFX_TOGGLED) && (mapsndfx[i].id != -1)) {
                mapsndfx[i].id = playsound(mapsndfx[i].snum, mapsndfx[i].x, mapsndfx[i].y, mapsndfx[i].loops,
                        ST_UNIQUE);
            }
        }
    }

    public static void quicksave() {
        if (gPlayer[myconnectindex].health != 0) {
            gQuickSaving = true;
        }
    }

    public static boolean canLoad(FileEntry fil) {
        return fil.load(is -> {
            int nVersion = checkSave(is) & 0xFFFF;
            if (nVersion != currentGdxSave) {
                throw new IOException("Wrong file version");
            }
        });
    }

    public static void quickload() {
        if (numplayers > 1) {
            return;
        }

        final FileEntry loadFile = game.pSavemgr.getLast();
        if (canLoad(loadFile)) {
            game.changeScreen(gLoadingScreen);
            gLoadingScreen.init(() -> {
                if (!loadgame(loadFile)) {
                    game.setPrevScreen();
                    if (game.isCurrentScreen(gGameScreen)) {
                        showmessage("Incompatible version of saved game found!");
                        game.pNet.ready2send = true;
                    }
                }
            });
        }

    }

    public static void PlayerLoad() {
        numplayers = loader.numplayers;
        System.arraycopy(loader.connectpoint2, 0, connectpoint2, 0, MAXPLAYERS);

        for (int i = 0; i < MAXPLAYERS; i++) {
            gPlayer[i].copy(loader.gPlayer[i]);
            gPlayer[i].pInput.Copy(loader.gPlayer[i].pInput);
        }
    }

    public static void MapLoad() {
        boardService.setBoard(new Board(null, loader.sector, loader.wall, loader.sprite));
    }

    public static void AnimationLoad() {
        for (int i = 0; i < MAXANIMATES; i++) {
            gAnimationData[i].id = loader.gAnimationData[i].id;
            gAnimationData[i].type = loader.gAnimationData[i].type;
            gAnimationData[i].ptr = loader.gAnimationData[i].ptr;
            gAnimationData[i].goal = loader.gAnimationData[i].goal;
            gAnimationData[i].vel = loader.gAnimationData[i].vel;
            gAnimationData[i].acc = loader.gAnimationData[i].acc;
        }
        gAnimationCount = loader.gAnimationCount;

        for (int i = gAnimationCount - 1; i >= 0; i--) {
            ANIMATION gAnm = gAnimationData[i];
            Object object = (gAnm.ptr = getobject(gAnm.id, gAnm.type));
            switch (gAnm.type) {
                case WALLX:
                case WALLY:
                    game.pInt.setwallinterpolate(gAnm.id, (Wall) object);
                    break;
                case FLOORZ:
                    game.pInt.setfloorinterpolate(gAnm.id, (Sector) object);
                    break;
                case CEILZ:
                    game.pInt.setceilinterpolate(gAnm.id, (Sector) object);
                    break;
            }
        }
    }

    public static void SectorLoad() {
        System.arraycopy(loader.rotatespritelist, 0, rotatespritelist, 0, loader.rotatespritelist.length);
        rotatespritecnt = loader.rotatespritecnt;
        System.arraycopy(loader.warpsectorlist, 0, warpsectorlist, 0, loader.warpsectorlist.length);
        warpsectorcnt = loader.warpsectorcnt;
        System.arraycopy(loader.xpanningsectorlist, 0, xpanningsectorlist, 0, loader.xpanningsectorlist.length);
        xpanningsectorcnt = loader.xpanningsectorcnt;
        System.arraycopy(loader.ypanningwalllist, 0, ypanningwalllist, 0, loader.ypanningwalllist.length);
        ypanningwallcnt = loader.ypanningwallcnt;
        System.arraycopy(loader.floorpanninglist, 0, floorpanninglist, 0, loader.floorpanninglist.length);
        floorpanningcnt = loader.floorpanningcnt;
        System.arraycopy(loader.dragsectorlist, 0, dragsectorlist, 0, loader.dragsectorlist.length);
        System.arraycopy(loader.dragxdir, 0, dragxdir, 0, loader.dragxdir.length);
        System.arraycopy(loader.dragydir, 0, dragydir, 0, loader.dragydir.length);
        dragsectorcnt = loader.dragsectorcnt;
        System.arraycopy(loader.dragx1, 0, dragx1, 0, loader.dragx1.length);
        System.arraycopy(loader.dragy1, 0, dragy1, 0, loader.dragy1.length);
        System.arraycopy(loader.dragx2, 0, dragx2, 0, loader.dragx2.length);
        System.arraycopy(loader.dragy2, 0, dragy2, 0, loader.dragy2.length);
        System.arraycopy(loader.dragfloorz, 0, dragfloorz, 0, loader.dragfloorz.length);
        swingcnt = loader.swingcnt;
        for (int i = 0; i < 32; i++) {
            System.arraycopy(loader.swingwall[i], 0, swingwall[i], 0, loader.swingwall[i].length);
        }
        System.arraycopy(loader.swingsector, 0, swingsector, 0, loader.swingsector.length);
        System.arraycopy(loader.swingangopen, 0, swingangopen, 0, loader.swingangopen.length);
        System.arraycopy(loader.swingangclosed, 0, swingangclosed, 0, loader.swingangclosed.length);
        System.arraycopy(loader.swingangopendir, 0, swingangopendir, 0, loader.swingangopendir.length);
        System.arraycopy(loader.swingang, 0, swingang, 0, loader.swingang.length);
        System.arraycopy(loader.swinganginc, 0, swinganginc, 0, loader.swinganginc.length);
        for (int i = 0; i < 32; i++) {
            System.arraycopy(loader.swingx[i], 0, swingx[i], 0, loader.swingx[i].length);
        }
        for (int i = 0; i < 32; i++) {
            System.arraycopy(loader.swingy[i], 0, swingy[i], 0, loader.swingy[i].length);
        }
        System.arraycopy(loader.revolvesector, 0, revolvesector, 0, loader.revolvesector.length);
        System.arraycopy(loader.revolveang, 0, revolveang, 0, loader.revolveang.length);
        revolvecnt = loader.revolvecnt;
        for (int i = 0; i < 4; i++) {
            System.arraycopy(loader.revolvex[i], 0, revolvex[i], 0, loader.revolvex[i].length);
        }
        for (int i = 0; i < 4; i++) {
            System.arraycopy(loader.revolvey[i], 0, revolvey[i], 0, loader.revolvey[i].length);
        }
        System.arraycopy(loader.revolvepivotx, 0, revolvepivotx, 0, loader.revolvepivotx.length);
        System.arraycopy(loader.revolvepivoty, 0, revolvepivoty, 0, loader.revolvepivoty.length);
        for (int i = 0; i < 4; i++) {
            System.arraycopy(loader.subwaytracksector[i], 0, subwaytracksector[i], 0,
                    loader.subwaytracksector[i].length);
        }
        System.arraycopy(loader.subwaynumsectors, 0, subwaynumsectors, 0, loader.subwaynumsectors.length);
        subwaytrackcnt = loader.subwaytrackcnt;
        for (int i = 0; i < 4; i++) {
            System.arraycopy(loader.subwaystop[i], 0, subwaystop[i], 0, loader.subwaystop[i].length);
        }
        System.arraycopy(loader.subwaystopcnt, 0, subwaystopcnt, 0, loader.subwaystopcnt.length);
        System.arraycopy(loader.subwaytrackx1, 0, subwaytrackx1, 0, loader.subwaytrackx1.length);
        System.arraycopy(loader.subwaytracky1, 0, subwaytracky1, 0, loader.subwaytracky1.length);
        System.arraycopy(loader.subwaytrackx2, 0, subwaytrackx2, 0, loader.subwaytrackx2.length);
        System.arraycopy(loader.subwaytracky2, 0, subwaytracky2, 0, loader.subwaytracky2.length);
        System.arraycopy(loader.subwayx, 0, subwayx, 0, loader.subwayx.length);
        System.arraycopy(loader.subwaygoalstop, 0, subwaygoalstop, 0, loader.subwaygoalstop.length);
        System.arraycopy(loader.subwayvel, 0, subwayvel, 0, loader.subwayvel.length);
        System.arraycopy(loader.subwaypausetime, 0, subwaypausetime, 0, loader.subwaypausetime.length);
        System.arraycopy(loader.waterfountainwall, 0, waterfountainwall, 0, loader.waterfountainwall.length);
        System.arraycopy(loader.waterfountaincnt, 0, waterfountaincnt, 0, loader.waterfountaincnt.length);
        System.arraycopy(loader.slimesoundcnt, 0, slimesoundcnt, 0, loader.slimesoundcnt.length);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean loadgame(FileEntry fil) {
        return fil.load(is -> {
            Console.out.println("debug: start loadgame()", OsdColor.BLUE);
            boolean status = checkfile(is);
            if (status) {
                load();
                if (lastload == null || !lastload.exists()) {
                    lastload = fil;
                }
                return;
            }

            showmessage("Incompatible version of saved game found!");
            throw new IOException("Incompatible version of saved game found!");
        });
    }

    public static void SectorSave(OutputStream os) throws IOException {
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, rotatespritelist[i]);
        }
        StreamUtils.writeShort(os, rotatespritecnt); // 34
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, warpsectorlist[i]); // 66
        }
        StreamUtils.writeShort(os, warpsectorcnt); // 68
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, xpanningsectorlist[i]); // 100
        }
        StreamUtils.writeShort(os, xpanningsectorcnt); // 102
        for (int i = 0; i < 64; i++) {
            StreamUtils.writeShort(os, ypanningwalllist[i]); // 230
        }
        StreamUtils.writeShort(os, ypanningwallcnt); // 232
        for (int i = 0; i < 64; i++) {
            StreamUtils.writeShort(os, floorpanninglist[i]);
        }
        StreamUtils.writeShort(os, floorpanningcnt); // 362
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, dragsectorlist[i]); // 394
        }
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, dragxdir[i]); // 426
        }
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeShort(os, dragydir[i]); // 458
        }
        StreamUtils.writeShort(os, dragsectorcnt); // 460
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeInt(os, dragx1[i]); // 524
        }
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeInt(os, dragy1[i]); // 588
        }
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeInt(os, dragx2[i]); // 652
        }
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeInt(os, dragy2[i]); // 716
        }
        for (int i = 0; i < 16; i++) {
            StreamUtils.writeInt(os, dragfloorz[i]); // 780
        }
        StreamUtils.writeShort(os, swingcnt); // 782
        for (int a = 0; a < 32; ++a) {
            for (int b = 0; b < 5; ++b) {
                StreamUtils.writeShort(os, swingwall[a][b]); // 1102
            }
        }
        for (int i = 0; i < 32; i++) {
            StreamUtils.writeShort(os, swingsector[i]); // 1166
        }
        for (int i = 0; i < 32; i++) {
            StreamUtils.writeShort(os, swingangopen[i]); // 1230
        }
        for (int i = 0; i < 32; i++) {
            StreamUtils.writeShort(os, swingangclosed[i]); // 1294
        }
        for (int i = 0; i < 32; i++) {
            StreamUtils.writeShort(os, swingangopendir[i]); // 1358
        }
        for (int i = 0; i < 32; i++) {
            StreamUtils.writeShort(os, swingang[i]); // 1422
        }
        for (int i = 0; i < 32; i++) {
            StreamUtils.writeShort(os, swinganginc[i]); // 1486
        }
        for (int a = 0; a < 32; ++a) {
            for (int b = 0; b < 8; ++b) {
                StreamUtils.writeInt(os, swingx[a][b]);
            }
        }
        for (int a = 0; a < 32; ++a) {
            for (int b = 0; b < 8; ++b) {
                StreamUtils.writeInt(os, swingy[a][b]); // 3534
            }
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeShort(os, revolvesector[i]); // 3542
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeShort(os, revolveang[i]);
        }
        StreamUtils.writeShort(os, revolvecnt); // 3552
        for (int a = 0; a < 4; ++a) {
            for (int b = 0; b < 16; ++b) {
                StreamUtils.writeInt(os, revolvex[a][b]);
            }
        }
        // 3808
        for (int a = 0; a < 4; ++a) {
            for (int b = 0; b < 16; ++b) {
                StreamUtils.writeInt(os, revolvey[a][b]);
            }
        }
        // 4064
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, revolvepivotx[i]); // 4080
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, revolvepivoty[i]); // 4096
        }
        for (int a = 0; a < 4; ++a) {
            for (int b = 0; b < 128; ++b) {
                StreamUtils.writeShort(os, subwaytracksector[a][b]); // 5120
            }
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeShort(os, subwaynumsectors[i]);
        }
        StreamUtils.writeShort(os, subwaytrackcnt); // 5130
        for (int a = 0; a < 4; ++a) {
            for (int b = 0; b < 8; ++b) {
                StreamUtils.writeInt(os, subwaystop[a][b]); // 5258
            }
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, subwaystopcnt[i]);
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, subwaytrackx1[i]);
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, subwaytracky1[i]);
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, subwaytrackx2[i]);
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, subwaytracky2[i]);
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, subwayx[i]);
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, subwaygoalstop[i]);
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, subwayvel[i]);
        }
        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, subwaypausetime[i]); // 5402
        }
        for (int i = 0; i < MAXPLAYERS; i++) {
            StreamUtils.writeShort(os, waterfountainwall[i]);
        }
        for (int i = 0; i < MAXPLAYERS; i++) {
            StreamUtils.writeShort(os, waterfountaincnt[i]);
        }
        for (int i = 0; i < MAXPLAYERS; i++) {
            StreamUtils.writeShort(os, slimesoundcnt[i]); // 5498
        }
    }

    public static void PlayerSave(OutputStream os) throws IOException {
        StreamUtils.writeShort(os, numplayers);

        for (int i = 0; i < MAXPLAYERS; i++) {
            gPlayer[i].writeObject(os);
            gPlayer[i].pInput.writeObject(os);
        }
    }

    public static void MapSave(OutputStream os) throws IOException {
        Board board = boardService.getBoard();
        Sector[] sectors = board.getSectors();
        StreamUtils.writeInt(os, sectors.length);
        for (Sector s : sectors) {
            s.writeObject(os);
        }

        Wall[] walls = board.getWalls();
        StreamUtils.writeInt(os, walls.length);
        for (Wall wal : walls) {
            wal.writeObject(os);
        }

        List<Sprite> sprites = board.getSprites();
        StreamUtils.writeInt(os, sprites.size());
        for (Sprite s : sprites) {
            s.writeObject(os);
        }
    }

    public static void AnimationSave(OutputStream os) throws IOException {
        for (int i = 0; i < MAXANIMATES; i++) {
            StreamUtils.writeShort(os, gAnimationData[i].id);
            StreamUtils.writeByte(os, gAnimationData[i].type);
            StreamUtils.writeInt(os, gAnimationData[i].goal);
            StreamUtils.writeInt(os, gAnimationData[i].vel);
            StreamUtils.writeInt(os, gAnimationData[i].acc);
        }
        StreamUtils.writeInt(os, gAnimationCount);
    }

    public static void SaveVersion(OutputStream os, int nVersion) throws IOException {
        StreamUtils.writeString(os, savsign);
        StreamUtils.writeShort(os, nVersion);
    }

    public static void SaveGDXBlock(OutputStream os) throws IOException {
        if (gGameScreen.captBuffer != null) {
            StreamUtils.writeBytes(os, gGameScreen.captBuffer);
        } else {
            StreamUtils.writeBytes(os, new byte[SAVESCREENSHOTSIZE]);
        }
        gGameScreen.captBuffer = null;

        StreamUtils.writeByte(os, mUserFlag == UserFlag.UserMap ? (byte) 1 : 0);
        if (boardfilename != null && boardfilename.exists()) {
            if (boardfilename instanceof FileEntry) {
                StreamUtils.writeString(os, ((FileEntry) boardfilename).getPath().toString(), 144);
            } else {
                StreamUtils.writeString(os, boardfilename.getName(), 144);
            }
        } else {
            StreamUtils.writeString(os, "", 144);
        }
    }

    public static void SaveHeader(OutputStream os, String savename, long time) throws IOException {
        SaveVersion(os, currentGdxSave);

        StreamUtils.writeLong(os, time);
        StreamUtils.writeString(os, savename, SAVENAME);

        StreamUtils.writeInt(os, mission);
        StreamUtils.writeInt(os, gDifficulty);
    }

    public static void savegame(Directory dir, String savename, String filename) {
        FileEntry file = dir.getEntry(filename);
        if (file.exists()) {
            if (!file.delete()) {
                showmessage("Game not saved. Access denied!");
                return;
            }
        }

        Path path = dir.getPath().resolve(filename);
        try (OutputStream os = new BufferedOutputStream(Files.newOutputStream(path))) {
            long time = game.date.getCurrentDate();
            save(os, savename, time);

            file = dir.addEntry(path);
            if (file.exists()) {
                game.pSavemgr.add(savename, time, file);
                lastload = file;
                showmessage("GAME SAVED");
            } else {
                throw new FileNotFoundException(filename);
            }
        } catch (Exception e) {
            showmessage("Game not saved! " + e);
        }
    }

    public static void save(OutputStream os, String savename, long time) throws IOException {

        SaveHeader(os, savename, time);
        SaveGDXBlock(os); // ok

        PlayerSave(os);
        MapSave(os);
        SectorSave(os);
        AnimationSave(os);

        StreamUtils.writeInt(os, engine.getrand());
        StreamUtils.writeInt(os, hours);
        StreamUtils.writeInt(os, minutes);
        StreamUtils.writeInt(os, seconds);
        StreamUtils.writeInt(os, lockclock);

        StreamUtils.writeByte(os, parallaxtype);
        Renderer renderer = game.getRenderer();
        StreamUtils.writeInt(os, renderer.getParallaxOffset());
        
        for (int i = 0; i < MAXPSKYTILES; i++) {
            StreamUtils.writeShort(os, pskyoff[i]);
        }
    
        StreamUtils.writeShort(os, pskybits);
        StreamUtils.writeInt(os, visibility);
        StreamUtils.writeInt(os, parallaxvisibility);
        show2dsector.writeObject(os);
        show2dwall.writeObject(os);
        show2dsprite.writeObject(os);
        StreamUtils.writeByte(os, automapping);
        StreamUtils.writeByte(os, goreflag ? 1 : 0);

        tektagsave(os);

        tekstatsave(os);

        teksavemissioninfo(os);

    }

    public static void tekstatsave(OutputStream os) throws IOException {
        for (int i = 0; i < MAXSPRITES; i++) {
            spriteXT[i].writeObject(os);
        }
    }

    public static void teksavemissioninfo(OutputStream os) throws IOException { // ok
        for (int i = 0; i < MAXSYMBOLS; i++) {
            StreamUtils.writeByte(os, symbols[i] ? 1 : 0);
        }
        for (int i = 0; i < MAXSYMBOLS; i++) {
            StreamUtils.writeByte(os, symbolsdeposited[i] ? 1 : 0);
        }
        StreamUtils.writeInt(os, currentmapno);
        StreamUtils.writeByte(os, numlives);
        StreamUtils.writeByte(os, mission_accomplished);
        StreamUtils.writeInt(os, civillianskilled);
        StreamUtils.writeByte(os, generalplay);
        StreamUtils.writeByte(os, singlemapmode);
        StreamUtils.writeInt(os, allsymsdeposited);
        StreamUtils.writeInt(os, killedsonny);
    }

    public static void tektagsave(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, numanimates);
        for (int i = 0; i < numanimates; i++) {
            animpic[i].writeObject(os);
        }

        StreamUtils.writeShort(os, numdelayfuncs);
        for (int i = 0; i < numdelayfuncs; i++) {
            delayfunc[i].writeObject(os);
        }

        for (int i = 0; i < MAXPLAYERS; i++) {
            StreamUtils.writeByte(os, (byte) (onelev[i] ? 1 : 0));
        }

        StreamUtils.writeInt(os, secnt);
        for (int i = 0; i < MAXSECTORS; i++) {
            sectoreffect[i].writeObject(os);
        }

        for (int i = 0; i < MAXSECTORS; i++) {
            StreamUtils.writeInt(os, sexref[i]);
        }

        StreamUtils.writeInt(os, numdoors);
        for (int i = 0; i < numdoors; i++) {
            doortype[i].writeObject(os);
        }

        for (int i = 0; i < MAXSECTORS; i++) {
            StreamUtils.writeInt(os, doorxref[i]);
        }

        StreamUtils.writeInt(os, numfloordoors);
        for (int i = 0; i < numfloordoors; i++) {
           floordoor[i].writeObject(os);
        }

        for (int i = 0; i < MAXSECTORS; i++) {
            StreamUtils.writeInt(os, fdxref[i]);
        }

        StreamUtils.writeInt(os, numvehicles);
        for (int i = 0; i < numvehicles; i++) {
            sectorvehicle[i].writeObject(os);
        }

        for (int i = 0; i < MAXSECTORS; i++) {
            elevator[i].writeObject(os);
        }

        StreamUtils.writeInt(os, sprelevcnt);
        for (int i = 0; i < sprelevcnt; i++) {
            spriteelev[i].writeObject(os);
        }

        StreamUtils.writeInt(os, totalmapsndfx);
        for (int i = 0; i < totalmapsndfx; i++) {
            mapsndfx[i].writeObject(os);
        }
    }
}
