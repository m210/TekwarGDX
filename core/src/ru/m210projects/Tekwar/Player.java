package ru.m210projects.Tekwar;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Tekwar.Types.Input;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Build.Engine.MAXPLAYERS;

public class Player implements Serializable<Player> {

    public static final Player[] gPlayer = new Player[MAXPLAYERS];

    //Player struct

    public final Input pInput;
    public final short[] ammo = new short[8];
    public int posx, posy, posz, oposx, oposy, oposz;
    public float horiz, ang, ohoriz, oang;
    public int cursectnum, ocursectnum;

    // additional player variables
    public int hvel;
    public short playersprite, deaths, numbombs;
    public int health, score, saywatchit, invredcards, invbluecards, invaccutrak;
    public boolean ouse, ofire, dead, noclip, godMode;
    public int fireseq, weapons, lastchaingun, updategun, lastgun, firedonetics, drawweap;
    public boolean autocenter;

    public Player() {
        pInput = new Input();
    }

    public void copy(Player src) {
        posx = src.posx;
        posy = src.posy;
        posz = src.posz;
        oposx = src.oposx;
        oposy = src.oposy;
        oposz = src.oposz;
        ang = src.ang;
        horiz = src.horiz;
        oang = src.oang;
        ohoriz = src.ohoriz;
        hvel = src.hvel;
        cursectnum = src.cursectnum;
        ocursectnum = src.ocursectnum;
        playersprite = src.playersprite;
        deaths = src.deaths;
        numbombs = src.numbombs;

        health = src.health;
        score = src.score;
        saywatchit = src.saywatchit;
        invredcards = src.invredcards;
        invbluecards = src.invbluecards;
        invaccutrak = src.invaccutrak;

        System.arraycopy(src.ammo, 0, ammo, 0, 8);

        ouse = src.ouse;
        ofire = src.ofire;
        dead = src.dead;

        autocenter = src.autocenter;
        noclip = src.noclip;
        godMode = src.godMode;

        fireseq = src.fireseq;
        weapons = src.weapons;
        lastchaingun = src.lastchaingun;
        updategun = src.updategun;
        lastgun = src.lastgun;
        firedonetics = src.firedonetics;
        drawweap = src.drawweap;

    }

    @Override
    public Player readObject(InputStream is) throws IOException {
        posx = StreamUtils.readInt(is);
        posy = StreamUtils.readInt(is);
        posz = StreamUtils.readInt(is);
        oposx = StreamUtils.readInt(is);
        oposy = StreamUtils.readInt(is);
        oposz = StreamUtils.readInt(is);
        ang = StreamUtils.readFloat(is);
        horiz = StreamUtils.readFloat(is);
        oang = StreamUtils.readFloat(is);
        ohoriz = StreamUtils.readFloat(is);
        hvel = StreamUtils.readInt(is);
        cursectnum = StreamUtils.readShort(is);
        ocursectnum = StreamUtils.readShort(is);
        playersprite = StreamUtils.readShort(is);
        deaths = StreamUtils.readShort(is);
        numbombs = StreamUtils.readShort(is);

        health = StreamUtils.readInt(is);
        score = StreamUtils.readInt(is);
        saywatchit = StreamUtils.readInt(is);
        invredcards = StreamUtils.readInt(is);
        invbluecards = StreamUtils.readInt(is);
        invaccutrak = StreamUtils.readInt(is);

        for (int i = 0; i < 8; i++) {
            ammo[i] = StreamUtils.readShort(is);
        }

        ouse = StreamUtils.readBoolean(is);
        ofire = StreamUtils.readBoolean(is);
        dead = StreamUtils.readBoolean(is);

        autocenter = StreamUtils.readBoolean(is);
        noclip = StreamUtils.readBoolean(is);
        godMode = StreamUtils.readBoolean(is);

        fireseq = StreamUtils.readInt(is);
        weapons = StreamUtils.readInt(is);
        lastchaingun = StreamUtils.readInt(is);
        updategun = StreamUtils.readInt(is);
        lastgun = StreamUtils.readInt(is);
        firedonetics = StreamUtils.readInt(is);
        drawweap = StreamUtils.readInt(is);

        return this;
    }

    @Override
    public Player writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, posx);
        StreamUtils.writeInt(os, posy);
        StreamUtils.writeInt(os, posz);
        StreamUtils.writeInt(os, oposx);
        StreamUtils.writeInt(os, oposy);
        StreamUtils.writeInt(os, oposz);
        StreamUtils.writeFloat(os, ang);
        StreamUtils.writeFloat(os, horiz);
        StreamUtils.writeFloat(os, oang);
        StreamUtils.writeFloat(os, ohoriz);
        StreamUtils.writeInt(os, hvel);
        StreamUtils.writeShort(os, (short) cursectnum);
        StreamUtils.writeShort(os, (short) ocursectnum);
        StreamUtils.writeShort(os, playersprite);
        StreamUtils.writeShort(os, deaths);
        StreamUtils.writeShort(os, numbombs);
        StreamUtils.writeInt(os, health);
        StreamUtils.writeInt(os, score);
        StreamUtils.writeInt(os, saywatchit);
        StreamUtils.writeInt(os, invredcards);
        StreamUtils.writeInt(os, invbluecards);
        StreamUtils.writeInt(os, invaccutrak);

        for (int i = 0; i < 8; i++) {
            StreamUtils.writeShort(os, ammo[i]);
        }

        StreamUtils.writeBoolean(os, ouse);
        StreamUtils.writeBoolean(os, ofire);
        StreamUtils.writeBoolean(os, dead);
        StreamUtils.writeBoolean(os, autocenter);
        StreamUtils.writeBoolean(os, noclip);
        StreamUtils.writeBoolean(os, godMode);

        StreamUtils.writeInt(os, fireseq);
        StreamUtils.writeInt(os, weapons);
        StreamUtils.writeInt(os, lastchaingun);
        StreamUtils.writeInt(os, updategun);
        StreamUtils.writeInt(os, lastgun);
        StreamUtils.writeInt(os, firedonetics);
        StreamUtils.writeInt(os, drawweap);

        return this;
    }
}
