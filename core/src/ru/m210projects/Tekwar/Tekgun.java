package ru.m210projects.Tekwar;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Tekwar.Menus.MenuInterfaceSet;
import ru.m210projects.Tekwar.Types.Guntype;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Tekwar.Globals.*;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekchng.*;
import static ru.m210projects.Tekwar.Tekmap.mission;
import static ru.m210projects.Tekwar.Tekmap.missionaccomplished;
import static ru.m210projects.Tekwar.Tekprep.spriteXT;
import static ru.m210projects.Tekwar.Teksnd.playsound;
import static ru.m210projects.Tekwar.Tekspr.fillsprite;
import static ru.m210projects.Tekwar.Tekspr.movesprite;
import static ru.m210projects.Tekwar.Tekstat.*;
import static ru.m210projects.Tekwar.Tektag.flags32;
import static ru.m210projects.Tekwar.Tektag.krand_intercept;
import static ru.m210projects.Tekwar.View.*;

public class Tekgun {
//    public static final int NUMWEAPONS = 8;
    public static final short FORCEPROJECTILESTAT = 710;
    public static final short DARTPROJECTILESTAT = 712;
    public static final short BOMBPROJECTILESTAT = 714;
    public static final short BOMBPROJECTILESTAT2 = 716;
    public static final short ROCKETPROJECTILESTAT = 718;
    public static final short MATRIXPROJECTILESTAT = 720;
    public static final short MOVEBODYPARTSSTAT = 900;
    public static final short VANISH = 999;
    public static final int CLASS_NULL = 0;
    public static final int CLASS_FCIAGENT = 1;
    public static final int CLASS_CIVILLIAN = 2;
    public static final int CLASS_SPIDERDROID = 3;
    public static final int CLASS_COP = 4;
    public static final int CLASS_MECHCOP = 5;
    public static final int CLASS_TEKBURNOUT = 6;
    public static final int CLASS_TEKGOON = 7;
    public static final int CLASS_ASSASSINDROID = 8;
    public static final int CLASS_SECURITYDROID = 9;
    public static final int CLASS_TEKBOSS = 10;
    public static final int CLASS_TEKLORD = 11;
    public static final int DIEFRAMETIME = (160 / (JAKETWITCHPIC - JAKEDEATHPIC));

    //	public static int[] oneshot = new int[MAXPLAYERS];
    public static final int DRAWWEAPSPEED = 4;
    public static boolean goreflag = true;
    public static final Guntype[] guntype = {
            //   pic         firepic    endfirepic rep /           \ pos  tics/frame
            new Guntype(GUN07READY, GUN07FIRESTART, GUN07FIREEND, 0, new byte[]{0, 1, 0, 0, 0, 0, 0, 0}, 2, TICSPERFRAME * 8),
            new Guntype(GUN04READY, GUN04FIRESTART, GUN04FIREEND, 1, new byte[]{0, 1, 0, 0, 0, 0, 0, 0}, 2, TICSPERFRAME * 4),
            new Guntype(GUN01READY, GUN01FIRESTART, GUN01FIREEND, 1, new byte[]{0, 1, 0, 0, 0, 0, 0, 0}, 2, TICSPERFRAME * 4),
            new Guntype(GUN03READY, GUN03FIRESTART, GUN03FIREEND, 1, new byte[]{0, 1, 0, 0, 0, 0, 0, 0}, 2, TICSPERFRAME * 2),
            new Guntype(GUN02READY, GUN02FIRESTART, GUN02FIREEND, 1, new byte[]{0, 1, 0, 0, 0, 0, 0, 0}, 2, TICSPERFRAME * 2),
            new Guntype(GUN08READY, GUN08FIRESTART, GUN08FIREEND, 1, new byte[]{0, 1, 0, 0, 0, 0, 0, 0}, 2, TICSPERFRAME * 2),
            new Guntype(GUN05READY, GUN05FIRESTART, GUN05FIREEND, 0, new byte[]{0, 1, 1, 0, 0, 0, 0, 0}, 2, TICSPERFRAME * 8),
            // matrix hand
            new Guntype(3980, 3981, 3984, 1, new byte[]{0, 1, 0, 0, 0, 0, 0, 0}, 2, TICSPERFRAME * 3)
    };
    public static final int[] gunbobx = {0, 2, 4, 6, 8, 8, 8, 6, 4, 2};
    public static final int[] gunboby = {0, 2, 4, 6, 8, 10, 8, 6, 4, 2};
    public static int gunbob, bobtics;
    static final short[] dieframe = new short[MAXPLAYERS];
    static final short[] firepicdelay = new short[MAXPLAYERS];

    public static void restockammo(int snum) {
        gPlayer[snum].ammo[0] = MAXAMMO;
        gPlayer[snum].ammo[1] = MAXAMMO >> 1;
        gPlayer[snum].ammo[2] = 20;
        gPlayer[snum].ammo[3] = 0;
        gPlayer[snum].ammo[4] = MAXAMMO;
        gPlayer[snum].ammo[5] = 0;
        gPlayer[snum].ammo[6] = 0;
        gPlayer[snum].ammo[7] = 0;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean validplayer(int snum) {
        for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
            if (j == snum) {
                return (true);
            }
        }
        return (false);
    }

    public static int tekexplodebody(final int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return 0;
        }

        int ext = spr.getExtra();
        if ((validext(ext) == 0) || (!goreflag)) {
            return (0);
        }

        switch (spriteXT[ext].basepic) {
            case RUBWALKPIC:
            case JAKEWALKPIC:
            case COP1WALKPIC:
            case ANTWALKPIC:
            case SARAHWALKPIC:
            case MAWALKPIC:
            case DIWALKPIC:
            case ERWALKPIC:
            case SAMWALKPIC:
            case FRGWALKPIC:
            case SUNGWALKPIC:
            case COWWALKPIC:
            case COPBWALKPIC:
            case NIKAWALKPIC:
            case REBRWALKPIC:
            case TRENWALKPIC:
            case WINGWALKPIC:
            case HALTWALKPIC:
            case REDHWALKPIC:
            case ORANWALKPIC:
            case BLKSWALKPIC:
            case SFROWALKPIC:
            case SSALWALKPIC:
            case SGOLWALKPIC:
            case SWATWALKPIC:
                break;
            default:
                return (0);
        }

        int r = (krand_intercept(" GUN 787") % 72) + 8;
        for (int k = 0; k < r; k++) {
            int j = jsinsertsprite(spr.getSectnum(), MOVEBODYPARTSSTAT);
            Sprite spr2 = boardService.getSprite(j);
            if (spr2 == null) {
                continue;
            }

            spr2.setX(spr.getX());
            spr2.setY(spr.getY());
            spr2.setZ(spr.getZ() + (8 << 8));
            spr2.setCstat(0);
            spr2.setShade(0);
            spr2.setPal(0);
            spr2.setXrepeat(24);
            spr2.setYrepeat(24);
            spr2.setAng(spr.getAng());
            spr2.setXvel((short) ((krand_intercept(" GUN 799") & 511) - 256));
            spr2.setYvel((short) ((krand_intercept(" GUN 800") & 511) - 256));
            spr2.setZvel((short) -((krand_intercept(" GUN 801") & 8191) + 4096));
            spr2.setOwner(spr.getOwner());
            spr2.setClipdist(32);
            spr2.setLotag(360);
            spr2.setHitag(0);
            switch (k) {
                case 0:
                    spr2.setPicnum(GOREHEAD);
                    break;
                case 1:
                case 10:
                    spr2.setPicnum(GOREARM);
                    break;
                case 5:
                case 15:
                    spr2.setPicnum(GORELEG);
                    break;
                default:
                    spr2.setPicnum(GOREBLOOD);
                    break;
            }
        }
        playsound(S_GORE1 + (krand_intercept(" GUN 823") % 2), spr.getX(), spr.getY(), 0, ST_NOUPDATE);

        return (1);
    }

    public static void tekchangestun(int snum, int deltastun) {
        if (gPlayer[snum].godMode) {
            return;
        }

        switch (gDifficulty) {
            case 1:
                deltastun /= 2;
                break;
            case 3:
                deltastun *= 2;
                break;
        }
        stun[snum] += deltastun;
        if (stun[snum] < 0) {
            stun[snum] = 0;
        }
    }

    public static void playerpainsound(int p) {
        if (!validplayer(p)) {
            throw new AssertException("playerpainsnd: bad plr num");
        }

        if (krand_intercept(" GUN 341") < 1024) {
            playsound(S_PAIN1 + (krand_intercept(" GUN 342") & 1), gPlayer[p].posx, gPlayer[p].posy, 0, ST_NOUPDATE);
        }
    }

    public static int tekhasweapon(int gun, short snum) {
        if (TEKDEMO && gun > 2) {
            return 0;
        }

        if (mission == 7) {
            if (gun != 7) {
                if (snum == screenpeek) {
                    notininventory = 1;
                }
                return (0);
            } else {
                return (1);
            }
        } else {
            if (gun == 7) {
                if (snum == screenpeek) {
                    showmessage("ONLY IN MATRIX");
                }
                return (0);
            }
        }

        int hasit = (gPlayer[snum].weapons & flags32[gun + 1]);
        if (hasit != 0) {
            return (1);
        }

        notininventory = 1;
        return (0);
    }

    public static int tekgunrep(int gun)                // is "gun" an automatic weapon?
    {
        return (guntype[gun].rep);
    }

    public static void tekfiregun(int gun, short p)  // this kicks off an animation sequence
    {
        Guntype gunptr = guntype[gPlayer[p].lastgun];
        int fseq = gPlayer[p].fireseq - (gunptr.firepic - gunptr.pic);
        if ((gun != gPlayer[p].lastgun && fseq < 0) || gPlayer[p].fireseq != 0) {
            return;
        }
        if (tekgunrep(gun) != 0 || !gPlayer[p].ofire) {
            gPlayer[p].fireseq = 1;
        }
    }

    public static void tekdrawgun(int p) {
        int shade = 0;
        int gun = gPlayer[p].lastgun;
        Sector plrSec = boardService.getSector(gPlayer[p].cursectnum);
        if (plrSec != null) {
            shade = plrSec.getFloorshade();
        }

        int apic;
        if ((gPlayer[p].fireseq != 0 || (game.menu.gShowMenu && game.menu.getCurrentMenu() instanceof MenuInterfaceSet)) && (tekcfg.gCrosshair && (gViewMode == kView3D))) {
            apic = AIMPIC;
            if (gDifficulty <= 1) {
                apic = BIGAIMPIC;
            }

            Renderer renderer = game.getRenderer();
            int windowx1 = 0;
            int windowy1 = 0;
            int windowx2 = renderer.getWidth();
            int windowy2 = renderer.getHeight();

            int cx = windowx1 + ((windowx2 - windowx1) >> 1);
            int cy = windowy1 + ((windowy2 - windowy1) >> 1);
            renderer.rotatesprite(cx << 16, cy << 16, tekcfg.gCrossSize, 0, apic,
                    16, 0, 0, windowx1, windowy1, windowx2, windowy2);
        }

        if (gPlayer[p].fireseq == 0) {
            return;
        }

        int pic;
        if (gPlayer[p].firedonetics > 0) {
            pic = guntype[gun].firepic;
        } else {
            pic = guntype[gun].pic + gPlayer[p].fireseq - 1;
        }

        int x = 160;
        int stat = 1 | 2 | 32;
        if ((pic == 3981)) {
            if (gPlayer[p].pInput.svel != 0 && gPlayer[p].pInput.svel < 10) {
                overwritesprite(x, 100, 65536, 3990, shade, stat, 0);
                return;
            } else if (gPlayer[p].pInput.svel > 10) {
                overwritesprite(x, 100, 65536, 3986, shade, stat, 0);
                return;
            } else if (gPlayer[p].pInput.angvel != 0 && gPlayer[p].pInput.angvel < 10) {
                overwritesprite(x, 100, 65536, 3985, shade, stat, 0);
                return;
            } else if (gPlayer[p].pInput.angvel > 10) {
                overwritesprite(x, 100, 65536, 3989, shade, stat, 0);
                return;
            } else if (gPlayer[p].pInput.Jump) {
                overwritesprite(x, 100, 65536, 3994 + (gPlayer[p].pInput.Run ? 1 : 0), shade, stat, 0);
                return;
            } else if (gPlayer[p].pInput.Crouch) {
                overwritesprite(x, 100, 65536, 3998 + (gPlayer[p].pInput.Run ? 1 : 0), shade, stat, 0);
                return;
            }
        }

        if (gun == 5 || gun == 6) {
            stat |= 512;
        }

        overwritesprite(x + gunbobx[gunbob], 100 + gunboby[gunbob], 65536, pic, shade, stat, 0);
    }

    public static void doweapanim(int p) {
        int pic = guntype[gPlayer[p].lastgun].pic + gPlayer[p].fireseq - 1;
        if (gPlayer[p].firedonetics > 0) {
            pic = guntype[gPlayer[p].lastgun].firepic;
        }

        if ((gPlayer[p].pInput.vel | gPlayer[p].pInput.svel) != 0) {
            switch (pic) {
                case GUN01FIRESTART:
                case GUN02FIRESTART:
                case GUN03FIRESTART:
                case GUN04FIRESTART:
                case GUN05FIRESTART:
                case GUN06FIRESTART:
                case GUN07FIRESTART:
                case GUN08FIRESTART:
                case GUNDEMFIRESTART:

                    bobtics += (1 + (gPlayer[p].pInput.Run ? 4 : 0));
                    if (bobtics > TICSPERFRAME) {
                        bobtics = 0;
                        gunbob++;
                        if (gunbob > 9) {
                            gunbob = 0;
                        }
                    }

                    break;
                default:
                    gunbob = 0;
                    break;
            }
        }
    }

    public static void tekanimweap(int p) {
        if (game.nNetMode == NetMode.Multiplayer) {
            Sprite plrSpr = boardService.getSprite(gPlayer[p].playersprite);
            if (plrSpr != null) {
                if (gPlayer[p].health < 0 && dieframe[p] == 0) {
                    dieframe[p] = (short) TICSPERFRAME;
                } else if (dieframe[p] > 0) {
                    plrSpr.setPicnum((short) (JAKEDEATHPIC + (dieframe[p] / DIEFRAMETIME)));
                    dieframe[p] += TICSPERFRAME;
                    return;
                } else {
                    if (gPlayer[p].pInput.Fire && firepicdelay[p] == 0) {
                        plrSpr.setPicnum(JAKEATTACKPIC);
                        firepicdelay[p] = 16;
                    } else if (firepicdelay[p] > 0) {
                        firepicdelay[p]--;
                        if (firepicdelay[p] <= 0) {
                            firepicdelay[p] = 0;
                            plrSpr.setPicnum(JAKEWALKPIC);
                        }
                    }
                    if (firepicdelay[p] == 0 && gPlayer[p].pInput.vel == 0 && gPlayer[p].pInput.svel == 0) {
                        plrSpr.setPicnum(JAKESTANDPIC);
                    }
                }
            }
        }

        int newgun = gPlayer[p].pInput.selectedgun;
        int gun = gPlayer[p].lastgun;

        if (newgun != -1) {
            gPlayer[p].updategun = gun = newgun;
            if (gPlayer[p].fireseq != 0) {
                gPlayer[p].updategun |= (1 << 8);
            }
        }

        int seq = gPlayer[p].fireseq;
        if (seq == 0) {
            if (gPlayer[p].updategun != -1) {
                gPlayer[p].lastgun = (gPlayer[p].updategun & 0xFF);
                if (((gPlayer[p].updategun & 0xFF00) >> 8) != 0) {
                    gPlayer[p].drawweap = 1;
                }
                gPlayer[p].updategun = -1;
            } else {
                gPlayer[p].lastgun = gun;
            }
            if (gPlayer[p].drawweap == 0) {
                return;
            }
        }

        int firekey = 0;
        int usegun = gPlayer[p].lastgun;
        if (usegun != gun) {
            if (gPlayer[p].firedonetics >= 0) {
                gPlayer[p].firedonetics = 2;
            }
        } else {
            firekey = gPlayer[p].pInput.Fire ? 1 : 0;
            if (firekey != 0) {
                gPlayer[p].drawweap = 1;
            }
        }

        if ((firekey == 0) && gPlayer[p].pInput.Holster_Weapon && gPlayer[p].firedonetics == 1) {
            gPlayer[p].firedonetics = 2;
            gPlayer[p].updategun = -1;
        }

        if (gPlayer[p].firedonetics == 2) {
            gPlayer[p].firedonetics = -1;
            gPlayer[p].drawweap = 0;

            if (gPlayer[p].firedonetics <= 0) {
                Guntype gunptr = guntype[usegun];
                gPlayer[p].fireseq = gunptr.firepic - gunptr.pic;
                return;
            }
        }
        Guntype gunptr = guntype[usegun];
        int fseq = gunptr.firepic - gunptr.pic;

        int  tics = DRAWWEAPSPEED;
        if (seq - 1 >= fseq) {
            tics = gunptr.tics;
        }

        if (lockclock >= gPlayer[p].lastchaingun + tics) {
            gPlayer[p].lastchaingun = lockclock;
            int ammo = hasammo(usegun, p) ? 1 : 0;
            if (ammo == 0) {
                firekey = 0;
            }

            if (seq - 1 >= fseq && ammo != 0) {
                if (gunptr.action[seq - fseq - 1] != 0) {
                    switch (gun + 1) {
                        //jsa friday
                        case 1:
                            if (game.nNetMode == NetMode.Multiplayer) {
                                playsound(S_WEAPON1, gPlayer[p].posx, gPlayer[p].posy, 0, ST_NOUPDATE);
                            } else {
                                playsound(S_WEAPON1, 0, 0, 0, ST_IMMEDIATE);
                            }
                            break;
                        case 2:
                            if (game.nNetMode == NetMode.Multiplayer) {
                                playsound(S_WEAPON2, gPlayer[p].posx, gPlayer[p].posy, 0, ST_NOUPDATE);
                            } else {
                                playsound(S_WEAPON2, 0, 0, 0, ST_IMMEDIATE);
                            }
                            break;
                        case 3:
                            if (game.nNetMode == NetMode.Multiplayer) {
                                playsound(S_WEAPON3, gPlayer[p].posx, gPlayer[p].posy, 0, ST_NOUPDATE);
                            } else {
                                playsound(S_WEAPON3, 0, 0, 0, ST_IMMEDIATE);
                            }
                            break;
                        case 4:
                            if (game.nNetMode == NetMode.Multiplayer) {
                                playsound(S_WEAPON4, gPlayer[p].posx, gPlayer[p].posy, 0, ST_NOUPDATE);
                            } else {
                                playsound(S_WEAPON4, 0, 0, 0, ST_IMMEDIATE);
                            }
                            break;
                        case 5:
                            if (game.nNetMode == NetMode.Multiplayer) {
                                playsound(S_WEAPON5, gPlayer[p].posx, gPlayer[p].posy, 0, ST_NOUPDATE);
                            } else {
                                playsound(S_WEAPON5, 0, 0, 0, ST_IMMEDIATE);
                            }
                            break;
                        case 6:
                            if (game.nNetMode == NetMode.Multiplayer) {
                                playsound(S_WEAPON6, gPlayer[p].posx, gPlayer[p].posy, 0, ST_NOUPDATE);
                            } else {
                                playsound(S_WEAPON6, 0, 0, 0, ST_IMMEDIATE);
                            }
                            break;
                        case 7:
                            if (game.nNetMode == NetMode.Multiplayer) {
                                playsound(S_WEAPON7, gPlayer[p].posx, gPlayer[p].posy, 0, ST_NOUPDATE);
                            } else {
                                playsound(S_WEAPON7, 0, 0, 0, ST_IMMEDIATE);
                            }
                            break;
                        case 8:
                            playsound(S_WEAPON8, 0, 0, 0, ST_IMMEDIATE);
                            break;
                        default:
                            break;

                    }
                    shootgun(p, gPlayer[p].posx, gPlayer[p].posy, gPlayer[p].posz, (int) gPlayer[p].ang, (int) gPlayer[p].horiz, gPlayer[p].cursectnum, usegun);
                    if (game.nNetMode == NetMode.Multiplayer) {
                        Sprite plrSpr = boardService.getSprite(gPlayer[p].playersprite);
                        if (plrSpr != null) {
                            plrSpr.setPicnum(JAKEATTACKPIC + 1);
                            firepicdelay[p] = 8;
                        }
                    }
                }

                if (seq - 1 >= gunptr.endfirepic - gunptr.pic) {
                    gPlayer[p].fireseq = fseq + 1;
                    return;
                }

                if (firekey == 0) {
                    gPlayer[p].firedonetics = 1;
                    gPlayer[p].fireseq = fseq;
                    return;
                } else {
                    gPlayer[p].firedonetics = 0;
                }
            }
            if (gPlayer[p].drawweap != 0) {
                if (ammo != 0 || seq < fseq) {
                    seq++;
                } else {
                    gPlayer[p].firedonetics = 1;
                }
            } else if (gPlayer[p].firedonetics < 0) {
                seq--;
            }

            gPlayer[p].fireseq = seq;
        }
    }

    public static int changegun(int p, int next) {
        Player plr = gPlayer[p];

        int weap = plr.lastgun;
        do {
            weap = (weap + next);

            if (weap > 6) {
                weap = 0;
            }
            if (weap < 0) {
                weap = 6;
            }

            if ((plr.weapons & flags32[weap + 1]) != 0 && hasammo(weap, p)) {
                break;
            }
        } while (plr.lastgun != weap);

        if (plr.fireseq == 0 && plr.updategun != -1) {
            return -1;
        }

        return weap;
    }

    public static boolean hasammo(int gun, int p)     // does player[p] have ammo for "gun"?
    {
        return gPlayer[p].ammo[gun] > 0;
    }

    public static void shootgun(int snum, int x, int y, int z, int daang, int dahoriz,
                                int dasectnum, int guntype) {
        if (gPlayer[snum].health <= 0 || !hasammo(guntype, snum)) {
            return;
        }

        guntype += 1;
        Sprite hitSpr;
        switch (guntype) {
            case GUN1FLAG: {
                int cx = x + (EngineUtils.sin((daang + 2560 + 128) & 2047) >> 6);
                int cy = y + (EngineUtils.sin((daang + 2048 + 128) & 2047) >> 6);
                int j = jsinsertsprite(dasectnum, FORCEPROJECTILESTAT);
                if (j != -1) {
                    fillsprite(j, cx, cy, z, 128 + 2, -16, 0, 12, 16, 16, 0, 0, FORCEBALLPIC, daang,
                            EngineUtils.sin((daang + 2560 - 11) & 2047) >> 5, // -17 = travel 3 degrees left
                            EngineUtils.sin((daang + 2048 - 11) & 2047) >> 5, // -17 = travel 3 degrees left
                            100 - dahoriz, snum + MAXSPRITES, 0, 0);
                }
                break;
            }
            case GUN2FLAG: {
                gPlayer[snum].ammo[guntype - 1]--;
                if (gPlayer[snum].ammo[guntype - 1] < 0) {
                    gPlayer[snum].ammo[guntype - 1] = 0;
                    break;
                }
                int daang2 = (short) daang;
                int daz2 = (100 - dahoriz) * 2000;

                engine.hitscan(x, y, z, dasectnum, EngineUtils.sin((daang2 + 2560) & 2047), EngineUtils.sin((daang2 + 2048) & 2047), daz2, pHitInfo, CLIPMASK1);
                int hitsprite = pHitInfo.hitsprite;
                int hitwall = pHitInfo.hitwall;
                int hitsect = pHitInfo.hitsect;
                int hitx = pHitInfo.hitx;
                int hity = pHitInfo.hity;
                int hitz = pHitInfo.hitz;

                hitSpr = boardService.getSprite(hitsprite);
                if (hitSpr != null && (hitSpr.getStatnum() < MAXSTATUS)) {
                    if (playerhit(hitsprite) != 0) {
                        int pnum = playerhit;
                        playerpainsound(pnum);
                        playerwoundplayer(pnum, snum, 2);
                    } else {
                        int rv = damagesprite(hitsprite, tekgundamage(guntype));

                        if ((rv == 1) && (goreflag)) {
                            if (spewblood(hitsprite, hitz) != 0) {
                                hitSpr.setCstat(hitSpr.getCstat() & 0xFEFE);  // non hitscan and non block
                                // must preserve values from previous hitscan call,
                                // thus the bloodxhitx, bloodwall, etc...
                                engine.hitscan(x, y, z, dasectnum, EngineUtils.sin((daang2 + 2560) & 2047), EngineUtils.sin((daang2 + 2048) & 2047), daz2, pHitInfo, CLIPMASK1);
                                int bloodhitwall = pHitInfo.hitwall;
                                int bloodhitx = pHitInfo.hitx;
                                int bloodhity = pHitInfo.hity;
                                int bloodhitz = pHitInfo.hitz;
                                Wall hitWall = boardService.getWall(bloodhitwall);
                                if (hitWall != null) {
                                    bloodonwall(hitWall, hitSpr.getX(), hitSpr.getY(), hitSpr.getZ(),
                                            hitSpr.getSectnum(), daang2, bloodhitx, bloodhity, bloodhitz);
                                }
                            }
                        }
                        if (rv == 1) {
                            killscore(hitsprite, snum, guntype);
                        }
                    }
                }
                if (hitwall != -1) {
                    int j = jsinsertsprite(hitsect, (short) 3);
                    if (j != -1) {
                        fillsprite(j, hitx, hity, hitz + (8 << 8), 2, 0, 0, 32, 22, 22, 0, 0,
                                EXPLOSION, daang, 0, 0, 0, snum + MAXSPRITES, 63, 0);

                        movesprite(j,
                                -((EngineUtils.sin((512 + daang) & 2047) * TICSPERFRAME) << 4),
                                -((EngineUtils.sin(daang & 2047) * TICSPERFRAME) << 4), 0, 4 << 8, 4 << 8, 1);
                        playsound(S_RIC1, hitx, hity, 0, ST_NOUPDATE);
                    }
                }
                break;
            }
            case GUN3FLAG: {
                gPlayer[snum].ammo[guntype - 1]--;
                if (gPlayer[snum].ammo[guntype - 1] < 0) {
                    gPlayer[snum].ammo[guntype - 1] = 0;
                    break;
                }
                int cx = x + (EngineUtils.sin((daang + 2560 + 256) & 2047) >> 6);
                int cy = y + (EngineUtils.sin((daang + 2048 + 256) & 2047) >> 6);
                if (gPlayer[snum].invaccutrak > 0) {
                    int j = jsinsertsprite(dasectnum, BOMBPROJECTILESTAT);
                    Sprite spr2 = boardService.getSprite(j);
                    if (spr2 != null) {
                        fillsprite(j, cx, cy, z + (4 << 8), 128, -16, 0, 32, 16, 16, 0, 0, BOMBPIC, daang,
                                EngineUtils.sin((daang + 2560 - 11) & 2047) >> 5, // -17 = travel 3 degrees left
                                EngineUtils.sin((daang + 2048 - 11) & 2047) >> 5, // -17 = travel 3 degrees left
                                100 - dahoriz, snum, 0, 0);
                        spr2.setOwner(snum);
                    }
                } else {
                    int j = jsinsertsprite(dasectnum, BOMBPROJECTILESTAT2);
                    Sprite spr2 = boardService.getSprite(j);
                    if (spr2 != null) {
                        fillsprite(j, cx, cy, z + (4 << 8), 128, -16, 0, 32, 16, 16, 0, 0, BOMBPIC, daang,
                                EngineUtils.sin((daang + 2560 - 11) & 2047) >> 5, // -17 = travel 3 degrees left
                                EngineUtils.sin((daang + 2048 - 11) & 2047) >> 5, // -17 = travel 3 degrees left
                                100 - dahoriz, snum, 0, 0);
                        spr2.setOwner(snum);
                    }
                }
                break;
            }
            case GUN4FLAG: {
                gPlayer[snum].ammo[guntype - 1]--;
                if (gPlayer[snum].ammo[guntype - 1] < 0) {
                    gPlayer[snum].ammo[guntype - 1] = 0;
                    break;
                }
                int cx = x + (EngineUtils.sin((daang + 2560 + 128) & 2047) >> 6);
                int cy = y + (EngineUtils.sin((daang + 2048 + 128) & 2047) >> 6);
                int j = jsinsertsprite(dasectnum, DARTPROJECTILESTAT);
                Sprite spr2 = boardService.getSprite(j);
                if (spr2 != null) {
                    fillsprite(j, cx, cy, z + (5 << 8), 128, -35, 0, 12, 16, 16, 0, 0, 338, daang,
                            EngineUtils.sin((daang + 2560 - 6) & 2047) >> 5, // -17 = travel 3 degrees left
                            EngineUtils.sin((daang + 2048 - 6) & 2047) >> 5, // -17 = travel 3 degrees left
                            100 - dahoriz, snum, 0, 0);
                    spr2.setOwner(snum);
                    playsound(S_RIC1, cx, cy, 0, ST_NOUPDATE);
                }
                break;
            }
            case GUN5FLAG: {
                int daz2 = (100 - dahoriz) * 2000;
                engine.hitscan(x, y, z, dasectnum, EngineUtils.sin((daang + 2560) & 2047), EngineUtils.sin((daang + 2048) & 2047), daz2, pHitInfo, CLIPMASK1);
                int hitsprite = pHitInfo.hitsprite;

                hitSpr = boardService.getSprite(hitsprite);
                if (hitSpr != null && (hitSpr.getStatnum() < MAXSTATUS)) {
                    int xydist = klabs(gPlayer[snum].posx - hitSpr.getX()) + klabs(gPlayer[snum].posy - hitSpr.getY());
                    int zdist = klabs((gPlayer[snum].posz >> 8) - ((hitSpr.getZ() >> 8) - (engine.getTile(hitSpr.getPicnum()).getHeight() >> 1)));
                    if ((xydist > 768) || (zdist > 50)) {
                        break;
                    }
                    if (playerhit(hitsprite) != 0) {
                        int pnum = playerhit;
                        playerpainsound(pnum);
                        playerwoundplayer(pnum, snum, 5);
                    } else {
                        int rv = damagesprite(hitsprite, tekgundamage(guntype));
                        if (rv == 1) {
                            killscore(hitsprite, snum, guntype);
                        }
                    }
                }
                break;
            }
            case GUN6FLAG: {
                gPlayer[snum].ammo[guntype - 1]--;
                if (gPlayer[snum].ammo[guntype - 1] < 0) {
                    gPlayer[snum].ammo[guntype - 1] = 0;
                    break;
                }
                int daz2 = (100 - dahoriz) * 2000;
                engine.hitscan(x, y, z, dasectnum, EngineUtils.sin((daang + 2560) & 2047), EngineUtils.sin((daang + 2048) & 2047), daz2, pHitInfo, CLIPMASK1);
                int hitsprite = pHitInfo.hitsprite;

                hitSpr = boardService.getSprite(hitsprite);
                if (hitSpr != null && (hitSpr.getStatnum() < MAXSTATUS)) {
                    int xydist = klabs(gPlayer[snum].posx - hitSpr.getX()) + klabs(gPlayer[snum].posy - hitSpr.getY());
                    int zdist = klabs((gPlayer[snum].posz >> 8) - ((hitSpr.getZ() >> 8) - (engine.getTile(hitSpr.getPicnum()).getHeight() >> 1)));
                    if ((xydist > 2560) || (zdist > 576)) {
                        break;
                    }

                    if (playerhit(hitsprite) != 0) {
                        int pnum = playerhit;
                        if (playervirus(pnum, FIREPIC) != 0) {
                            playerwoundplayer(pnum, snum, 6);
                        }
                    } else {
                        attachvirus(hitsprite, FIREPIC);
                    }
                }
                break;
            }
            case GUN7FLAG: {
                gPlayer[snum].ammo[guntype - 1]--;
                if (gPlayer[snum].ammo[guntype - 1] < 0) {
                    gPlayer[snum].ammo[guntype - 1] = 0;
                    break;
                }
                for (int i = 0; i < 3; i++) {
                    int cx = x + (EngineUtils.sin((daang + 2560 + 256) & 2047) >> 6);
                    int cy = y + (EngineUtils.sin((daang + 2048 + 256) & 2047) >> 6);
                    int j = jsinsertsprite(dasectnum, ROCKETPROJECTILESTAT);
                    Sprite spr3 = boardService.getSprite(j);
                    if (spr3 != null) {
                        fillsprite(j, cx, cy, z + (4 << 8), 128, -24, 0, 12, 16, 16, 0, 0, 335, daang,
                                EngineUtils.sin((daang + 2560 - 11) & 2047) >> (2 + i),
                                EngineUtils.sin((daang + 2048 - 11) & 2047) >> (2 + i),
                                (100 - dahoriz) - (i << 2),
                                snum, 0, 0);
                        spr3.setOwner(snum);
                    }
                }
                break;
            }
            case GUN8FLAG: {
                gPlayer[snum].ammo[guntype - 1]--;
                if (gPlayer[snum].ammo[guntype - 1] < 0) {
                    gPlayer[snum].ammo[guntype - 1] = 0;
                    break;
                }
                int cx = x + (EngineUtils.sin((daang + 2560) & 2047) >> 6);
                int cy = y + (EngineUtils.sin((daang + 2048) & 2047) >> 6);
                int j = jsinsertsprite(dasectnum, MATRIXPROJECTILESTAT);
                Sprite spr3 = boardService.getSprite(j);
                if (spr3 != null) {
                    fillsprite(j, cx, cy, z + (5 << 8), 128, -35, 0, 12, 16, 16, 0, 0, 3765, daang,
                            EngineUtils.sin((daang + 2560) & 2047) >> 5,
                            EngineUtils.sin((daang + 2048) & 2047) >> 5,
                            100 - dahoriz, snum, 0, 0);
                    spr3.setOwner(snum);
                }
                break;
            }
            default:
                break;
        }

        if (guntype != GUN1FLAG) {
            playergunshot(snum);
        }
    }

    public static void killscore(final int hs, int snum, int guntype) {
        Sprite spr = boardService.getSprite(hs);
        if (spr == null) {
            return;
        }

        int ext = spr.getExtra();
        int score = 0;
        if (!validplayer(snum)) {
            return;
        }
        if (validext(ext) == 0) {
            return;
        }

        if (isanandroid(hs) != 0) {
            score = 200;
        } else if (isahologram(hs) != 0) {
            score = 100;
        } else {
            switch (spriteXT[ext].sclass) {
                case CLASS_NULL:
                    return;
                case CLASS_FCIAGENT:
                case CLASS_COP:
                case CLASS_CIVILLIAN:
                    score = -500;
                    break;
                case CLASS_SPIDERDROID:
                    score = 50;
                    break;
                case CLASS_MECHCOP:
                    score = -50;
                    break;
                case CLASS_TEKBURNOUT:
                case CLASS_SECURITYDROID:
                    score = 100;
                    break;
                case CLASS_TEKGOON:
                    score = 200;
                    break;
                case CLASS_ASSASSINDROID:
                case CLASS_TEKBOSS:
                    score = 300;
                    break;
                case CLASS_TEKLORD:
                    score = 500;
                    break;
            }
            switch (guntype) {
                case 0:
                case 1:
                    if (score < 0) {
                        score = 0;
                    } else {
                        score += (score << 1);
                    }
                    break;
            }
        }

        if (game.nNetMode == NetMode.Multiplayer) {
            score >>= 4;
        }
        changescore(snum, score);
    }

    public static void playerwoundplayer(int plrhit, int plr, int guntype) {

        if (!validplayer(plrhit) || !validplayer(plr)) {
            return;
        }

        int damage, score;
        switch (guntype) {
            case 2:
                score = 5;
                damage = 48;
                break;
            case 3:
                score = 10;
                damage = 192;
                break;
            case 4:
            case 7:
                score = 10;
                damage = 512;
                break;
            case 5:
                score = 15;
                damage = 1024;
                break;
            case 6:
                score = 10;
                damage = 24;
                break;
            default:
                score = 5;
                damage = 32;
                break;
        }

        boolean killed = changehealth(plrhit, -damage);
        if (killed) {
            changescore(plr, (score << 1));
        } else {
            changescore(plr, score);
        }
    }

    public static int tekgundamage(int gun) {
        int damage;
        if (gun == 4) {
            damage = 20;
        } else {
            damage = 2;
        }

        switch (gDifficulty) {
            case 0:
            case 1:
            case 2:
                damage += 4;
                break;
            case 3:
            default:
                damage += 10;
                break;
        }

        return (damage);
    }

    public static void bloodonwall(Wall wal, int x, int y, int z, short sect, int daang, int hitx, int hity, int hitz) {
        if (wal.getLotag() != 0) {
            return;
        }

        switch (wal.getPicnum()) {
            case 15:
            case 71:
            case 72:
            case 87:
            case 93:
            case 92:
            case 95:
            case 97:
            case 98:
            case 99:
            case 124:
            case 126:
            case 147:
            case 152:
            case 214:
            case 215:
            case 235:
            case 236:
            case 636:
            case 855:
            case 1457:
                return;
            default:
                break;
        }

        if (wal.getLotag() == 0) {
            wal.setLotag(1);
            engine.neartag(x, y, z, sect, daang, neartag, 512, 1);

            if (neartag.tagwall != -1) {
                int j = jsinsertsprite(sect, BLOODFLOW);
                Sprite spr2 = boardService.getSprite(j);
                if (spr2 != null) {
                    spr2.setPicnum(WALLBLOOD);
                    spr2.setX(hitx);
                    spr2.setY(hity);
                    spr2.setZ(hitz + 2048);
                    Sector sec = boardService.getSector(sect);
                    if (sec != null && spr2.getZ() > sec.getFloorz()) {
                        spr2.setZ(sec.getFloorz() - 2048);
                    }
                    spr2.setXrepeat(16);
                    spr2.setYrepeat(16);
                    spr2.setXvel(0);
                    spr2.setYvel(0);
                    spr2.setZvel(0);
                    spr2.setCstat(0x0090);
                    spr2.setAng(wallnormal(wal));
                    spr2.setShade(0);
                    spr2.setExtra(-1);
                    spr2.setLotag(0);
                    spr2.setHitag(0);
                }
            }
            wal.setLotag(0);
        }
    }

    public static void gunstatuslistcode() {
        ListNode<Sprite> i = boardService.getStatNode(FORCEPROJECTILESTAT), nexti;  //moving force ball
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();
            int dax = (((spr.getXvel()) * TICSPERFRAME) << 11);
            int day = (((spr.getYvel()) * TICSPERFRAME) << 11);
            int daz = (((spr.getZvel()) * TICSPERFRAME) << 3);
            int hitobject = movesprite(index, dax, day, daz, 4 << 8, 4 << 8, 1);

            if ((hitobject & HIT_TYPE_MASK) == HIT_SPRITE) {  // hit a sprite
                int hitsprite = (hitobject & HIT_INDEX_MASK);
                Sprite hitSpr = boardService.getSprite(hitsprite);
                if (hitSpr != null) {
                    int ext = hitSpr.getExtra();
                    if (validext(ext) != 0) {
                        switch (spriteXT[ext].walkpic) {
                            case RUBWALKPIC:
                            case FRGWALKPIC:
                            case COP1WALKPIC:
                            case ANTWALKPIC:
                            case SARAHWALKPIC:
                            case MAWALKPIC:
                            case DIWALKPIC:
                            case ERWALKPIC:
                            case SAMWALKPIC:
                            case SUNGWALKPIC:
                            case COWWALKPIC:
                            case COPBWALKPIC:
                            case NIKAWALKPIC:
                            case REBRWALKPIC:
                            case TRENWALKPIC:
                            case WINGWALKPIC:
                            case HALTWALKPIC:
                            case REDHWALKPIC:
                            case ORANWALKPIC:
                            case BLKSWALKPIC:
                            case SFROWALKPIC:
                            case SSALWALKPIC:
                            case SGOLWALKPIC:
                            case SWATWALKPIC:
                                attachvirus(hitsprite, FORCEBALLPIC);
                                break;
                            default:
                                forceexplosion(index);
                                break;
                        }
                        jsdeletesprite(index);
                    } else {
                        forceexplosion(index);
                        jsdeletesprite(index);
                    }
                }
            } else if (hitobject != 0) {
                forceexplosion(index);
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(ROCKETPROJECTILESTAT);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int dax = (((spr.getXvel()) * TICSPERFRAME) << 10);
            int day = (((spr.getYvel()) * TICSPERFRAME) << 10);
            int daz = (((spr.getZvel()) * TICSPERFRAME) << 3);
            int index = i.getIndex();
            int hitobject = movesprite(index, dax, day, daz, 4 << 8, 4 << 8, 1);
            if ((hitobject & HIT_TYPE_MASK) == HIT_SPRITE) {  // hit a sprite
                int hitsprite = (hitobject & HIT_INDEX_MASK);
                Sprite hitSpr = boardService.getSprite(hitsprite);
                if (hitSpr != null) {
                    int hit = playerhit(hitsprite);
                    int pnum = playerhit;
                    if (hit != 0) {
                        playerpainsound(pnum);
                        playerwoundplayer(pnum, spr.getOwner(), 7);
                    } else if (spr.getPicnum() != hitSpr.getPicnum()) {
                        if (isahologram(hitsprite) != 0) {
                            showmessage("WAS A HOLOGRAM");
                            killscore(hitsprite, spr.getOwner(), 3);
                            engine.changespritestat(hitsprite, VANISH);
                        } else if (isanandroid(hitsprite) != 0) {
                            showmessage("WAS AN ANDROID");
                            killscore(hitsprite, spr.getOwner(), 3);
                            androidexplosion(hitsprite);
                            engine.changespritestat(hitsprite, VANISH);
                        } else {
                            blastmark(hitsprite);
                            int rv = damagesprite(hitsprite, 500);
                            if (rv == 1) {
                                killscore(hitsprite, spr.getOwner(), 3);
                            }
                        }
                    }
                }
            }
            if (hitobject != 0) {
                bombexplosion(index);
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(MATRIXPROJECTILESTAT);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int dax = (((spr.getXvel()) * TICSPERFRAME) << 12);
            int day = (((spr.getYvel()) * TICSPERFRAME) << 12);
            int daz = (((spr.getZvel()) * TICSPERFRAME) << 4);

            int hitobject = movesprite(index, dax, day, daz, 4 << 8, 4 << 8, 1);

            if ((hitobject & HIT_TYPE_MASK) == HIT_SPRITE) {  // hit a sprite
                int hitsprite = (hitobject & HIT_INDEX_MASK);
                int hit = playerhit(hitsprite);
                if (hit == 0) {
                    int rv = damagesprite(hitsprite, 500);
                    if (rv == 1) {
                        killscore(hitsprite, spr.getOwner(), 8);
                    }
                }
            }
            if (hitobject != 0) {
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(BOMBPROJECTILESTAT);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int dax = (((spr.getXvel()) * TICSPERFRAME) << 12);
            int day = (((spr.getYvel()) * TICSPERFRAME) << 12);
            int daz = (((spr.getZvel()) * TICSPERFRAME) << 4);
            int hitobject = movesprite(index, dax, day, daz, 4 << 8, 4 << 8, 1);
            if ((hitobject & HIT_TYPE_MASK) == HIT_SPRITE) {  // hit a sprite
                int hitsprite = (hitobject & HIT_INDEX_MASK);
                Sprite hitSpr = boardService.getSprite(hitsprite);
                if (hitSpr != null) {
                    int hit = playerhit(hitsprite);
                    int pnum = playerhit;
                    if (hit != 0) {
                        playerpainsound(pnum);
                        playerwoundplayer(pnum, spr.getOwner(), 3);
                    } else if (spr.getPicnum() != hitSpr.getPicnum()) {
                        if (isahologram(hitsprite) != 0) {
                            showmessage("WAS A HOLOGRAM");
                            killscore(hitsprite, spr.getOwner(), 3);
                            engine.changespritestat(hitsprite, VANISH);
                            //jsdeletesprite(hitsprite);
                        } else if (isanandroid(hitsprite) != 0) {
                            showmessage("WAS AN ANDROID");
                            killscore(hitsprite, spr.getOwner(), 3);
                            androidexplosion(hitsprite);
                            engine.changespritestat(hitsprite, VANISH);
                            //jsdeletesprite(hitsprite);
                        } else {
                            blastmark(hitsprite);
                            int rv = damagesprite(hitsprite, 500);
                            if (rv == 1) {
                                killscore(hitsprite, spr.getOwner(), 3);
                            }
                        }
                    }
                }
            }
            if (hitobject != 0) {
                bombexplosion(index);
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(BOMBPROJECTILESTAT2);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            spr.setXvel(spr.getXvel() + (((krand_intercept(" GUN1025") & 64) - 32) >> 1));
            spr.setYvel(spr.getYvel() + (((krand_intercept(" GUN1026") & 64) - 32) >> 1));
            spr.setZvel(spr.getZvel() + (((krand_intercept(" GUN1027") & 31) - 16) >> 1));

            int dax = (((spr.getXvel()) * TICSPERFRAME) << 12);
            int day = (((spr.getYvel()) * TICSPERFRAME) << 12);
            int daz = (((spr.getZvel()) * TICSPERFRAME) << 4);

            int hitobject = movesprite(index, dax, day, daz, 4 << 8, 4 << 8, 1);
            if ((hitobject & HIT_TYPE_MASK) == HIT_SPRITE) {  // hit a sprite
                int hitsprite = (hitobject & HIT_INDEX_MASK);
                Sprite hitSpr = boardService.getSprite(hitsprite);
                if (hitSpr != null) {
                    int hit = playerhit(hitsprite);
                    int pnum = playerhit;
                    if (hit != 0) {
                        playerpainsound(pnum);
                        playerwoundplayer(pnum, spr.getOwner(), 3);
                    } else if (spr.getPicnum() != hitSpr.getPicnum()) {
                        if (isahologram(hitsprite) != 0) {
                            showmessage("WAS A HOLOGRAM");
                            killscore(hitsprite, spr.getOwner(), 3);
                            engine.changespritestat(hitsprite, VANISH);
                            //jsdeletesprite(hitsprite);
                        } else if (isanandroid(hitsprite) != 0) {
                            showmessage("WAS AN ANDROID");
                            killscore(hitsprite, spr.getOwner(), 3);
                            androidexplosion(hitsprite);
                            engine.changespritestat(hitsprite, VANISH);
                            //jsdeletesprite(hitsprite);
                        } else {
                            blastmark(hitsprite);
                            int rv = damagesprite(hitsprite, 500);
                            if (rv == 1) {
                                killscore(hitsprite, spr.getOwner(), 3);
                            }
                        }
                    }
                }
            }
            if (hitobject != 0) {
                bombexplosion(index);
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(DARTPROJECTILESTAT);
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();

            int dax = (((spr.getXvel()) * TICSPERFRAME) << 13);
            int day = (((spr.getYvel()) * TICSPERFRAME) << 13);
            int daz = (((spr.getZvel()) * TICSPERFRAME) << 5);

            int hitobject = movesprite(index, dax, day, daz, 4 << 8, 4 << 8, 1);

            if ((hitobject & HIT_TYPE_MASK) == HIT_SPRITE) {  // hit a sprite
                int hitsprite = (hitobject & HIT_INDEX_MASK);
                Sprite hitSpr = boardService.getSprite(hitsprite);
                if (hitSpr != null) {
                    int hit = playerhit(hitsprite);
                    int pnum = playerhit;
                    if (hit != 0) {
                        playerpainsound(pnum);
                        playerwoundplayer(pnum, spr.getOwner(), 4);
                    } else if (spr.getPicnum() != hitSpr.getPicnum()) {
                        if (isahologram(hitsprite) != 0) {
                            showmessage("WAS A HOLOGRAM");
                            killscore(hitsprite, spr.getOwner(), 4);
                            engine.changespritestat(hitsprite, VANISH);
                        } else if (isanandroid(hitsprite) != 0) {
                            showmessage("WAS AN ANDROID");
                            killscore(hitsprite, spr.getOwner(), 4);
                            androidexplosion(hitsprite);
                            engine.changespritestat(hitsprite, VANISH);
                        } else if (tekexplodebody(hitsprite) != 0) {
                            missionaccomplished(hitsprite);
                            killscore(hitsprite, spr.getOwner(), 4);
                            engine.changespritestat(hitsprite, VANISH);
                        } else {
                            int rv = damagesprite(hitsprite, 500);
                            if (rv == 1) {
                                killscore(hitsprite, spr.getOwner(), 3);
                            }
                        }
                    }
                }
            }
            if (hitobject != 0) {
                int j = jsinsertsprite(spr.getSectnum(), (short) 3);
                if (j != -1) {
                    fillsprite(j, spr.getX(), spr.getY(), spr.getZ() + (8 << 8), 0, -4, 0, 32,
                            64, 64, 0, 0,
                            EXPLOSION, spr.getAng(), 0, 0, 0, index, 63, 0);
                }
                jsdeletesprite(index);
            }

            i = nexti;
        }

        i = boardService.getStatNode(MOVEBODYPARTSSTAT);
        //flying body parts (gore option)
        while (i != null) {
            nexti = i.getNext();
            Sprite spr = i.get();
            int index = i.getIndex();
            spr.setX(spr.getX() + ((spr.getXvel() * TICSPERFRAME) >> 5));
            spr.setY(spr.getY() + ((spr.getYvel() * TICSPERFRAME) >> 5));
            spr.setZ(spr.getZ() + ((spr.getZvel() * TICSPERFRAME) >> 5));
            spr.setZvel(spr.getZvel() + (TICSPERFRAME << 7));
            Sector sec = boardService.getSector(spr.getSectnum());

            if (sec != null) {
                if (spr.getZ() < sec.getCeilingz() + (4 << 8)) {
                    spr.setZ(sec.getCeilingz() + (4 << 8));
                    spr.setZvel(-(spr.getZvel() >> 1));
                }
                if (spr.getZ() > sec.getFloorz() - (4 << 8)) {
                    spr.setZ(sec.getFloorz() - (4 << 8));
                    if (spr.getPicnum() == GOREBLOOD) {
                        spr.setXvel(0);
                        spr.setYvel(0);
                        spr.setZvel(0);
                        spr.setCstat(spr.getCstat() | 0x20);
                    } else {
                        spr.setZvel(-(spr.getZvel() >> 1));
                    }
                }
            }

            spr.setLotag(spr.getLotag() - TICSPERFRAME);
            if (spr.getLotag() < 0) {
                jsdeletesprite(index);
            }
            i = nexti;
        }
    }

    public static int wallnormal(Wall wal) {
        int w1x = wal.getX();
        int w1y = wal.getY();
        wal = wal.getWall2();
        int w2x = wal.getX();
        int w2y = wal.getY();
        int wnorm = EngineUtils.getAngle(w2x - w1x, w2y - w1y);
        wnorm = ((wnorm + 512) & 2047);

        return (wnorm);
    }

}
