package ru.m210projects.Tekwar;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Point;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Tekwar.Types.*;

import java.util.Arrays;

import static java.lang.Math.abs;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClampAngle;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Tekwar.Animate.setanimation;
import static ru.m210projects.Tekwar.Globals.*;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Names.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tekchng.*;
import static ru.m210projects.Tekwar.Tekgun.*;
import static ru.m210projects.Tekwar.Tekmap.*;
import static ru.m210projects.Tekwar.Tekprep.subwaysound;
import static ru.m210projects.Tekwar.Teksnd.*;
import static ru.m210projects.Tekwar.Tekspr.movesprite;
import static ru.m210projects.Tekwar.Tekstat.*;
import static ru.m210projects.Tekwar.Types.ANIMATION.WALLX;
import static ru.m210projects.Tekwar.Types.ANIMATION.WALLY;
import static ru.m210projects.Tekwar.View.showmessage;

public class Tektag {

    public static final int BOBBMAX = 512;
    public static final int BOBBDELTA = 128;

    public static final int VEHICLEHEIGHT = -13312;
    public static final int MAXANIMPICS = 32;
    public static final int MAXDELAYFUNCTIONS = 32;
    public static final int DOORUPTAG = 6;
    public static final int DOORDOWNTAG = 7;
    public static final int DOORSPLITHOR = 8;
    public static final int DOORSPLITVER = 9;
    public static final int PLATFORMELEVTAG = 1000;// additional sector tags
    public static final int BOXELEVTAG = 1003;
    public static final int PLATFORMDELAYTAG = 1004;
    public static final int BOXDELAYTAG = 1005;
    public static final int DOORFLOORTAG = 1006;
    public static final int PLATFORMDROPTAG = 1007;
    public static final int DST_BAYDOOR = 10;
    public static final int DST_HYDRAULICDOOR = 20;
    public static final int DST_ELEVATORDOOR = 30;
    public static final int DST_MATRIXDOOR1 = 40;
    public static final int DST_MATRIXDOOR2 = 50;
    public static final int DST_MATRIXDOOR3 = 60;
    public static final int DST_MATRIXDOOR4 = 70;
    public static final int SPRITEELEVATORTAG = 1500;
    public static final int PULSELIGHT = 0;// sector effect tags flags32[]
    public static final int FLICKERLIGHT = 1;
    public static final int DELAYEFFECT = 2;
    public static final int WPANNING = 3;
    public static final int FPANNING = 4;
    public static final int CPANNING = 5;
    public static final int FLICKERDELAY = 6;
    public static final int BLINKDELAY = 7;
    public static final int STEADYLIGHT = 8;
    public static final int WARPSECTOR = 9;
    public static final int KILLSECTOR = 10;
    public static final int DOORSPEEDEFFECT = 11;
    public static final int QUICKCLOSE = 12;
    public static final int SOUNDON = 13;
    public static final int SOUNDOFF = 14;
    public static final int DOORSUBTYPE = 15;
    public static final int DOORDELAY = (CLKIPS * 4);
    public static final int DOORSPEED = 128;
    public static final int ELEVSPEED = 256;

    public static final int MAXDOORS = 200;
    public static final int MAXFLOORDOORS = 25;

    public static final int D_NOTHING = 0;
    public static final int D_OPENDOOR = 1;
    public static final int D_CLOSEDOOR = 2;
    public static final int D_OPENING = 3;
    public static final int D_CLOSING = 4;
    public static final int D_WAITING = 5;
    public static final int D_SHUTSOUND = 6;
    public static final int D_OPENSOUND = 7;

    public static final int DOORCLOSED = 0;
    public static final int DOOROPENING = 1;
    public static final int DOOROPENED = 2;
    public static final int DOORCLOSING = 3;

    public static final int MAXSECTORVEHICLES = 10;
    public static final int MAXVEHICLEPOINTS = 200;
    public static final int MAXVEHICLETRACKS = 50;
    public static final int MAXVEHICLESECTORS = 30;
    public static final int SECTORVEHICLETAG = 1010;

    public static final int MAXSPRITEELEVS = 25;
    public static final int MAXPARTS = 20;
    public static final int MAXELEVFLOORS = 20;
    public static final int MAXELEVDOORS = 4;

    public static final int MAXMAPSOUNDFX = 32;
    public static final int MAP_SFX_AMBIENT = 0;
    public static final int MAP_SFX_SECTOR = 1;
    public static final int MAP_SFX_TOGGLED = 2;

    public static final int E_OPENINGDOOR = 0;
    public static final int E_CLOSINGDOOR = 1;
    public static final int E_WAITING = 2;
    public static final int E_MOVING = 3;
    public static final int E_NEXTFLOOR = 4;

    public static final int E_GOINGUP = 0;
    public static final int E_GOINGDOWN = 1;

    public static final int E_WAITDELAY = CLKIPS * 4;
    public static final int E_DOOROPENPOS = 15360;
    public static final short[] opwallfind = new short[2];
    public static final int NEWMAPLOTAG = 25;
    public static final int[] flags32 = {0x80000000, 0x40000000, 0x20000000, 0x10000000, 0x08000000, 0x04000000, 0x02000000,
            0x01000000, 0x00800000, 0x00400000, 0x00200000, 0x00100000, 0x00080000, 0x00040000, 0x00020000, 0x00010000,
            0x00008000, 0x00004000, 0x00002000, 0x00001000, 0x00000800, 0x00000400, 0x00000200, 0x00000100, 0x00000080,
            0x00000040, 0x00000020, 0x00000010, 0x00000008, 0x00000004, 0x00000002, 0x00000001};
    public static int ambupdateclock;
    public static int totalmapsndfx = 0;
    public static final Spriteelev[] spriteelev = new Spriteelev[MAXSPRITEELEVS];
    public static int sprelevcnt;

    public static final Doortype[] doortype = new Doortype[MAXDOORS];
    public static final int[] doorxref = new int[MAXSECTORS];
    public static int numdoors;

    public static final Floordoor[] floordoor = new Floordoor[MAXFLOORDOORS];
    public static final int[] fdxref = new int[MAXSECTORS];
    public static int numfloordoors;

    public static final Sectoreffect[] sectoreffect = new Sectoreffect[MAXSECTORS];
    public static final Elevatortype[] elevator = new Elevatortype[MAXSECTORS];
    public static final Sectorvehicle[] sectorvehicle = new Sectorvehicle[MAXSECTORVEHICLES];
    public static final Mapsndfxtype[] mapsndfx = new Mapsndfxtype[MAXMAPSOUNDFX];
    public static int numvehicles;
    public static int headbob, bobstep = BOBBDELTA;
    public static final boolean[] onelev = new boolean[MAXPLAYERS];
    public static final int[] subwaystopdir = {1, 1, 1, 1};
    public static int numanimates;
    public static short numdelayfuncs;
    public static int loopinsound = -1;
    public static int baydoorloop = -1;
    public static int ambsubloop = -1;
    // Board animation variables
    public static final short[] rotatespritelist = new short[16];
    public static short rotatespritecnt;
    public static final short[] warpsectorlist = new short[64];
    public static short warpsectorcnt;
    public static final short[] xpanningsectorlist = new short[16];
    public static short xpanningsectorcnt;
    public static final short[] ypanningwalllist = new short[64];
    public static short ypanningwallcnt;
    public static final short[] floorpanninglist = new short[64];
    public static short floorpanningcnt;
    public static final short[] dragsectorlist = new short[16];
    public static final short[] dragxdir = new short[16];
    public static final short[] dragydir = new short[16];
    public static short dragsectorcnt;
    public static final int[] dragx1 = new int[16];
    public static final int[] dragy1 = new int[16];
    public static final int[] dragx2 = new int[16];
    public static final int[] dragy2 = new int[16];
    public static final int[] dragfloorz = new int[16];
    public static short swingcnt;
    public static final short[][] swingwall = new short[32][5];
    public static final short[] swingsector = new short[32];
    public static final short[] swingangopen = new short[32];
    public static final short[] swingangclosed = new short[32];
    public static final short[] swingangopendir = new short[32];
    public static final short[] swingang = new short[32];
    public static final short[] swinganginc = new short[32];
    public static final int[][] swingx = new int[32][8];
    public static final int[][] swingy = new int[32][8];
    public static final short[] revolvesector = new short[4];
    public static final short[] revolveang = new short[4];
    public static short revolvecnt;
    public static final int[][] revolvex = new int[4][32];
    public static final int[][] revolvey = new int[4][32];
    public static final int[] revolvepivotx = new int[4];
    public static final int[] revolvepivoty = new int[4];
    public static final short[][] subwaytracksector = new short[4][128];
    public static final short[] subwaynumsectors = new short[4];
    public static short subwaytrackcnt;
    public static final int[][] subwaystop = new int[4][8];
    public static final int[] subwaystopcnt = new int[4];
    public static final int[] subwaytrackx1 = new int[4];
    public static final int[] subwaytracky1 = new int[4];
    public static final int[] subwaytrackx2 = new int[4];
    public static final int[] subwaytracky2 = new int[4];
    public static final int[] subwayx = new int[4];
    public static final int[] subwaygoalstop = new int[4];
    public static final int[] subwayvel = new int[4];
    public static final int[] subwaypausetime = new int[4];
    public static final short[] waterfountainwall = new short[MAXPLAYERS];
    public static final short[] waterfountaincnt = new short[MAXPLAYERS];
    public static final short[] slimesoundcnt = new short[MAXPLAYERS];
    public static int warpx, warpy, warpz, warpang;
    public static int warpsect;
    public static int tekwarpx, tekwarpy, tekwarpz;
    public static int tekwarsect;
    static final Delayfunc[] delayfunc = new Delayfunc[MAXDELAYFUNCTIONS];
    static final Animpic[] animpic = new Animpic[MAXANIMPICS];
    static int secnt;
    static final int[] sexref = new int[MAXSECTORS];
    static final short[] onveh = new short[MAXPLAYERS];

    public static void tekheadbob() {
        if (tekcfg.gHeadBob && !game.menu.gShowMenu) {
            headbob += bobstep;
            if (headbob < -BOBBMAX || headbob > BOBBMAX) {
                bobstep = -bobstep;
            }
        }
    }

    public static int krand_intercept(String ignoredStg) {
        return (engine.krand());
    }

    public static void tagcode() {
        for (int i = 0; i < warpsectorcnt; i++) {
            final int dasector = warpsectorlist[i];
            int j = (lockclock & 127) >> 2;
            if (j >= 16) {
                j = 31 - j;
            }

            Sector sec = boardService.getSector(dasector);
            if (sec == null) {
                continue;
            }

            sec.setCeilingshade(j);
            sec.setFloorshade(j);
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                wal.setShade(j);
            }
        }

        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
            Sector plrSec = boardService.getSector(gPlayer[p].cursectnum);
            if (plrSec != null && plrSec.getLotag() == 10) // warp sector
            {
                if (gPlayer[p].cursectnum != gPlayer[p].ocursectnum) {
                    Sprite spr2 = boardService.getSprite(gPlayer[p].playersprite);
                    if (spr2 != null) {
                        warpsprite(gPlayer[p].playersprite);
                        gPlayer[p].oposx = gPlayer[p].posx = spr2.getX();
                        gPlayer[p].oposy = gPlayer[p].posy = spr2.getY();
                        gPlayer[p].oposz = gPlayer[p].posz = spr2.getZ();
                        gPlayer[p].ang = spr2.getAng();
                        gPlayer[p].cursectnum = spr2.getSectnum();
                        spr2.setZ(spr2.getZ() + (KENSPLAYERHEIGHT << 8));
                    }
                }
            }
        }

        for (int i = 0; i < xpanningsectorcnt; i++) {
            final int dasector = xpanningsectorlist[i];
            Sector sec = boardService.getSector(dasector);
            if (sec == null) {
                continue;
            }

            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                wal.setXpanning((byte) ((lockclock >> 2) & 255));
            }
        }

        for (int i = 0; i < ypanningwallcnt; i++) {
            Wall wal = boardService.getWall(ypanningwalllist[i]);
            if(wal != null) {
                wal.setYpanning((byte) ~(lockclock & 255));
            }
        }

        for (int i = 0; i < rotatespritecnt; i++) {
            Sprite spr = boardService.getSprite(rotatespritelist[i]);
            if (spr != null) {
                spr.setAng(spr.getAng() + (TICSPERFRAME << 2));
                spr.setAng(spr.getAng() & 2047);
            }
        }

        // kens slime floor
        for (int i = 0; i < floorpanningcnt; i++) {
            Sector sec = boardService.getSector(floorpanninglist[i]);
            if (sec != null) {
                sec.setFloorxpanning((byte) ((lockclock >> 2) & 255));
                sec.setFloorypanning((byte) ((lockclock >> 2) & 255));
            }
        }

        for (int i = 0; i < dragsectorcnt; i++) {
            final int dasector = dragsectorlist[i];
            Sector sec = boardService.getSector(dasector);
            if (sec == null || sec.getWallNode() == null) {
                continue;
            }

            Wall wallptr = sec.getWallNode().get();

            if (wallptr.getX() + dragxdir[i] < dragx1[i]) {
                dragxdir[i] = 16;
            }
            if (wallptr.getY() + dragydir[i] < dragy1[i]) {
                dragydir[i] = 16;
            }
            if (wallptr.getX() + dragxdir[i] > dragx2[i]) {
                dragxdir[i] = -16;
            }
            if (wallptr.getY() + dragydir[i] > dragy2[i]) {
                dragydir[i] = -16;
            }
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                engine.dragpoint(wn.getIndex(), wal.getX() + dragxdir[i], wal.getY() + dragydir[i]);
            }

            int j = sec.getFloorz();
            game.pInt.setfloorinterpolate(dasector, sec);
            sec.setFloorz(dragfloorz[i] + (EngineUtils.sin((lockclock << 4) & 2047) >> 3));
            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                if (gPlayer[p].cursectnum == dasector) {
                    Sprite spr = boardService.getSprite(gPlayer[p].playersprite);
                    if (spr != null) {
                        gPlayer[p].posx += dragxdir[i];
                        gPlayer[p].posy += dragydir[i];
                        gPlayer[p].posz += (sec.getFloorz() - j);
                        engine.setsprite(gPlayer[p].playersprite, gPlayer[p].posx, gPlayer[p].posy,
                                gPlayer[p].posz + (KENSPLAYERHEIGHT << 8));

                        spr.setAng((short) gPlayer[p].ang);
                    }
                }
            }
        }

        for (int i = 0; i < swingcnt; i++) {
            if (swinganginc[i] != 0) {
                int oldang = swingang[i];
                for (int j = 0; j < (TICSPERFRAME << 2); j++) {
                    swingang[i] = (short) ((swingang[i] + 2048 + swinganginc[i]) & 2047);
                    if (swingang[i] == swingangclosed[i]) {
                        if (j == ((TICSPERFRAME << 2) - 1)) {
                            playsound(S_JUMP, swingx[i][0], swingy[i][0], 0, ST_UPDATE);
                        }
                        swinganginc[i] = 0;
                    }
                    if (swingang[i] == swingangopen[i]) {
                        if (j == ((TICSPERFRAME << 2) - 1)) {
                            playsound(S_JUMP, swingx[i][0], swingy[i][0], 0, ST_UPDATE);
                        }
                        swinganginc[i] = 0;
                    }
                }
                for (int k = 1; k <= 3; k++) {
                    Point out = EngineUtils.rotatepoint(swingx[i][0], swingy[i][0], swingx[i][k], swingy[i][k], swingang[i]);
                    engine.dragpoint(swingwall[i][k], out.getX(), out.getY());
                }

                if (swinganginc[i] != 0) {
                    for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                        if (gPlayer[p].cursectnum >= 0 && ((gPlayer[p].cursectnum == swingsector[i])
                                || (testneighborsectors(gPlayer[p].cursectnum, swingsector[i]) == 1))) {
                            int cnt = 256;
                            int good;
                            do {
                                good = 1;

                                // swingangopendir is -1 if forwards, 1 is backwards
                                int l = (swingangopendir[i] > 0) ? 1 : 0;
                                for (int k = l + 3; k >= l; k--) {
                                    if (engine.clipinsidebox(gPlayer[p].posx, gPlayer[p].posy, swingwall[i][k],
                                            128) != 0) {
                                        good = 0;
                                        break;
                                    }
                                }

                                if (good == 0) {
                                    if (cnt == 256) {
                                        swinganginc[i] = (short) -swinganginc[i];
                                        swingang[i] = (short) oldang;
                                    } else {
                                        swingang[i] = (short) ((swingang[i] + 2048 - swinganginc[i]) & 2047);
                                    }
                                    for (int k = 1; k <= 3; k++) {
                                        Point out = EngineUtils.rotatepoint(swingx[i][0], swingy[i][0], swingx[i][k],
                                                swingy[i][k], swingang[i]);
                                        engine.dragpoint(swingwall[i][k], out.getX(), out.getY());
                                    }
                                    if (swingang[i] == swingangclosed[i]) {
                                        swinganginc[i] = 0;
                                        break;
                                    }
                                    if (swingang[i] == swingangopen[i]) {
                                        swinganginc[i] = 0;
                                        break;
                                    }
                                    cnt--;
                                }
                            } while ((good == 0) && (cnt > 0));
                        }
                    }
                }

            }
        }

        for (int i = 0; i < revolvecnt; i++) {
            Sector sec = boardService.getSector(revolvesector[i]);
            if (sec == null) {
                continue;
            }

            int startwall = sec.getWallptr();
            revolveang[i] = (short) ((revolveang[i] + 2048 - (TICSPERFRAME << 2)) & 2047);
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                int w = wn.getIndex();
                Point out = EngineUtils.rotatepoint(revolvepivotx[i], revolvepivoty[i], revolvex[i][w - startwall],
                        revolvey[i][w - startwall], revolveang[i]);
                int dax = out.getX();
                int day = out.getY();
                engine.dragpoint(w, dax, day);
            }
        }

        for (int i = 0; i < subwaytrackcnt; i++) {
            if (subwaysound[i] == -1) {
                subwaysound[i] = playsound(S_SUBWAYLOOP, subwayx[i], subwaytracky1[i], -1, ST_VEHUPDATE);
            } else {
                updatevehiclesnds(subwaysound[i], subwayx[i], subwaytracky1[i]);
            }

            final int dasector = subwaytracksector[i][0];
            Sector sec1 = boardService.getSector(dasector);
            if (sec1 == null) {
                continue;
            }

            for (ListNode<Wall> wn = sec1.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                game.pInt.setwallinterpolate(wn.getIndex(), wal);
                if (wal.getX() > subwaytrackx1[i]) {
                    if (wal.getY() > subwaytracky1[i]) {
                        if (wal.getX() < subwaytrackx2[i]) {
                            if (wal.getY() < subwaytracky2[i]) {
                                wal.setX(wal.getX() + (subwayvel[i] & 0xfffffffc));
                            }
                        }
                    }
                }
            }

            for (int j = 1; j < subwaynumsectors[i]; j++) {
                final int dasector2 = subwaytracksector[i][j];
                Sector subwaySec = boardService.getSector(dasector2);
                if (subwaySec == null) {
                    continue;
                }

                for (ListNode<Wall> wn = subwaySec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    game.pInt.setwallinterpolate(wn.getIndex(), wal);
                    wal.setX(wal.getX() + (subwayvel[i] & 0xfffffffc));
                }

                ListNode<Sprite> node = boardService.getSectNode(dasector2), next;
                while (node != null) {
                    next = node.getNext();
                    Sprite s1 = node.get();
                    game.pInt.setsprinterpolate(node.getIndex(), s1);
                    s1.setX(s1.getX() + (subwayvel[i] & 0xfffffffc));
                    node = next;
                }
            }

            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                Sector psec = boardService.getSector(gPlayer[p].cursectnum);
                if (psec == null) {
                    continue;
                }

                if (gPlayer[p].cursectnum != subwaytracksector[i][0]) {
                    Sector tracksector = boardService.getSector(subwaytracksector[i][0]);
                    if (tracksector != null && psec.getFloorz() != tracksector.getFloorz()) {
                        if (gPlayer[p].posx > subwaytrackx1[i]) {
                            if (gPlayer[p].posy > subwaytracky1[i]) {
                                if (gPlayer[p].posx < subwaytrackx2[i]) {
                                    if (gPlayer[p].posy < subwaytracky2[i]) {
                                        gPlayer[p].posx += (subwayvel[i] & 0xfffffffc);
                                        Sprite spr = boardService.getSprite(gPlayer[p].playersprite);
                                        if (spr != null) {
                                            // Update sprite representation of player
                                            engine.setsprite(gPlayer[p].playersprite, gPlayer[p].posx, gPlayer[p].posy,
                                                    gPlayer[p].posz + (KENSPLAYERHEIGHT << 8));
                                            spr.setAng((short) gPlayer[p].ang);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            subwayx[i] += (subwayvel[i] & 0xfffffffc);
            if (subwaygoalstop[i] >= 8) {
                continue;
            }

            int k = subwaystop[i][subwaygoalstop[i]] - subwayx[i];
            if (k > 0) {
                if (k > 2048) {
                    if (subwayvel[i] == 12) {
                        playsound(S_SUBWAYSTART, subwayx[0], subwaytracky1[0], 0, ST_UNIQUE | ST_NOUPDATE);
                    }
                    if (subwayvel[i] < 128) {
                        subwayvel[i]++;
                    }
                } else {
                    if (subwayvel[i] == 32) {
                        playsound(S_SUBWAYSTOP, subwayx[0], subwaytracky1[0], 0, ST_UNIQUE | ST_NOUPDATE);
                    }
                    subwayvel[i] = (k >> 4) + 1;
                }
            } else if (k < 0) {
                if (k < -2048) {
                    if (subwayvel[i] == -12) {
                        playsound(S_SUBWAYSTART, subwayx[0], subwaytracky1[0], 0, ST_UNIQUE | ST_NOUPDATE);
                    }
                    if (subwayvel[i] > -128) {
                        subwayvel[i]--;
                    }
                } else {
                    if (subwayvel[i] == -32) {
                        playsound(S_SUBWAYSTOP, subwayx[0], subwaytracky1[0], 0, ST_UNIQUE | ST_NOUPDATE);
                    }
                    subwayvel[i] = ((k >> 4) - 1);
                }
            }

            if (((subwayvel[i] >> 2) == 0) && (Math.abs(k) < 2048)) {
                if (subwaypausetime[i] == 720) {
                    for (int j = 1; j < subwaynumsectors[i]; j++) // Open all subway doors
                    {
                        final int dasector2 = subwaytracksector[i][j];
                        Sector sec = boardService.getSector(revolvesector[i]);
                        if (sec == null) {
                            continue;
                        }

                        if (sec.getLotag() == 17) {
                            sec.setLotag(16);
                            playsound(S_BIGSWINGCL, subwayx[i], subwaytracky1[i], 0, ST_NOUPDATE | ST_UNIQUE);
                            operatesector(dasector2);
                            sec.setLotag(17);
                        }
                    }
                }
                if ((subwaypausetime[i] >= 120) && (subwaypausetime[i] - TICSPERFRAME < 120)) {
                    for (int j = 1; j < subwaynumsectors[i]; j++) // Close all subway doors
                    {
                        final int dasector2 = subwaytracksector[i][j];
                        Sector sec = boardService.getSector(dasector2);
                        if (sec == null) {
                            continue;
                        }

                        if (sec.getLotag() == 17) {
                            sec.setLotag(16);
                            playsound(S_BIGSWINGCL, subwayx[i], subwaytracky1[i], 0, ST_NOUPDATE | ST_UNIQUE);
                            operatesector(dasector2);
                            sec.setLotag(17);
                        }
                    }
                }

                subwaypausetime[i] -= TICSPERFRAME;
                if (subwaypausetime[i] < 0) {
                    subwaypausetime[i] = 720;
                    if (subwaygoalstop[i] == (subwaystopcnt[i] - 1)) {
                        subwaystopdir[i] = -1;
                        subwaygoalstop[i] = subwaystopcnt[i] - 2;
                    } else if (subwaygoalstop[i] == 0) {
                        subwaygoalstop[i] = 1;
                        subwaystopdir[i] = 1;
                    } else {

                        subwaygoalstop[i] += subwaystopdir[i];
                    }
                }
            }
        }

        tektagcode();
    }

    public static void tektagcode() {
        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
            tekanimweap(p);
            tekhealstun(p);
        }

        for (int i = 0; i < numdoors; i++) {
            movedoors(i);
        }

        if (game.nNetMode != NetMode.Multiplayer) {
            for (int i = 0; i < secnt; i++) {
                final int s = sexref[i];
                Sector sect = boardService.getSector(s);
                Sectoreffect septr = sectoreffect[s];
                if (sect == null || septr == null || septr.triggerable != 0) {
                    continue;
                }

                int effect = septr.sectorflags;
                if ((effect & flags32[WPANNING]) != 0) {
                    int tics = TICSPERFRAME;

                    int dax = (tics * septr.cos) >> 15;
                    int day = (tics * septr.sin) >> 13;
                    for (ListNode<Wall> wn = sect.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setXpanning(wal.getXpanning() + (char) dax);
                        wal.setYpanning(wal.getYpanning() - (char) day);
                    }
                }

                if ((effect & flags32[FPANNING]) != 0) {
                    int tics = TICSPERFRAME;
                    int dax = (tics * septr.cos);
                    int day = (tics * septr.sin);
                    ListNode<Sprite> node = boardService.getSectNode(s);
                    while (node != null) {
                        ListNode<Sprite> next = node.getNext();
                        if (node.get().getOwner() < MAXSPRITES) {
                            int dax2 = dax >> 10;
                            int day2 = day >> 10;
                            movesprite(node.getIndex(), dax2, day2, 0, 4 << 8, 4 << 8, 0);
                        }
                        node = next;
                    }
                    for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                        if (gPlayer[p].cursectnum == s) {
                            if (gPlayer[p].posz >= (sect.getFloorz() - (42 << 8))) {
                                engine.clipmove(gPlayer[p].posx, gPlayer[p].posy, gPlayer[p].posz,
                                        gPlayer[p].cursectnum, (long) dax << 4, (long) day << 4, 128, 4 << 8, 4 << 8, CLIPMASK0);
                                gPlayer[p].posx = clipmove_x;
                                gPlayer[p].posy = clipmove_y;
                                gPlayer[p].posz = clipmove_z;
                                gPlayer[p].cursectnum = clipmove_sectnum;

                                engine.setsprite(gPlayer[p].playersprite, gPlayer[p].posx, gPlayer[p].posy,
                                        gPlayer[p].posz + (42 << 8));
                            }
                        }
                    }
                    dax >>= 12;
                    day >>= 12;
                    sect.setFloorxpanning(sect.getFloorxpanning() - (char) dax);
                    sect.setFloorypanning(sect.getFloorypanning() + (char) day);
                }
                if ((effect & flags32[CPANNING]) != 0) {
                    int tics = TICSPERFRAME;
                    int dax = (tics * septr.cos) >> 12;
                    int day = (tics * septr.sin) >> 12;
                    sect.setCeilingxpanning(sect.getCeilingxpanning() - (char) dax);
                    sect.setCeilingypanning(sect.getCeilingypanning() + (char) day);
                }
                if ((septr.delay -= TICSPERFRAME) > 0) {
                    continue;
                }
                // negative overflow here without this - jeffy
                if (septr.delay < 0) {
                    septr.delay = 0;
                }
                septr.delay += septr.delayreset;
                if ((effect & flags32[PULSELIGHT]) != 0) {
                    sect.setCeilingshade(sect.getCeilingshade() + septr.animate);
                    int lo, hi;
                    if (septr.hi > septr.lo) {
                        hi = septr.hi;
                        lo = septr.lo;
                    } else {
                        hi = septr.lo;
                        lo = septr.hi;
                    }
                    if (septr.animate < 0) {
                        if (sect.getCeilingshade() <= lo) {
                            septr.animate = abs(septr.animate);
                        }
                    } else {
                        if (sect.getCeilingshade() >= hi) {
                            septr.animate = -septr.animate;
                        }
                    }
                    sect.setFloorshade(sect.getCeilingshade());
                    for (ListNode<Wall> wn = sect.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setShade(sect.getCeilingshade());
                    }

                } else if ((effect & flags32[FLICKERLIGHT]) != 0) {
                    int r = krand_intercept("TAG 1491");
                    if (r < 16384) {
                        sect.setCeilingshade((byte) septr.hi);
                    } else if (r > 16384) {
                        sect.setCeilingshade((byte) septr.lo);
                    }
                    sect.setFloorshade(sect.getCeilingshade());
                    for (ListNode<Wall> wn = sect.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setShade(sect.getCeilingshade());
                    }
                } else if ((effect & flags32[FLICKERDELAY]) != 0) {
                    if (sect.getCeilingshade() == septr.lo) {
                        sect.setCeilingshade((byte) septr.hi);
                    } else {
                        sect.setCeilingshade((byte) septr.lo);
                        septr.delay >>= 2;
                    }
                    sect.setFloorshade(sect.getCeilingshade());
                    for (ListNode<Wall> wn = sect.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setShade(sect.getCeilingshade());
                    }
                } else if ((effect & flags32[BLINKDELAY]) != 0) {
                    if (sect.getCeilingshade() == septr.lo) {
                        sect.setCeilingshade((byte) septr.hi);
                    } else {
                        sect.setCeilingshade((byte) septr.lo);
                    }
                    sect.setFloorshade(sect.getCeilingshade());
                    for (ListNode<Wall> wn = sect.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setShade(sect.getCeilingshade());
                    }
                }
                if ((effect & flags32[KILLSECTOR]) != 0) {
                    int floorz = sect.getFloorz();
                    for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                        if (gPlayer[p].cursectnum == s) {
                            // matrix specific check here
                            if ((klabs(gPlayer[p].posz - floorz) < 10240) || (mission == 7)) {
                                int k;
                                if ((k = fdxref[s]) != -1) {
                                    if (!gPlayer[p].noclip && floordoor[k].state != DOORCLOSED) {
                                        changehealth(p, -septr.damage);
                                        changescore(p, -10);
                                    }
                                } else {
                                    if (!gPlayer[p].noclip && septr.delay == septr.delayreset) {
                                        changehealth(p, -septr.damage);
                                        changescore(p, -10);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (game.nNetMode != NetMode.Multiplayer) {
            for (int i = 0; i < sprelevcnt; i++) {
                movesprelevs(i);
            }
            for (int i = 0; i < numfloordoors; i++) {
                movefloordoor(i);
            }
            for (int i = 0; i < numvehicles; i++) {
                movevehicles(i);
            }
        }

        tekdoanimpic();
        tekdodelayfuncs();
        if ((lockclock - ambupdateclock) > 120) {
            checkmapsndfx(screenpeek);
        }
    }

    public static void setanimpic(int pic, int tics, int frames) {
        for (int i = 0; i < numanimates; i++) {
            if (animpic[i].pic == pic) {
                return;
            }
        }

        if (numanimates + 1 < MAXANIMPICS) {
            animpic[numanimates].pic = (short) pic;
            animpic[numanimates].tics = (short) tics;
            animpic[numanimates].frames = (short) frames;
            animpic[numanimates].nextclock = lockclock + tics;
            numanimates++;
        }
    }

    public static void tekdodelayfuncs() {
        int n = numdelayfuncs;
        for (int i = 0; i < n; i++) {
            if (delayfunc[i].func == 0) {
                continue;
            }
            delayfunc[i].tics -= TICSPERFRAME;
            if (delayfunc[i].tics <= 0) {
                int p = delayfunc[i].parm;
                delayfunc[i].func = (short) p; // (*delayfunc[i].func)(p);
                delayfunc[i].set(delayfunc[numdelayfuncs - 1]); // memmove(delayfunc[i],delayfunc[numdelayfuncs-1],
                // sizeof(struct delayfunc));
                delayfunc[numdelayfuncs - 1].set(0);
                numdelayfuncs--;
            }
        }
    }

    public static void tekdoanimpic() {
        int n = numanimates;
        for (int i = 0; i < n; i++) {
            if (lockclock < animpic[i].nextclock) {
                continue;
            }
            if (animpic[i].frames > 0) {
                if (--animpic[i].frames > 0) {
                    animpic[i].pic++;
                    animpic[i].nextclock = lockclock + animpic[i].tics;
                }
            } else if (animpic[i].frames < 0) {
                if (++animpic[i].frames < 0) {
                    animpic[i].pic--;
                    animpic[i].nextclock = lockclock + animpic[i].tics;
                }
            } else {
                numanimates--;
                if (numanimates > 0) {
                    animpic[i].set(animpic[numanimates]); // memmove(animpic[i],animpic[numanimates], sizeof(struct
                    // animpic));
                    animpic[i].set(0); // memset(animpic[numanimates],0,sizeof(struct animpic));
                }
            }
        }
    }

    public static void movefloordoor(int d) {
        int tics = TICSPERFRAME << 2;
        Floordoor dptr = floordoor[d];
        switch (dptr.state) {
            case DOOROPENING:
                if (dptr.dist1 > 0) {
                    int s = tics;
                    dptr.dist1 -= s;
                    if (dptr.dist1 < 0) {
                        s += dptr.dist1;
                        dptr.dist1 = 0;
                    }

                    Wall w = boardService.getWall(dptr.wall1);
                    if (w == null) {
                        break;
                    }

                    switch (dptr.dir) {
                        case 0:
                            engine.dragpoint(dptr.wall1, w.getX(), w.getY() - s);
                            w = w.getWall2();
                            engine.dragpoint(w.getPoint2(), w.getX(), w.getY() - s);
                            break;
                        case 1:
                            engine.dragpoint(dptr.wall1, w.getX() + s, w.getY());
                            w = w.getWall2();
                            engine.dragpoint(w.getPoint2(), w.getX() + s, w.getY());
                            break;
                        case 2:
                            engine.dragpoint(dptr.wall1, w.getX(), w.getY() + s);
                            w = w.getWall2();
                            engine.dragpoint(w.getPoint2(), w.getX(), w.getY() + s);
                            break;
                        case 3:
                            engine.dragpoint(dptr.wall1, w.getX() - s, w.getY());
                            w = w.getWall2();
                            engine.dragpoint(w.getPoint2(), w.getX() - s, w.getY());
                            break;
                    }
                }
                if (dptr.dist2 > 0) {
                    int s = tics;
                    dptr.dist2 -= s;
                    if (dptr.dist2 < 0) {
                        s += dptr.dist2;
                        dptr.dist2 = 0;
                    }

                    Wall w = boardService.getWall(dptr.wall2);
                    if (w == null) {
                        break;
                    }

                    switch (dptr.dir) {
                        case 0:
                            engine.dragpoint(dptr.wall2, w.getX(), w.getY() + s);
                            w = w.getWall2();
                            engine.dragpoint(w.getPoint2(), w.getX(), w.getY() + s);
                            break;
                        case 1:
                            engine.dragpoint(dptr.wall2, w.getX() - s, w.getY());
                            w = w.getWall2();
                            engine.dragpoint(w.getPoint2(), w.getX() - s, w.getY());
                            break;
                        case 2:
                            engine.dragpoint(dptr.wall2, w.getX(), w.getY() - s);
                            w = w.getWall2();
                            engine.dragpoint(w.getPoint2(), w.getX(), w.getY() - s);
                            break;
                        case 3:
                            engine.dragpoint(dptr.wall2, w.getX() + s, w.getY());
                            w = w.getWall2();
                            engine.dragpoint(w.getPoint2(), w.getX() + s, w.getY());
                            break;
                    }
                }
                if (dptr.dist1 <= 0 && dptr.dist2 <= 0) {
                    dptr.state = DOOROPENED;
                }
                break;
            case DOORCLOSING:
            case DOOROPENED:
            case DOORCLOSED:
                break;
        }
    }

    public static void movesprelevs(int e) {
        Spriteelev s = spriteelev[e];
        int tics = TICSPERFRAME << 6;

        switch (s.state) {
            case E_WAITING:
                s.delay -= TICSPERFRAME;
                if (s.delay <= 0) {
                    s.state = E_CLOSINGDOOR;
                }
                return;
            case E_CLOSINGDOOR:
                s.doorpos -= tics;
                if (s.doorpos <= 0) {
                    s.doorpos = 0;
                    s.state = E_NEXTFLOOR;
                }
                break;
            case E_OPENINGDOOR:
                s.doorpos += tics;
                if (s.doorpos >= E_DOOROPENPOS) {
                    s.doorpos = E_DOOROPENPOS;
                    s.state = E_WAITING;
                    s.delay = E_WAITDELAY;
                }
                break;
            case E_MOVING:
                if (s.curfloor >= 0) {
                    int goalz = s.floorz[s.curfloor];
                    if (s.curdir == E_GOINGUP) {
                        s.floorpos -= tics;
                        if (s.floorpos <= goalz) {
                            s.floorpos += abs(s.floorpos - goalz);
                            s.state = E_OPENINGDOOR;
                        }
                    } else {
                        s.floorpos += tics;
                        if (s.floorpos >= goalz) {
                            s.floorpos -= abs(s.floorpos - goalz);
                            s.state = E_OPENINGDOOR;
                        }
                    }
                }
                break;
            case E_NEXTFLOOR:
                if (s.curdir == E_GOINGUP) {
                    s.curfloor++;
                    if (s.curfloor > s.floors) {
                        s.curfloor -= 2;
                        s.curdir = E_GOINGDOWN;
                    }
                } else if (s.curdir == E_GOINGDOWN) {
                    s.curfloor--;
                    if (s.curfloor < 0) {
                        s.curfloor += 2;
                        s.curdir = E_GOINGUP;
                    }
                }
                s.state = E_MOVING;
                break;
        }

        for (int i = 0; i < s.parts; i++) {
            int j = s.sprnum[i];
            Sprite spr = boardService.getSprite(j);
            if (spr != null) {
                game.pInt.setsprinterpolate(j, spr);
                spr.setZ(s.startz[i] + s.floorpos - s.floorz[0]);
                for (int n = 0; n < s.doors; n++) {
                    if (j == s.door[n]) {
                        spr.setZ(spr.getZ() - s.doorpos);
                    }
                }
            }
        }
    }

    public static void tekpreptags() {
        totalmapsndfx = 0;
        secnt = 0;

        for (int j = 0; j < MAXSECTORS; j++) {
            sectoreffect[j].reset();
            sexref[j] = 0;
            elevator[j].reset();
        }
        for (int j = 0; j < MAXDOORS; j++) {
            doortype[j].reset();
        }
        numfloordoors = 0;
        Arrays.fill(fdxref, -1);

        for (int j = 0; j < MAXFLOORDOORS; j++) {
            floordoor[j].reset();
        }
        for (int j = 0; j < MAXSPRITEELEVS; j++) {
            spriteelev[j].reset();
        }
        for (int j = 0; j < MAXMAPSOUNDFX; j++) {
            mapsndfx[j].reset();
        }
        numvehicles = 0;
        for (int j = 0; j < MAXSECTORVEHICLES; j++) {
            sectorvehicle[j].reset();
        }

        numdoors = 0;
        for (int j = 0; j < boardService.getSectorCount(); j++) {
            Sector sec = boardService.getSector(j);
            if (sec == null) {
                continue;
            }

            final int startwall = sec.getWallptr();
            final int endwall = startwall + sec.getWallnum() - 1;

            if (sec.getCeilingpal() != 0) {
                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    wal.setPal(sec.getCeilingpal());
                }
            }

            switch (sec.getLotag()) {
                case DOORUPTAG:
                case DOORDOWNTAG:
                case DOORSPLITHOR:
                case DOORSPLITVER:
                case PLATFORMELEVTAG:
                case PLATFORMDELAYTAG:
                case BOXELEVTAG:
                case BOXDELAYTAG:
                case PLATFORMDROPTAG: {
                    if (sec.getLotag() == BOXDELAYTAG || sec.getLotag() == PLATFORMDELAYTAG) {
                        int k = engine.nextsectorneighborz(j, sec.getFloorz(), 1, 1);
                        elevator[j].hilevel = sec.getFloorz();
                        Sector seck = boardService.getSector(k);
                        if (seck != null) {
                            elevator[j].lolevel = seck.getFloorz();
                        }
                    }
                    int n = numdoors++;
                    if (numdoors >= MAXDOORS) {
                        break;
                    }
                    switch (sec.getLotag()) {
                        case DOORUPTAG:
                        case DOORDOWNTAG:
                        case DOORSPLITHOR:
                            doortype[n].step = DOORSPEED;
                            break;
                        case DOORSPLITVER:
                            doortype[n].step = 4;
                            break;
                        case PLATFORMELEVTAG:
                        case PLATFORMDELAYTAG:
                        case BOXELEVTAG:
                        case BOXDELAYTAG:
                        case PLATFORMDROPTAG:
                            doortype[n].step = ELEVSPEED;
                            break;
                    }
                    doortype[n].centx = doortype[n].centy = 0;
                    int k = 0;
                    for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        final int i = wn.getIndex();
                        if (wal.getLotag() == 6 && k < 8) {
                            doortype[n].walls[k] = (short) i;
                            doortype[n].walls[k + 1] = (short) (i + 1);
                            doortype[n].walls[k + 2] = (short) (i + 2);
                            doortype[n].walls[k + 3] = (short) (i - 1);
                            Wall wal2 = boardService.getWall(doortype[n].walls[k]);
                            if (wal2 != null) {
                                int dax = wal2.getX();
                                Wall wal3  = boardService.getWall(doortype[n].walls[k + 3]);
                                Wall wal4 = boardService.getWall(i + 2);
                                if (wal3 != null && wal4 != null) {
                                    if (wal3.getX() == dax) {
                                        int day = wal4.getY();
                                        doortype[n].points[k / 2] = day;
                                        day = wal4.getWall2().getY();
                                        doortype[n].points[(k / 2) + 1] = day;
                                    } else {
                                        dax = wal4.getX();
                                        doortype[n].points[k / 2] = dax;
                                        dax = wal4.getWall2().getX();
                                        doortype[n].points[(k / 2) + 1] = dax;
                                    }
                                }
                            }
                            k += 4;
                        }
                        doortype[n].centx += wal.getX();
                        doortype[n].centy += wal.getY();
                    }
                    doortype[n].centx /= (endwall - startwall + 1);
                    doortype[n].centy /= (endwall - startwall + 1);
                    doortype[n].centz = (sec.getCeilingz() + sec.getFloorz()) / 2;
                    doortype[n].type = sec.getLotag();
                    doortype[n].sector = j;
                    doorxref[j] = n;
                    break;
                }
                case DOORFLOORTAG: {
                    int k = fdxref[j] = numfloordoors++;
                    floordoor[k].state = DOORCLOSED;
                    floordoor[k].sectnum = j;

                    int w1 = -1, w2 = -1, w3 = -1, w4 = -1;
                    for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        if (wal.getLotag() == 6) {
                            if (floordoor[k].wall1 == 0) {
                                w1 = floordoor[k].wall1 = (short) wn.getIndex();
                                w2 = wal.getPoint2();
                            } else {
                                w3 = floordoor[k].wall2 = (short) wn.getIndex();
                                w4 = wal.getPoint2();
                            }
                        }
                    }

                    Wall wal1 = boardService.getWall(w1);
                    Wall wal2 = boardService.getWall(w2);
                    Wall wal3 = boardService.getWall(w3);
                    Wall wal4 = boardService.getWall(w4);
                    if (wal1 == null || wal2 == null || wal3 == null || wal4 == null) {
                        continue;
                    }

                    int x1 = wal1.getX(); // close the doors all the way
                    int y1 = wal1.getY();
                    int x2 = wal4.getX();
                    int y2 = wal4.getY();

                    engine.dragpoint(w1, (x1 + x2) / 2, (y1 + y2) / 2);
                    if (x1 != x2) {
                        if (x1 < x2) {
                            engine.dragpoint(w4, (x1 + x2) / 2 + 1, (y1 + y2) / 2);
                            floordoor[k].dir = 3;
                        } else {
                            engine.dragpoint(w4, (x1 + x2) / 2 - 1, (y1 + y2) / 2);
                            floordoor[k].dir = 1;
                        }
                        x1 = wal1.getX();
                        Wall nextWal = boardService.getWall(wal1.getNextwall());
                        if (nextWal != null) {
                            floordoor[k].dist1 = abs(x1 - nextWal.getWall2().getWall2().getX()) - 50;
                        }
                    } else if (y1 != y2) {
                        if (y1 < y2) {
                            engine.dragpoint(w4, (x1 + x2) / 2, (y1 + y2) / 2 + 1);
                            floordoor[k].dir = 0;
                        } else {
                            engine.dragpoint(w4, (x1 + x2) / 2, (y1 + y2) / 2 - 1);
                            floordoor[k].dir = 2;
                        }
                        y1 = wal1.getY();
                        Wall nextWal = boardService.getWall(wal1.getNextwall());
                        if (nextWal != null) {
                            floordoor[k].dist1 = abs(y1 - nextWal.getWall2().getWall2().getY()) - 50;
                        }
                    }
                    x1 = wal2.getX();
                    y1 = wal2.getY();
                    x2 = wal3.getX();
                    y2 = wal3.getY();
                    engine.dragpoint(w2, (x1 + x2) / 2, (y1 + y2) / 2);
                    if (x1 != x2) {
                        if (x1 < x2) {
                            engine.dragpoint(w3, (x1 + x2) / 2 + 1, (y1 + y2) / 2);
                        } else {
                            engine.dragpoint(w3, (x1 + x2) / 2 - 1, (y1 + y2) / 2);
                        }
                        x1 = wal2.getX();
                        Wall nextWal = boardService.getWall(wal1.getNextwall());
                        if (nextWal != null) {
                            floordoor[k].dist2 = abs(x1 - nextWal.getWall2().getWall2().getX()) - 50;
                        }
                    } else if (y1 != y2) {
                        if (y1 < y2) {
                            engine.dragpoint(w3, (x1 + x2) / 2, (y1 + y2) / 2 + 1);
                        } else {
                            engine.dragpoint(w3, (x1 + x2) / 2, (y1 + y2) / 2 - 1);
                        }

                        y1 = wal2.getY();
                        Wall nextWal = boardService.getWall(wal1.getNextwall());
                        if (nextWal != null) {
                            floordoor[k].dist2 = abs(y1 - nextWal.getWall2().getWall2().getY()) - 50;
                        }
                    }
                    break;
                }
                default:
                    break;
            }

            if (sec.getHitag() >= SECTORVEHICLETAG && sec.getHitag() < SECTORVEHICLETAG + MAXSECTORVEHICLES) {
                int k = sec.getHitag() - SECTORVEHICLETAG;
                if (sectorvehicle[k].pivotx == 0 && sectorvehicle[k].pivoty == 0) {
                    numvehicles++;
                    for (int i = 0; i < MAXSPRITES; i++) {
                        Sprite spr = boardService.getSprite(i);
                        if (spr != null && spr.getLotag() == sec.getHitag() && spr.getHitag() == sec.getHitag()) {
                            sectorvehicle[k].pivotx = spr.getX();
                            sectorvehicle[k].pivoty = spr.getY();
                            jsdeletesprite(i);
                            break;
                        }
                    }
                }
                int x1 = sectorvehicle[k].pivotx;
                int y1 = sectorvehicle[k].pivoty;
                int n = sectorvehicle[k].numpoints;

                int killwcnt = 0;
                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    if (wal.getLotag() == 6) {
                        sectorvehicle[k].killw[killwcnt++] = (short) wn.getIndex();
                    }
                    sectorvehicle[k].point[n] = (short) wn.getIndex();
                    sectorvehicle[k].pointx[n] = x1 - wal.getX();
                    sectorvehicle[k].pointy[n] = y1 - wal.getY();
                    n++;
                    if (n >= MAXVEHICLEPOINTS) {
                        throw new AssertException("tekprepareboard: vehicle #" + sec.getHitag() + " has too many points");
                    }
                }
                sectorvehicle[k].numpoints = (short) n;
                n = sectorvehicle[k].numsectors++;
                sectorvehicle[k].sector[n] = (short) j;
                sectorvehicle[k].soundindex = -1;
            }
        }

        sprelevcnt = 0;
        for (int i = 0; i < MAXSPRITES; i++) {
            Sprite spr = boardService.getSprite(i);
            if (spr != null && spr.getStatnum() < MAXSTATUS) {
                switch (spr.getPicnum()) {
                    case SNDFX_SECTOR:
                    case SNDFX_AMBIENT:
                    case SNDFX_TOGGLED:
                        if (totalmapsndfx == MAXMAPSOUNDFX) {
                            jsdeletesprite(i);
                            break;
                        }
                        mapsndfx[totalmapsndfx].x = spr.getX();
                        mapsndfx[totalmapsndfx].y = spr.getY();
                        mapsndfx[totalmapsndfx].sector = spr.getSectnum();
                        mapsndfx[totalmapsndfx].snum = spr.getLotag();
                        mapsndfx[totalmapsndfx].id = -1;
                        if (mapsndfx[totalmapsndfx].snum > TOTALSOUNDS) {
                            mapsndfx[totalmapsndfx].snum = (TOTALSOUNDS - 1);
                        }
                        mapsndfx[totalmapsndfx].loops = spr.getHitag() - 1;
                        switch (spr.getPicnum()) {
                            case SNDFX_AMBIENT:
                                mapsndfx[totalmapsndfx].type = MAP_SFX_AMBIENT;
                                break;
                            case SNDFX_SECTOR:
                                mapsndfx[totalmapsndfx].type = MAP_SFX_SECTOR;
                                break;
                            case SNDFX_TOGGLED:
                                mapsndfx[totalmapsndfx].type = MAP_SFX_TOGGLED;
                                break;
                        }
                        totalmapsndfx++;
                        jsdeletesprite(i);
                        break;
                    case SECTOREFFECT:
                        if (spr.getLotag() < 1000) {
                            continue;
                        }
                        final int s = spr.getSectnum();
                        final Sector sprSec = boardService.getSector(s);
                        if (sprSec == null) {
                            continue;
                        }

                        if (sectoreffect[s].sectorflags == 0) {
                            sexref[secnt++] = spr.getSectnum();
                            if (secnt == sexref.length) {
                                throw new AssertException("setupboard: Sector Effector limit exceeded");
                            }
                            sectoreffect[s].warpto = -1;
                        }

                        int effect = flags32[spr.getLotag() - 1000];
                        sectoreffect[s].sectorflags |= effect;
                        if ((effect & flags32[SOUNDON]) != 0) {
                            // match mapsndfx with same hitag
                            sectoreffect[s].hi = spr.getHitag();
                        } else if ((effect & flags32[SOUNDOFF]) != 0) {
                            // match mapsndfx with same hitag
                            sectoreffect[s].hi = spr.getHitag();
                        } else if ((effect & flags32[PULSELIGHT]) != 0 || (effect & flags32[FLICKERLIGHT]) != 0
                                || (effect & flags32[FLICKERDELAY]) != 0 || (effect & flags32[BLINKDELAY]) != 0
                                || (effect & flags32[STEADYLIGHT]) != 0) {
                            sectoreffect[s].lo = sprSec.getFloorshade();
                            sectoreffect[s].hi = sprSec.getCeilingshade();
                            sectoreffect[s].animate = 1;
                            if (spr.getHitag() != 0) {
                                sectoreffect[s].triggerable = spr.getHitag();
                            } else {
                                sectoreffect[s].triggerable = 0;
                            }
                            for (ListNode<Wall> wn = sprSec.getWallNode(); wn != null; wn = wn.getNext()) {
                                Wall wal = wn.get();
                                wal.setShade(sprSec.getFloorshade());
                            }
                            sprSec.setCeilingshade(sprSec.getFloorshade());
                        } else if ((effect & flags32[DELAYEFFECT]) != 0) {
                            sectoreffect[s].delay = spr.getHitag();
                            sectoreffect[s].delayreset = sectoreffect[s].delay;
                        }
                        if ((effect & flags32[WPANNING]) != 0) {
                            int angle = sectoreffect[s].ang = spr.getAng();
                            sectoreffect[s].sin = EngineUtils.sin(((angle + 2048) & 2047));
                            sectoreffect[s].cos = EngineUtils.sin(((angle + 2560) & 2047));
                            for (ListNode<Wall> wn = sprSec.getWallNode(); wn != null; wn = wn.getNext()) {
                                Wall wal = wn.get();
                                if (wal.getLotag() == 0) {
                                    wal.setLotag(spr.getLotag());
                                }
                            }
                        }
                        if ((effect & flags32[FPANNING]) != 0 || (effect & flags32[CPANNING]) != 0) {
                            int angle = sectoreffect[s].ang = spr.getAng();
                            sectoreffect[s].sin = EngineUtils.sin(((angle + 2048) & 2047));
                            sectoreffect[s].cos = EngineUtils.sin(((angle + 2560) & 2047));
                        }
                        if ((effect & flags32[WARPSECTOR]) != 0) {
                            for (int j = 0; j < MAXSECTORS; j++) {
                                Sector sec = boardService.getSector(j);
                                if (sec != null && sec.getHitag() == spr.getHitag()) {
                                    sectoreffect[s].warpto = (short) j;
                                    break;
                                }
                            }
                        }
                        if ((effect & flags32[KILLSECTOR]) != 0) {
                            if (spr.getHitag() == 0) {
                                sectoreffect[s].damage = 9999;
                            } else {
                                sectoreffect[s].damage = spr.getHitag();
                            }
                        }
                        if ((effect & flags32[DOORSPEEDEFFECT]) != 0) {
                            doortype[doorxref[spr.getSectnum()]].step = spr.getHitag();
                        }
                        // jeff added 9-20
                        if ((effect & flags32[DOORSUBTYPE]) != 0) {
                            doortype[doorxref[spr.getSectnum()]].subtype = spr.getHitag();
                        }
                        jsdeletesprite(i);
                        break;
                    default:
                        if (spr.getLotag() >= SECTORVEHICLETAG && spr.getLotag() < SECTORVEHICLETAG + MAXSECTORVEHICLES) {
                            int k = spr.getLotag() - SECTORVEHICLETAG;
                            int n = spr.getHitag();

                            if (spr.getPicnum() != SPEEDPIC) {
                                sectorvehicle[k].trackx[n] = spr.getX();
                                sectorvehicle[k].tracky[n] = spr.getY();
                            }

                            if (spr.getPicnum() == STOPPIC) {
                                sectorvehicle[k].stop[n] = 1;
                                sectorvehicle[k].waitdelay = CLKIPS * 8;
                            } else {
                                if (spr.getPicnum() != SPEEDPIC) {
                                    sectorvehicle[k].stop[n] = 0;
                                }
                            }
                            if (spr.getPicnum() == SPEEDPIC) {
                                if (sectorvehicle[k].speedto == 0 && spr.getHitag() > 0) {
                                    sectorvehicle[k].speedto = spr.getHitag();
                                    sectorvehicle[k].movespeed = sectorvehicle[k].speedto;
                                }
                                jsdeletesprite(i);
                                break;
                            }
                            sectorvehicle[k].tracknum++;
                            jsdeletesprite(i);
                        }
                        if ((spr.getLotag() < 2000) && (spr.getLotag() >= SPRITEELEVATORTAG)) {
                            int j = (spr.getLotag() - SPRITEELEVATORTAG);
                            if (spr.getHitag() >= 100) {
                                int k = (spr.getHitag() - 100) + 1;
                                if (k >= MAXELEVFLOORS) {
                                    throw new AssertException(
                                            "setupboard: Only " + MAXELEVFLOORS + " levels allowed for sprite elevators");
                                }
                                spriteelev[j].floorz[k] = spr.getZ();
                                spriteelev[j].floors++;
                            } else {
                                int k = spriteelev[j].parts;
                                spriteelev[j].sprnum[k] = i;
                                if (spr.getHitag() == 6 || spr.getHitag() == 7) {
                                    if (spriteelev[j].floorpos == 0) {
                                        sprelevcnt++;
                                    }
                                    spriteelev[j].door[spriteelev[j].doors] = i;
                                    spriteelev[j].floorz[0] = spr.getZ();
                                    spriteelev[j].floorpos = spr.getZ();
                                    spriteelev[j].doors++;
                                }
                                spriteelev[j].startz[k] = spr.getZ();
                                spriteelev[j].parts++;
                            }
                        }
                        break;
                }
            }

        }

        numanimates = 0;
        for (int j = 0; j < MAXANIMPICS; j++) {
            animpic[j].set(0);
        }
        numdelayfuncs = 0;
        for (int j = 0; j < MAXDELAYFUNCTIONS; j++) {
            delayfunc[j].set(0);
        }
    }

    public static void movedoors(int d) {
        Doortype door = doortype[d];
        final int s = door.sector;
        Sector sec = boardService.getSector(s);
        if (sec == null) {
            return;
        }

        switch (door.state) {
            case D_NOTHING:
                break;

            case D_WAITING:
                int stayopen = 0;
                for (int i = 0; i < secnt; i++) {
                    int sx = sexref[i];
                    if (sx == door.sector) {
                        if (((sectoreffect[sx].sectorflags) & flags32[QUICKCLOSE]) != 0) {
                            if (mission == 7) {
                                stayopen = 1;
                            } else {
                                door.delay = 0;
                            }
                        }
                    }
                }
                if (stayopen == 0) {
                    door.delay -= TICSPERFRAME;
                }
                if (door.delay <= 0) {
                    door.delay = 0;
                    if (door.type < PLATFORMELEVTAG) {
                        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                            if (gPlayer[i].cursectnum == s) {
                                door.delay = DOORDELAY;
                                break;
                            }
                        }
                    }
                    if (door.delay == 0) {
                        door.state = D_CLOSEDOOR;
                    }
                }
                break;

            case D_OPENDOOR:
                switch (door.type) {
                    case DOORUPTAG: {
                        switch (door.subtype) {
                            case DST_BAYDOOR:
                                playsound(S_BAYDOOR_OPEN, door.centx, door.centy, 0, ST_UPDATE);
                                if (baydoorloop == -1) {
                                    baydoorloop = playsound(S_BAYDOORLOOP, door.centx, door.centy, 20, ST_UNIQUE);
                                }
                                break;
                            case DST_HYDRAULICDOOR:
                                playsound(S_AIRDOOR_OPEN, door.centx, door.centy, 0, ST_UPDATE);
                                playsound(S_AIRDOOR, door.centx, door.centy, 0, ST_UPDATE);
                                break;

                            case DST_ELEVATORDOOR:
                                playsound(S_ELEVATOR_DOOR, door.centx, door.centy, 0, ST_UPDATE);
                                break;

                            case DST_MATRIXDOOR1:
                                playsound(S_MATRIX1, door.centx, door.centy, 0, ST_UPDATE);
                                break;

                            case DST_MATRIXDOOR2:
                                playsound(S_MATRIX2, door.centx, door.centy, 0, ST_UPDATE);
                                break;
                            case DST_MATRIXDOOR3:
                                playsound(S_MATRIX3, door.centx, door.centy, 0, ST_UPDATE);
                                break;
                            case DST_MATRIXDOOR4:
                                playsound(S_MATRIX4, door.centx, door.centy, 0, ST_UPDATE);
                                break;

                            default:
                                if (mission == 7) {
                                    playsound(S_MATRIXDOOR2, door.centx, door.centy, 0, ST_UPDATE);
                                } else {
                                    playsound(S_UPDOWNDR2_OP, door.centx, door.centy, 0, ST_UPDATE);
                                }
                                break;
                        }
                        Sector goalSec = boardService.getSector(engine.nextsectorneighborz(s, sec.getFloorz(), -1, -1));
                        if (goalSec != null) {
                            door.goalz[0] = goalSec.getCeilingz();
                        }
                        break;
                    }
                    case DOORDOWNTAG: {
                        playsound(S_BIGSWINGOP, door.centx, door.centy, 0, ST_UPDATE);
                        Sector goalSec = boardService.getSector(engine.nextsectorneighborz(s, sec.getCeilingz(), 1, 1));
                        if (goalSec != null) {
                            door.goalz[0] = goalSec.getFloorz();
                        }
                        break;}

                    case PLATFORMDROPTAG: {
                        Sector goalSec = boardService.getSector(engine.nextsectorneighborz(s, sec.getCeilingz(), 1, 1));
                        if (goalSec != null) {
                            door.goalz[0] = goalSec.getFloorz();
                        }
                        break;
                    }
                    case DOORSPLITHOR:
                        if (mission == 7) {
                            playsound(S_MATRIXDOOR1, door.centx, door.centy, 0, ST_UPDATE);
                        } else {
                            playsound(S_WH_7, door.centx, door.centy, 0, ST_UPDATE);
                        }
                        int s1 = engine.nextsectorneighborz(s, sec.getCeilingz(), -1, -1);
                        int s2 = engine.nextsectorneighborz(s, sec.getFloorz(), 1, 1);
                        Sector sec1 = boardService.getSector(s1);
                        Sector sec2 = boardService.getSector(s2);
                        if (sec1 == null || sec2 == null) {
                            break;
                        }

                        door.goalz[0] = sec1.getCeilingz();
                        door.goalz[2] = sec2.getFloorz();
                        break;
                    case DOORSPLITVER:
                        playsound(S_SIDEDOOR1, door.centx, door.centy, 0, ST_UPDATE);
                        door.goalz[0] = door.points[0];
                        door.goalz[1] = door.points[1];
                        door.goalz[2] = door.points[2];
                        door.goalz[3] = door.points[3];
                        break;
                    case BOXDELAYTAG:
                    case PLATFORMDELAYTAG:
                        playsound(S_PLATFORMSTART, door.centx, door.centy, 0, ST_UPDATE);
                        if (loopinsound == -1) {
                            loopinsound = playsound(S_PLATFORMLOOP, door.centx, door.centy, 20, ST_UNIQUE);
                        }
                        door.goalz[0] = elevator[s].lolevel;
                        break;
                    default:
                        break;
                }
                door.state = D_OPENING;
                break;

            case D_CLOSEDOOR:
                switch (door.type) {
                    case DOORUPTAG: {
                        switch (door.subtype) {
                            case DST_BAYDOOR:
                                playsound(S_BAYDOOR_OPEN, door.centx, door.centy, 0, ST_UPDATE);
                                if (baydoorloop == -1) {
                                    baydoorloop = playsound(S_BAYDOORLOOP, door.centx, door.centy, 20, ST_UNIQUE);
                                }
                                break;

                            case DST_HYDRAULICDOOR:
                                playsound(S_AIRDOOR_OPEN, door.centx, door.centy, 0, ST_UPDATE);
                                playsound(S_AIRDOOR, door.centx, door.centy, 0, ST_UPDATE);
                                break;

                            case DST_ELEVATORDOOR:
                                playsound(S_ELEVATOR_DOOR, door.centx, door.centy, 0, ST_UPDATE);
                                break;

                            case DST_MATRIXDOOR1:
                                playsound(S_MATRIX1, door.centx, door.centy, 0, ST_UPDATE);
                                break;

                            case DST_MATRIXDOOR2:
                                playsound(S_MATRIX2, door.centx, door.centy, 0, ST_UPDATE);
                                break;
                            case DST_MATRIXDOOR3:
                                playsound(S_MATRIX3, door.centx, door.centy, 0, ST_UPDATE);
                                break;
                            case DST_MATRIXDOOR4:
                                playsound(S_MATRIX4, door.centx, door.centy, 0, ST_UPDATE);
                                break;

                            default:
                                if (mission == 7) {
                                    playsound(S_MATRIXDOOR2, door.centx, door.centy, 0, ST_UPDATE);
                                } else {
                                    playsound(S_UPDOWNDR2_CL, door.centx, door.centy, 0, ST_UPDATE);
                                }
                                break;
                        }
                        Sector goalSec = boardService.getSector(engine.nextsectorneighborz(s, sec.getCeilingz(), 1, 1));
                        if (goalSec != null) {
                            door.goalz[0] = goalSec.getFloorz();
                        }
                        break;
                    }
                    case DOORDOWNTAG:
                        playsound(S_BIGSWINGOP, door.centx, door.centy, 0, ST_UPDATE);
                        door.goalz[0] = sec.getCeilingz();
                        break;
                    case DOORSPLITHOR:
                        if (mission == 7) {
                            playsound(S_MATRIXDOOR1, door.centx, door.centy, 0, ST_UPDATE);
                        } else {
                            playsound(S_WH_7, door.centx, door.centy, 0, ST_UPDATE);
                        }
                        door.goalz[0] = door.centz;
                        door.goalz[2] = door.centz;
                        break;
                    case DOORSPLITVER:
                        playsound(S_SIDEDOOR2, door.centx, door.centy, 0, ST_UPDATE);
                        Wall w1 = boardService.getWall(door.walls[0]);
                        Wall w4 = boardService.getWall(door.walls[3]);
                        if (w1 == null || w4 == null) {
                            break;
                        }

                        if (w1.getX() == w4.getX()) {
                            door.goalz[0] = door.centy;
                            door.goalz[2] = door.centy;
                        } else {
                            door.goalz[0] = door.centx;
                            door.goalz[2] = door.centx;
                        }
                        door.goalz[1] = door.points[0];
                        door.goalz[3] = door.points[2];
                        break;
                    case BOXDELAYTAG:
                    case PLATFORMDELAYTAG:
                        playsound(S_PLATFORMSTART, door.centx, door.centy, 0, ST_UPDATE);
                        if (loopinsound == -1) {
                            loopinsound = playsound(S_PLATFORMLOOP, door.centx, door.centy, 20, ST_UNIQUE);
                        }
                        door.goalz[0] = elevator[s].hilevel;
                        break;
                    default:
                        break;
                }
                door.state = D_CLOSING;
                int hitag = sec.getHitag();
                if (hitag > 0) {
                    for (int i = 0; i < MAXSPRITES; i++) {
                        Sprite spr = boardService.getSprite(i);
                        if (spr != null && spr.getHitag() == hitag) {
                            switch (spr.getPicnum()) {
                                case SWITCH2ON:
                                    spr.setPicnum(SWITCH2OFF);
                                    break;
                                case SWITCH3ON:
                                    spr.setPicnum(SWITCH3OFF);
                                    break;
                            }
                        }
                    }
                    for (int i = 0; i < boardService.getWallCount(); i++) {
                        Wall wallt = boardService.getWall(i);
                        if (wallt != null && wallt.getHitag() == hitag) {
                            switch (wallt.getPicnum()) {
                                case SWITCH2ON:
                                    wallt.setPicnum(SWITCH2OFF);
                                    break;
                                case SWITCH3ON:
                                    wallt.setPicnum(SWITCH3OFF);
                                    break;
                            }
                        }
                    }
                }
                break;

            case D_OPENING:
                if (door.type == DOORUPTAG) {
                    game.pInt.setceilinterpolate(s, sec);
                } else {
                    game.pInt.setfloorinterpolate(s, sec);
                }
                switch (door.type) {
                    case DOORUPTAG:
                    case DOORDOWNTAG:
                    case PLATFORMDROPTAG: {
                        int z;
                        if (door.type == DOORUPTAG) {
                            z = sec.getCeilingz();
                        } else {
                            z = sec.getFloorz();
                        }

                        z = stepdoor(z, door.goalz[0], door, D_OPENSOUND);
                        if (door.type == DOORUPTAG) {
                            sec.setCeilingz(z);
                        } else {
                            sec.setFloorz(z);
                        }
                        break;
                    }
                    case DOORSPLITHOR: {
                        int z = sec.getCeilingz();
                        z = stepdoor(z, door.goalz[0], door, D_OPENSOUND);
                        sec.setCeilingz(z);
                        z = sec.getFloorz();
                        z = stepdoor(z, door.goalz[2], door, D_OPENSOUND);
                        sec.setFloorz(z);
                        break;
                    }
                    case DOORSPLITVER:
                        Wall w1 = boardService.getWall(door.walls[0]);
                        Wall w4 = boardService.getWall(door.walls[3]);
                        if (w1 == null || w4 == null) {
                            break;
                        }

                        if (w1.getX() == w4.getX()) {
                            for (int i = 0; i < 8; i++) {
                                int j = door.walls[i];
                                Wall w = boardService.getWall(j);
                                if (w != null) {
                                    int z = stepdoor(w.getY(), door.goalz[i >> 1], door, D_OPENSOUND);
                                    engine.dragpoint(j, w.getX(), z);
                                }
                            }
                        } else {
                            for (int i = 0; i < 8; i++) {
                                int j = door.walls[i];
                                Wall w = boardService.getWall(j);
                                if (w != null) {
                                    int z = stepdoor(w.getX(), door.goalz[i >> 1], door, D_OPENSOUND);
                                    engine.dragpoint(j, z, w.getY());
                                }
                            }
                        }
                        break;
                    case BOXELEVTAG:
                    case PLATFORMELEVTAG:
                    case BOXDELAYTAG:
                    case PLATFORMDELAYTAG:
                        int size = sec.getCeilingz() - sec.getFloorz();
                        int z = stepdoor(sec.getFloorz(), door.goalz[0], door, D_OPENSOUND);
                        sec.setFloorz(z);
                        if (door.type == BOXDELAYTAG || door.type == BOXELEVTAG) {
                            sec.setCeilingz(sec.getFloorz() + size);
                        }
                        break;
                    default:
                        break;
                }
                break;

            case D_CLOSING:
                if (door.type == DOORUPTAG) {
                    game.pInt.setceilinterpolate(s, sec);
                } else {
                    game.pInt.setfloorinterpolate(s, sec);
                }
                switch (door.type) {
                    case DOORUPTAG:
                    case DOORDOWNTAG: {
                        int z;
                        if (door.type == DOORUPTAG) {
                            z = sec.getCeilingz();
                        } else {
                            z = sec.getFloorz();
                        }
                        z = stepdoor(z, door.goalz[0], door, D_SHUTSOUND);
                        if (door.type == DOORUPTAG) {
                            sec.setCeilingz(z);
                        } else {
                            sec.setFloorz(z);
                        }
                        break;
                    }
                    case DOORSPLITHOR: {
                        int z = sec.getCeilingz();
                        z = stepdoor(z, door.goalz[0], door, D_SHUTSOUND);
                        sec.setCeilingz(z);
                        z = sec.getFloorz();
                        z = stepdoor(z, door.goalz[2], door, D_SHUTSOUND);
                        sec.setFloorz(z);
                        break;
                    }
                    case DOORSPLITVER:
                        ListNode<Sprite> node = boardService.getSectNode(s);
                        if (node != null) {
                            door.state = D_OPENDOOR;
                        }
                        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                            if (engine.inside(gPlayer[i].posx, gPlayer[i].posy, (short) s) != 0) {
                                door.state = D_OPENDOOR;
                            }
                        }
                        Wall w1 = boardService.getWall(door.walls[0]);
                        Wall w4 = boardService.getWall(door.walls[3]);
                        if (w1 == null || w4 == null) {
                            break;
                        }

                        if (w1.getX() == w4.getX()) {
                            for (int i = 0; i < 8; i++) {
                                int j = door.walls[i];
                                Wall w = boardService.getWall(j);
                                if (w != null) {
                                    int z = stepdoor(w.getY(), door.goalz[i >> 1], door, D_SHUTSOUND);
                                    engine.dragpoint(j, w.getX(), z);
                                }
                            }
                        } else {
                            for (int i = 0; i < 8; i++) {
                                int j = door.walls[i];
                                Wall w = boardService.getWall(j);
                                if (w != null) {
                                    int z = stepdoor(w.getX(), door.goalz[i >> 1], door, D_SHUTSOUND);
                                    engine.dragpoint(j, z, w.getY());
                                }
                            }
                        }
                        break;
                    case BOXDELAYTAG:
                    case PLATFORMDELAYTAG:
                        int size = sec.getCeilingz() - sec.getFloorz();
                        int z = stepdoor(sec.getFloorz(), door.goalz[0], door, D_SHUTSOUND);
                        sec.setFloorz(z);
                        if (door.type == BOXDELAYTAG) {
                            sec.setCeilingz(sec.getFloorz() + size);
                        }
                        break;
                    default:
                        break;
                }
                break;

            case D_OPENSOUND:
                switch (door.type) {
                    case DOORUPTAG:
                        switch (door.subtype) {
                            case DST_BAYDOOR:
                                playsound(S_BAYDOOR_CLOSE, door.centx, door.centy, 0, ST_UPDATE);
                                if (baydoorloop >= 0) {
                                    stopsound(baydoorloop);
                                    baydoorloop = -1;
                                }
                                break;
                            case DST_HYDRAULICDOOR:
                                playsound(S_AIRDOOR_CLOSE, door.centx, door.centy, 0, ST_UPDATE);
                                break;

                            case DST_ELEVATORDOOR:
                            case DST_MATRIXDOOR1:
                            case DST_MATRIXDOOR2:
                            case DST_MATRIXDOOR3:
                            case DST_MATRIXDOOR4:
                                break;
                            default:
                                if (mission != 7) {
                                    playsound(S_DOORKLUNK, door.centx, door.centy, 0, ST_UPDATE);
                                }
                                break;
                        }
                        door.state = D_WAITING;
                        showsect2d(door.sector, door.goalz[0]);
                        break;

                    case DOORDOWNTAG:
                        playsound(S_WH_6, door.centx, door.centy, 0, ST_UPDATE);
                        showsect2dtoggle(door.sector, 0);
                        door.state = D_WAITING;
                        break;

                    case BOXELEVTAG:
                    case PLATFORMELEVTAG:
                    case PLATFORMDROPTAG:
                        door.state = D_WAITING;
                        showsect2d(door.sector, door.goalz[0]);
                        break;
                    case PLATFORMDELAYTAG:
                    default:
                        if (door.type == BOXDELAYTAG || door.type == PLATFORMDELAYTAG) {
                            playsound(S_PLATFORMSTOP, door.centx, door.centy, 0, ST_UPDATE);
                            if (loopinsound >= 0) {
                                stopsound(loopinsound);
                                loopinsound = -1;
                            }
                            showsect2d(door.sector, door.goalz[0]);
                        } else {
                            showsect2dtoggle(door.sector, 0);
                        }
                        door.state = D_WAITING;
                        break;
                }
                break;

            case D_SHUTSOUND:
                switch (door.type) {
                    case DOORUPTAG:
                        switch (door.subtype) {
                            case DST_BAYDOOR:
                                playsound(S_BAYDOOR_CLOSE, door.centx, door.centy, 0, ST_UPDATE);
                                if (baydoorloop >= 0) {
                                    stopsound(baydoorloop);
                                    baydoorloop = -1;
                                }
                                break;

                            case DST_HYDRAULICDOOR:
                                playsound(S_AIRDOOR_CLOSE, door.centx, door.centy, 0, ST_UPDATE);
                                break;

                            case DST_ELEVATORDOOR:
                            case DST_MATRIXDOOR1:
                            case DST_MATRIXDOOR2:
                            case DST_MATRIXDOOR3:
                            case DST_MATRIXDOOR4:
                                break;
                            default:
                                if (mission != 7) {
                                    playsound(S_DOORKLUNK, door.centx, door.centy, 0, ST_UPDATE);
                                }
                                break;
                        }
                        door.state = D_NOTHING;
                        showsect2d(door.sector, door.goalz[0]);
                        break;

                    case DOORDOWNTAG:
                        showsect2dtoggle(door.sector, 1);
                        playsound(S_WH_6, door.centx, door.centy, 0, ST_UPDATE);
                        break;

                    case BOXELEVTAG:
                    case PLATFORMELEVTAG:
                    case BOXDELAYTAG:
                    case PLATFORMDELAYTAG:
                        playsound(S_PLATFORMSTOP, door.centx, door.centy, 0, ST_UPDATE);
                        if (loopinsound >= 0) {
                            stopsound(loopinsound);
                            loopinsound = -1;
                        }
                        showsect2d(door.sector, door.goalz[0]);
                        break;
                    default:
                        showsect2dtoggle(door.sector, 1);
                        break;
                }
                door.state = D_NOTHING;
                break;
        }

    }

    public static void showwall2d(int w, int onoff) {
        if (onoff != 0) {
            show2dwall.setBit(w);
        } else {
            show2dwall.clearBit(w);
        }
    }

    public static void showsect2d(int s, int z) {
        Sector sec = boardService.getSector(s);
        if (sec == null) {
            return;
        }

        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall wal = wn.get();
            int i = wn.getIndex();
            if (wal.getNextwall() != -1) {
                Sector nextSec = boardService.getSector(wal.getNextsector());
                if (nextSec != null && nextSec.getFloorz() == z) {
                    showwall2d(i, 0);
                    showwall2d(wal.getNextwall(), 0);
                } else {
                    showwall2d(i, 1);
                    showwall2d(wal.getNextwall(), 1);
                }
            }
        }
    }

    public static void showsect2dtoggle(int s, int onoff) {
        Sector sec = boardService.getSector(s);
        if (sec == null) {
            return;
        }

        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall wal = wn.get();
            int i = wn.getIndex();
            if (wal.getNextwall() != -1) {
                showwall2d(i, onoff);
                showwall2d(wal.getNextwall(), onoff);
            }
        }
    }

    public static int testneighborsectors(int sect1, int sect2) {
        Sector sec1 = boardService.getSector(sect1);
        Sector sec2 = boardService.getSector(sect2);

        if (sec1 == null || sec2 == null) {
            return 0;
        }

        final int num1 = sec1.getWallnum();
        final int num2 = sec2.getWallnum();

        // traverse walls of sector with fewest walls (for speed)
        if (num1 < num2) {
            int startwall = sec1.getWallptr();
            for (int i = (num1 - 1); i >= 0; i--) {
                Wall w = boardService.getWall(i + startwall);
                if (w != null && w.getNextsector() == sect2) {
                    return (1);
                }
            }
        } else {
            int startwall = sec2.getWallptr();
            for (int i = (num2 - 1); i >= 0; i--) {
                Wall w = boardService.getWall(i + startwall);
                if (w != null && w.getNextsector() == sect1) {
                    return (1);
                }
            }
        }
        return (0);
    }

    public static void warpsprite(short spritenum) {
        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null) {
            return;
        }

        warp(spr.getX(), spr.getY(), spr.getZ(), spr.getAng(), spr.getSectnum());

        game.pInt.setsprinterpolate(spritenum, spr);
        spr.setX(warpx);
        spr.setY(warpy);
        spr.setZ(warpz);
        spr.setAng((short) warpang);

        engine.changespritesect(spritenum, warpsect);
    }

    public static void warp(int x, int y, int z, int daang, short dasector) {
        Sector warpSec = boardService.getSector(dasector);
        if (warpSec == null) {
            return;
        }

        warpx = x;
        warpy = y;
        warpz = z;
        warpang = daang;
        warpsect = dasector;

        for (int i = 0; i < warpsectorcnt; i++) {
            Sector sec = boardService.getSector(warpsectorlist[i]);
            if (sec != null && sec.getHitag() == warpSec.getHitag() && warpsectorlist[i] != warpsect) {
                warpsect = warpsectorlist[i];
                warpSec = sec;
                break;
            }
        }

//        for (i = 0; i < warpsectorcnt; i++) {
//            Sector sec;
//            if (warpsectorlist[i] == warpsect) {
//                j = warpSec.getHitag();
//
//                do {
//                    i++;
//                    if (i >= warpsectorcnt) {
//                        i = 0;
//                    }
//
//                    sec = boardService.getSector(warpsectorlist[i]);
//                    if (sec == null) {
//                        return;
//                    }
//                } while (sec.getHitag() != j);
//                warpsect = warpsectorlist[i];
//        warpSec = boardService.getSector(warpsect);
//                break;
//            }
//        }

        // find center of sector
        int dax = 0, day = 0;
        Wall w = null;
        int startwall = warpSec.getWallptr();
        int endwall = (startwall + warpSec.getWallnum() - 1);
        for (ListNode<Wall> wn = warpSec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall wal = wn.get();
            dax += wal.getX();
            day += wal.getY();
            if (wal.getNextsector() >= 0) {
                w = wal;
            }
        }

        warpx = dax / (endwall - startwall + 1);
        warpy = day / (endwall - startwall + 1);
        warpz = warpSec.getFloorz() - (42 << 8);
        warpsect = engine.updatesector(warpx, warpy, warpsect);
        if (w != null) {
            dax = ((w.getX() + w.getWall2().getX()) >> 1);
            day = ((w.getY() + w.getWall2().getY()) >> 1);
            warpang = EngineUtils.getAngle(dax - warpx, day - warpy);
        }
    }

    public static void operatesector(final int dasector) { // Door code
        Sector sec = boardService.getSector(dasector);
        if (sec == null) {
            return;
        }

        final int datag = sec.getLotag();
        // lights out / on
        if (datag == 33) {
            if (sec.getVisibility() >= 212) {
                sec.setVisibility(0);
            } else {
                sec.setVisibility((byte) 212);
            }
            return;
        }

        final int startwall = sec.getWallptr();
        final int endwall = startwall + sec.getWallnum() - 1;
        int centx = 0;
        int centy = 0;
        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall wal = wn.get();
            centx += wal.getX();
            centy += wal.getY();
        }
        centx /= (endwall - startwall + 1);
        centy /= (endwall - startwall + 1);

        // kens swinging door
        if (datag == 13) {
            for (int i = 0; i < swingcnt; i++) {
                if (swingsector[i] == dasector) {
                    if (swinganginc[i] == 0) {
                        if (swingang[i] == swingangclosed[i]) {
                            swinganginc[i] = swingangopendir[i];
                        } else {
                            swinganginc[i] = (short) -swingangopendir[i];
                        }
                    } else {
                        swinganginc[i] = (short) -swinganginc[i];
                    }
                }
            }
        }

        // kens true sideways double-sliding door
        if (datag == 16) {
            // get 2 closest line segments to center (dax, day)
            opwallfind[0] = -1;
            opwallfind[1] = -1;
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();
                if (wal.getLotag() == 6) {
                    if (opwallfind[0] == -1) {
                        opwallfind[0] = (short) wn.getIndex();
                    } else {
                        opwallfind[1] = (short) wn.getIndex();
                    }
                }
            }

            for (int j = 0; j < 2; j++) {
                Wall w = boardService.getWall(opwallfind[j]);
                if (w == null) {
                    continue;
                }

                if ((((w.getX() + w.getWall2().getX()) >> 1) == centx)
                        && (((w.getY() + w.getWall2().getY()) >> 1) == centy)) { // door was
                    // closed.
                    // Find
                    // what
                    // direction
                    // door
                    // should
                    // open
                    int i = opwallfind[j] - 1;
                    if (i < startwall) {
                        i = endwall;
                    }
                    Wall w2 = boardService.getWall(i);
                    if (w2 == null) {
                        continue;
                    }

                    int dax2 = w2.getX() - w.getX();
                    int day2 = w2.getY() - w.getY();
                    if (dax2 != 0) {
                        dax2 = w.getWall2().getWall2().getWall2().getX();
                        dax2 -= w.getWall2().getWall2().getX();
                        setanimation(opwallfind[j], w.getX() + dax2, 4, 0, WALLX);
                        setanimation(i, w2.getX() + dax2, 4, 0, WALLX);
                        setanimation(w.getPoint2(),
                                w.getWall2().getX() + dax2, 4, 0,
                                WALLX);
                        setanimation(w.getWall2().getPoint2(),
                                w.getWall2().getWall2().getX() + dax2, 4, 0, WALLX);
                    } else if (day2 != 0) {
                        day2 = w.getWall2().getWall2().getWall2().getY();
                        day2 -= w.getWall2().getWall2().getY();
                        setanimation(opwallfind[j], w.getY() + day2, 4, 0, WALLY);
                        setanimation(i, w2.getY() + day2, 4, 0, WALLY);
                        setanimation(w.getPoint2(), w.getWall2().getY() + day2, 4, 0,
                                WALLY);
                        setanimation(w.getWall2().getPoint2(),
                                w.getWall2().getWall2().getY() + day2, 4, 0, WALLY);
                    }
                } else { // door was not closed
                    int i = opwallfind[j] - 1;
                    if (i < startwall) {
                        i = endwall;
                    }
                    Wall w2 = boardService.getWall(i);
                    if (w2 == null) {
                        continue;
                    }

                    int dax2 = w2.getX() - w.getX();
                    int day2 = w2.getY() - w.getY();
                    if (dax2 != 0) {
                        setanimation(opwallfind[j], centx, 4, 0, WALLX);
                        setanimation(i, centx + dax2, 4, 0, WALLX);
                        setanimation(w.getPoint2(), centx, 4, 0, WALLX);
                        setanimation(w.getWall2().getPoint2(), centx + dax2, 4, 0, WALLX);
                    } else if (day2 != 0) {
                        setanimation(opwallfind[j], centy, 4, 0, WALLY);
                        setanimation(i, centy + day2, 4, 0, WALLY);
                        setanimation(w.getPoint2(), centy, 4, 0, WALLY);
                        setanimation(w.getWall2().getPoint2(), centy + day2, 4, 0, WALLY);
                    }
                }
            }
        }

        tekoperatesector(dasector);
    }

    public static void tekswitchtrigger(int snum, int sn) {
        Sprite spr = boardService.getSprite(sn);
        if (spr == null) {
            return;
        }

        int j = spr.getPicnum();
        switch (j) {
            case SWITCH2OFF:
                if (gPlayer[snum].invredcards == 0) {
                    showmessage("PASSAGE REQUIRES RED KEYCARD");
                    return;
                }
                break;
            case SWITCH4OFF:
                if (gPlayer[snum].invbluecards == 0) {
                    showmessage("PASSAGE REQUIRES BLUE KEYCARD");
                    return;
                }
                break;
        }

        switch (j) {
            case SWITCH2ON:
            case SWITCH2OFF:
            case SWITCH3ON:
            case SWITCH3OFF:
            case SWITCH4ON:
            case SWITCH4OFF:
                int dax = spr.getX();
                int day = spr.getY();
                playsound(S_KEYCARDBLIP, dax, day, 0, ST_UPDATE);
                break;
            default:
                break;
        }

        if (j == SWITCH2ON) {
            spr.setPicnum(SWITCH2OFF);
        }
        if (j == SWITCH2OFF) {
            spr.setPicnum(SWITCH2ON);
        }
        if (j == SWITCH3ON) {
            spr.setPicnum(SWITCH3OFF);
        }
        if (j == SWITCH3OFF) {
            spr.setPicnum(SWITCH3ON);
        }
        if (j == SWITCH4ON) {
            spr.setPicnum(SWITCH4OFF);
        }
        if (j == SWITCH4OFF) {
            spr.setPicnum(SWITCH4ON);
        }

        if (j == 3708) {
            spr.setPicnum(3709);
        }
        if (j == 3709) {
            spr.setPicnum(3708);
        }

        for (int i = 0; i < boardService.getSectorCount(); i++) {
            Sector sec = boardService.getSector(i);
            if (sec != null && sec.getHitag() == spr.getHitag()) {
                if (sec.getLotag() != 1) {
                    operatesector(i);
                }
            }
        }

        ListNode<Sprite> node = boardService.getStatNode(0), next;
        while (node != null) {
            next = node.getNext();
            if (node.get().getHitag() == spr.getHitag()) {
                operatesprite(node.getIndex());
            }
            node = next;
        }

    }

    public static void teknewsector(int p) {
        Sector plrSec = boardService.getSector(gPlayer[p].cursectnum);
        if (plrSec == null) {
            return;
        }

        Sectoreffect septr = sectoreffect[gPlayer[p].cursectnum];
        if ((plrSec.getLotag() == NEWMAPLOTAG) && game.nNetMode != NetMode.Multiplayer) {
            if (mUserFlag == UserFlag.None) {
                changemap(plrSec.getHitag());
                return;
            }
        }

        if (plrSec.getLotag() == 4) {
            playsound(S_SPLASH, gPlayer[p].posx, gPlayer[p].posy, 0, ST_UPDATE);
        }

        if (septr != null) {
            if (septr.warpto >= 0) {
                final int sn = gPlayer[p].playersprite;
                Sprite plrSpr = boardService.getSprite(sn);
                if (plrSpr == null) {
                    return;
                }

                tekwarp(plrSpr.getX(), plrSpr.getY(), plrSpr.getZ(), septr.warpto);
                game.pInt.setsprinterpolate(sn, plrSpr);

                plrSpr.setX(tekwarpx);
                plrSpr.setY(tekwarpy);
                plrSpr.setZ(tekwarpz);
                septr.warpto = (short) tekwarsect;

                engine.changespritesect(sn, septr.warpto);
                gPlayer[p].posx = plrSpr.getX();
                gPlayer[p].posy = plrSpr.getY();
                gPlayer[p].posz = plrSpr.getZ();
                gPlayer[p].ang = plrSpr.getAng();
                gPlayer[p].cursectnum = plrSpr.getSectnum();
                plrSpr.setZ(plrSpr.getZ() + (KENSPLAYERHEIGHT << 8));
                plrSec = boardService.getSector(gPlayer[p].cursectnum);
                if (plrSec == null) {
                    return;
                }
            }
        }

        for (int n = connecthead; n >= 0; n = connectpoint2[n]) {
            if ((plrSec.getLotag() == 1) || (plrSec.getLotag() == 2)) {
                for (int i = 0; i < boardService.getSectorCount(); i++) {
                    Sector sec = boardService.getSector(i);
                    if (sec != null && sec.getHitag() == plrSec.getHitag()) {
                        if ((sec.getLotag() != 1) && (sec.getLotag() != 2)) {
                            operatesector(i);
                        }
                    }
                }

                ListNode<Sprite> node = boardService.getStatNode(0), next;
                while (node != null) {
                    next = node.getNext();
                    if (node.get().getHitag() == plrSec.getHitag()) {
                        operatesprite(node.getIndex());
                    }
                    node = next;
                }
            }
            if (TEKDEMO && plrSec.getLotag() == 10) {
                int newsect = plrSec.getHitag();
                Sector newsec = boardService.getSector(newsect);
                if (newsec == null || newsec.getWallNode() == null) {
                    return;
                }

                int x = newsec.getWallNode().get().getX();
                int y = newsec.getWallNode().get().getY();
                final int sn = gPlayer[p].playersprite;
                Sprite plrSpr = boardService.getSprite(sn);
                if (plrSpr == null) {
                    return;
                }

                game.pInt.setsprinterpolate(sn, plrSpr);
                tekwarp(x, y, newsec.getFloorz(), newsect);
                plrSpr.setX(tekwarpx);
                plrSpr.setY(tekwarpy);
                plrSpr.setZ(tekwarpz);
                if (septr != null) {
                    septr.warpto = (short) tekwarsect;
                }

                engine.changespritesect(sn, newsect);
                gPlayer[p].posx = plrSpr.getX();
                gPlayer[p].posy = plrSpr.getY();
                gPlayer[p].posz = plrSpr.getZ();
                gPlayer[p].ang = plrSpr.getAng();
                gPlayer[p].cursectnum = plrSpr.getSectnum();

                plrSpr.setZ(plrSpr.getZ() + (KENSPLAYERHEIGHT << 8));
                plrSec = boardService.getSector(gPlayer[p].cursectnum);
                if (plrSec == null) {
                    return;
                }
            }
        }

        checkmapsndfx(p);
        sectortriggersprites(p);
    }

    public static void tekwarp(int x, int y, int z, int dasector) {
        Sector sec =  boardService.getSector(dasector);
        if (sec == null) {
            return;
        }

        tekwarpx = x;
        tekwarpy = y;
        tekwarpz = z;
        tekwarsect = dasector;

        // find center of sector
        int startwall = sec.getWallptr();
        int endwall = (startwall + sec.getWallnum() - 1);
        int dax = 0;
        int day = 0;
        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall wal = wn.get();
            dax += wal.getX();
            day += wal.getY();
        }
        tekwarpx = dax / (endwall - startwall + 1);
        tekwarpy = day / (endwall - startwall + 1);
        tekwarpz = sec.getFloorz() - (42 << 8);

        tekwarsect = engine.updatesector(tekwarpx, tekwarpy, tekwarsect);
    }

    public static int stepdoor(int z, int z2, Doortype door, int newstate) {
        if (z < z2) {
            z += (door.step * TICSPERFRAME);
            if (z >= z2) {
                door.delay = DOORDELAY;
                door.state = newstate;
                z = z2;
            }
        } else if (z > z2) {
            z -= (door.step * TICSPERFRAME);
            if (z <= z2) {
                door.delay = DOORDELAY;
                door.state = newstate;
                z = z2;
            }
        }
        return (z);
    }

    public static void movevehicles(int v) {
        Sectorvehicle vptr = sectorvehicle[v];
        if (vptr.soundindex == -1) {
            int i;
            for (i = 0; i < 32; i++) { // find mapno using names array
                if (boardfilename != null && boardfilename.getName().equalsIgnoreCase(mapnames[i])) {
                    break;
                }
            }

            switch (v) {
                case 0:
                    switch (i) {
                        case 4: // level1.map
                        case 17: // mid3.map
                            vptr.soundindex = playsound(S_TRAMBUSLOOP, vptr.pivotx, vptr.pivoty, -1, ST_VEHUPDATE);
                            break;
                        case 8: // city1.map
                        case 26: // ware2.map
                            vptr.soundindex = playsound(S_TRUCKLOOP, vptr.pivotx, vptr.pivoty, -1, ST_VEHUPDATE);
                            break;
                        case 11: // beach1.map
                        case 19: // sewer2.map
                        case 20: // inds1.map
                        case 25: // ware1.map
                            vptr.soundindex = playsound(S_FORKLIFTLOOP, vptr.pivotx, vptr.pivoty, -1, ST_VEHUPDATE);
                            break;
                        default:
                            break;
                    }
                    break;
                case 1:
                    switch (i) {
                        case 4: // level1.map
                            vptr.soundindex = playsound(S_TRAMBUSLOOP, vptr.pivotx, vptr.pivoty, -1, ST_VEHUPDATE);
                            break;
                        case 11: // beach1.map
                            vptr.soundindex = playsound(S_BOATLOOP, vptr.pivotx, vptr.pivoty, -1, ST_VEHUPDATE);
                            break;
                        case 26: // ware2.map
                            vptr.soundindex = playsound(S_CARTLOOP, vptr.pivotx, vptr.pivoty, -1, ST_VEHUPDATE);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    Console.out.println("Debug:: sound for mapno " + i + " not found", OsdColor.BLUE);
                    break;
            }
        }

        if (vptr.waittics > 0) {
            vptr.waittics -= TICSPERFRAME;
            if (vptr.soundindex != -1) {
                updatevehiclesnds(vptr.soundindex, vptr.pivotx, vptr.pivoty);
            }
            return;
        }

        int px = vptr.pivotx;
        int py = vptr.pivoty;

        int track = vptr.track;
        int distx = vptr.distx;
        int disty = vptr.disty;

        int stoptrack = vptr.stoptrack;
        int x = distx + disty;
        if (vptr.stop[track] != 0 && x > 0 && x < 8192) {
            vptr.accelto = 2;
            vptr.speedto = 32;
        } else if (vptr.accelto != 8) {
            vptr.accelto = 8;
            vptr.speedto = vptr.movespeed;
        }
        if (distx == 0 && disty == 0) {
            if (vptr.stop[stoptrack] != 0) {
                for (int i = 0; i < vptr.numsectors; i++) {
                    int s = vptr.sector[i];
                    Sector sec = boardService.getSector(s);
                    if (sec != null && sec.getLotag() != 0) {
                        operatesector(s);
                    }
                }
                vptr.waittics = vptr.waitdelay;
                vptr.acceleration = 0;
                vptr.speed = 0;

            }
            distx = vptr.trackx[track] - px;
            disty = vptr.tracky[track] - py;
            vptr.angleto = (short) EngineUtils.getAngle(distx, disty);
            vptr.accelto = 8;
            vptr.distx = abs(distx);
            vptr.disty = abs(disty);
            distx = vptr.distx;
            disty = vptr.disty;
        }
        int a = vptr.acceleration;
        int ato = vptr.accelto;
        if (a < ato) {
            a += TICSPERFRAME;
            if (a > ato) {
                a = ato;
            }
            vptr.acceleration = (short) a;
        } else if (a > ato) {
            a -= TICSPERFRAME;
            if (a < ato) {
                a = ato;
            }
            vptr.acceleration = (short) a;
        }
        int s = vptr.speed;
        int sto = vptr.speedto;
        if (s > sto) {
            s -= a;
            if (s <= sto) {
                s = sto;
            }
            vptr.speed = (short) s;
        } else if (s < sto) {
            s += a;
            if (s > sto) {
                s = sto;
            }
            vptr.speed = (short) s;
        }
        int rotang = vptr.angle;
        int curang = rotang;
        if (curang != vptr.angleto) {
            vptr.angle = vptr.angleto;
            curang = vptr.angle;
        }
        int xvect = (s * TICSPERFRAME * EngineUtils.sin(((curang + 2560) & 2047))) >> 3;
        int xvect2 = xvect >> 13;
        int yvect = (s * TICSPERFRAME * EngineUtils.sin(((curang + 2048) & 2047))) >> 3;
        int yvect2 = yvect >> 13;
        distx -= abs(xvect2);
        if (distx < 0) {
            if (xvect2 < 0) {
                xvect2 -= distx;
            } else {
                xvect2 += distx;
            }
            distx = 0;
            vptr.angleto = (short) EngineUtils.getAngle(vptr.trackx[track] - px, vptr.tracky[track] - py);
        }
        disty -= abs(yvect2);
        if (disty < 0) {
            if (yvect2 < 0) {
                yvect2 -= disty;
            } else {
                yvect2 += disty;
            }
            disty = 0;
            vptr.angleto = (short) EngineUtils.getAngle(vptr.trackx[track] - px, vptr.tracky[track] - py);
        }
        if (distx == 0 && disty == 0) {
            vptr.stoptrack = (short) track;
            track = (short) ((track + 1) % vptr.tracknum);
            vptr.track = (short) track;
            // jsa vehicles
            if (v == 0) {
                if (boardfilename != null && (boardfilename.getName().equalsIgnoreCase("CITY1.MAP")
                        || boardfilename.getName().equalsIgnoreCase("WARE2.MAP"))) {
                    playsound(S_TRUCKSTOP, vptr.pivotx, vptr.pivoty, 0, ST_AMBUPDATE);
                }
            }
        }
        vptr.distx = distx;
        vptr.disty = disty;
        px += xvect2;
        py += yvect2;
        int n = vptr.numpoints;
        for (int i = 0; i < n; i++) {
            int p = vptr.point[i];
            x = vptr.pointx[i];
            int y = vptr.pointy[i];
            Point out = EngineUtils.rotatepoint(px, py, px - x, py - y, curang);
            x = out.getX();
            y = out.getY();
            engine.dragpoint(p, x, y);
        }
        vptr.pivotx = px;
        vptr.pivoty = py;
        rotang = (short) (((curang - rotang) + 2048) & 2047);
        n = vptr.numsectors;

        int lox = 0x7FFFFFFF;
        int hix = -(0x7FFFFFFF);
        int loy = lox;
        int hiy = hix;
        for (int i = 0; i < 4; i++) {
            a = vptr.killw[i];
            Wall wal = boardService.getWall(a);
            if (wal != null) {
                if (wal.getX() < lox) {
                    lox = wal.getX();
                } else if (wal.getX() > hix) {
                    hix = wal.getX();
                }
                if (wal.getY() < loy) {
                    loy = wal.getY();
                } else if (wal.getY() > hiy) {
                    hiy = wal.getY();
                }
            }
        }

        for (int i = 0; i < MAXPLAYERS; i++) {
            onveh[i] = 0;
        }
        for (int i = 0; i < n; i++) {
            ListNode<Sprite> node = boardService.getSectNode(vptr.sector[i]), next;
            while (node != null) {
                next = node.getNext();
                Sprite spr2 = node.get();

                x = spr2.getX();
                int y = spr2.getY();
                x += xvect2;
                y += yvect2;
                Point out = EngineUtils.rotatepoint(px, py, x, y, rotang);
                game.pInt.setsprinterpolate(node.getIndex(), spr2);
                spr2.setX(out.getX());
                spr2.setY(out.getY());
                spr2.setAng(spr2.getAng() + rotang);
                node = next;
            }

            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                x = gPlayer[p].posx;
                int y = gPlayer[p].posy;
                if (gPlayer[p].cursectnum == vptr.sector[i]) {
                    x += xvect2;
                    y += yvect2;
                    Point out = EngineUtils.rotatepoint(px, py, x, y, rotang);
                    gPlayer[p].posx = out.getX();
                    gPlayer[p].posy = out.getY();
                    gPlayer[p].ang += rotang;
                    gPlayer[p].ang = BClampAngle(gPlayer[p].ang);
                    onveh[p] = 1;
                }
            }
        }

        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {

            if (onveh[p] != 0) {
                continue;
            }
            x = gPlayer[p].posx;
            int y = gPlayer[p].posy;

            if (x > lox && x < hix && y > loy && y < hiy) {
                if (!gPlayer[p].noclip && (gPlayer[p].health > 0) && (gPlayer[p].posz > VEHICLEHEIGHT)) {
                    for (int i = 0; i < 4; i++) {
                        a = vptr.killw[i];
                        if (engine.clipinsidebox(x, y, a, 128) != 0) {
                            changehealth(p, -9999);
                            changescore(p, -100);
                            if (goreflag) {
                                tekexplodebody(gPlayer[p].playersprite);
                            }
                        }
                    }
                }
            }
        }
        if (vptr.soundindex != -1) {
            updatevehiclesnds(vptr.soundindex, vptr.pivotx, vptr.pivoty);
        }
    }

    public static void checkmapsndfx(int p) {
        final int s = gPlayer[p].cursectnum;
        Sector plrSec = boardService.getSector(s);
        if (plrSec == null) {
            return;
        }
        Sectoreffect septr = sectoreffect[s];

        for (int i = 0; i < totalmapsndfx; i++) {
            switch (mapsndfx[i].type) {
                case MAP_SFX_AMBIENT:
                    int dist = abs(gPlayer[p].posx - mapsndfx[i].x) + abs(gPlayer[p].posy - mapsndfx[i].y);
                    if ((dist > AMBUPDATEDIST) && (mapsndfx[i].id != -1)) {
                        stopsound(mapsndfx[i].id);
                        mapsndfx[i].id = -1;
                    } else if ((dist < AMBUPDATEDIST) && (mapsndfx[i].id == -1)) {
                        mapsndfx[i].id = playsound(mapsndfx[i].snum, mapsndfx[i].x, mapsndfx[i].y, mapsndfx[i].loops,
                                ST_AMBUPDATE);
                    }
                    break;
                case MAP_SFX_SECTOR:
                    if ((gPlayer[p].cursectnum != gPlayer[p].ocursectnum)
                            && (gPlayer[p].cursectnum == mapsndfx[i].sector)) {
                        mapsndfx[i].id = playsound(mapsndfx[i].snum, mapsndfx[i].x, mapsndfx[i].y, mapsndfx[i].loops,
                                ST_UNIQUE);
                    }
                    break;
                default:
                    break;
            }
        }

        if (boardfilename.getName().toUpperCase().regionMatches(0, "SUBWAY", 0, 6)) { // Bstrcasecmp("SUBWAY", boardfilename, 6)
            // == 0 ) {
            if (ambsubloop == -1) {
                ambsubloop = playsound(S_SUBSTATIONLOOP, 0, 0, -1, ST_IMMEDIATE);
            }
        } else {
            if (ambsubloop != -1) {
                stopsound(ambsubloop);
                ambsubloop = -1;
            }
        }

        if (septr != null) {
            int effect = septr.sectorflags;
            if ((effect & flags32[SOUNDON]) != 0) {
                for (int i = 0; i < totalmapsndfx; i++) {
                    if (mapsndfx[i].type == MAP_SFX_TOGGLED) {
                        Sector sec = boardService.getSector(mapsndfx[i].sector);
                        if (sec != null && sec.getHitag() == septr.hi) {
                            if (mapsndfx[i].id == -1) {
                                mapsndfx[i].id = playsound(mapsndfx[i].snum, mapsndfx[i].x, mapsndfx[i].y,
                                        mapsndfx[i].loops, ST_UNIQUE | ST_IMMEDIATE);
                            }
                        }
                    }
                }
            }
            if ((effect & flags32[SOUNDOFF]) != 0) {
                for (int i = 0; i < totalmapsndfx; i++) {
                    if (mapsndfx[i].type == MAP_SFX_TOGGLED) {
                        Sector sec = boardService.getSector(mapsndfx[i].sector);
                        if (sec != null && sec.getHitag() == septr.hi) {
                            if (mapsndfx[i].id != -1) {
                                stopsound(mapsndfx[i].id);
                                mapsndfx[i].id = -1;
                            }
                        }
                    }
                }
            }
        }

        ambupdateclock = lockclock;
    }

    public static void tekoperatesector(final int dasector) {
        Sector sec = boardService.getSector(dasector);
        if (sec == null) {
            return;
        }

        int datag = sec.getLotag();
        switch (datag) {
            case BOXELEVTAG:
            case PLATFORMELEVTAG:
            case BOXDELAYTAG:
            case PLATFORMDELAYTAG:
            case DOORUPTAG: // a door that opens up
            case DOORDOWNTAG:
            case DOORSPLITHOR:
            case DOORSPLITVER:
            case PLATFORMDROPTAG:
                int i = doorxref[dasector];
                if (i == -1) {
                    throw new AssertException("operatesector: invalid door reference for sector " + dasector);
                }
                switch (doortype[i].state) {
                    case D_NOTHING:
                    case D_CLOSING:
                    case D_CLOSEDOOR:
                    case D_SHUTSOUND:
                        doortype[i].state = D_OPENDOOR;
                        break;
                    default:
                        if (datag != PLATFORMDROPTAG) {
                            doortype[i].state = D_CLOSEDOOR;
                        }
                        break;
                }
                break;
            case DOORFLOORTAG:
                floordoor[fdxref[dasector]].state = DOOROPENING;
                playsound(S_FLOOROPEN, 0, 0, 0, ST_IMMEDIATE);
                break;
        }
    }

    public static void clearvehiclesoundindexes() {
        for (int i = 0; i < MAXSECTORVEHICLES; i++) {
            sectorvehicle[i].soundindex = -1;
        }
    }
}
