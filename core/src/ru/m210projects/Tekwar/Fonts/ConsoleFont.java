package ru.m210projects.Tekwar.Fonts;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.AtlasCharInfo;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.art.ArtEntry;

import static ru.m210projects.Tekwar.Names.ALPHABET2;

public class ConsoleFont extends Font {

    public ConsoleFont(final Engine draw) {
        this.size = 8;

        final int cols = 16;
        final int rows = 8;
        ArtEntry pic = draw.getTile(ALPHABET2);

        for (char ch = 0; ch < cols * rows; ch++) {
            this.addCharInfo(ch, new AtlasCharInfo(this, ch, ALPHABET2, pic.getWidth(), pic.getHeight(), cols, rows));
        }

        Font font = EngineUtils.getLargeFont();
        this.addCharInfo('!', font.getCharInfo('!'));
        this.addCharInfo('$', font.getCharInfo('$'));
        this.addCharInfo('%', font.getCharInfo('%'));
        this.addCharInfo('(', font.getCharInfo('('));
        this.addCharInfo(')', font.getCharInfo(')'));
        this.addCharInfo('/', font.getCharInfo('/'));
        this.addCharInfo('-', font.getCharInfo('-'));
        this.addCharInfo('.', font.getCharInfo('.'));
        this.addCharInfo(',', font.getCharInfo(','));

        setVerticalScaled(false);
    }

    @Override
    public int drawText(Renderer renderer, int x, int y, char[] text, float scale, int shade, int palnum, TextAlign align, Transparent transparent, boolean shadow) {
        for (int i = 0; i < text.length; i++) {
            text[i] = Character.toUpperCase(text[i]);
        }
        return super.drawText(renderer, x, y, text, scale, shade, palnum, align, transparent, shadow);
    }
}
