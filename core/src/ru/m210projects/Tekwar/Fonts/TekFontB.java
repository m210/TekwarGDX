package ru.m210projects.Tekwar.Fonts;

import ru.m210projects.Build.Engine;

import static ru.m210projects.Tekwar.Names.ALPHABET2;

public class TekFontB extends TekFontA {

    public TekFontB(final Engine draw) {
        super(draw);
    }

    @Override
    protected int getAtlas() {
        return ALPHABET2;
    }
}
