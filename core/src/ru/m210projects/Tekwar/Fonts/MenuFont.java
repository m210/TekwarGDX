package ru.m210projects.Tekwar.Fonts;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.Tekwar.Names.*;

public class MenuFont extends Font {

    public MenuFont(Engine draw) {
        this.size = (int) (10 * 0.5f);

        this.addCharInfo(' ', new CharInfo(this, -1, 1.0f, 10));

        for (int i = 0; i < 26; i++) {
            int nTile = i + MFONT_A;
            addChar((char) ('A' + i), nTile, (draw.getTile(nTile).getWidth()) + 1, 0);
            addChar((char) ('a' + i), nTile, (draw.getTile(nTile).getWidth()) + 1, 0);
        }

        for (int i = 0; i < 10; i++) {
            int nTile = i + MFONT_0;
            addChar((char) ('0' + i), nTile, (draw.getTile(nTile).getWidth()) + 1, 0);
        }

        for (int i = 0; i < 15; i++) {
            int nTile = i + MFONT_SPECIAL1;
            addChar((char) ('!' + i), nTile, (draw.getTile(nTile).getWidth()) + 1, 0);
        }

        this.addChar('_', 1543, 10, 2);
    }

    protected void addChar(char ch, int nTile, int width, int yoffset) {
        this.addCharInfo(ch, new CharInfo(this, nTile, 0.5f, width, width, 0, yoffset));
    }

}
