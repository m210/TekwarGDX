package ru.m210projects.Tekwar.Fonts;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.font.AtlasCharInfo;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;

import static ru.m210projects.Tekwar.Names.ALPHABET;

public class TekFontA extends Font {

    public TekFontA(final Engine draw) {
        this.size = 8 / 2;

        final int cols = 16;
        final int rows = 8;
        ArtEntry pic = draw.getTile(getAtlas());

        for (char ch = 0; ch < cols * rows; ch++) {
            this.addCharInfo(ch, new AtlasCharInfo(this, ch, getAtlas(), 0.5f, pic.getWidth(), pic.getHeight(), cols, rows));
        }

        setVerticalScaled(false);
    }

    protected int getAtlas() {
        return ALPHABET;
    }
}
