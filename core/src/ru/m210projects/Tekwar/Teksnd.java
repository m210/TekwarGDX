package ru.m210projects.Tekwar;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import ru.m210projects.Build.Architecture.common.audio.AudioChannel;
import ru.m210projects.Build.Architecture.common.audio.BuildAudio;
import ru.m210projects.Build.Architecture.common.audio.Source;
import ru.m210projects.Build.Script.CueScript;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.grp.GrpEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Tekwar.Types.HmiEntry;
import ru.m210projects.Tekwar.Types.Songtype;
import ru.m210projects.Tekwar.Types.Soundtype;
import ru.m210projects.Tekwar.Types.TekGroup;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Tekwar.Globals.*;
import static ru.m210projects.Tekwar.Main.*;
import static ru.m210projects.Tekwar.Player.gPlayer;
import static ru.m210projects.Tekwar.Tektag.*;

public class Teksnd {

    public static final int MAXSOUNDS = 256;
    public static final int TOTALSOUNDS = 208;

//    public static final int MAXBASESONGLENGTH = 44136;
    public static final int AVAILMODES = 3;
    public static final int SONGSPERLEVEL = 3;
    public static final int NUMLEVELS = 7;
    public static final int AMBUPDATEDIST = 4000;
    public static final List<Entry> cdTrackEntries = new ArrayList<>();
    public static TekGroup fhsongs = null;
    public static Songtype songptr;
    public static int totalsongsperlevel;
    public static TekGroup fhsounds = null;
    public static final ByteBuffer[] pSfx = new ByteBuffer[TOTALSOUNDS];
    public static final Soundtype[] dsoundptr = new Soundtype[MAXSOUNDS];
    public static int currTrack = -1;
    public static Entry currSong = DUMMY_ENTRY;
    public static BuildAudio audio;

    public static void searchCDtracks() {
        CueScript cueScript;

        cdTrackEntries.clear();
        Directory gameDir = game.getCache().getGameDirectory();
        // search cue scripts at first
        for (Entry file : gameDir) {
            if (file.isExtension("cue")) {
                cueScript = new CueScript(file.getName(), file);
                for (String track : cueScript.getTracks()) {
                    Entry entry = game.getCache().getEntry(track, true);
                    if (entry.exists()) {
                        cdTrackEntries.add(entry);
                    }
                }

                if (!cdTrackEntries.isEmpty()) {
                    break;
                }
            }
        }

        // if not found try to find in ogg
        if (cdTrackEntries.isEmpty()) {
            for (Entry file : gameDir) {
                if (file.isExtension("ogg")) {
                    cdTrackEntries.add(file);
                }
            }
        }

        if (!cdTrackEntries.isEmpty()) {
            Console.out.println(cdTrackEntries.size() + " cd tracks found...");
        } else {
            Console.out.println("Cd tracks not found.");
        }
    }

    public static void sndInit() {
        tekcfg.setAudioDriver(tekcfg.getAudioDriver());
        tekcfg.setMidiDevice(tekcfg.getMidiDevice());
        Teksnd.audio = tekcfg.getAudio();

        if (fhsongs == null) {
            setupmidi();
        }
        if (fhsounds == null) {
            setupdigi();
        }

//        tekcfg.setSoundVolume(!tekcfg.isNoSound() ? tekcfg.getSoundVolume() : 0);
//        tekcfg.setMusicVolume(!tekcfg.isMuteMusic() ? tekcfg.getMusicVolume() : 0);
    }

    public static Entry getMusicEntry(int index) {
        if (fhsongs != null) {
            return fhsongs.getEntry(index);
        }

        return DUMMY_ENTRY;
    }

    public static Entry getSoundEntry(int index) {
        if (fhsounds != null) {
            return fhsounds.getEntry(index);
        }

        return DUMMY_ENTRY;
    }

    public static void sndHandlePause(boolean gPaused) {
        if (gPaused) {
            if (songptr.handle != null) {
                songptr.handle.pause();
            }
            stopallsounds();
        } else {
            if (!tekcfg.isMuteMusic() && songptr.handle != null) {
                songptr.handle.play();
            }
        }
    }

    public static void sndStopMusic() {
        if (songptr.handle != null) {
            songptr.handle.stop();
            songptr.handle = null;
        }
        currTrack = -1;
        currSong = DUMMY_ENTRY;
    }

    public static void setupmidi() {
        Entry songsEntry = game.getCache().getEntry("SONGS", true);
        try {
            fhsongs = new TekGroup("SONGS", songsEntry::getInputStream, HmiEntry.class);
        } catch (IOException e) {
            Console.out.println("setupmidi: cant open songs", OsdColor.RED);
        }
        songptr = new Songtype();
        totalsongsperlevel = SONGSPERLEVEL * AVAILMODES;
    }

    public static void menusong(int insubway) {
        int index = NUMLEVELS * (AVAILMODES * SONGSPERLEVEL);
        if (insubway != 0) {
            index = (NUMLEVELS * (AVAILMODES * SONGSPERLEVEL) + 3);
        }

        index += 2; //MPU-401


        songptr.entry = getMusicEntry(index);
        playsong();
    }

    public static boolean playCDtrack(int nTrack) {
        if (game.isCurrentScreen(gMissionScreen) || tekcfg.getMusicType() == 0 || nTrack < 0) {
            return false;
        }

        nTrack = BClipRange(nTrack, 1, cdTrackEntries.size());
        if (songptr.handle != null && songptr.handle.isPlaying() && currTrack == nTrack) {
            return true;
        }

        if (cdTrackEntries.get(nTrack - 1) != null) { // #GDX 11.10.2024 Change MIDI to CD music change
            Music newHandle = newMusic(cdTrackEntries.get(nTrack - 1));
            if (newHandle != null) {
                sndStopMusic();
                songptr.handle = newHandle;
                currTrack = nTrack;
                newHandle.setLooping(true);
                newHandle.play();
                return true;
            }
        }

        return false;
    }

    private static Music newMusic(Entry entry) {
        return audio.newMusic(entry);
    }

    public static void startmusic(int level) {
        if (level > 6) {
            return;
        }

        int index = totalsongsperlevel * (level);
        songptr.entry = getMusicEntry(index);
        playsong();
    }

    public static void playsong() {
        if (tekcfg.isMuteMusic()) {
            return;
        }

        if (!cdTrackEntries.isEmpty() && playCDtrack((int) (Math.random() * cdTrackEntries.size()))) {
            return;
        }

        if (!songptr.entry.exists()) {
            return;
        }

        if (currSong.equals(songptr.entry)) {
            return;
        }

        sndStopMusic();
        songptr.handle = newMusic(songptr.entry);
        if (songptr.handle != null) {
            currSong = songptr.entry;
            songptr.handle.setLooping(true);
            songptr.handle.play();
        }

    }

    public static void setupdigi() {
        Entry soundsEntry = game.getCache().getEntry("sounds", true);
        try {
            fhsounds = new TekGroup("sounds", soundsEntry::getInputStream, GrpEntry.class);
        } catch (IOException e) {
            Console.out.println("setupdigi: cant open sounds", OsdColor.RED);
        }

        for (int i = 0; i < MAXSOUNDS; i++) {
            dsoundptr[i] = new Soundtype();
            dsoundptr[i].type = ST_UPDATE;
            dsoundptr[i].sndnum = -1;
        }
    }

    public static void updatesounds(int snum) {
        if (tekcfg.isNoSound()) {
            return;
        }

        audio.setListener(gPlayer[snum].posx, gPlayer[snum].posz >> 4, gPlayer[snum].posy, (int) gPlayer[snum].ang);

        for (int i = 0; i < MAXSOUNDS; i++) {
            if (dsoundptr[i].hVoice == null) {
                continue;
            }

            if ((dsoundptr[i].type & (ST_IMMEDIATE | ST_NOUPDATE | ST_VEHUPDATE)) != 0) {
                continue;
            }

            if (dsoundptr[i].loop > 0) {
                dsoundptr[i].loop -= Gdx.graphics.getDeltaTime();
                if (dsoundptr[i].loop <= 0) {
//                        dsoundptr[i].hVoice.setLooping(false, 0, 0);
                    stopsound(i);
                    return;
                }
            }

            int dist = abs(gPlayer[snum].posx - dsoundptr[i].x) + abs(gPlayer[snum].posy - dsoundptr[i].y);
            int vol = 39000 - (dist << 2);
            if (dsoundptr[i].type == ST_AMBUPDATE) {
                if (dist < AMBUPDATEDIST) {
                    vol = (AMBUPDATEDIST << 3) - (dist << 3);
                } else {
                    vol = 0;
                }
            } else {
                if (dist < 1500) {
                    vol = 0x7fff;
                } else if (dist > 8500) {
                    vol = 0x1f00;
                }
            }

            vol = BClipRange((int) ((vol / 32767f) * 255), 0, 255);
            dsoundptr[i].hVoice.setVolume(vol / 255.0f);
        }
    }

    public static int playsound(int sn, int sndx, int sndy, int loop, int type) {
        if (tekcfg.isNoSound() || (sn < 0) || (sn >= TOTALSOUNDS)) {
            return -1;
        }

        for (int i = 0; i < MAXSOUNDS; i++) {
            if (dsoundptr[i].hVoice != null && !dsoundptr[i].hVoice.isActive()) {
                stopsound(i);
            }
        }

        if ((type & (ST_UNIQUE | ST_AMBUPDATE | ST_TOGGLE)) != 0) {
            for (int i = 0; i < MAXSOUNDS; i++) {
                if (dsoundptr[i].hVoice == null || (dsoundptr[i].hVoice != null && !dsoundptr[i].hVoice.isActive())) {
                    continue;
                }

                if (dsoundptr[i].sndnum == sn) {
                    if ((type & ST_TOGGLE) != 0) {
                        stopsound(i);
                    }
                    return -1;
                }
            }
        }

        int sound = 0;
        while (dsoundptr[sound].hVoice != null && dsoundptr[sound].hVoice.isActive()) {
            sound++;
            if (sound == MAXSOUNDS) {
                return (-1);
            }
        }

        dsoundptr[sound].type = (short) type;
        dsoundptr[sound].x = sndx;
        dsoundptr[sound].y = sndy;

        if (pSfx[sn] == null) { // no longer in cache
            Entry entry = getSoundEntry(sn);
            if (!entry.exists()) {
                Console.out.println("playsound: bad read of digilist", OsdColor.RED);
            }

            ByteBuffer buf = ByteBuffer.allocateDirect((int) entry.getSize());
            buf.order(ByteOrder.LITTLE_ENDIAN);
            buf.put(entry.getBytes());
            pSfx[sn] = buf;
        }
        pSfx[sn].rewind();

        int vol = 0x7fff;
        if ((type & ST_IMMEDIATE) == 0) {
            int dist = abs(gPlayer[screenpeek].posx - sndx) + abs(gPlayer[screenpeek].posy - sndy);
            if ((type & ST_AMBUPDATE) != 0 || (type & ST_VEHUPDATE) != 0) {
                if (dist < AMBUPDATEDIST) {
                    vol = (AMBUPDATEDIST << 3) - (dist << 3);
                } else {
                    vol = 0;
                }
            } else if ((type & ST_IMMEDIATE) == 0) {
                vol = 39000 - (dist << 2);

                if (dist < 1500) {
                    vol = 0x7fff;
                } else if (dist > 8500) {
                    if (sn >= 151) // S_MALE_COMEONYOU
                    {
                        vol = 0x0000;
                    } else {
                        vol = 0x1f00;
                    }
                }
            }
        }

        vol = BClipRange((int) ((vol / 32767f) * 255), 1, 255);
        dsoundptr[sound].loop = loop;
        if (loop != 0 || (type & ST_AMBUPDATE) != 0 || (type & ST_VEHUPDATE) != 0) {
            dsoundptr[sound].hVoice = newSound(pSfx[sn], 11025, 8, 255);
        } else {
            dsoundptr[sound].hVoice = newSound(pSfx[sn], 11025, 8, 80 * (vol + 1));
        }

        if (dsoundptr[sound].hVoice != null) {

            if ((type & ST_IMMEDIATE) == 0) {
                dsoundptr[sound].hVoice.setPosition(sndx, 0, sndy);
            }

            if (loop != 0) {
                dsoundptr[sound].hVoice.loop(vol / 255.0f);
            } else {
                dsoundptr[sound].hVoice.play(vol / 255.0f);
            }
        }
        dsoundptr[sound].sndnum = sn;
        return sound;
    }

    public static Source newSound(ByteBuffer buffer, int rate, int bits, int priority) {
        return (Source) audio.newSound(buffer, rate, bits, priority);
    }

    public static Source newSound(ByteBuffer buffer, int rate, int bits, int channel, int priority) {
        return (Source) audio.newSound(buffer, rate, bits, AudioChannel.parseChannel(channel), priority);
    }

    public static void stopallsounds() {
        for (int i = 0; i < MAXSOUNDS; i++) {
            stopsound(i);
        }

        audio.stopAllSounds();

        // clear variables that track looping sounds
        loopinsound = -1;
        baydoorloop = -1;
        ambsubloop = -1;
    }

    public static void stopsound(int i) {
        if (dsoundptr[i].hVoice == null || i >= TOTALSOUNDS) {
            return;
        }

        if (dsoundptr[i].loop != 0) {
//            dsoundptr[i].hVoice.setLooping(false, 0, 0);
            dsoundptr[i].loop = 0;
        }
        dsoundptr[i].hVoice.stop();
        dsoundptr[i].hVoice = null;
        dsoundptr[i].type = ST_UPDATE;
        dsoundptr[i].sndnum = -1;
    }

    public static void updatevehiclesnds(int i, int sndx, int sndy) {
        if ((i < 0) || (i > TOTALSOUNDS)) {
            return;
        }

        dsoundptr[i].x = sndx;
        dsoundptr[i].y = sndy;

        int dist = abs(gPlayer[screenpeek].posx - sndx) + abs(gPlayer[screenpeek].posy - sndy);
        int vol = 32767 - (dist << 2);
        if (dist < 1000) {
            vol = 32767;
        } else if (dist > 9000) {
            vol = 0;
        }

        vol = BClipRange((int) ((vol / 32767f) * 255), 0, 255);
        if (dsoundptr[i].hVoice != null) {
            dsoundptr[i].hVoice.setPosition(sndx, 0, sndy);
            dsoundptr[i].hVoice.setVolume(vol / 255.0f);
        }
    }
}