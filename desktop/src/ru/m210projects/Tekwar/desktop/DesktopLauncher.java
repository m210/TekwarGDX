package ru.m210projects.Tekwar.desktop;

import com.badlogic.gdx.backends.LwjglLauncherUtil;
import ru.m210projects.Tekwar.Config;
import ru.m210projects.Tekwar.Main;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Locale;

public class DesktopLauncher {

    public static final String appname = "TekwarGDX";

    public static void main(final String[] arg) throws IOException {
        Config cfg = new Config(Paths.get(arg[0], (appname + ".ini").toLowerCase(Locale.ROOT)));
        cfg.load();
        cfg.setGamePath(cfg.getCfgPath().getParent());
        cfg.addMidiDevices(LwjglLauncherUtil.getMidiDevices());
        boolean isDemo = false;
        LwjglLauncherUtil.launch(new Main(cfg, appname, "?.??",false, isDemo), null);
    }
}
